
var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id) return state.text; // optgroup
                return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
            }

            

            $("#country_list").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                width: 'auto', 
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });

            var form = $('#submit_form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //step 1
                    userid: {
                        minlength: 1,
                        required: true
                    },
                    /*rpassword: {
                        minlength: 5,
                        required: true,
                        equalTo: "#submit_form_password"
                    },*/
                    //step 2
                    purpose: {
                        required: true
                    },
                    //step 3
                    desired_amt: {
                        digits: true,
                        required: true
                    },
                    prescribe_amt: {
                        required: true
                    },
                    purpose: {
                        required: true
                    },
                    fullname: {
                        required: true
                    },
                    comaker1: {
                        required: true
                    },
                    comaker2: {
                        required: true
                    },
                    terms: {
                        required: true,
                    },
                    product: {
                        required: true,
                    },
                    payment:{
                        required: true,   
                    },
                    /*'payment[]': {
                        required: true,
                        minlength: 1
                    }*/
                },

                messages: { // custom messages for radio buttons and checkboxes
                    'aaaaa[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.validator.format("Please select at least one option")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label.closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    form[0].submit();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            var displayConfirm = function() {
                $('#tab4 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":radio")) {
                        input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
                    }
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") == 'payment[]') {
                        var payment = [];
                        $('[name="payment[]"]:checked', form).each(function(){ 
                            payment.push($(this).attr('data-title'));
                        });
                        $(this).html(payment.join("<br>"));
                    }
                });
            }

            var getID = function() {
                return 1;
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

               
   
            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    return false;
                    
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    
                    handleTitle(tab, navigation, clickedIndex);
                },
                onFirst: function (tab, navigation, index) {
                        //alert(index);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();
                    //console.log(index);
       
                   if($("#blocker").val()=="")
                   {    
                         return false;
                   }

                    /*
                    if(index!=2||index!=3)
                    {

                            
                             alert(v1);
                             
                    }*/
                    /*$.ajax({        
                       url: url = "<?php echo site_url('Creditors/ajax_edit_creditors')?>/"+$("#userid").val(),
                       type: "POST",
                       dataType: "JSON",
                       success: function(data)
                       {    
                        if(data!==null){
                            toastr.success('Your ID is Exisiting!','Message:');
                        }else{
                            toastr.error('Your ID is Not Existing!','Message:');
                            $("#userid").val('');
                        }
                       },
                       error: function(jqXHR, textStatus, errorThrown) {
                           console.log('Server Side Error!');
                       }
                   });   */      

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function ()
            {
                if($("#comaker1").val()==$("#comaker2").val())
                {
                    toastr.error('COMAKERS SHOULD NOT BE THE SAME!','Message:'); 
                }else{
                    $.ajax({
                       url: "ElectronicForm/ajax_saved_loan_ef",
                       type: "POST",
                       data:{
                            product_id: raw_product_id,
                            creditor_id: raw_creditor_id,
                            applied_amt: raw_applied_amt,
                            suggested_amt: raw_suggested_amt,
                            pres_interest: p_interest,
                            pres_total: p_total ,
                            pres_cutoff: p_cutoff,
                            app_interest: a_interest,
                            app_total: a_total,
                            app_cutoff: a_cutoff,
                            terms: raw_terms,
                            purpose: $("#purpose").val(),
                            payment_opt: $('input[name="payment_opt"]:checked').val(),
                            comaker1: $("#comaker1").val(),
                            comaker2: $("#comaker2").val(),
                            remarks: $("#remarks").val()
                       },
                       dataType: "JSON",
                       success: function(data)
                       {  
                            //toastr.info('Loan details Saved!','Message:');
                            location = location;
                       },
                       error: function(jqXHR, textStatus, errorThrown) {
                           console.log('Server Side Error!');
                       }
                   });
                }

            }).hide();
            //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('#country_list', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        }

    };

}();

jQuery(document).ready(function() {
    FormWizard.init();

});

