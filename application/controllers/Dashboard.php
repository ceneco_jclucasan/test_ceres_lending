<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('Dashboard_m');
		$this->load->model('Trans_loan_m');
	}

	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      $session_data = $this->session->userdata('logged_in');
	      $data['id'] = $session_data['id'];
	      $data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      $data['privileges'] = $session_data['privileges'];
		  $data['group_id'] = $session_data['group_id'];
	      	$data['picture'] = $session_data['picture'];

	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      $this->load->view('templates/header');
		  $this->load->view('templates/nav1', $data);
		  $this->load->view('dashboard_v');
		  $this->load->view('templates/footer');
		  //exec("C:\Users\Lenovo\Desktop\Debug\MySqlConnection.exe $id");
		  //exec("C:\Users\WSAM08\Documents\Test\AppTest.java");
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('Login', 'refresh');
	    }
	}

	public function manager()
	{
	    if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	      	$data['id'] = $session_data['id'];
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
 	      	
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "view_dashboard") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('dashboard_manager_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('Login', 'refresh');
	}

	public function ajax_change_pw($id)
	{
		$user = $this->Dashboard_m->get_by_id($id);
		if (MD5($this->input->post('oldpw')) == $user->password) {
			$session_data = $this->session->userdata('logged_in');
			//date_default_timezone_set('Asia/Singapore');
		    //$date = date('Y-m-d H:i:s');
			$data = array(
					'password' => MD5($this->input->post('newpw')),
					//'added_by' => $session_data['username'],
					//'added_date' => $date,
				);
			$this->Dashboard_m->update(array('id' => $id), $data);
			echo json_encode(array("status" => "true"));			
		} else {
			echo json_encode(array("status" => "false"));
		}	
	}

	public function cp()
	{
		$session_data = $this->session->userdata('logged_in');
		$user = $this->Dashboard_m->get_by_id($session_data['id']);
		if (MD5($this->input->post('curr_pws')) == $user->password) {
			$session_data = $this->session->userdata('logged_in');
			//date_default_timezone_set('Asia/Singapore');
		    //$date = date('Y-m-d H:i:s');
			$data = array(
					'password' => MD5($this->input->post('new_pw')),
					//'added_by' => $session_data['username'],
					//'added_date' => $date,
				);
			$this->Dashboard_m->update(array('id' => $session_data['id']), $data);
			echo json_encode(array("status" => "true","pw"=>$user->password,"user"=>$user,"pass"=>$this->input->post('curr_pws'),"md5"=>MD5($this->input->post('curr_pws'))));		
		} else {
			echo json_encode(array("status" => "false","pw"=>$user->password,"user"=>$user,"pass"=>$this->input->post('curr_pws'),"md5"=>MD5($this->input->post('curr_pws'))));
		}	
	}

	public function reset_pass()
	{
		$session_data = $this->session->userdata('logged_in');
		$user = $this->Dashboard_m->get_by_id($session_data['id']);
		$session_data = $this->session->userdata('logged_in');
			$data = array(
					'password' => MD5('user1234'),
					'reset_by' => $session_data['id'],
					'reset_dt' => date('Y-m-d'),
			);
		$this->Dashboard_m->update(array('emp_id' => $this->input->post('id2')), $data);
		echo json_encode(array("status" => "true","id"=>$this->input->post('id2'),"pw"=>$user->password,"user"=>$user));		
	}

	public function ajax_change_pin($id)
	{
		$user = $this->Dashboard_m->get_by_id($id);
		if ($this->input->post('oldpin') == $user->reset_pin) {
			$session_data = $this->session->userdata('logged_in');
			//date_default_timezone_set('Asia/Singapore');
		    //$date = date('Y-m-d H:i:s');
			$data = array(
					'reset_pin' => $this->input->post('pin'),
					//'added_by' => $session_data['username'],
					//'added_date' => $date,
				);
			$this->Dashboard_m->update(array('id' => $id), $data);
			echo json_encode(array("status" => "true"));			
		} else {
			echo json_encode(array("status" => "false"));
		}	
	}

	public function ajax_sms_settings($id) 
	{
		//$data = $this->Dashboard_m->sms_settings($id);
		//echo json_encode($data);
	}

	public function ajax_sms_footer() 
	{
		$session_data = $this->session->userdata('logged_in');
		$data = $this->Dashboard_m->sms_footer($session_data['id']);
		echo json_encode($data);
	}

	public function ajax_change_sms($id)
	{
		$session_data = $this->session->userdata('logged_in');
		//date_default_timezone_set('Asia/Singapore');
	    //$date = date('Y-m-d H:i:s');
	    if ($session_data['group_id'] == "1") {
			$data = array(
				'sms_ip' => $this->input->post('sms_ip'),
				'sms_port' => $this->input->post('sms_port'),
				'sms_user' => $this->input->post('sms_user'),
				'sms_pass' => $this->input->post('sms_pass'),
				//'sms_footer' => $this->input->post('sms_foot'),
				//'added_by' => $session_data['username'],
				//'added_date' => $date,
			);
		}
		$this->Dashboard_m->update_sms(array('id' => $id), $data);
		echo json_encode(array("status" => "true", "foot" => $this->input->post('sms_foot')));
	}

	public function ajax_change_sms_footer()
	{
		$session_data = $this->session->userdata('logged_in');
		//date_default_timezone_set('Asia/Singapore');
	    //$date = date('Y-m-d H:i:s');
	    $list = $this->Dashboard_m->sms_footer($session_data['id']);
	    if (sizeof($list) == 0) {
	    	$data = array(
	    		'user_id' => $session_data['id'],
				'sms_footer' => $this->input->post('sms_foot'),
				//'added_by' => $session_data['username'],
				//'added_date' => $date,
			);
			$this->Dashboard_m->save_sms_footer($data);
	    } else {
	    	$data = array(
				'sms_footer' => $this->input->post('sms_foot'),
				//'added_by' => $session_data['username'],
				//'added_date' => $date,
			);
			$this->Dashboard_m->update_sms_footer(array('user_id' => $session_data['id']), $data);
	    }
		echo json_encode(array("status" => "true", "foot" => $this->input->post('sms_foot')));
	}

	public function ajax_check_pin($id) 
	{
		$user = $this->Dashboard_m->get_by_id($id);
		if ($user->reset_pin == "0") {
			echo json_encode(array("status" => "true"));			
		} else {
			echo json_encode(array("status" => "false"));
		}
	}

	public function ajax_create_new_pin()
	{
		$id = $this->input->post('id');
		$data = array(
				'reset_pin' => $this->input->post('pin_no'),
			);
		$this->Dashboard_m->update(array('id' => $id), $data);
		echo json_encode(array("status" => "true"));			
	}

	public function ajax_list_loan_app() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->Trans_loan_m->get_pending_app();
		date_default_timezone_set('Asia/Singapore');	 
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$dt = date_create($myList->created_on);
			$dt_format = date_format($dt,"Y-m-d");
			if($dt_format==date('Y-m-d'))
			{				
				$row['trn'] = $myList->id;
				$row['creditor'] = $myList->fullname;
				$row['amount'] = number_format($myList->applied_amount, 2, '.', ',').' (for '.$myList->terms.' months)';
				$button = '';
				$privileges = $session_data['privileges'];
			    for ($i=0; $i<sizeof($privileges); $i++)
			    { 
	               if ($privileges[$i]->privilege_name == "loanapp_edit") 
	               {
						$button = $button.'<a class="btn green btn-outline sbold" data-toggle="modal" href="'.base_url().'Loans/Loan_app" title="View"><i class="fa fa-street-view" aria-hidden="true"></i> View</a>';	
				   } 
				}			
				$row['action'] = $button;
				$data[] = $row;
			}
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}

	public function ajax_list_loan_approved() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->Trans_loan_m->get_released_loan();
		date_default_timezone_set('Asia/Singapore');	 
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$dt = date_create($myList->release_on);
			$dt_format = date_format($dt,"Y-m-d");
			if($dt_format==date('Y-m-d'))
			{
				$row['trn'] = $myList->id;
				$row['creditor'] = $myList->fullname;
				$row['amount'] = number_format($myList->approved_amount, 2, '.', ',').' (for '.$myList->approved_term.' months)';
				$button = '';
				$privileges = $session_data['privileges'];
			    for ($i=0; $i<sizeof($privileges); $i++)
			    { 
	               if ($privileges[$i]->privilege_name == "loanapp_edit") 
	               {
						 $button = $button.'<a class="btn green btn-outline sbold" data-toggle="modal" href="'.base_url().'Loans/Loan_validation_dt?id='.$myList->id.'&status=released" title="Edit" ><i class="fa fa-street-view" aria-hidden="true"></i> View</a>';	
				   }
				}			
				$row['action'] = $button;
				$row['released_dt'] = date("M d, Y", strtotime($myList->release_on));
				$data[] = $row;				
			}
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}
}