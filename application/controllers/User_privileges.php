<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_privileges extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_privileges_m');
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      $session_data = $this->session->userdata('logged_in');
	      $data['id'] = $session_data['id'];
	      $data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      $data['privileges'] = $session_data['privileges'];
	      $data['group_id'] = $session_data['group_id'];
	      $group_id = $session_data['group_id'];
	      	$data['picture'] = $session_data['picture'];
	      if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'no_code';
	      	}
		  if ($group_id == "1") {
		   	$this->load->view('templates/header');
		  	$this->load->view('templates/nav1', $data);	
		  	$this->load->view('user_privileges_v');
		  	$this->load->view('templates/footer');
		  } else if ($viewpage == "") {
		   	redirect('dashboard', 'refresh');	
		  }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login', 'refresh');
	}
	
	public function ajax_list() 
	{
		$list = $this->User_privileges_m->get_datatables();	
		$data = array();
		
		foreach($list as $arr) {
			$row = array();
			$row['priv'] = $arr->privilege_name;
			$row['desc'] = $arr->description;			
			$row['action'] = '<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_privilege('."'".$arr->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" title="Delete" onclick="delete_privilege('."'".$arr->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';				  
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function ajax_edit($id)
	{
		$data = $this->User_privileges_m->get_by_id($id);
		echo json_encode($data);
	}
	
	public function ajax_add()
	{	
		$session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');
	    $date = date('Y-m-d H:i:s');

		$data = array(
				'privilege_name' => $this->input->post('privilege_name'),
				'description' => $this->input->post('description'),
				//'add_date' => $date,
				//'add_by' => $session_data['firstname'].' '.$session_data['lastname'],
			);
		$insert = $this->User_privileges_m->save($data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_update()
	{
		$session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');
	    $date = date('Y-m-d H:i:s');

		$data = array(
				'privilege_name' => $this->input->post('privilege_name'),
				'description' => $this->input->post('description'),
				//'update_date' => $date,
				//'update_by' => $session_data['firstname'].' '.$session_data['lastname'],
			);
		$this->User_privileges_m->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_delete($id)
	{
		$this->User_privileges_m->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

}
?>