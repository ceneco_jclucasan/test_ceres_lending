<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->library('session');
   $this->load->model('Login_m','',TRUE);
 }

 function index()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');

   $this->form_validation->set_rules('username', 'Username', 'trim|required|prep_for_form');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database|prep_for_form');

   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('login_v');
   }
   else
   {
     /*$result = $this->Login_m->login($username, $password);
     foreach($result as $row)
     {
       if ($row->is_logged_in == 1) {
         $this->form_validation->set_message('check_database', 'This username is already logged in from another device.');
         return false;
       } else if ($row->is_suspended == 1) {
         $this->form_validation->set_message('check_database', 'This username is suspended.');
         return false;
       } else {*/
         //Go to private area
         redirect('Dashboard', 'refresh');
       /*}
     }*/
   }
 }

 function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $username = $this->input->post('username');
   //query the database
   $result = $this->Login_m->login($username, $password);
   if($result)
   {
     $id = $result['0']->id;
     $privileges = $this->Login_m->get_privileges_by_id($id);
     $sess_array = array();
     foreach($result as $row)
     {
       if ($row->is_suspended == 1) {
         $this->form_validation->set_message('check_database', 'This user account is suspended.');
         return false;
       } else {
         $sess_array = array(
           'id' => $row->id,
           'username' => $row->username,
           'firstname' => $row->firstname,
           'lastname' => $row->lastname,
           'privileges' => $privileges,
           'group_id' => $row->group_id,
           'emp_code' => $row->emp_code,
           'picture' => $row->picture
         );
         $this->session->set_userdata('logged_in', $sess_array);
         return TRUE;
       }
     }
   }
   else
   {
     $this->form_validation->set_message('check_database', '<div class="alert alert-danger">
                    <button class="close" data-close="alert"></button><span> Invalid username or password. </span></div>');
     return false;
   }
 }
}
?>