<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Tblemployees extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Tblemployees_m','tblTblemployees');
		$this->load->helper('url');
		$this->load->database();
	}

public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['picture'] = $session_data['picture'];

	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'no_code';
	      	}
 	      	$privileges = $session_data['privileges'];
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "Tblemployees_view") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('Tblemployees_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login', 'refresh');
	}

	
public function ajax_list() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->tblTblemployees->get_datatables();
		$data = array();
		//$no = "";
		
		foreach ($list as $myList) {
			//$no++;
			$row = array();
			$row['lname'] = $myList->lastname;
			$row['fname'] = $myList->firstname;
			$row['mname'] = $myList->middlename;
			$row['suffix'] = $myList->suffix;
			$row['email'] = $myList->email;
			$row['position'] = $myList->position;
			$row['status'] = $myList->is_active;
			$privileges = $session_data['privileges'];
			$button = '';


		    for ($i=0; $i<sizeof($privileges); $i++) { 
               if ($privileges[$i]->privilege_name == "Tblemployees_edit") 
               {
					 $button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_Tblemployees('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';	
			   } else if ($privileges[$i]->privilege_name == "Tblemployees_delete"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_Tblemployees('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }

			}
			
			$row['action'] = $button;
			$data[] = $row;
		}

			$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}
	
	public function ajax_list_querybuilder() 
	{
		$query = $this->input->post('query');
		$list = $this->tblTblemployees->get_query_builder_result($query);	
		$data = array();
		$no = "";
		
		foreach($list as $myList) {
			$no++;	  
			                                                                                    $row[] = $myList->lastname;
$row[] = $myList->firstname;
$row[] = $myList->middlename;
$row[] = $myList->suffix;
$row[] = $myList->email;
$row[] = $myList->position;
$row[] = $myList->is_active;		
			$row[] = '<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_Tblemployees('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
			<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_Tblemployees('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';		  
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->tblTblemployees->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{	
			$data = array(
			'lastname' => $this->input->post('lastname'),
'firstname' => $this->input->post('firstname'),
'middlename' => $this->input->post('middlename'),
'suffix' => $this->input->post('suffix'),
'email' => $this->input->post('email'),
'position' => $this->input->post('position'),
'is_active' => $this->input->post('is_active'),
'code' => $this->input->post('code'),
);
			$insert = $this->tblTblemployees->save($data);
			echo json_encode(array("status" => TRUE));
	}
	public function ajax_update()
	{
			$data = array(
			'lastname' => $this->input->post('lastname'),
'firstname' => $this->input->post('firstname'),
'middlename' => $this->input->post('middlename'),
'suffix' => $this->input->post('suffix'),
'email' => $this->input->post('email'),
'position' => $this->input->post('position'),
'is_active' => $this->input->post('is_active'),
'code' => $this->input->post('code'),
);
			$this->tblTblemployees->update(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
	}
	public function ajax_delete($id)
	{
			$this->tblTblemployees->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
	}

}
