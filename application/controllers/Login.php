<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller { 
 	function __construct()
 	{
   		parent::__construct();
		$this->load->library('session');
   		$this->load->helper('url');
		$this->load->model('Login_m');
 	}
 
 	function index()
	{
	    $this->load->helper(array('form'));
	    $this->load->view('login_v');
	}

 	public function ajax_show_pin($username) {
		$list = $this->Login_m->get_by_uname($username);
		if (sizeof($list) > 0) {
			echo json_encode(array("reset_pin" => $list->reset_pin));
		} else {
			echo json_encode(array("reset_pin" => "0"));
		}	
	}

	public function ajax_reset_password() {
		$list = $this->Login_m->get_by_uname($this->input->post('uname'));
		if (sizeof($list) > 0) {
			if ($list->reset_pin == $this->input->post('reset_pin')) {
				echo json_encode(array("reset_pin" => "true", "arrsize" => "1"));
			} else {
				echo json_encode(array("reset_pin" => "false", "arrsize" => "0"));
			}
		} else {
			echo json_encode(array("arrsize" => "0"));
		}	
	}

	public function ajax_create_new_pin()
	{
		$list = $this->Login_m->get_by_uname($this->input->post('uname'));
		if (sizeof($list) > 0) {
			if ($list->reset_pin == $this->input->post('reset_pin')) {
				$data = array(
						'password' => MD5($this->input->post('new_pass2')),
					);
				$this->Dashboard_m->update_log(array('id' => $list->id), $data);
				echo json_encode(array("reset_pin" => "true", "arrsize" => "1"));
			} else {
				echo json_encode(array("reset_pin" => "false", "arrsize" => "0"));
			}
		} else {
			echo json_encode(array("arrsize" => "0"));
		}			
	}

	public function ajax_save_new_pw()
	{
		$session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');
	    $date = date('Y-m-d H:i:s');

		$data = array(
				'password' => MD5($this->input->post('new_pass2')),
				//'update_date' => $date,
				//'update_by' => $session_data['firstname'].' '.$session_data['lastname'],
			);
		$this->Login_m->update_log(array('username' => $this->input->post('uname'),'reset_pin' => $this->input->post('reset_pin')), $data);
		echo json_encode(array("status" => TRUE));							
	}
 
}
