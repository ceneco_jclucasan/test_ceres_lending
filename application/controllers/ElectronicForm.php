<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class ElectronicForm extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Netpays_m','tblNetpays');
		$this->load->helper('url');
		$this->load->library('csvimport');	
		$this->load->model('Products_m','tblProducts');
		$this->load->model('Trans_loan_m','tblTransloan');
		$this->load->model('Creditors_m','tblCreditors');
		$this->load->database();
	}

	public function Index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
 	      	
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
 	      	$datas['product'] = $this->tblProducts->get_datatables_products();
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "ea_view") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('ef_v',$datas);
			  	//$this->load->view('templates/form-wizard');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function LoanForm()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
 	      	$datas['product'] = $this->tblProducts->get_datatables_products();
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "ea_view_form") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('loan_app_form_v',$datas);
			  	//$this->load->view('templates/form-wizard');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function ajax_edit($id)
	{
		$data = $this->tblNetpays->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_saved_loan_ef()
	{	
		$creditor = $this->tblCreditors->get_by_id_creditors($this->input->post('creditor_id'));
		date_default_timezone_set('Asia/Singapore');	  
		$currDate  = date("Y-m-d H:i:s");
		$data = array(
			'creditor_id' => $this->input->post('creditor_id'),
			'product_id' => $this->input->post('product_id'),
			'applied_amount' => $this->input->post('applied_amt'),

			'interest_amount' => $this->input->post('app_interest'),
			'total_amount' => $this->input->post('app_total'),
			'payable_cutoff' => $this->input->post('app_cutoff'),

			'suggested_amount' => $this->input->post('suggested_amt'),





			'prescribe_total' => $this->input->post('pres_total'),
			'prescribe_interest' => $this->input->post('pres_interest'),
			'prescribe_cutoff' => $this->input->post('pres_cutoff'),

			'terms' => $this->input->post('terms'),
			'purpose' => $this->input->post('purpose'),
			'payment_option' => $this->input->post('payment_opt'),
			'comaker1' => $this->input->post('comaker1'),
			'comaker2' => $this->input->post('comaker2'),
			'creditors_remark' => $this->input->post('remarks'),
			'created_by' => $creditor->fullname,
			'created_on' => $currDate,
		);
		$insert = $this->tblTransloan->save($data);
		echo json_encode(array("status" => TRUE,"po"=>$this->input->post('payment_opt'),"pp"=>$this->input->post('creditor_id')));
	}

	public function ajax_update()
	{
		$data = array(
		 'class_name' => $this->input->post('class')
		);
		$this->tblNetpays->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->tblNetpays->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
}
