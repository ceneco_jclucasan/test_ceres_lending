<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Ledger extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Manual_Payment_m','TblManualPayment');
		$this->load->model('Trans_loan_m','tblTransloan');
		$this->load->model('Ledger_m','tblLedger');
		$this->load->helper('url');
		$this->load->library('csvimport');	
		$this->load->database();
	}

	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	      	$data['id'] = $session_data['id'];
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
 	      	
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "view_ledger") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('Ledger_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function Uploads()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	      	$data['id'] = $session_data['id'];
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
 	      	
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "view_ledger_upload") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('Ledger_upload_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}
	
	public function ajax_list($id) 
	{
		//$this->input->post('id')	
		//'3108646'
		$list = $this->tblTransloan->get_by_creditor($id);
		$data = array();
		//step by step
		//get fist from transloan the amount approved
		//get the amount in trans_loan_detail were orno is nuot null and amount is not null or below 0 (uploaded)
		/*foreach ($list as $myList)
		{
			$row = array();
			$row['amount'] = $myList->approved_amount;	
			$d = date_create($myList->approved_on);
			$row['tdate'] = date_format($d,"Y-m-d"); 
			$data[] = $row;

			$get_trans_detail = $this->tblTransloan->get_loan_by_head($myList->id);
			foreach ($get_trans_detail as $key)
			{
				$row2 = array();
				$row2['amount'] = $key->amount; 
				$data[] = $row2;
			}
		}
		$get_manual_payment = $this->TblManualPayment->get_payment_creditor('5107915');
		foreach ($get_manual_payment as $key)
		{
			$row = array();
			$row['amount'] = $key->amount;
			$row['tdate'] = $key->ordate;
			$row['orno'] = $key->orno;
			$data[] = $row;
		}
		*/
		$a=1;
		$b=1;
		$c=1;
		$total_balance =0;
		foreach ($list as $myList)
		{
			$row = array();
			if($myList->status=="Released")
			{			
				$total_balance += $myList->approved_amount;	
				//$total_balance += $myList->approved_amount;	
				$row['beg'] = number_format($myList->approved_amount, 2, '.', ',');	
				//$row['beg'] = $myList->approved_total;	
				$row['debit'] = '';//0;
				$d = date_create($myList->approved_on);
				$row['tdate'] = date_format($d,"Y-m-d"); 
				$row['particulars'] = $myList->purpose;//'Beg. Loan Amt.'; 
				$row['balance'] = number_format($total_balance, 2, '.', ',');//$myList->approved_amount; 
				//$row['balance'] = $myList->approved_total; 
				$row['id'] = $myList->id; 
				$row['reference'] = 'TRN-'.$myList->id; 
				$row['counter'] = $a; 
				$data[] = $row;
				$a++;	
				//$total_balance = $myList->approved_amount;
				//$total_balance = $myList->approved_total;	
				//$total_balance += $myList->approved_amount;	
			}
			//check from uploaded
			$get_trans_detail = $this->tblTransloan->check_for_payment($myList->id);
			foreach ($get_trans_detail as $key)
			{
				$bal = floatval($total_balance) - floatval($key->amount);
				$total_balance = $bal;
				$row2 = array();
				$row2['debit'] = number_format($key->amount, 2, '.', ',');
				$row2['beg'] = 0;	
				$d2 = date_create($key->created_dt);
				$row2['tdate'] = date_format($d2,"Y-m-d"); 
				$row2['balance'] = number_format($bal, 2, '.', ','); 
				$row2['particulars'] = 'Uploaded Collection'; 
				$row2['id'] = $myList->id;
				$row2['reference'] = $key->or_arno; 
				$row2['counter'] = $a; 
				$data[] = $row2;		
				$a++;	
			}

			$check = $this->tblTransloan->get_loan_by_head($myList->id);
			//check from manual
			foreach ($check as $value)
			{
				$get_manual_payment = $this->TblManualPayment->get_payment_trans_detail($value->id);
				foreach ($get_manual_payment as $row_data)
				{	
					$bal3 = floatval($total_balance) - floatval($row_data->amount);
					$total_balance = $bal3;
					$row3 = array();
					$row3['debit'] = number_format($row_data->amount, 2, '.', ',');
					$row3['tdate'] = $row_data->ordate;
					$row3['orno'] = $row_data->orno;
					$row3['beg'] = '';//0;	
					$row3['balance'] = number_format($bal3, 2, '.', ','); 
					$row3['particulars'] = $row_data->partticulars;	
					$row3['id'] = $myList->id; 				
					$row3['reference'] = $row_data->orno; 
			
					$row3['counter'] = $a; 
					$data[] = $row3;
				$a++;	

				}
			}
				$b++;
				$c++;	
		}

				//echo $total_balance;	
		/*$get_manual_payment = $this->TblManualPayment->get_payment_creditor('5107915');
		foreach ($get_manual_payment as $key)
		{
			$row = array();
			$row['amount'] = $key->amount;
			$row['tdate'] = $key->ordate;
			$row['orno'] = $key->orno;
			$data[] = $row;

		}*/	
		$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_jget_ledger($id) 
	{
		$list = $this->tblLedger->get_creditors_ledger($id);
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['ref'] = $myList->reference_no;
			$row['date'] = $myList->dt;
			$row['particulars'] = $myList->particulars;
			//($myList->mons_rem!=nuull?number_format(floatval($myList->principal)*$myList->mons_rem, 2, '.', ','):floatval($myList->principal))
			$row['cr'] = ($myList->mons_rem!=null?number_format(floatval($myList->principal)*$myList->mons_rem, 2, '.', ','):floatval($myList->principal));//number_format(floatval($myList->principal)*$myList->mons_rem, 2, '.', ',');//number_format(($myList->type=='Cr'?floatval($myList->amount):0), 2, '.', ',');
			$row['dr'] = ($myList->mons_rem!=null?number_format(floatval($myList->interest)*$myList->mons_rem, 2, '.', ','):floatval($myList->interest));//number_format(floatval($myList->interest)*$myList->mons_rem, 2, '.', ',');//number_format(($myList->type=='Dr'?floatval($myList->amount):0), 2, '.', ',');
			$row['balance'] = number_format($myList->balance, 2, '.', ',');
			$data[] = $row;
		}
		$output = array("data" => $data,);
		echo json_encode($output);
	}

	public function importbeginning()
	{
		$currDate  = date("Y-m-d H:i:s");
		$session_data = $this->session->userdata('logged_in');
        $data['addressbook'] = $this->tblLedger->get_addressbook();
        $data['error'] = '';    //initialize image upload error array to empty
 
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';

        $session_data = $this->session->userdata('logged_in');	   
 
        $this->load->library('upload', $config); 
 
        // If upload failed, display error
        if (!$this->upload->do_upload()) {
            $data['error'] = $this->upload->display_errors();
 
            $this->load->view('csvindex', $data);
        } else {
            $file_data = $this->upload->data();
            $file_path =  './uploads/'.$file_data['file_name'];
 
            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);

		        foreach ($csv_array as $row)
                {              		
		             $data = array(
		                        'reference_no'=>$this->input->post('refno'),
		                        'particulars'=>$this->input->post('particulars'),
		                        'uploaded_by'=>  $session_data['firstname'].' '.$session_data['lastname'],
		                        'uploaded_dt'=> date("Y-m-d"),
		                        'creditor_code'=> $row['creditor'],
		                        'dt	'=> $row['date'],
		                        'principal'=> $row['principal'],
		                        'interest'=> $row['interest'],
		                        'total'=> $row['total'],
		                        'balance'=> $row['beginning'],
		                        'is_final'=> 1,//$row['creditor_id'],
		                    );                   
		            //insert to repository temporary
		            $uploadid = $this->tblLedger->insert_csv($data);
                
                }///////////////////////
                
                $this->session->set_flashdata('success', 'Csv Data Imported Succesfully');
                redirect(base_url().'Ledger/uploads?id=ok');
            } else 
                $data['error'] = "Error occured";
                $this->load->view('csvindex', $data);
            } 
    }
}
