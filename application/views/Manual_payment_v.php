<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">

                        </div>
                        <!--START CONTENT HERE -JC -->
                      <!-- <div class="blog-img-thumb">
                                            <a href="javascript:;">
                                                <img src="company logo.png" class="img-responsive">
                                            </a>
                                        </div> -->
                                        <h1 class="page-title"> &nbsp;  </h1>
                                        <div class="tab-pane" id="tab_1">
                                            <div class="portlet box blue">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>Add Payment</div>
                                                    <div class="tools">                                                        
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <form action="#" id="form" class="horizontal-form">
                                                        <div class="form-body">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">OR No.</label>
                                                                        <input type="text" id="orno" name="orno" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">OR Date</label>
                                                                        
                                                                        <div class="input-group date col-md-12 dt_ndd">
                                                                          <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                                          <input type="text" class="form-control" name="ordate" id="ordate" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Principal</label>
                                                                        <input type="number" id="principal" name="principal" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Interest</label>  
                                                                        <input type="number" id="interest" name="interest" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                            </div>
                                                            <!--/row-->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Creditor</label>
                                                                        <div class="input-group input-group-sm">
                                                                            <input type="text" class="form-control" id="search_creditor" name="search_creditor" placeholder="Search for...">
                                                                            <input type="hidden" name="get_id_only" id="get_id_only">
                                                                            <span class="input-group-btn">
                                                                                <button type="button" class="btn btn-primary btn-lg" onclick="select()" id="load" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Searching">Search</button>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Amount</label>
                                                                        <input type="number" id="amount" name="amount" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                            </div>
                                                            <!--/row-->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Loan & Period</label>
                                                                        <select class="form-control" id="period" name="period_id" data-placeholder="Choose a Category" tabindex="1">
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Particulars</label>
                                                                        <textarea class="form-control" row="3" id="particulars" name="particulars"></textarea>
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                            </div>
                                                        </div>
                                                        <div class="form-actions right">
                                                            <button type="button" class="btn default" onclick="clearFields()" >Clear</button>
                                                            <button type="button" onclick="save()" class="btn green">
                                                                <i class="fa fa-check"></i> Save</button>

                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"> </div>
                        <br>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Payment</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">Creditor</th>
                                                    <th class="all">Period</th>
                                                    <th class="all">Amount</th>
                                                    <th class="all">OR No</th>
                                                    <th class="all">OR Date</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>

<script type="text/javascript">
var save_method  = "add";
var table; 
var table_creditor; 
$(document).ready(function() { 
table = $('#sample_1').DataTable();
/*$('#load').on('click', function() {
    var $this = $(this);
  $this.button('loading');   
});*/

$(".dt_ndd").datetimepicker({
         format: "YYYY-MM-DD",
         locale: "en",
         allowInputToggle: true
      });
list();
});

function list()
{
    $('#sample_1').DataTable().destroy();
     $('#sample_1').DataTable( {        
        "ajax": "<?php echo site_url('Manual_Payment/ajax_list')?>", 
        "columns": [
            { "data": "creditor" },
            { "data": "period" },
            { "data": "amount" },
            { "data": "orno" },
            { "data": "ordate" },
            { "data": "action" }
        ]       
    } );
}

function save()
{
    var url;
    if(validate_fields()==true)
    {
        if (save_method == 'add')
        {
            url = "<?php echo site_url('Manual_Payment/ajax_add')?>";
            $.ajax({
                url: url,
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    //$('#tblcreditors').dataTable().fnDestroy();
                    //$('#tblcreditors').DataTable().destroy();
                    $('#form')[0].reset();                    
                    //table_creditor.dataTable();
                    //table_creditor.dataTable().fnDestroy();
                    //select();
                    //$('#tblcreditors').DataTable();
                    $('#tblcreditors').dataTable().fnClearTable();
                    toastr.success('Payment Saved','Message:');
                    list();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    alert('Error in adding or updating data!');
                }
            });
        } else {
            url = "<?php echo site_url('Manual_Payment/ajax_update')?>";
            $.ajax({
                url: url,
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    $('#form')[0].reset();                    
                    //table_creditor.dataTable();
                    //table_creditor.dataTable().fnDestroy();
                    //select();
                    //$('#tblcreditors').DataTable();
                    $('#tblcreditors').dataTable().fnClearTable();
                    toastr.success('Payment Saved','Message:');
                    list();
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    alert('Error in adding or updating data!');
                }
            });
        }
    }else{
         toastr.error('Check Fields!','Message:');
    }
    
}

function add_user()
{
  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('[name="emp_id"]').trigger('chosen:updated');
  $('[name="group_id2"]').trigger('chosen:updated');
  $('#user_grp').show();
  $('#pw').show();
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add User'); // Set Title to Bootstrap modal title
}

function delete_user(id)
{
    swal({
      title: "Are you sure you want to delete?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "error",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('users/ajax_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table
                        delete_all_privileges(id);                        
                        list();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                });

          swal("Deleted!", "User account has been deleted.", "success");
    });
}

function search_creditor()
{
  $('#modal_form').modal({backdrop: 'static',keyboard: false});
  table = $('#tblcreditors').DataTable();
}

function select()
{
    $('#load').button('reset');
    $('#modal_form').modal({backdrop: 'static',keyboard: false});
    //table_creditor = $('#tblcreditors').DataTable();
    //$('#tblcreditors').dataTable().fnDestroy();
    if($("#search_creditor").val()!="")
    {
       table_creditor = $('#tblcreditors').DataTable();
       $('#tblcreditors').dataTable().fnDestroy();
        $('#tblcreditors').DataTable( {
            //"processing": true,
            //"retrieve": true,
            //"paging": true,
            //"serverSide": true,
            "ajax": "<?php echo site_url('Creditors/search_creditors')?>/"+$("#search_creditor").val(),
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "company" },
                { "data": "action" },
            ] 
        } ); 

    }else{
        toastr.error('Search Field is Empty!','Message:');
    }    
}

function select_creditor(id)
{
                var period = [];
                $.ajax({
                    url: "<?php echo site_url('Loans/get_loan_period')?>",
                    type: "POST",
                    data:{
                        id:id
                    },
                    dataType: "JSON",
                    success: function(data)
                    {
                        $("#get_id_only").val(id);
                        $('#load').button('reset');
                        $('#modal_form').modal('hide');
                        $("#period").empty();
                        $('#period').append($("<option selected disabled>Select Period</option>"));
                        $.each(data.data, function(key, value) {    
                            period.push({text:value.payperiod+" (₱"+value.deduction+")",id:value.id});
                        });

                        $('#period').append($("<option value='others'>Others</option>"));
                        $("#period").select2({
                             data: period
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                });
}

function clearFields()
{
    $('#form')[0].reset();
}

function validate_fields()
{
      var orno = $('[name="orno"]').val();
      var ordate = $('[name="ordate"]').val();
      var search_creditor = $('[name="search_creditor"]').val();
      var amount = $('[name="amount"]').val();
      var period_id = $('[name="period_id"]').val();
      var get_id_only = $('[name="get_id_only"]').val();
      var principal = $('[name="principal"]').val();
      var interest = $('[name="interest"]').val();

      if(orno!=""&&ordate!=""&&get_id_only!=""&&amount!=""&&principal!=""&&interest!=""&&period_id!=null)
      {
        return true;
      }else{
        return false;
      }
}

function edit_payment(bid,hid)
{
   save_method = 'update';
    $('#form')[0].reset();
    $.ajax({
      url : "<?php echo site_url('Manual_Payment/ajax_edit_h')?>/" + hid,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {           
        $("#orno").val(data.orno);
        $("#particulars").val(data.partticulars);
        $("#ordate").val(data.ordate);
        $("#search_creditor").val(data.creditor);
        select_creditor(data.creditor);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });

    $.ajax({
      url : "<?php echo site_url('Manual_Payment/ajax_edit_b')?>/" + bid,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {           
        $("#amount").val(data.amount);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
}

</script>

<div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title-priv"> </h3>
         </div>
         <div class="modal-body form">
            <div class="portlet light portlet-fit portlet-datatable bordered">
               <!-- <div class="portlet-title">
                  <div class="col-md-8">
                     <div class="btn-group">
                        <div class="input-group">
                           <span class="input-group-btn">
                           <button class="btn btn-primary" type="button" onclick="select()"><i class="fa fa-search-plus" aria-hidden="true"></i></button>
                           </span>
                           <input type="text" class="form-control" placeholder="Firstname, Lastname or Employee Code" id="search_creditor">
                        </div>
                     </div>
                  </div>
               </div> -->
               <div class="portlet-body">
                  <div class="table-container">
                     <table class="table table-striped table-bordered table-hover" id="tblcreditors">
                        <thead>
                           <tr>
                              <th> ID </th>
                              <th> Name </th>
                              <th> Company </th>
                              <th> Action </th>
                           </tr>
                        </thead>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div> 
</div>