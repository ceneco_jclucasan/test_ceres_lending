<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                        <br>

                         <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Ledger</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="col-md-6">
                                                    <form action="#" id="form" class="horizontal-form">
                                                        <div class="form-body">
                                                            <!--/row-->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <button type="button" class="btn btn-primary" onclick="search_creditor()"><i class="fa fa-search" aria-hidden="true"></i>  Search Creditor</button>
                                                                        <h3><p>Creditor: <span id="creditor"></span></p></h3>
                                                                        <!-- <div class="input-group input-group-sm">
                                                                            <input type="text" class="form-control" id="search_creditor" name="search_creditor" placeholder="Search for...">
                                                                            <span class="input-group-btn">
                                                                                <button type="button" class="btn btn-primary btn-lg" onclick="view_ledger()" id="load" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Searching">Search</button>
                                                                            </span>
                                                                        </div> -->
                                                                    </div>
                                                                </div>
                                                                <!--/span-->
                                                            </div>
                                                        </div>
                                                        <!-- <div class="form-actions right">
                                                            <button type="button" onclick="save()" class="btn green">
                                                                <i class="fa fa-check"></i> View</button>
                                                        </div> -->
                                                    </form>
                                                </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                   <!--  <th class="all">Counter</th> -->
                                                    <th class="all">Reference</th>
                                                    <th class="all">Date</th>
                                                    <th class="all">Particulars</th>
                                                    <th class="all">Total Principal</th>
                                                    <th class="all">Total Interest</th>
                                                    <th class="all">Balance</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                    </div>
                </div>

<script type="text/javascript">
var table;
  $(document).ready(function() {
       //list();
       $('[name="class"]').select2({theme:'classic'});
      $(".dt_ndd").datetimepicker({
                   format: "YYYY",
                   locale: "en",
                   allowInputToggle: true,
                   viewMode: 'years',
               });
      view_ledger();
  });

  function search_creditor()
 {
  $('#modal_form').modal({backdrop: 'static',keyboard: false});
  table = $('#tblcreditors').DataTable();
 }

 /* function view_ledger(id)
  {
    var table = $('#sample_1').DataTable();
    table.destroy();
      $('#sample_1').DataTable( {
        
        "ajax": "<?php echo site_url('Ledger/ajax_list')?>/"+id, 
        "iDisplayLength": 100,
        "order": [[ 1, "asc" ]],
        //"order": [[ 2, "asc" ]],
        //"bSort" : false,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'print',
        ],
        "columns": [
            //{ "data": "counter" },
            { "data": "reference" },
            { "data": "tdate" },
            { "data": "particulars" },
            { "data": "beg" },  
            { "data": "debit" },
            { "data": "balance" },
        ]
       
    } );
  }*/

  function view_ledger(id)
  {
    var table = $('#sample_1').DataTable();
    table.destroy();
      $('#sample_1').DataTable( {
        
        "ajax": "<?php echo site_url('Ledger/ajax_jget_ledger')?>/"+id, 
        "iDisplayLength": 100,
        //"order": [[ 1, "asc" ]],
        //"order": [[ 2, "asc" ]],
        "bSort" : false,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5',
            'print',
        ],
        "columns": [
            //{ "data": "counter" },
            { "data": "ref" },
            { "data": "date" },
            { "data": "particulars" },
            { "data": "dr" },
            { "data": "cr" },  
            { "data": "balance" },
        ]
       
    } );
  }

  function edit_bracket(id)
  {
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('Creditors/ajax_edit_bracket/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {           
        $('[name="id"]').val(data.id);
        $('[name="fy"]').val(data.from_years);
        $('[name="ty"]').val(data.to_years);
        $('[name="amount"]').val(data.amount);
        $('[name="class"]').select2().val(data.creditor_class_id).trigger("change");
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Bracket'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }



function save()
{
   var url;
    if (save_method == 'add') {
        url = "<?php echo site_url('Creditors/ajax_add_bracket')?>";
    } else {
        url = "<?php echo site_url('Creditors/ajax_update_bracket')?>";
    }
    $.ajax({
        url: url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data) {
            //if success close modal and reload ajax table
            $('#modal_form').modal('hide');
            table.ajax.reload( null, false );
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
        }
    });
}

function add_bracket()
{
  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('[name="class"]').select2().val('').trigger("change");
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add Bracket'); // Set Title to Bootstrap modal title
}

function delete_bracket(id)
{
    swal({
      title: "Are you sure you want to delete?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "error",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('Creditors/ajax_delete_bracket')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table                  
                         table.ajax.reload( null, false );
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                });

          swal("Deleted!", "Bracket has been deleted.", "success");
    });
}

function select()
{
    if($("#search_creditor").val()!="")
    {
       $('#tblcreditors').dataTable().fnDestroy();
        $('#tblcreditors').DataTable( {
            //"processing": true,
            //"retrieve": true,
            //"paging": true,
            //"serverSide": true,
            "ajax": "<?php echo site_url('Creditors/search_creditors')?>/"+$("#search_creditor").val(),
            "columns": [
                { "data": "code" },
                { "data": "name" },
                { "data": "company" },
                { "data": "action" },
            ] 
        } ); 
    }else{
        toastr.error('Search Field is Empty!','Message:');
    }    
 }

 function select_creditor(id)
{
    $.ajax({
          url : "<?php echo site_url('Creditors/ajax_edit_creditors')?>/" +id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {           
            $('#modal_form').modal('hide');
            view_ledger(data.employee_code);
            $("#creditor").empty();
            $("#creditor").append("<b>"+data.fullname+"</b>");
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error in getting data!');
          }
    });
}

</script>

<div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title-priv"> </h3>
         </div>
         <div class="modal-body form">
            <div class="portlet light portlet-fit portlet-datatable bordered">
               <div class="portlet-title">
                  <div class="col-md-8">
                     <div class="btn-group">
                        <div class="input-group">
                           <span class="input-group-btn">
                           <button class="btn btn-primary" type="button" onclick="select()"><i class="fa fa-search-plus" aria-hidden="true"></i></button>
                           </span>
                           <input type="text" class="form-control" placeholder="Firstname, Lastname or Employee Code" id="search_creditor">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="portlet-body">
                  <div class="table-container">
                     <table class="table table-striped table-bordered table-hover" id="tblcreditors">
                        <thead>
                           <tr>
                              <th> ID </th>
                              <th> Name </th>
                              <th> Company </th>
                              <th> Action </th>
                           </tr>
                        </thead>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div> 
</div>