<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>
<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">

                        </div>
                        <!--START CONTENT HERE -JC -->
                       <div class="blog-img-thumb">
                                            <a href="javascript:;">
                                                <img src="company logo.png" class="img-responsive">
                                            </a>
                                        </div> 
                                        <h1 class="page-title"> GoodLife Dashboard
                            <small>statistics, charts, recent events and reports</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                    </div>
                </div>

<script type="text/javascript">
  $(document).ready(function() { 
       
  });

</script>

<div class="modal fade" id="modal_form_privilege" role="dialog">
              <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 class="modal-title-priv"> </h3>
                </div>
                <div class="modal-body form">
                      <table id="tblPriv" class="table table-striped table-bordered table-hover" 
                                            data-search="true" 
                                            data-pagination="true"
                                            data-page-size="5"
                                            data-show-export="true"
                                            data-mobile-responsive="true"
                                            data-sort-name = "priv_name"
                                            data-sort-order = "desc"
                                             data-url="<?php echo site_url('Privillege')?>"
                                            data-page-list="[5, 10, ALL]">
                                      <thead>
                                        <tr>
                                          <th data-field="priv_name"  data-sortable = "true">Privilege Names</th>
                                          <th data-field="desc"  data-sortable = "true">Description</th>
                                          <th data-field="action" style="width:125px;">Action</th>
                                        </tr>
                                      </thead>
                                </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->   
</div>

<div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">User Account Form</h3>
         </div>
         <div class="modal-body form">
            <form action="#" id="form" class="form-horizontal">
               <input type="hidden" value="" name="id"/> 
               <div class="form-body">
                  <div class="form-group">
                     <label class="control-label col-md-3">Name</label>
                     <div class="col-md-9">
                        <select name="emp_id" data-placeholder="Select..." class="form-control"></select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Email</label>
                     <div class="col-md-9">
                        <input name="email" placeholder="" class="form-control" type="text">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Username</label>
                     <div class="col-md-9">
                        <input name="username" placeholder="" class="form-control" type="text">
                     </div>
                  </div>
                  <div id="pw" class="form-group">
                     <label class="control-label col-md-3">Password</label>
                     <div class="col-md-9">
                        <input name="password" placeholder="" class="form-control" type="password">
                     </div>
                  </div>
                  <div id="user_grp" class="form-group">
                     <label class="control-label col-md-3">User Group</label>
                     <div class="col-md-9">
                        <select name="group_id2" data-placeholder="Select..." class="form-control"></select>
                     </div>
                  </div>
                  <div id="alert2" class="form-group">
                     <div class="col-md-8 pull-right">
                        <div class="alert alert-danger alert-dismissable">
                           <h4>  <i class="icon fa fa-remove"></i> This user already have an account.</h4>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->