<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>
<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">

                        </div>
                        <!--START CONTENT HERE -JC -->
                      <!-- <div class="blog-img-thumb">
                                            <a href="javascript:;">
                                                <img src="company logo.png" class="img-responsive">
                                            </a>
                                        </div> -->
                                        <h1 class="page-title"> GoodLife Dashboard
                            <small>statistics, charts, recent events and reports</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                        <div class="row">
                            <div class="col-lg-3   col-md-2 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 red" href="<?php echo base_url(); ?>Loans/Loan_app">
                                    <div class="visual">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="12,5"><?php print_r($this->Trans_loan_m->count_pending_loans()); ?></span> </div>
                                        <div class="desc"> Pending Loans </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 blue" href="<?php echo base_url(); ?>Loans/Loan_validation?status=checking">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="1349"><?php print_r($this->Trans_loan_m->count_validated_loans()); ?></span>
                                        </div>
                                        <div class="desc"> Validation</div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 green" href="<?php echo base_url(); ?>Loans/Loan_validation?status=approval">
                                    <div class="visual">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="549"><?php print_r($this->Trans_loan_m->count_recommended_loans()); ?></span>
                                        </div>
                                        <div class="desc"> Recommendation </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 purple" href="<?php echo base_url(); ?>Loans/Loan_disbursement">
                                    <div class="visual">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number"> 
                                            <span data-counter="counterup" data-value="89"><?php print_r($this->Trans_loan_m->count_approved_loans()); ?></span> </div>
                                        <div class="desc"> Approval </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet yellow box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-share font-dark hide"></i>
                                            <span class="caption-subject font-dark bold uppercase">Today's Applicant </span>
                                        </div>
                                  
                                    </div>
                                    <div class="portlet-body">
                                       <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="tblpending">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">Transaction</th>
                                                    <th class="all">Creditor</th>
                                                    <th class="all">Applied Amount</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-share font-dark hide"></i>
                                            <span class="caption-subject font-dark bold uppercase">Today's Release</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="tblrelease">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">Transaction</th>
                                                    <th class="all">Creditor</th>
                                                    <th class="all">Approved Amount</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-xs-12 col-sm-12">
                                <div class="portlet light ">
                                    <div id="chart_1_1_1_legendPlaceholder"> </div>
                                         <div id="bar-chart" class="chart"  style="width:100%; height:500px !important;"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<script type="text/javascript">
  $(document).ready(function() { 
       load_chart();
       $('#alert2').hide();
       list2();
       $('#tblpending').DataTable();
       $('#tblrelease').DataTable();
  });

  function load_chart()
  {
    $.ajax({
                  url : "<?php echo site_url('Creditors/ajax_creditors_company')?>/" ,
                  type: "POST",
                  dataType: "JSON",
                  success: function(data)
                  {   
                      var a = new Array();  
                      for (x = 0; x < data.data.length; x++) {
                        a.push([data.data[x]['company'],data.data[x]['counter']]);
                  }

                   var bar_data = {
                    data: a,
                    color: "#3c8dbc"
                  };  

                  $.plot('#bar-chart', [bar_data], 
                  {
                      grid  : {
                        borderWidth: 1,
                        borderColor: '#f3f3f3',
                        tickColor  : '#f3f3f3',
                    hoverable  : true
                      },
                      series: {
                        bars: {
                          show    : true,
                          barWidth: 0.7,
                          align   : 'center', 
                      numbers : {
                        show: true,
                        yAlign: function(y) { return y + 10; },
                        xAlign: function(x) { return x; },
                      }
                        }
                      },
                      xaxis : {
                        mode      : 'categories',
                        tickLength: 0
                      }
                    });
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error: "ajax_list"');
                  }
                });
  }

  function list()
  {
     $.ajax({
                  url : "<?php echo site_url('Users/ajax_list')?>/" ,
                  type: "POST",
                  dataType: "JSON",
                  success: function(data)
                  {                
                      var a = new Array();  
                      for (x = 0; x < data.data.length; x++) {
                        a[x] = 
                        {
                            "email": data.data[x][0],
                            "fn": data.data[x][1],
                            "ln": data.data[x][2],
                            "ug": data.data[x][3],
                            "as": data.data[x][5],
                            "action": data.data[x][4],
                        };
                      }
                      $('#tblUsers').bootstrapTable({ data: a });
                      $('#tblUsers').bootstrapTable('load',a);
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error: "ajax_list"');
                  }
                });
  }

  function manage_privilages(id)
  {
      $('#modal_form_privilege').modal('show'); // show bootstrap modal

            $.ajax({
                  url : "<?php echo site_url('users/ajax_list_user/')?>/" + id,
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {   
                   // $('.modal-title-priv').text('Privileges of '+data.username); // Set Title to Bootstrap modal title
                    var a = new Array();
                        for (x = 0; x < data.data.length; x++) {
                          a[x] = 
                          {
                            "priv_name": data.data[x][0],
                            "desc": data.data[x][1],
                            "action": data.data[x][2],
                          };
                        }
                        $('#tblPriv').bootstrapTable({ data: a });
                        $('#tblPriv').bootstrapTable('load',a);
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data!');
                  }
            });
  }

  function add_priv_user(privid,userid)
  {
                if (confirm('Are you sure add this privilege?')) {

                    $.ajax({
                      url :  "<?php echo site_url('Users/ajax_update_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                            toastr.success('Privilege has been added!','Message:');
                            manage_privilages(userid);

                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
                  }
  }

  function remove_priv_user(privid,userid)
  {    
                if (confirm('Are you sure remoive this privilege?')) {
                    $.ajax({
                      url :  "<?php echo site_url('Users/ajax_delete_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                        toastr.error('Privilege has been removed!','Message:');
                        manage_privilages(userid);
                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
                  }
  }

  function edit_user(id)
  {
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        //Ajax Load data from ajax
        $.ajax({
          url : "<?php echo site_url('users/ajax_edit/')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {           
            $('[name="id"]').val(data.id);
            $('[name="emp_id"]').val(data.emp_id).trigger('chosen:updated');
            $('[name="email"]').val(data.email);
            $('[name="username"]').val(data.username);
            $('[name="password"]').val(data.password);
            $('[name="group_id2"]').val(data.group_id).trigger('chosen:updated');
            $('#user_grp').hide();
            $('#pw').hide();
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error in getting data!');
          }
        });
  }

  function dropdown_list_employees()
  {
      $('[name="emp_id"]').empty();
      var a = "";
      var b = "";
      $.ajax({
          url: "<?php echo site_url('users/ajax_dropdown_list_employees')?>",
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('[name="emp_id"]').append('<option> </option>');
              for (var x = 0; x < data.data.length; x++) {
                  a = data.data[x][0];
                  b = data.data[x][1];
                  if (a != "1") {
                      $('[name="emp_id"]').append('<option value="' + a + '">' + b + '</option>');
                  }
              };
              $('[name="emp_id"]').trigger('chosen:updated');
          },
          error: function(jqXHR, textStatus, errorThrown)
          {
              alert('Error in getting data for dropdown list!');
          }
      });
  }

function save()
{
    var url;
    if (save_method == 'add') {
        url = "<?php echo site_url('users/ajax_add')?>";
        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                //if success close modal and reload ajax table
                if (data.data[0] == 'This user already have an account.') {
                    $('#alert2').fadeIn();
                    setTimeout(function() {
                        $('#alert2').fadeOut();
                    }, 2000);
                } else {
                    console.log(data.data[0]);
                    //save privileges here
                    get_group_privileges($('[name="group_id2"]').val(), data.data[0]);
                    $('#modal_form').modal('hide');
                    list();
                };
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert('Error in adding or updating data!');
            }
        });
    } else {
        url = "<?php echo site_url('users/ajax_update')?>";
        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                //if success close modal and reload ajax table
                $('#modal_form').modal('hide');
                list();
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert('Error in adding or updating data!');
            }
        });
    }
}

function add_user()
{
  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('[name="emp_id"]').trigger('chosen:updated');
  $('[name="group_id2"]').trigger('chosen:updated');
  $('#user_grp').show();
  $('#pw').show();
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add User'); // Set Title to Bootstrap modal title
}

function delete_user(id)
{
    swal({
      title: "Are you sure you want to delete?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "error",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('users/ajax_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table
                        delete_all_privileges(id);                        
                        list();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                });

          swal("Deleted!", "User account has been deleted.", "success");
    });
}

function get_group_privileges_by_id()
{
    for (var i = 0; i < document.getElementsByName("privilege_checkbox[]").length; i++) {
        //console.log("group_privilege[" + data.data[i][0] + "]");
        document.getElementsByName("privilege_checkbox[]")[i].checked = false;
    };
    $.ajax({
        url: "<?php echo site_url('user_groups/ajax_get_privileges_by_id')?>/" + $('[name="group_id"]').val(),
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            for (var i = 0; i < data.data.length; i++) {
                //console.log("group_privilege[" + data.data[i][0] + "]");
                document.getElementById("user_privilege[" + data.data[i][0] + "]").checked = true;
            };
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function dropdown_list_group()
{
    $('[name="group_id"]').empty();
    $('[name="group_id2"]').empty();
    var a = "";
    var b = "";
    $.ajax({
        url: "<?php echo site_url('users/ajax_dropdown_list_group')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="group_id"]').append('<option> </option>');
            $('[name="group_id2"]').append('<option> </option>');
            for (var x = 0; x < data.data.length; x++) {
                a = data.data[x][0];
                b = data.data[x][1];
                if (a != "1") {
                    $('[name="group_id"]').append('<option value="' + a + '">' + b + '</option>');
                    $('[name="group_id2"]').append('<option value="' + a + '">' + b + '</option>');
                }
            };
            $('[name="group_id"]').trigger('chosen:updated');
            $('[name="group_id2"]').trigger('chosen:updated');
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            alert('Error in getting data for dropdown list!');
        }
    });
}

function get_group_privileges(group_id, user_id)
{
    $.ajax({
        url: "<?php echo site_url('user_groups/ajax_get_privileges_by_id')?>/" + group_id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            for (var i = 0; i < data.data.length; i++) {
                $.ajax({
                    url: "<?php echo site_url('users/ajax_update_privileges')?>",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        user_id: user_id,
                        privilege_id: data.data[i][0]
                    },
                    success: function(data2)
                    {
                        console.log(data2.data[0]);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('error');
                    }
                });
            };
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function delete_all_privileges(id)
{
    $.ajax({
        url: "<?php echo site_url('users/ajax_delete_all_privileges')?>/" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            alert('Error in deleting data!');
        }
    });
}

function suspend_user(id)
{  
    swal({
      title: "Are you sure you want to suspend user?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Suspend",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('users/ajax_suspend_user')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        list();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in adding or updating data!');
                    }
                });

          swal("Suspended!", "Account", "warning");
    });
}

function unsuspend_user(id)
{

  swal({
      title: "Are you sure you want to unsuspend user?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Unsuspend",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('users/ajax_unsuspend_user')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        list();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in adding or updating data!');
                    }
                });

          swal("Unsuspended!", "Account", "success");
    });   
}

function list2()
  {
    $('#tblpending').DataTable( {        
        "ajax": "<?php echo site_url('Dashboard/ajax_list_loan_app')?>", 
        "iDisplayLength": 5,
        "columns": [
            { "data": "trn" },
            { "data": "creditor" },
            { "data": "amount" },
            { "data": "action" }
        ]
       
    } );
     $('#tblrelease').DataTable( {        
        "ajax": "<?php echo site_url('Dashboard/ajax_list_loan_approved')?>", 
        "iDisplayLength": 5,
        "columns": [
            { "data": "trn" },
            { "data": "creditor" },
            { "data": "amount" },
            { "data": "action" }
        ]
       
    } );

     
  }
</script>

<div class="modal fade" id="modal_form_privilege" role="dialog">
              <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 class="modal-title-priv"> </h3>
                </div>
                <div class="modal-body form">
                      <table id="tblPriv" class="table table-striped table-bordered table-hover" 
                                            data-search="true" 
                                            data-pagination="true"
                                            data-page-size="5"
                                            data-show-export="true"
                                            data-mobile-responsive="true"
                                            data-sort-name = "priv_name"
                                            data-sort-order = "desc"
                                             data-url="<?php echo site_url('Privillege')?>"
                                            data-page-list="[5, 10, ALL]">
                                      <thead>
                                        <tr>
                                          <th data-field="priv_name"  data-sortable = "true">Privilege Names</th>
                                          <th data-field="desc"  data-sortable = "true">Description</th>
                                          <th data-field="action" style="width:125px;">Action</th>
                                        </tr>
                                      </thead>
                                </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->   
</div>

<div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">User Account Form</h3>
         </div>
         <div class="modal-body form">
            <form action="#" id="form" class="form-horizontal">
               <input type="hidden" value="" name="id"/> 
               <div class="form-body">
                  <div class="form-group">
                     <label class="control-label col-md-3">Name</label>
                     <div class="col-md-9">
                        <select name="emp_id" data-placeholder="Select..." class="form-control"></select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Email</label>
                     <div class="col-md-9">
                        <input name="email" placeholder="" class="form-control" type="text">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Username</label>
                     <div class="col-md-9">
                        <input name="username" placeholder="" class="form-control" type="text">
                     </div>
                  </div>
                  <div id="pw" class="form-group">
                     <label class="control-label col-md-3">Password</label>
                     <div class="col-md-9">
                        <input name="password" placeholder="" class="form-control" type="password">
                     </div>
                  </div>
                  <div id="user_grp" class="form-group">
                     <label class="control-label col-md-3">User Group</label>
                     <div class="col-md-9">
                        <select name="group_id2" data-placeholder="Select..." class="form-control"></select>
                     </div>
                  </div>
                  <div id="alert2" class="form-group">
                     <div class="col-md-8 pull-right">
                        <div class="alert alert-danger alert-dismissable">
                           <h4>  <i class="icon fa fa-remove"></i> This user already have an account.</h4>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->