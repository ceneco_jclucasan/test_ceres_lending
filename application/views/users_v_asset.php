
 <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Manage User Accounts<small></small> </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Admin</a></li>
            <li><a href="#">User Accounts</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Users List</h3>
              <!-- <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div> -->
            </div>
            <div class="box-body table-responsive">

             <!--- Start of custom details-->
              <div id="container">
    <?php
      for ($i=0; $i<sizeof($privileges); $i++) { 
        if ($privileges[$i]->privilege_name == "Add Users") 
        {                
          echo '<div id="toolbar">
                  <button class="btn btn-success" onclick="add_user()"><i class="glyphicon glyphicon-plus"></i> Add User</button>
                </div>';
        }
      }
    ?>
                <table id="table" class="table table-striped table-bordered table-hover" 
                        data-toolbar="#toolbar"
                        data-search="true" 
                        data-pagination="true"
                        data-show-export="true"
                        data-show-columns="true"
                        data-mobile-responsive="true"
                        data-check-on-init="true"
                        data-page-list="[10, 25, 50, 100, ALL]"
                        data-row-style="rowStyle">
                  <thead>
                    <tr>
                      <th data-field="email">Email Address</th>
                      <th data-field="firstname">First Name</th>
                      <th data-field="lastname">Last Name</th>
                      <th data-field="group">User Group</th>
                      <th data-field="action" style="width:125px;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
    <?php
      for ($i=0; $i<sizeof($privileges); $i++) { 
        if ($privileges[$i]->privilege_name == "View Suspended Users") 
        {                
          echo '<div class="box box-warning collapsed-box">
                  <div class="box-header with-border">
                    <h3 class="box-title">Suspended User Accounts</h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div><!-- /.box-tools -->
                  </div><!-- /.box-header -->
                  <div class="box-body" style="display: none;">
                    <table id="table_suspended" class="table table-striped table-bordered table-hover" 
                      data-search="true" 
                      data-pagination="true"
                      data-show-export="false"
                      data-show-columns="false"
                      data-mobile-responsive="true"
                      data-check-on-init="true"
                      data-page-list="[10, 25, 50, 100, ALL]">
                      <thead>
                        <tr>
                          <th data-field="email2">Email Address</th>
                          <th data-field="firstname2">First Name</th>
                          <th data-field="lastname2">Last Name</th>
                          <th data-field="group2">User Group</th>
                          <th data-field="action2" style="width:125px;">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->';
        }
      }
    ?>
              </div>
            </div>

            <script type="text/javascript">
              var save_method; //for save method string
              $(document).ready(function() {
                list();
                list_suspended();
                list_privileges();
                dropdown_list_group();   
                dropdown_list_employees();
                $('#alert').hide(); 
                $('#alert2').hide();    
                $('[name="emp_id"]').select2({width:'100%'});
                $('[name="group_id"]').select2({width:'100%'});
                $('[name="group_id2"]').select2({width:'100%'});
                //dropdown_list();
              });

              function list()
              {
                $.ajax({
                  url : "<?php echo site_url('users/ajax_list')?>",
                  type: "POST",
                  dataType: "JSON",
                  success: function(data)
                  {    
                      var a = new Array();  
                      
                      for (x = 0; x < data.data.length; x++) {
                        a[x] = 
                        {
                            "email": data.data[x][0],
                            "firstname": data.data[x][1],
                            "lastname": data.data[x][2],
                            "group": data.data[x][3],
                            "action": data.data[x][4],
                        };
                      }
                      //console.log(a);
                      $('#table').bootstrapTable({ data: a });
                      $('#table').bootstrapTable('load',a);
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting table list!');
                  }
                });
              }

              function list_suspended()
              {
                $.ajax({
                  url : "<?php echo site_url('users/ajax_list_suspended')?>",
                  type: "POST",
                  dataType: "JSON",
                  success: function(data)
                  {    
                      var a = new Array();  
                      
                      for (x = 0; x < data.data.length; x++) {
                        a[x] = 
                        {
                            "email2": data.data[x][0],
                            "firstname2": data.data[x][1],
                            "lastname2": data.data[x][2],
                            "group2": data.data[x][3],
                            "action2": data.data[x][4],
                        };
                      }
                      //console.log(a);
                      $('#table_suspended').bootstrapTable({ data: a });
                      $('#table_suspended').bootstrapTable('load',a);
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting table list!');
                  }
                });
              }

              function add_user()
              {
                save_method = 'add';
                $('#form')[0].reset(); // reset form on modals
                $('[name="emp_id"]').trigger('chosen:updated');
                $('[name="group_id2"]').trigger('chosen:updated');
                $('#user_grp').show();
                $('#pw').show();
                $('#modal_form').modal('show'); // show bootstrap modal
                $('.modal-title').text('Add User'); // Set Title to Bootstrap modal title
              }

              function edit_user(id)
              {
                save_method = 'update';
                $('#form')[0].reset(); // reset form on modals
                //Ajax Load data from ajax
                $.ajax({
                  url : "<?php echo site_url('users/ajax_edit/')?>/" + id,
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {           
                    $('[name="id"]').val(data.id);
                    $('[name="emp_id"]').val(data.emp_id).trigger('chosen:updated');
                    $('[name="email"]').val(data.email);
                    $('[name="username"]').val(data.username);
                    $('[name="password"]').val(data.password);
                    $('[name="group_id2"]').val(data.group_id).trigger('chosen:updated');
                    $('#user_grp').hide();
                    $('#pw').hide();
                    $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                    $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data!');
                  }
                });
              }

              function save()
              {
                var url;
                if(save_method == 'add') {
                  url = "<?php echo site_url('users/ajax_add')?>";
                  // ajax adding data to database
                  $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function(data)
                    {
                      //if success close modal and reload ajax table
                      if (data.data[0] == 'This user already have an account.') {
                        $('#alert2').fadeIn();
                        setTimeout(function(){ $('#alert2').fadeOut(); }, 2000);
                      } else {
                        console.log(data.data[0]); 
                        //save privileges here
                        get_group_privileges($('[name="group_id2"]').val(), data.data[0]);
                        $('#modal_form').modal('hide');
                        list();
                      };
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in adding or updating data!');
                    }
                  });
                } else {
                  url = "<?php echo site_url('users/ajax_update')?>";
                  // ajax adding data to database
                  $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function(data)
                    {
                      //if success close modal and reload ajax table
                      $('#modal_form').modal('hide');
                      list();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in adding or updating data!');
                    }
                  });
                }
              }

              function delete_user(id)
              {
                BootstrapDialog.show({
                  title: 'Delete User Account',
                  type: 'type-danger',
                  message: 'Do you want to delete this data?',
                  buttons: [{
                    label: '  Yes',
                    icon: 'glyphicon glyphicon-ok',
                    cssClass: 'btn-primary',
                    action: function(dialog) {
                      $.ajax({
                        url : "<?php echo site_url('users/ajax_delete')?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                           //if success reload ajax table
                           delete_all_privileges(id);
                           dialog.close();
                           list();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error in deleting data!');
                        }
                      }); 
                    }
                  }, {
                    label: '  No',
                    icon: 'glyphicon glyphicon-remove',
                    cssClass: 'btn-danger',
                    action: function(dialog) {
                      dialog.close(); 
                    }
                  }]
                });
              }

              function delete_all_privileges(id)
              {
                $.ajax({
                  url : "<?php echo site_url('users/ajax_delete_all_privileges')?>/" + id,
                  type: "POST",
                  dataType: "JSON",
                  success: function(data)
                  {
                     
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in deleting data!');
                  }
                });
              }

              function manage_privilages(id)
              {
                save_method = 'add';
                $('#form_users')[0].reset(); // reset form on modals
                $.ajax({
                  url : "<?php echo site_url('users/ajax_edit/')?>/" + id,
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {           
                    if (id == "1") {
                      $('#usergrp').hide();
                      $('[name="id2"]').val(data.id);
                      $('[name="name2"]').val(data.firstname + ' ' + data.lastname); 
                      $('[name="group_id"]').val(data.group_id).trigger('chosen:updated'); 
                      get_privileges_by_id();                                                                             
                      $('#modal_form_users').modal('show'); // show bootstrap modal when complete loaded
                      $('.modal-title').text('Manage Privileges'); // Set title to Bootstrap modal title
                    } else {
                      $('#usergrp').show();
                      $('[name="id2"]').val(data.id);
                      $('[name="name2"]').val(data.firstname + ' ' + data.lastname); 
                      $('[name="group_id"]').val(data.group_id).trigger('chosen:updated'); 
                      get_privileges_by_id();                                                                             
                      $('#modal_form_users').modal('show'); // show bootstrap modal when complete loaded
                      $('.modal-title').text('Manage Privileges'); // Set title to Bootstrap modal title
                    }
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data!');
                  }
                });
              }

              function list_privileges()
              {
                $('#list').empty();
                var a = "";
                var b = "";
                var c = "";
                $.ajax({
                    url : "<?php echo site_url('users/ajax_list_privilege/')?>",
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    { 
                      for (var i = 0; i < data.data.length; i++) {
                        //console.log(data.data[i]);
                        for (var n = 0; n < data.data[i].length; n++) {
                          if (n == 0) {
                            a = '<td><input type="checkbox" id="user_privilege[' + data.data[i][n] + ']" name="privilege_checkbox[]" value="1"><input type="hidden" id="privilege_hidden[' + i + ']" value="' + data.data[i][n] + '"></td>';
                          } else if (n == 1) {
                            b = '<td>' + data.data[i][n] + '</td>';
                          } else if (n == 2) {
                            c = '<td>' + data.data[i][n] + '</td>';
                          }
                        };
                        $('#list').append('<tr>' + c + a +'</tr>');
                      };
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });
              }

              function get_privileges_by_id()
              {
                $.ajax({
                    url : "<?php echo site_url('users/ajax_get_privileges_by_id')?>/" + $('[name="id2"]').val(),
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    { 
                      for (var i = 0; i < data.data.length; i++) {
                        //console.log("group_privilege[" + data.data[i][0] + "]");
                        document.getElementById("user_privilege[" + data.data[i][0] + "]").checked = true;
                      };
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      alert('Error get data from ajax');
                    }
                });
              }

              function update_privileges()
              {
                var user_id = $('#id2').val();
                update_user_group(user_id);
                var privilege_id = "";
                for (var i = 0; i < $('#table_privileges tr').length-1; i++) {
                  privilege_id = document.getElementById("privilege_hidden[" + i + "]").value;
                  if (document.getElementsByName("privilege_checkbox[]")[i].checked) {
                    $.ajax({
                      url : "<?php echo site_url('users/ajax_update_privileges')?>",
                      type: "POST",
                      dataType: "JSON",
                      data: { user_id : user_id, privilege_id : privilege_id },
                      success: function(data)
                      { 
                        $('#alert').fadeIn();
                        setTimeout(function(){ $('#alert').fadeOut(); }, 2000);
                        console.log(data.data[0]);
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        alert('error');
                      }
                    });
                    //console.log(i + ": Checked!");
                    console.log(privilege_id + ": Checked!");
                  } else {
                    $.ajax({
                      url : "<?php echo site_url('users/ajax_delete_privileges')?>",
                      type: "POST",
                      dataType: "JSON",
                      data: { user_id : user_id, privilege_id : privilege_id },
                      success: function(data)
                      { 
                        $('#alert').fadeIn();
                        setTimeout(function(){ $('#alert').fadeOut(); }, 2000);
                        console.log(data.data[0]);
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        alert('error');
                      }
                    });
                    //console.log(i + ": Unchecked!");
                    console.log(privilege_id + ": Unchecked!");
                  };
                };
                //alert($('#table_privileges tr').length-1);
              }

              function dropdown_list_group()
              {
                $('[name="group_id"]').empty();
                $('[name="group_id2"]').empty();
                var a = "";
                var b = "";
                $.ajax({
                  url : "<?php echo site_url('users/ajax_dropdown_list_group')?>",
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {           
                      $('[name="group_id"]').append('<option> </option>');
                      $('[name="group_id2"]').append('<option> </option>');
                      for (var x = 0; x < data.data.length; x++) {
                        a = data.data[x][0];
                        b = data.data[x][1];
                        if (a != "1") {
                          $('[name="group_id"]').append('<option value="' + a + '">' + b + '</option>');
                          $('[name="group_id2"]').append('<option value="' + a + '">' + b + '</option>');
                        }
                      };
                      $('[name="group_id"]').trigger('chosen:updated');
                      $('[name="group_id2"]').trigger('chosen:updated');
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data for dropdown list!');
                  }
                });
              }

              function dropdown_list_employees()
              {
                $('[name="emp_id"]').empty();
                var a = "";
                var b = "";
                $.ajax({
                  url : "<?php echo site_url('users/ajax_dropdown_list_employees')?>",
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {           
                      $('[name="emp_id"]').append('<option> </option>');
                      for (var x = 0; x < data.data.length; x++) {
                        a = data.data[x][0];
                        b = data.data[x][1];
                        if (a != "1") {
                          $('[name="emp_id"]').append('<option value="' + a + '">' + b + '</option>');
                        }
                      };
                      $('[name="emp_id"]').trigger('chosen:updated');
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data for dropdown list!');
                  }
                });
              }

              function get_group_privileges_by_id()
              {
                for (var i = 0; i < document.getElementsByName("privilege_checkbox[]").length; i++) {
                  //console.log("group_privilege[" + data.data[i][0] + "]");
                  document.getElementsByName("privilege_checkbox[]")[i].checked = false;
                };
                $.ajax({
                    url : "<?php echo site_url('user_groups/ajax_get_privileges_by_id')?>/" + $('[name="group_id"]').val(),
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    { 
                      for (var i = 0; i < data.data.length; i++) {
                        //console.log("group_privilege[" + data.data[i][0] + "]");
                        document.getElementById("user_privilege[" + data.data[i][0] + "]").checked = true;
                      };
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      alert('Error get data from ajax');
                    }
                });
              }

              function update_user_group(id)
              {
                if (id != "1") {
                  $.ajax({
                    url : "<?php echo site_url('users/ajax_update_user_group')?>",
                    type: "POST",
                    data: $('#form_users').serialize(),
                    dataType: "JSON",
                    success: function(data)
                    {
                        list();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in adding or updating data!');
                    }
                  });
                };
              }

              function suspend_user(id)
              {
                BootstrapDialog.show({
                  title: 'Suspend User Account',
                  type: 'type-warning',
                  message: 'Do you want to suspend this user?',
                  buttons: [{
                    label: '  Yes',
                    icon: 'glyphicon glyphicon-ok',
                    cssClass: 'btn-primary',
                    action: function(dialog) {
                      $.ajax({
                        url : "<?php echo site_url('users/ajax_suspend_user')?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                            dialog.close();
                            list();
                            list_suspended();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error in adding or updating data!');
                        }
                      });
                    }
                  }, {
                    label: '  No',
                    icon: 'glyphicon glyphicon-remove',
                    cssClass: 'btn-danger',
                    action: function(dialog) {
                      dialog.close(); 
                    }
                  }]
                });
              }

              function unsuspend_user(id)
              {
                BootstrapDialog.show({
                  title: 'Unsuspend User Account',
                  type: 'type-warning',
                  message: 'Do you want to unsuspend this user?',
                  buttons: [{
                    label: '  Yes',
                    icon: 'glyphicon glyphicon-ok',
                    cssClass: 'btn-primary',
                    action: function(dialog) {
                      $.ajax({
                        url : "<?php echo site_url('users/ajax_unsuspend_user')?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                            dialog.close();
                            list();
                            list_suspended();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error in adding or updating data!');
                        }
                      });
                    }
                  }, {
                    label: '  No',
                    icon: 'glyphicon glyphicon-remove',
                    cssClass: 'btn-danger',
                    action: function(dialog) {
                      dialog.close(); 
                    }
                  }]
                });
              }

              function get_group_privileges(group_id, user_id)
              {
                $.ajax({
                    url : "<?php echo site_url('user_groups/ajax_get_privileges_by_id')?>/" + group_id,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    { 
                      for (var i = 0; i < data.data.length; i++) {
                        $.ajax({
                          url : "<?php echo site_url('users/ajax_update_privileges')?>",
                          type: "POST",
                          dataType: "JSON",
                          data: { user_id : user_id, privilege_id : data.data[i][0] },
                          success: function(data2)
                          { 
                            console.log(data2.data[0]);
                          },
                          error: function (jqXHR, textStatus, errorThrown)
                          {
                            alert('error');
                          }
                        });
                      };
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      alert('Error get data from ajax');
                    }
                });
              }

            </script>

            <!-- MODAL FORM -->
            <div class="modal fade" id="modal_form" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">User Account Form</h3>
                  </div>
                  <div class="modal-body form">
                    <form action="#" id="form" class="form-horizontal">
                      <input type="hidden" value="" name="id"/> 
                      <div class="form-body">
                        <div class="form-group">
                          <label class="control-label col-md-3">Name</label>
                          <div class="col-md-9">
                            <select name="emp_id" data-placeholder="Select..." class="form-control"></select>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Email</label>
                          <div class="col-md-9">
                            <input name="email" placeholder="" class="form-control" type="text">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Username</label>
                          <div class="col-md-9">
                            <input name="username" placeholder="" class="form-control" type="text">
                          </div>
                        </div>
                        <div id="pw" class="form-group">
                          <label class="control-label col-md-3">Password</label>
                          <div class="col-md-9">
                            <input name="password" placeholder="" class="form-control" type="password">
                          </div>
                        </div>
                        <div id="user_grp" class="form-group">
                          <label class="control-label col-md-3">User Group</label>
                          <div class="col-md-9">
                            <select name="group_id2" data-placeholder="Select..." class="form-control"></select>
                          </div>
                        </div>
                        <div id="alert2" class="form-group">
                          <div class="col-md-8 pull-right">
                            <div class="alert alert-danger alert-dismissable">
                              <h4>  <i class="icon fa fa-remove"></i> This user already have an account.</h4>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- PRIVILAGES MODAL FORM -->
            <div class="modal fade" id="modal_form_users" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Manage Privileges Form</h3>
                  </div>
                  <div class="modal-body form">
                    <form action="#" id="form_users" class="form-horizontal">
                      <input type="hidden" value="" name="id2" id="id2"/> 
                      <div class="form-body">
                        <div class="form-group">
                          <label class="control-label col-md-2">Name</label>
                          <div class="col-md-10">
                            <input name="name2" placeholder="" class="form-control" type="text" disabled>
                          </div>
                        </div>
                        <div id="usergrp" class="form-group">
                          <label class="control-label col-md-2">User Group</label>
                          <div class="col-md-10">
                            <select name="group_id" data-placeholder="Select..." class="form-control" onchange="get_group_privileges_by_id()"></select>
                          </div>
                          <!-- <div class="col-md-2">
                            <a class="btn btn-sm btn-success" title="Add to Group" onclick="get_group_privileges_by_id()"><i class="glyphicon glyphicon-ok"></i></a>
                          </div> -->
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 table-responsive">
                            <h5><b>Privileges</b></h5>
                            <table id="table_privileges" class="table table-striped table-bordered table-hover">
                              <thead>
                                <tr>
                                  <!-- <th style="width:30%">Group Name</th> -->
                                  <th style="width:60%">Description</th>
                                  <th style="width:10%">Action</th>
                                </tr>
                              </thead>
                              <tbody id="list">
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div id="alert" class="form-group">
                          <div class="col-md-4 pull-right">
                            <div class="alert alert-success alert-dismissable">
                              <h4>  <i class="icon fa fa-check"></i> Privileges Updated!</h4>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" id="btnUpdatePrivileges" onclick="update_privileges()" class="btn btn-primary">Update</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!--- End of custom details-->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->