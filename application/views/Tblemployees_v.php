<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">                        
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Employees</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                      
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                      
                      <br> 

                      <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Employee Listing</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="col-md-6">
                                                    <div class="btn-group">
                                                           <button class="btn btn-success" onclick="add_Tblemployees()"><i class="glyphicon glyphicon-plus"></i> Add Employee</button>
                                                    </div>
                                                </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                   <th class="all">Last name</th>
                                                   <th class="all">First name</th>
                                                   <th class="all">Middle name</th>
                                                   <th class="all" > Suffix</th>
                                                   <th class="all"> Email</th>
                                                   <th class="all" > Position</th>
                                                   <th class="all">Is Active</th>
                                                   <th class="all">Action</th
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

                     
                    </div>
                    <!-- END CONTENT BODY -->
                </div>


<script type="text/javascript">
    
                var save_method; //for save method string
                var table;
                $(document).ready(function() {


                  list();
                });

                function list()
                {
                   table =  $('#sample_1').DataTable( {
                      
                      "ajax": "<?php echo site_url('Tblemployees/ajax_list')?>/",
                      "columns": [
                          { "data": "lname" },
                          { "data": "fname" },
                          { "data": "mname" },
                          { "data": "suffix" },
                          { "data": "email" },
                          { "data": "position" },
                          { "data": "status" },
                          { "data": "action" }
                      ],
        "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
                  } );
                }       


             
              function add_Tblemployees()
              {
                save_method = 'add';
                $('#form_Tblemployees')[0].reset(); // reset form on modals
                $('#modal_form_Tblemployees').modal('show'); // show bootstrap modal
                $('.modal-title').text('Add Tblemployees'); // Set Title to Bootstrap modal title
              }
              
               function edit_Tblemployees(id)
              {
                  save_method = 'update';
                  $('#form_Tblemployees')[0].reset()
                  $.ajax({
                    url : "<?php echo site_url('Tblemployees/ajax_edit/')?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    {         
                        $('[name="id"]').val(data.id);
                        $('[name="lastname"]').val(data.lastname);
                        $('[name="firstname"]').val(data.firstname);
                        $('[name="middlename"]').val(data.middlename);
                        $('[name="code"]').val(data.code);
                        $('[name="suffix"]').val(data.suffix);
                        $('[name="email"]').val(data.email);
                        $('[name="position"]').val(data.position);
                        $('[name="is_active"]').val(data.is_active);
                       $('#modal_form_Tblemployees').modal('show'); // show bootstrap modal when complete loaded
                       $('.modal-title').text('Edit Tblemployees'); // Set title to Bootstrap modal title
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                  });
              }

              function save_Tblemployees()
              {
                var url;
                if(save_method == 'add') 
                {
                    url = "<?php echo site_url('Tblemployees/ajax_add')?>";
                }
                else
                {
                  url = "<?php echo site_url('Tblemployees/ajax_update')?>";
                }

                    $.ajax({
                      url : url,
                      type: "POST",
                      data: $('#form_Tblemployees').serialize(),
                      dataType: "JSON",
                      success: function(data)
                      {
                         //if success close modal and reload ajax table
                         $('#modal_form_Tblemployees').modal('hide');
                          table.ajax.reload( null, false );
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
              }

       function delete_Tblemployees(id)
              {     
                    swal({
                      title: "Are you sure you want to delete?",
                      //text: "Your will not be able to recover this imaginary file!",
                      type: "error",
                      showCancelButton: true,
                      confirmButtonClass: "btn-danger",
                      confirmButtonText: "Delete",
                      closeOnConfirm: false
                    },
                    function(){
                      $.ajax({
                                    url: "<?php echo site_url('Tblemployees/ajax_delete')?>/" + id,
                                    type: "POST",
                                    dataType: "JSON",
                                    success: function(data)
                                    {
                                        //if success reload ajax table                    
                                        table.ajax.reload( null, false );
                                    },
                                    error: function(jqXHR, textStatus, errorThrown)
                                    {
                                        alert('Error in deleting data!');
                                    }
                                });

                          swal("Deleted!", "Employee has been deleted.", "success");
                    });

                  }

              function view_Tblemployees(id)
              {
                  save_method = 'update';

                  //Ajax Load data from ajax
                  $.ajax({
                    url : "<?php echo site_url('Tblemployees/ajax_edit/')?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    {         
                        window.location.replace("<?php echo site_url('Tblemployeesdt?id=')?>"+data.id);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                  });
              }   
              

                function printlist_Tblemployees()
                {
                  var query = $('#sql-parsed').val();
                  if (query == "") 
                  {
                    window.open("<?php echo site_url('printlist_controller?id=')?>" + "all");
                  }
                  else
                  {
                    window.open("<?php echo site_url('printlist_controller?')?>" + encodeURIComponent(query));
                  }
                }

 

        

                
</script>

<div class="modal fade" id="modal_form_Tblemployees" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Employee</h3>
         </div>
         <div class="modal-body form">
            <form action="#" id="form_Tblemployees" class="form-horizontal">
               <input type="hidden" value="" name="id"/> 
               <div class="form-group">
                  <label class="control-label col-md-3">Last name *</label>
                  <div class="col-md-6">
                     <input name="lastname" placeholder="Enter Lastname" class="form-control" type="text" required>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-md-3">First name *</label>
                  <div class="col-md-6">
                     <input name="firstname" placeholder="Enter Firstname" class="form-control" type="text" required>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-md-3">Middle name *</label>
                  <div class="col-md-6">
                     <input name="middlename" placeholder="Enter Middlename" class="form-control" type="text" required>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-md-3">Suffix</label>
                  <div class="col-md-6">
                     <input name="suffix" placeholder="Enter Suffix" class="form-control" type="text" >
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-md-3">Email</label>
                  <div class="col-md-6">
                     <input name="email" placeholder="Enter Email" class="form-control" type="text" >
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-md-3">Position</label>
                  <div class="col-md-6">
                     <input name="position" placeholder="Enter Position" class="form-control" type="text" >
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-md-3">Employee Code</label>
                  <div class="col-md-6">
                     <input name="code" placeholder="Enter Code" class="form-control" type="text" >
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label col-md-3">Is Active</label>
                  <div class="col-md-6">
                     <input name="is_active" placeholder="Enter Is Active" class="form-control" type="text" >
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save_Tblemployees()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>