
<div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div> -->
                        <!--START CONTENT HERE -JC -->
                            <div class="portlet light bordered" id="form_wizard_1">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-red"></i>
                                            <span class="caption-subject font-red bold uppercase"> E-Loan Assistant -
                                                <span class="step-title"> Step 1 of 4 </span>
                                            </span>
                                        </div>
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form class="form-horizontal" action="#" id="submit_form" method="POST">
                                            <div class="form-wizard">
                                                <div class="form-body">
                                                    <ul class="nav nav-pills nav-justified steps">
                                                        <li>
                                                            <a href="#tab1" data-toggle="tab" class="step">
                                                                <span class="number"> 1 </span>
                                                                <span class="desc">
                                                                    <i class="fa fa-check"></i> Enter ID Number </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab2" data-toggle="tab" class="step">
                                                                <span class="number"> 2 </span>
                                                                <span class="desc">
                                                                    <i class="fa fa-check"></i> Account Details </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab3" data-toggle="tab" class="step active">
                                                                <span class="number"> 3 </span>
                                                                <span class="desc">
                                                                    <i class="fa fa-check"></i> Enter Loan Amount </span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab4" data-toggle="tab" class="step">
                                                                <span class="number"> 4 </span>
                                                                <span class="desc">
                                                                    <i class="fa fa-check"></i> Confirm </span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                                        <div class="progress-bar progress-bar-success"> </div>
                                                    </div>
                                                    <div class="tab-content">
                                                        <div class="alert alert-danger display-none">
                                                            <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. </div>
                                                        <div class="alert alert-success display-none">
                                                            <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                                                        <div class="tab-pane active" id="tab1">
                                                            <h3 class="block">Enter your Employee ID Number</h3>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">ID Number
                                                                    <span class="required"> * </span>
                                                                </label>
                                                                <div class="col-md-2">
                                                                    <input type="hidden" id="blocker" name="blocker">
                                                                    <!-- <input type="text" class="form-control" name="userid" id="userid" /> -->
                                                                    <div class="input-group">
                                                                      <input type="text" class="form-control" name="userid"  id="userid" placeholder="00000">
                                                                      <span class="input-group-btn">
                                                                        <button class="btn btn-primary" type="button" onclick="getId()">Search</button>
                                                                      </span>
                                                                    </div><!-- /input-group -->
                                                                   <!--  <span class="help-block"> Provide your ID </span> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab2">
                                                          <div class="row">
                                                            <div class="col-md-3 col-md-offset-2">
                                                               <label class="control-label col-md-1">&nbsp;</label>
                                                               <div class="col-md-12 req">
                                                                  <img class="img-responsive img-rounded" id="emp_picture" alt="avatar" style="margin: 0 auto;width:350px;height:350px;">
                                                               </div>
                                                            </div>
                                                            <div class="col-md-6">
                          <!--                                      <div class="form-group">
                                                                <label class="control-label col-md-3">ID:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" id="data_id"> </p>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Name:</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static" id="data_name"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Address:</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static" id="data_address"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Birthday:</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static" id="data_bday"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Branch:</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static" id="data_branch"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Contact No:</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static" id="data_cn"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Date of Employment:</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static" id="data_doe"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Lenght of Service:</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static" id="data_los"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Gender:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" id="data_gender"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Classification:</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static" id="data_classification"> </p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Purpose:</label>
                                                                <div class="col-md-8">
                                                                  <textarea class="form-control" id="purpose" name="purpose"></textarea>
                                                                </div>
                                                            </div>                                                            
                                                            </div>
                                                <!--             <div class="col-md-4"> -->
                                <!--                                <div class="form-group">
                                                                <label class="control-label col-md-3">Outstanding Balance:</label>
                                                                <div class="col-md-8">
                                                                    <p class="form-control-static" id="data_osb"> </p>
                                                                </div>
                                                            </div> -->

                            <!--                                 </div>  -->
                                                         </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab3">
                                <!--                             <h3 class="block" id="displayInfo"></h3>
                                                            <h4 class="form-section">Loan Amount</h4> -->
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Enter Amount:</label>
                                                                <div class="col-md-3">
                                                                    <div class="input-group">
                                                                        <input type="text" name="desired_amt" id="desired_amt" class="form-control">
                                                                      <span class="input-group-btn">
                                                                        <button class="btn btn-success" type="button" onclick="calculate()">Calculate</button>
                                                                      </span>
                                                                    </div>                                                                    
                                                                </div>
                                                                <!-- <label class="control-label col-md-1">Prescribe Amount:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" name=prescribe_amt"" id="prescribe_amt" class="form-control" readonly>
                                                                </div> -->
                                                                <label class="control-label col-md-1">Terms:</label>
                                                                <div class="col-md-2">
                                                                    <!-- <input type="text" name="terms" id="terms" class="form-control"> -->
                                                                    <select class="form-control" style="width:100%" id="terms" name="terms">
                                                                    <option value="12">12 months</option>
                                                                    <!-- <option value="18">18 months</option>
 -->                                                                    <option value="24">24 months</option>
                                                                  <!--   <option value="36">36 months</option> -->
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <h4>Loan Summary Computation</h4>
                                                            <hr>
                                                          
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-2">Principal:</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="d_principal" class="form-control" readonly>
                                                                    </div>
                                                                    <label class="control-label col-md-2">Terms:</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="d_terms" class="form-control" readonly>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label col-md-2">Interest:</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="d_interest" class="form-control" readonly>
                                                                    </div>
                                                                    <label class="control-label col-md-2">Total Amount:</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="d_total_amount" class="form-control" readonly>
                                                                    </div>
                                                                    <label class="control-label col-md-2">Payable Per Cut-off:</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="d_cutoffpayment" class="form-control" readonly>
                                                                    </div>
                                                                </div>

                                                            <h4>Prescribe</h4>
                                                            <hr>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Principal:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="p_principal" class="form-control" readonly>
                                                                </div>
                                                                <label class="control-label col-md-2">Terms:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="p_terms" class="form-control" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Interest:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="p_interest" class="form-control" readonly>
                                                                </div>
                                                                <label class="control-label col-md-2">Total Amount:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="p_total_amount" class="form-control" readonly>
                                                                </div>
                                                                <label class="control-label col-md-2">Payable Per Cut-off:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="p_cutoffpayment" class="form-control" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Co-Maker1:</label>
                                                                <div class="col-md-3">
                                                                    <select class="form-control" style="width:100%" id="comaker1" name="comaker1">
                                                                    </select>
                                                                </div>
                                                                <label class="control-label col-md-2">Co-Maker2:</label>
                                                                <div class="col-md-3">
                                                                    <select class="form-control" style="width:100%" id="comaker2" name="comaker2">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                             <h4>Payment type</h4>
                                                            <hr>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Choose Payment Type :</label>
                                                                <div class="col-md-34">
                                                                   <div class="radio-list">
                                                                        <label>
                                                                            <input type="radio"  name="payment"  value="Check"  data-title="Male"/> Check </label>
                                                                        <label>
                                                                            <input type="radio" name="payment"  value="ATM" data-title="ATM"/> ATM </label>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab4">
                                                            <h4 class="block" id="displayInfo"></h4>
                       <!--                                      <h4 class="block">Loan summary and confirmation</h4> -->
                                                            
                                                            <div class="form-group">
<!--                                                                 <label class="control-label col-md-3">Name:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" id="step4_name"> </p>
                                                                </div>
                                                            </div> -->
<!--                                                             <div class="form-group"> 
                                                                <label class="control-label col-md-3">Amount Applied:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="desired_amt"> </p>
                                                                </div>
                                                            </div> -->
<!--                                                             <div class="form-group">
                                                                <label class="control-label col-md-3">Terms:</label>
                                                                <div class="col-md-4">
                                                                    <p class="form-control-static" data-display="terms"> </p>
                                                                </div>
                                                            </div> -->
                                                            <!-- <div class="form-group">
                                                                <label class="control-label col-md-3">Choose Payment Type :</label>
                                                                <div class="col-md-4">
                                                                   <div class="radio-list">
                                                                        <label>
                                                                            <input type="radio" name="payment"  value="Check"  id="payment_opt" /> Check </label>
                                                                        <label>
                                                                            <input type="radio"   name="payment"  value="ATM" id="payment_opt"/> ATM </label>
                                                                    </div>
                                                                </div> 
                                                            </div> -->
                                                            <h4>Loan Summary Computation</h4>
                                                            <hr>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Principal:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="d_principal2" class="form-control" readonly>
                                                                </div>
                                                                <label class="control-label col-md-2">Terms:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="d_terms2" class="form-control" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Interest:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="d_interest2" class="form-control" readonly>
                                                                </div>
                                                                <label class="control-label col-md-2">Total Amount:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="d_total_amount2" class="form-control" readonly>
                                                                </div>
                                                                <label class="control-label col-md-2">Payable Per Cut-off:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="d_cutoffpayment2" class="form-control" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <a href="javascript:;" class="btn default button-previous">
                                                                <i class="fa fa-angle-left"></i> Back </a>
                                                            <a href="javascript:;"  class="btn btn-outline green button-next"> Continue
                                                                <i class="fa fa-angle-right"></i>
                                                            </a>
                                                            <a href="javascript:;" class="btn green button-submit"> Submit
                                                                <i class="fa fa-check"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                        <!--END CONTENT-->
                        
                    </div>
                </div>

<script type="text/javascript">
 var form = $('#submit_form');
 var year_of_service = 0;
 var emp_class = 0;
 var raw_creditor_id = 0;
 var raw_product_id = 0;
 var raw_applied_amt = 0;
 var raw_suggested_amt = 0;
 var raw_terms = 0;
 var raw_purpose = "";
 var raw_payment_opt = "";

 var a_interest = 0;
 var a_total = 0;
 var a_cutoff = 0;

 var p_interest = 0;
 var p_total = 0;
 var p_cutoff = 0;

 var emp_company;
  $(document).ready(function() {
     $("#comaker1").select2();
     $("#comaker2").select2();
  });

  function getId() {
      $.ajax({
               url: "Creditors/ajax_edit_creditors/"+$("#userid").val(),
               type: "POST",
               dataType: "JSON",
               success: function(data)
               {  

                if(data!=null){
                    $("#blocker").val($("#userid").val());
                    toastr.success('Your ID is Exisiting!','Message:');
                    $("#data_name").text(data.fullname);
                    $("#fullname").val(data.fullname);
                    $("#data_id").text(data.id);
                    $("#data_bday").text(data.birthdate);
                    $("#data_gender").text(data.gender);
                    $("#step4_name").text(data.fullname);
                    $("#displayInfo").text(data.fullname);
                    if(data.picture==null||data.picture=="")
                    {
                        $("#emp_picture").attr("src","<?php echo base_url(); ?>uploads/files/defaultAvatar.png");
                    }else{
                        $("#emp_picture").attr("src","<?php echo base_url(); ?>uploads/files/"+data.picture);
                    }
                    $("#data_address").text(data.Address);
                    raw_creditor_id = data.employee_code;
                    var year = data.employed_date;//data.lengthofservice;//emp_years;//data.employed_date;
                    year_of_service = year;//year.split("-");
                    emp_class = data.employee_class_id;
                    comakers(data.id);
                    emp_company = data.emp_company;
                     /*if(data.lengthofservice<5){
                        toastr.error('Your Length of Service is Sufficient!','Message:');
                        $("#blocker").val('');
                        $("#userid").val('');
                    }*/

                    $("#data_branch").text(data.emp_company);
                    $("#data_cn").text(data.telno);
                    $("#data_doe").text(data.employed_date);
                    $("#data_los").text(data.lengthofservice);
                    $("#data_classification").text(data.emp_classification);





                }else{
                    toastr.error('Your ID is Not Exisiting!','Message:');
                    $("#blocker").val('');
                    $("#userid").val('');
                }
               },
               error: function(jqXHR, textStatus, errorThrown) {
                   //alert('Error adding / update data');
                   console.log('Server Side Error!');
               }
           });
  }

  function calculate()
  {
      var amount = $("#desired_amt").val();
      var prescribe = $("#prescribe_amt").val();
      var terms = $("#terms").val();
      var interest = 0;
      raw_product_id = 1;//default salary loan $("#product").val();
      raw_applied_amt =amount;
      raw_terms = terms;
      raw_purpose = $("#purpose").val();
      interest = parseFloat(amount) * parseFloat(terms/100);
      total = parseFloat(interest) + parseFloat(amount);
      payable_cutoff = parseFloat(total) / (parseFloat(terms*2));

      a_cutoff = payable_cutoff;
      a_total = total;
      a_interest = interest;

      $("#d_interest").val(numberWithCommas(interest.toFixed(2)));
      $("#d_principal").val(numberWithCommas(parseFloat(amount)));
      $("#d_terms").val(terms);
      $("#d_total_amount").val(numberWithCommas(total.toFixed(2)));
      $("#d_cutoffpayment").val(numberWithCommas(payable_cutoff.toFixed(2)));

      $("#d_interest2").val(numberWithCommas(interest.toFixed(2)));                                                                                                                                                                                                        
      $("#d_principal2").val(numberWithCommas(parseFloat(amount)));
      $("#d_terms2").val(terms);
      $("#d_total_amount2").val(numberWithCommas(total.toFixed(2)));
      $("#d_cutoffpayment2").val(numberWithCommas(payable_cutoff.toFixed(2)));

            $.ajax({
               url: "Creditors/ajax_get_bracket/"+emp_class,
               type: "POST",
               dataType: "JSON",
               success: function(data)
               {  
                            if(getAge(year_of_service)>=data[0].from_years&&getAge(year_of_service)<=data[0].to_years)
                            {
                                $("#prescribe_amt").val(numberWithCommas(data[0].amount));
                                p_interest = parseFloat(data[0].amount) * parseFloat(terms/100);
                                p_total = parseFloat(p_interest) + parseFloat(data[0].amount);
                                p_payable_cutoff = parseFloat(p_total) / (parseFloat(terms*2));
                                $("#p_principal").val(numberWithCommas(data[0].amount));
                                $("#p_terms").val(terms);
                                $("#p_interest").val(numberWithCommas(p_interest.toFixed(2)));
                                $("#p_total_amount").val(numberWithCommas(p_total.toFixed(2)));
                                $("#p_cutoffpayment").val(numberWithCommas(p_payable_cutoff.toFixed(2)));
                                raw_suggested_amt = data[0].amount;
                                p_cutoff = p_payable_cutoff;
                            }
                            else if(getAge(year_of_service)>=data[1].from_years&&getAge(year_of_service)<=data[1].to_years)
                            {
                                $("#prescribe_amt").val(numberWithCommas(data[1].amount));
                                p_interest = parseFloat(data[1].amount) * parseFloat(terms/100);
                                p_total = parseFloat(p_interest) + parseFloat(data[1].amount);
                                p_payable_cutoff = parseFloat(p_total) / (parseFloat(terms*2));
                                $("#p_principal").val(numberWithCommas(data[1].amount));
                                $("#p_terms").val(terms);
                                $("#p_interest").val(numberWithCommas(p_interest.toFixed(2)));
                                $("#p_total_amount").val(numberWithCommas(p_total.toFixed(2)));
                                $("#p_cutoffpayment").val(numberWithCommas(p_payable_cutoff.toFixed(2)));
                                raw_suggested_amt = data[1].amount;
                                p_cutoff = p_payable_cutoff;
                            }
                            else if(getAge(year_of_service)>=data[2].to_years)
                            {
                                $("#prescribe_amt").val(numberWithCommas(data[2].amount));
                                p_interest = parseFloat(data[2].amount) * parseFloat(terms/100);
                                p_total = parseFloat(p_interest) + parseFloat(data[2].amount);
                                p_payable_cutoff = parseFloat(p_total) / (parseFloat(terms*2));
                                $("#p_principal").val(numberWithCommas(data[2].amount));
                                $("#p_terms").val(terms);
                                $("#p_interest").val(numberWithCommas(p_interest.toFixed(2)));
                                $("#p_total_amount").val(numberWithCommas(p_total.toFixed(2)));
                                $("#p_cutoffpayment").val(numberWithCommas(p_payable_cutoff.toFixed(2)));
                                raw_suggested_amt = data[2].amount;
                                p_cutoff = p_payable_cutoff;
                            }
               },
               error: function(jqXHR, textStatus, errorThrown) {

                   console.log('Server Side Error!');
               }
           });    
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function comakers(id)
  {
    var creditor = [];
      $.ajax({
               url: "Creditors/get_comakers/"+id,
               type: "POST",
               dataType: "JSON",
               success: function(data)
               {  
                     $("#comaker1,#comaker2").empty();
                     $('#comaker2').append($("<option selected disabled>Select Comaker 2</option>"));
                     $('#comaker1').append($("<option selected disabled>Select Comaker 1</option>"));
                     $.each(data, function(key, value) {
                        if(value.emp_company==emp_company){      
                            if(getAge(value.employed_date)>=5)
                            {
                                creditor.push({text:value.fullname,id:value.employee_code});
                            }                      
                        }
                         /*var data = [{
                                 id: value.id,
                                 text: value.fullname
                             }
                         ];
                         $("#comaker1,#comaker2").select2({
                             data: data
                         });*/
                     });
                     $("#comaker1,#comaker2").select2({
                             data: creditor
                      });
               },
               error: function(jqXHR, textStatus, errorThrown) {

                   console.log('Server Side Error!');
               }
           });       
  }

  function check_comakers()
  {
    /*if($("#comaker1").val()==$("#comaker2").val())
    {    
          toastr.error('COMAKERS SHOULD NOT BE THE SAME!','Message:');
          $("#comaker1,#comaker2").select2().val("").trigger("change");
    }*/
  }
 
  function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
  }
</script>


