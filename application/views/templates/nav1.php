     <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="<?php echo site_url(); ?>Dashboard">
                            <img src="<?php echo base_url();?>assets/layouts/layout/img/logo.png" alt="logo" class="logo-default" />  </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="<?php echo base_url();?>uploads/files/<?php echo ($picture!=null?$picture:'defaultavatar.png'); ?>" />
                                    <span class="username username-hide-on-mobile" id="display_name"> <?php echo $username; ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="<?php echo site_url(); ?>Creditors/creditors_creditors_add?id=<?php echo $emp_code; ?>">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <!-- <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li> -->
                                    <li>
                                        <a href="<?php echo site_url(); ?>Dashboard/logout">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                            <!-- <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li> -->
                            
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                                <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <!-- <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div> -->
                                </form>
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                            <li class="nav-item start open">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-screen-desktop"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start active open">
                                       <a href="<?php echo site_url(); ?>Dashboard/manager" class="nav-link ">
                                            <i class="fa fa-tachometer"></i>
                                            <span class="title">Manager</span>
                                            <span class="selected"></span>
                                        </a>                                                                                                                     
                                    </li>
                                </ul>
                            </li>
     <!-- List -->                       
                          <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-list"></i>
                                    <span class="title">Lists</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">

                                <?php
                                    for ($i=0; $i<sizeof($privileges); $i++) { 
                                      if ($privileges[$i]->privilege_name == "creditors_view_creditors") 
                                      {
                                ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url(); ?>Creditors/creditors_creditors" class="nav-link">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Creditors</span>
                                        </a>
                                    </li>
                                <?php }} ?>

                                <?php
                                    for ($i=0; $i<sizeof($privileges); $i++) { 
                                      if ($privileges[$i]->privilege_name == "creditors_view_bracket") 
                                      {
                                ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url(); ?>Creditors/creditors_bracket" class="nav-link">
                                            <i class="fa fa-money"></i>
                                            <span class="title">Loan Brackets</span>
                                        </a>
                                    </li>
                                <?php }} ?>

                                <?php
                                    for ($i=0; $i<sizeof($privileges); $i++) { 
                                      if ($privileges[$i]->privilege_name == "creditors_view_class") 
                                      {
                                ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url(); ?>Creditors/creditors_class" class="nav-link">
                                            <i class="fa fa-reorder"></i>
                                            <span class="title">Classification</span>
                                        </a>
                                    </li>
                                <?php }} ?>                                                                       

                                  <li class="nav-item">
                                        <a href="<?php echo site_url(); ?>Domain" class="nav-link ">
                                           <i class="fa fa-university"></i>
                                            <span class="title">Company/Branch</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>  

                                </ul>
                            </li>                              
  <!-- end of List -->
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-tasks"></i>
                                    <span class="title">Tasks</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start active open">
                                        <a href="<?php echo site_url(); ?>ElectronicForm/LoanForm" class="nav-link ">
                                           <i class="fa fa-file-text-o"></i>
                                            <span class="title">New Loan Application</span>
                                        </a>
                                    </li>  
                                     <li class="nav-item  ">
                                        <a href="<?php echo site_url(); ?>Loans/Loan_app" class="nav-link ">
                                            <i class="icon-pencil"></i>
                                            <span class="title">Loan Validation</span>
                                        </a>
                                    </li>
                                     <li class="nav-item  ">
                                        <a href="<?php echo site_url(); ?>Loans/Loan_validation?status=checking" class="nav-link ">
                                            <i class="icon-check"></i>
                                            <span class="title">Loan Recommendation</span>
                                        </a> 
                                    </li>
                                     <li class="nav-item  ">
                                        <a href="<?php echo site_url(); ?>Loans/Loan_validation?status=approval" class="nav-link ">
                                            <i class="fa fa-thumbs-up"></i>
                                            <span class="title">Loan Approval</span>
                                        </a>
                                    </li>                                       
                                    <li class="nav-item  ">
                                        <a href="<?php echo site_url(); ?>Loans/Loan_disbursement" class="nav-link ">
                                            <i class="fa fa-pencil-square-o"></i>
                                            <span class="title">Disbursement</span>
                                        </a>
                                    </li>  
                                    <li class="nav-item  ">
                                        <a href="<?php echo site_url(); ?>Loans/Loan_approved" class="nav-link ">
                                             <i class="fa fa-pencil-square-o"></i>
                                            <span class="title">List of Released Loans</span>
                                        </a>
                                    </li> 
                                    <li class="nav-item  ">
                                        <a href="<?php echo site_url(); ?>Loans/Loan_transaction_monitor" class="nav-link ">
                                             <i class="fa fa-pencil-square-o"></i>
                                            <span class="title">Loan Monitoring</span>
                                        </a>
                                    </li>
                                     <li class="nav-item  ">
                                        <a href="" class="nav-link ">
                                            <i class="fa fa-minus-circle"></i>
                                            <span class="title">Deduction Adjustment</span>
                                        </a>
                                    </li>       
                                     <li class="nav-item ">
                                        <a href="<?php echo site_url(); ?>Manual_Payment" class="nav-link ">
                                            <i class="fa fa-money"></i>
                                            <span class="title">Payment Entry</span>
                                        </a>
                                    </li>
                                    </li>   
                                </ul>
                         </li>
                          <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-book-open"></i>
                                    <span class="title">Reports</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <!-- <li class="nav-item start active open">
                                        <a href="Dashboard" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Payment Summary</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>   -->
                                    <li class="nav-item start active open">
                                        <a href="<?php echo site_url(); ?>Ledger" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Ledger</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li> 
                                    <li class="nav-item start active open">
                                        <a href="<?php echo site_url(); ?>Reports/deduction_upload" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Deduction</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>
                                    <li class="nav-item start active open">
                                        <a href="<?php echo site_url(); ?>Reports/beginning" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Beginning</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li>   
                                </ul>
                            </li>       
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-wrench"></i>
                                    <span class="title">Tools</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item start active open">
                                        <a href="<?php echo site_url(); ?>ElectronicForm" class="nav-link ">
                                            <i class="fa fa-magic"></i>
                                            <span class="title">Electronic Loan Assistant</span>
                                            <span class="selected"></span>
                                        </a>
                                    </li> 
                                    <li class="nav-item start active open">
                                        <a href="<?php echo site_url(); ?>Netpays" class="nav-link ">
                                            <i class="fa fa-upload"></i>
                                            <span class="title">Payment & Netpay Upload</span>
                                        </a>
                                    </li> 
                                    <li class="nav-item start active open">
                                        <a href="<?php echo site_url(); ?>Ledger/Uploads" class="nav-link ">
                                            <i class="fa fa-upload"></i>
                                            <span class="title">Beginning Balance Upload</span>
                                        </a>
                                    </li> 
                                </ul>
                            </li>                        
                           
                            <li class="heading">
                                <h3 class="uppercase">Admin Menu</h3>
                            </li>

                            <li class="nav-item  ">

                                <?php 
                                   if ($group_id == "1" || $group_id == "2") {                                       
                                ?>
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-settings"></i>
                                    <span class="title">Settings</span>
                                    <span class="arrow"></span>
                                </a>                                
                                <ul class="sub-menu">
                                <?php
                                    for ($i=0; $i<sizeof($privileges); $i++) { 
                                      if ($privileges[$i]->privilege_name == "Tblemployees_view") 
                                      {
                                ?>
                                    <li class="nav-item  ">
                                        <a href="<?php echo site_url(); ?>Tblemployees" class="nav-link ">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Employees</span>
                                        </a>
                                    </li>
                                <?php }} ?>

                                <?php
                                    for ($i=0; $i<sizeof($privileges); $i++) { 
                                  if ($privileges[$i]->privilege_name == "View Users") 
                                  {
                                ?>
                                    <li class="nav-item  ">
                                        <a href="<?php echo site_url(); ?>Users" class="nav-link ">
                                            <i class="fa fa-user"></i>
                                            <span class="title">Users</span>
                                        </a>
                                    </li>
                                <?php } } ?>

                                <?php
                                    for ($i=0; $i<sizeof($privileges); $i++) { 
                                        if ($privileges[$i]->privilege_name == "View Groups") 
                                        {
                                ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url(); ?>User_groups" class="nav-link ">
                                            <i class="fa fa-users"></i>
                                            <span class="title">Groups</span>
                                        </a>
                                    </li>
                                <?php }} ?>
                                
                                <?php
                                            if ($group_id == "1") 
                                            {
                                ?>
                                    <li class="nav-item  ">
                                        <a href="<?php echo site_url(); ?>User_privileges" class="nav-link ">
                                            <i class="fa fa-key"></i>
                                            <span class="title">Privileges</span>
                                        </a>
                                    </li>
                                      <?php } ?>
                                </ul>
                                <?php  }  ?>
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>

                
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
