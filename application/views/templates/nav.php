<div class="wrapper">
<header class="main-header">
   <a href="Dashboard" class="logo">
   <span>E-proc Ver. 1.0</span>
   </a>
   <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="container">
         <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
         </div>
         <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
               <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url();?>assets/images/defaultAvatar.png" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $username; ?></span>
                  </a>
                  <ul class="dropdown-menu">
                     <li class="user-header">
                        <img src="<?php echo base_url();?>assets/images/defaultAvatar.png" class="img-circle" alt="User Image">
                        <p>
                           <?php echo $username; ?>
                        </p>
                     </li>
                     <li class="user-footer">
                        <div class="pull-right">
                           <a href="Dashboard/logout" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                     </li>
                  </ul>
               </li>
               <li>
                  <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
               </li>
            </ul>
         </div>         
      </div>
   </nav>
</header>
<aside class="main-sidebar">
   <section class="sidebar">

      <ul class="sidebar-menu">
         <li class="header">MAIN Menu</li>
         <li class="treeview">
            <a href="#">
            <i class="fa fa-dashboard text-aqua"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
               <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Suppliers</a></li>
               <li><a href="index2.html"><i class="fa fa-circle-o"></i> Purchaser</a></li>
               <li><a href="index2.html"><i class="fa fa-circle-o"></i> Staff</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">  
            <i class="fa fa-th-list text-aqua"></i> <span>List</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
<!--              <?php 
              for ($i=0; $i<sizeof($privileges); $i++) { 
                if ($privileges[$i]->privilege_name == "Suppliers_view") 
                {
                  echo '<li><a href="Suppliers"><i class="fa fa-users text-red"></i> Suppliers</a></li>';
                }
              }

              for ($i=0; $i<sizeof($privileges); $i++) { 
                if ($privileges[$i]->privilege_name == "Itemcategory_view") 
                {
                  echo '<li><a href="Itemcategory"><i class="fa fa-cubes text-yellow"></i> Item Category</a></li>';
                }
              }
              ?>   -->
                 
            </ul>
         </li>         

         <li class="treeview">
            <a href="#">
            <i class="fa fa-tasks text-aqua"></i>
            <span>Transaction</span><i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
               <li><a href="index2.html"><i class="fa fa-circle-o"></i> Submenu 4</a></li>
            </ul>
         </li>
         <!-- <li>
            <a href="pages/widgets.html">
            <i class="fa fa-th"></i> <span>Widgets</span> <small class="label pull-right bg-green">new</small>
            </a>
         </li> -->
         <li class="treeview">
            <a href="#">
            <i class="fa fa-pie-chart text-aqua"></i><span>Reports</span>
            <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
               <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Submenu 1</a></li>
               <li><a href="index2.html"><i class="fa fa-circle-o"></i> Submenu 2</a>
               </li> <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Submenu 3</a></li>
               <li><a href="index2.html"><i class="fa fa-circle-o"></i> Submenu 4</a></li>
            </ul>
         </li>
         <li class="header">Administrative Menu</li>
         <li class="treeview">
            <a href="#">
            <i class="fa fa-laptop text-aqua"></i>
            <span>Users</span>
            <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <?php 
            if ($group_id == "1" || $group_id == "2") {
            
      
              for ($i=0; $i<sizeof($privileges); $i++) { 
                if ($privileges[$i]->privilege_name == "View Users") 
                {
                  echo '<li><a href="Users"><i class="fa fa-user text-red"></i> Accounts</a></li>';
                }
              }
            
              for ($i=0; $i<sizeof($privileges); $i++) { 
                if ($privileges[$i]->privilege_name == "View Groups") 
                {
                  echo '<li><a href="User_groups"> <i class="fa fa-users text-yellow"></i> Groups</a></li>';
                }
              }
            
              if ($group_id == "1") 
              {
                  echo '<li><a href="User_privileges"><i class="fa fa-unlock text-aqua"></i> Privileges</a></li>';
              }
                echo '</ul>
                    </li>';
            }
          ?>
         </li>
         <li class="treeview">
            <a href="#">
            <i class="fa fa-cogs"></i>
            <span>System Settings</span>
            <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
               <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Submenu 1</a></li>
               <li><a href="index2.html"><i class="fa fa-circle-o"></i> Submenu 2</a>
               </li> <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Submenu 3</a></li>
               <li><a href="index2.html"><i class="fa fa-circle-o"></i> Submenu 4</a></li>
            </ul>
         </li>         
      </ul>
   </section>
</aside>
<div class="content-wrapper">