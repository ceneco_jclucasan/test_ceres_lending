<!--  <div class="page-footer">
                <div class="page-footer-inner"> 2016 &copy; Metronic Theme By
                    <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div> -->
            <!-- END FOOTER -->
        </div>
        </div>
        <!-- BEGIN QUICK NAV -->
        <!-- <nav class="quick-nav">
            <a class="quick-nav-trigger" href="#0">
                <span aria-hidden="true"></span>
            </a>
            <ul>
                <li>
                    <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank" class="active">
                        <span>Purchase Metronic</span>
                        <i class="icon-basket"></i>
                    </a>
                </li>
                <li>
                    <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/reviews/4021469?ref=keenthemes" target="_blank">
                        <span>Customer Reviews</span>
                        <i class="icon-users"></i>
                    </a>
                </li>
                <li>
                    <a href="http://keenthemes.com/showcast/" target="_blank">
                        <span>Showcase</span>
                        <i class="icon-user"></i>
                    </a>
                </li>
                <li>
                    <a href="http://keenthemes.com/metronic-theme/changelog/" target="_blank">
                        <span>Changelog</span>
                        <i class="icon-graph"></i>
                    </a>
                </li>
            </ul>
            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav> -->
        <div class="quick-nav-overlay"></div>

    <script type="text/javascript">
      $(document).ready(function() {
        $('#alert_change_pw').hide(); 
        $('#alert_change_pw2').hide();
        $('#alert_change_pw3').hide();
        $('#alert_change_pw4').hide();
        $('#alert_change_pw5').hide();
        $('#alert_change_pw6').hide();
        $('[name="newpw2"]').keypress(function (e) {
          var key = e.which;
          if(key == 13)  // the enter key code
          {
            $('#btn_pw').click();
            return false;  
          }
        });
        $('[name="pin2"]').keypress(function (e) {
          var key = e.which;
          if(key == 13)  // the enter key code
          {
            $('#btn_pin').click();
            return false;  
          }
        });
      });

      function checkPass()
      {
        //Store the password field objects into variables ...
        var newpw = document.getElementById('newpw');
        var newpw2 = document.getElementById('newpw2');
        //Store the Confimation Message Object ...
        var message = document.getElementById('confirmMessage');
        //Set the colors we will be using ...
        var goodColor = "#66CC66";
        var badColor = "#FF0000";
        //Compare the values in the password field 
        //and the confirmation field
        if(newpw.value == newpw2.value){
          //The passwords match. 
          //Set the color to the good color and inform
          //the user that they have entered the correct password 
          //pass2.style.backgroundColor = goodColor;
          message.style.color = goodColor;
          message.innerHTML = "Passwords Match!"
        }else{
          //The passwords do not match.
          //Set the color to the bad color and
          //notify the user.
          //pass2.style.backgroundColor = badColor;
          message.style.color = badColor;
          message.innerHTML = "Passwords Do Not Match!"
        }
      }

      function checkPin()
      {
        //Store the password field objects into variables ...
        var pin = document.getElementById('pin');
        var pin2 = document.getElementById('pin2');
        //Store the Confimation Message Object ...
        var message = document.getElementById('confirmMessage2');
        //Set the colors we will be using ...
        var goodColor = "#66CC66";
        var badColor = "#FF0000";
        //Compare the values in the password field 
        //and the confirmation field
        if(pin.value == pin2.value){
          //The passwords match. 
          //Set the color to the good color and inform
          //the user that they have entered the correct password 
          //pass2.style.backgroundColor = goodColor;
          message.style.color = goodColor;
          message.innerHTML = "Reset PIN Match!"
        }else{
          //The passwords do not match.
          //Set the color to the bad color and
          //notify the user.
          //pass2.style.backgroundColor = badColor;
          message.style.color = badColor;
          message.innerHTML = "Reset PINs Do Not Match!"
        }
      }

      function change_pw() {
        if ($('[name="newpw"]').val() == "" || $('[name="newpw2"]').val() == "" || $('[name="oldpw"]').val() == "") {
            $('#alert_change_pw4').fadeIn();
            setTimeout(function(){ $('#alert_change_pw4').fadeOut(); }, 2000);
        } else if ($('[name="newpw"]').val() == $('[name="newpw2"]').val()) {
            $.ajax({
              url : "<?php echo site_url('dashboard/ajax_change_pw')?>/" + "<?php echo $id; ?>",
              type: "POST",
              data: $('#form_password').serialize(),
              dataType: "JSON",
              success: function(data)
              {
                if (data.status == "true") {
                    $('#alert_change_pw').fadeIn();
                    $.ajax({
                        url: "<?php echo site_url('dashboard/logout')?>",
                        type: "POST"
                    });
                    setTimeout(function(){ location.reload(); }, 1000);
                } else {
                    $('#alert_change_pw3').fadeIn();
                    setTimeout(function(){ $('#alert_change_pw3').fadeOut(); }, 2000);
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error in adding or updating data!');
              }
            });
        } else if ($('[name="newpw"]').val() != $('[name="newpw2"]').val()){
            $('#alert_change_pw2').fadeIn();
            setTimeout(function(){ $('#alert_change_pw2').fadeOut(); }, 2000);
        }
      }

      function change_pin() {

        if($('[name="pin_no"]').val().length >= 6 ){
                     if ($('[name="pin"]').val() == "" || $('[name="pin2"]').val() == "" || $('[name="oldpin"]').val() == "") {
            $('#alert_change_pw4').fadeIn();
            setTimeout(function(){ $('#alert_change_pw4').fadeOut(); }, 2000);
        } else if ($('[name="pin"]').val() == $('[name="pin2"]').val()) {
            $.ajax({
              url : "<?php echo site_url('dashboard/ajax_change_pin')?>/" + "<?php echo $id; ?>",
              type: "POST",
              data: $('#form_password').serialize(),
              dataType: "JSON",
              success: function(data)
              {
                if (data.status == "true") {
                    $('#alert_change_pw').fadeIn();
                    setTimeout(function(){ $('#alert_change_pw').fadeOut(); }, 2000);
                    $('#form_password')[0].reset();
                    document.getElementById('confirmMessage2').innerHTML = "";
                } else {
                    $('#alert_change_pw6').fadeIn();
                    setTimeout(function(){ $('#alert_change_pw6').fadeOut(); }, 2000);
                }
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error in adding or updating data!');
              }
            });
        } else if ($('[name="pin"]').val() != $('[name="pin2"]').val()){
            $('#alert_change_pw5').fadeIn();
            setTimeout(function(){ $('#alert_change_pw5').fadeOut(); }, 2000);
        }
                }else{
                  alert("PIN is 6 or more characters!");
                }      
      }

      function logout() {
        $.ajax({
            url: "<?php echo site_url('dashboard/logout')?>",
            type: "POST"
        });
      } 
    </script>

     <!-- Scripts -->
   

           
            
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
       
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url();?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-datetimepicker-mrs/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

                <script src="<?php echo base_url();?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <!--  
        <script src="<?php echo base_url();?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url();?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url();?>assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <!-- <script src="<?php echo base_url();?>assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> -->
        <script src="<?php echo base_url();?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

        

        <script src="<?php echo base_url();?>assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
        <!-- <script src="<?php echo base_url();?>assets/pages/scripts/portfolio-1.js" type="text/javascript"></script> -->

        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-table-develop/dist/bootstrap-table.js"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-table-develop/dist/extensions/mobile/bootstrap-table-mobile.js"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-table-develop/dist/extensions/export/bootstrap-table-export.js"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-table-develop/dist/extensions/export/tableExport.js"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-table-develop/dist/extensions/filter-control/bootstrap-table-filter-control.js"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-table-develop/dist/extensions/editable/bootstrap-table-editable.js"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url();?>assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>js/vendor/jquery.ui.widget.js"></script> 
        <script src="<?php echo base_url();?>js/jquery.iframe-transport.js"></script>
        <script src="<?php echo base_url();?>js/jquery.fileupload.js"></script> 

        <script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <!-- <script src="<?php echo base_url();?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script> -->
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/pages/scripts/form-wizard.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url();?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url();?>assets/global/plugins/ladda/spin.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/ladda/ladda.min.js" type="text/javascript"></script>
        
        <script src="<?php echo base_url();?>assets/pages/scripts/ui-buttons-spinners.min.js" type="text/javascript"></script>
            <!-- END THEME LAYOUT SCRIPTS -->
  </body>
</html>
