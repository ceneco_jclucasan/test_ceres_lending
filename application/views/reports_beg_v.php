<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                    <i class="fa fa-circle"></i>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                        <br>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Beginning</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="col-md-6">
                                       <form action="#" id="form" class="form-horizontal">
                                         <input type="hidden" value="" name="id"/> 
                                         <div class="form-body">               
                                            <div class="form-group">
                                               <label class="control-label col-md-3">Reference No.</label>
                                               <div class="col-md-6">
                                                 <div class="input-group">
                                                      <input type="text" id="ref" class="form-control" placeholder="reference number">
                                                      <span class="input-group-btn">
                                                        <button type="button" id="btnSave" onclick="search()" class="btn btn-primary">Generate</button>
                                                      </span>
                                                    </div><!-- /input-group -->
                                               </div>
                                            </div>                                              
                                         </div>
                                      </form> 
                                    </div>
                                    <div class="portlet-body">
                                    <!-- <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                       &nbsp;
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="javascript:;" onclick="exportfile()" >
                                                                    <i class="fa fa-file-pdf-o"></i> Export</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                           <thead id="thead1">
                               <tr>
                                   <th class="all">Creditor</th>
                                   <th class="all">Reference</th>
                                   <th class="all">Particulars</th>
                                   <th class="all">Date</th>
                                   <th class="all">Principal</th>
                                   <th class="all">Interest</th>
                                   <th class="all">Total Principal</th>
                                   <th class="all">Total Interest</th>
                                   <th class="all">Total</th>
                                   <th class="all">Months Remaining</th>
                               </tr>
                           </thead>
                           <tbody id="body1">
                               
                           </tbody>
                           </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

<script type="text/javascript">
var table;
var arr=[];
var arr2;
  $(document).ready(function() {
       //$('#sample_1').DataTable();
       //list();
  });

function search()
  {
    var table = $('#sample_2').DataTable();
    table.destroy();
    table = $('#sample_2').DataTable( {          
          "ajax": "<?php echo site_url('Netpays/ajax_list_repo_beg')?>/"+$("#ref").val(), 
        dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
            'csvHtml5',
            'print',
        ],
          "columns": [
              { "data": "creditor" },
              { "data": "ref" },
              { "data": "particulars" },
              { "data": "date" },
              { "data": "principal" },
              { "data": "interest" },
              { "data": "tp" },
              { "data": "ti" },
              { "data": "total" },
              { "data": "months" }
          ]         
      } );
  }




  function exportToCsv(filename, rows)
  {
                  var processRow = function (row) {
                      var finalVal = '';
                      for (var j = 0; j < row.length; j++) {
                          var innerValue = row[j] === null ? '' : row[j].toString();
                          if (row[j] instanceof Date) {
                              innerValue = row[j].toLocaleString();
                          };
                          var result = innerValue.replace(/"/g, '""');
                          if (result.search(/("|,|\n)/g) >= 0)
                              result = '"' + result + '"';
                          if (j > 0)
                              finalVal += ',';
                          finalVal += result;
                      }
                      return finalVal + '\n';
                  };

                  var csvFile = '';
                  for (var i = 0; i < rows.length; i++) {
                      csvFile += processRow(rows[i]);
                  }

                  var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
                  if (navigator.msSaveBlob) { // IE 10+
                      navigator.msSaveBlob(blob, filename);
                  } else {
                      var link = document.createElement("a");
                      if (link.download !== undefined) { // feature detection
                          // Browsers that support HTML5 download attribute
                          var url = URL.createObjectURL(blob);
                          link.setAttribute("href", url);
                          link.setAttribute("download", filename);
                          link.style.visibility = 'hidden';
                          document.body.appendChild(link);
                          link.click();
                          document.body.removeChild(link);
                      }
                  }
  }

  function exportfile() {
    //console.log(arr[0].data);
    //console.log(arr2);
    var year = moment().format('YYYY');
    var filename = year+"-"+$("#month").val()+"-"+$("#type").val();
    exportToCsv(filename+".csv", arr2);
  }
</script>

<div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Classification Form</h3>
         </div>
         <div class="modal-body form">
             <form action="#" id="form" class="form-horizontal">
               <input type="hidden" value="" name="id"/> 
               <div class="form-body">                 
                  <div class="form-group">
                     <label class="control-label col-md-3">Classification</label>
                     <div class="col-md-9">
                        <input name="class" name="class" placeholder="" class="form-control" type="text">
                     </div>
                  </div>             
                  <div class="form-group">
                     <label class="control-label col-md-3">Classification</label>
                     <div class="col-md-9">
                        <select class="form-control" name="type" id="type">
                          <option value="Semi-Monthly">Semi-Monthly</option>
                          <option value="Monthly">Monthly</option>
                        </select>
                     </div>
                  </div>
               </div>
            </form> 
         </div>
         <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
         </div>
      </div>
   </div>
</div>