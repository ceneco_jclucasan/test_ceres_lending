<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Loans Application Validation</span>
                                </li>
                            </ul>
                            <!-- <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div> -->                       
                        </div>
                        <!--START CONTENT HERE -JC -->
                        <div class="clearfix"> </div>
                        <br>

                         <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Loan Application Validation</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                                    <div class="btn-group">
                                                         <button class="btn btn-success" onclick="add_classification()"><i class="glyphicon glyphicon-plus"></i>  Add Classification</button>
                                                    </div>
                                                </div> -->
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">TRN</th>
                                                    <th class="all">Creditor</th>
                                                    <th class="all">Product</th>
                                                    <th class="all">Amount</th>
                                                    <th class="all">Terms</th>
                                                    <th class="all">Co-maker</th>
                                                    <th class="all">Co-maker</th>
                                                    <th class="all">Purpose</th>
                                                    <th class="all">Remarks</th>
                                                    <th class="all">Date Applied</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        <!--END CONTENT-->
                        
                    </div>
                </div>

<script type="text/javascript">
var upload_docus = [];
var table1;
  $(document).ready(function() {
       list();
       uploads();
  });

  function list()
  {
     table1 = $('#sample_1').DataTable( {        
        "ajax": "<?php echo site_url('Loans/ajax_list_loan_app')?>", 
        "columns": [
            { "data": "trn" },
            { "data": "creditor" },
            { "data": "product_name" },
            { "data": "amount" },
            { "data": "terms" },
            { "data": "com1" },
            { "data": "com2" },
            { "data": "purpose" },
            { "data": "validators_remark" },
            { "data": "created_dt" },
            { "data": "action" }
        ]
       
    } );
  }

  function documents(id)
  {
     /*table = $('#sample_2').DataTable( {        
        "ajax": "<?php echo site_url('Loans/ajax_list_loan_app_documents/')?>/"+id, 
        "columns": [
            { "data": "filename" },
            { "data": "action" }
        ]       
    } );*/


/*'<div id="js-grid-juicy-projects" class="cbp">'
                                '<div class="cbp-item graphic">'
                                    '<div class="cbp-caption">'
                                        '<div class="cbp-caption-defaultWrap">'
                                            '<img src="<?php echo base_url(); ?>uploads/files/'+data.data[x]['filename']+'" alt=""> </div>'
                                        '<div class="cbp-caption-activeWrap">'
                                            '<div class="cbp-l-caption-alignCenter">'
                                                '<div class="cbp-l-caption-body">'
                                                    '<a href="<?php echo base_url(); ?>uploads/files/'+data.data[x]['filename']+'" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="Dashboard<br>by Paul Flavius Nechita">view larger</a>'
                                                '</div>  </div> </div> </div>'
                                   ' <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">Dashboard</div>'
                                    '<div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center">Web Design / Graphic</div> </div>';*/
     

     $.ajax({
      url : "<?php echo site_url('Loans/ajax_list_loan_app_documents/')?>/"+id, 
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {       
          /*for (x = 0; x < data.data.length; x++) {
                      $("#body2").append('<tr><td width="15%">'+data.data[x]['filename']+'</td><td width="35%"><div id="station">'+data.data[x]['action']+'</div></td></tr>');
                    }
*/
$("#body2").empty();
                                 for (x = 0; x < data.data.length; x++) {
                      $("#body2").append('<tr><td width="15%">'+'<div id="js-grid-juicy-projects" class="cbp fefe">'+
                                '<div class="cbp-item graphic logos">'+
                                    '<div class="cbp-caption">'+
                                        '<div class="cbp-caption-defaultWrap">'+
                                            '<img src="<?php echo base_url(); ?>uploads/files/'+data.data[x]['filename']+'" alt=""> </div>'+
                                        '<div class="cbp-caption-activeWrap">'+
                                            '<div class="cbp-l-caption-alignCenter">'+
                                                '<div class="cbp-l-caption-body">'+
                                                    '<a href="<?php echo base_url(); ?>uploads/files/'+data.data[x]['filename']+'" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="'+data.data[x]['filename']+'">view larger</a>'+
                                                '</div>  </div> </div> </div>'+
                                   ' <div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">&nbsp;</div>'+
                                    '<div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center">&nbsp;</div>'
                                    +'</td><td width="35%"><div id="station">'+data.data[x]['action']+'</div></td></tr>');
}

                                            // init cubeportfolio
    $('.fefe').cubeportfolio({
        filters: '#js-filters-juicy-projects',
        loadMore: '#js-loadMore-juicy-projects',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        defaultFilter: '*',
        animationType: 'quicksand',
        gapHorizontal: 35,
        gapVertical: 30,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 5
        }, {
            width: 1100,
            cols: 4
        }, {
            width: 800,
            cols: 3
        }, {
            width: 480,
            cols: 2
        }, {
            width: 320,
            cols: 1
        }],
        caption: 'overlayBottomReveal',
        displayType: 'sequentially',
        displayTypeSpeed: 80,

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 10000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage('AJAX Error! Please refresh the page!');
                });
        },
    });
                         
                  

             



      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }

  function edit_loan(id)
  {    
    $('#sample_2').DataTable().destroy();
        documents(id);  
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('Loans/ajax_get_loan_data')?>/" + id+"/",
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {           
        $('#loan_id').val(data.data.id);    
        $('.form').find('input:text').val('');
        $('#creditor').val(data.data.fullname);
        $('#amount').val(numberWithCommas(data.data.applied_amount));
        $('#terms').val(data.data.terms);
        $('#purpose').val(data.data.purpose);
        $('#c1').val(data.comaker1.fullname);
        $('#c2').val(data.comaker2.fullname);  
        $('#remarks').val(data.data.validators_remark);
        upload_docus = [];
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function validate()
  {
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('Loans/ajax_validate/')?>/",
      type: "POST",
      data:{
        id: $("#loan_id").val(),
      },
      dataType: "JSON",
      success: function(data)
      {           
        $("#full").modal('hide');
        $("#validatebtn").prop("disabled",true);
        table1.ajax.reload( null, false );
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }

  function save_1()
  {
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('Loans/ajax_save_validation_details/')?>/",
      type: "POST",
      data:{
        remarks: $("#remarks").val(),
        docus: upload_docus,
        id: $("#loan_id").val(),
      },
      dataType: "JSON",
      success: function(data)
      {           
        $("#full").modal('hide');
        table1.ajax.reload( null, false );
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }

  function uploads() {
    var str = "<?php echo base_url();?>";
    var url = str + 'uploads/ ';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function(e, data) {
            $.each(data.result.files, function(key, value) {
                upload_docus.push(value.name);
                console.log(value);
            });
            alert("done");
        },
        progressall: function(e, data) {
        },
        submit: function(e, data) {
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}

</script>

<div class="modal fade" id="modal_form_privilege" role="dialog">
              <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 class="modal-title-priv"> </h3>
                </div>
                <div class="modal-body form">
                      <table id="tblPriv" class="table table-striped table-bordered table-hover" 
                                            data-search="true" 
                                            data-pagination="true"
                                            data-page-size="5"
                                            data-show-export="true"
                                            data-mobile-responsive="true"
                                            data-sort-name = "priv_name"
                                            data-sort-order = "desc"
                                             data-url="<?php echo site_url('Privillege')?>"
                                            data-page-list="[5, 10, ALL]">
                                      <thead>
                                        <tr>
                                          <th data-field="priv_name"  data-sortable = "true">Privilege Names</th>
                                          <th data-field="desc"  data-sortable = "true">Description</th>
                                          <th data-field="action" style="width:125px;">Action</th>
                                        </tr>
                                      </thead>
                                </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->   
</div>

<div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">User Account Form</h3>
         </div>
         <div class="modal-body form">
            <form action="#" id="form" class="form-horizontal">
               <input type="hidden" value="" name="id"/> 
               <div class="form-body">
                  <div class="form-group">
                     <label class="control-label col-md-3">Name</label>
                     <div class="col-md-9">
                        <select name="emp_id" data-placeholder="Select..." class="form-control"></select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Email</label>
                     <div class="col-md-9">
                        <input name="email" placeholder="" class="form-control" type="text">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Username</label>
                     <div class="col-md-9">
                        <input name="username" placeholder="" class="form-control" type="text">
                     </div>
                  </div>
                  <div id="pw" class="form-group">
                     <label class="control-label col-md-3">Password</label>
                     <div class="col-md-9">
                        <input name="password" placeholder="" class="form-control" type="password">
                     </div>
                  </div>
                  <div id="user_grp" class="form-group">
                     <label class="control-label col-md-3">User Group</label>
                     <div class="col-md-9">
                        <select name="group_id2" data-placeholder="Select..." class="form-control"></select>
                     </div>
                  </div>
                  <div id="alert2" class="form-group">
                     <div class="col-md-8 pull-right">
                        <div class="alert alert-danger alert-dismissable">
                           <h4>  <i class="icon fa fa-remove"></i> This user already have an account.</h4>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

 <div class="modal fade" id="full" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-full">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">Loan Details</h4>
                                                    </div>
                                                    <div class="modal-body"> 
                                                      <div class="row">
                                                                <input type="hidden" id="loan_id">
                                                                <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Creditor</label>
                                                                            <input type="text" id="creditor" class="form-control"  readonly>
                                                                        </div>
                                                                    </div>                                                                    
                                                                    <div class="col-md-6">
                                                                        <div class="form-group ">
                                                                            <label class="control-label">Amount</label>
                                                                            <input type="text" id="amount" class="form-control"  readonly>
                                                                        </div>
                                                                    </div>                                                                
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Terms</label>
                                                                            <input type="text" id="terms" class="form-control" readonly>
                                                                        </div>
                                                                    </div>                                                                    
                                                                    <div class="col-md-6">
                                                                        <div class="form-group ">
                                                                            <label class="control-label">Purpose</label>
                                                                            <input type="text" id="purpose" class="form-control"  readonly>
                                                                        </div>
                                                                    </div>                                                                
                                                              </div>

                                                              <div class="row">
                                                                <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Comaker 1</label>
                                                                            <input type="text" id="c1" class="form-control"  readonly>
                                                                        </div>
                                                                    </div>                                                                    
                                                                    <div class="col-md-6">
                                                                        <div class="form-group ">
                                                                            <label class="control-label">Comaker 2</label>
                                                                            <input type="text" id="c2" class="form-control"  readonly>
                                                                        </div>
                                                                    </div>                                                                
                                                              </div>

                                                              <div class="row">
                                                                <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Remarks</label>
                                                                            <textarea class="form-control" id="remarks"></textarea>
                                                                        </div>
                                                                    </div>                                                                    
                                                                    <div class="col-md-6">
                                                                        <div class="form-group ">
                                                                            <label class="control-label">Uploads</label>
                                                                            <input id="fileupload" type="file" name="files[]"  multiple>
                                                                        </div>
                                                                    </div>                                                                
                                                              </div>
                                                              <div class="portlet-body">
                                                                  <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                                                                      <thead id="thead2">
                                                                          <tr>
                                                                              <th class="all">Filename</th>
                                                                              <th class="all">Action</th>
                                                                          </tr>
                                                                      </thead>
                                                                      <tbody id="body2" class="ttest">
                                                                          
                                                                      </tbody>
                                                                  </table>
                                                              </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                       <button type="button" class="btn blue" onclick="save_1()">Save</button> 
                                                        <?php
                                                            for ($i=0; $i<sizeof($privileges); $i++) { 
                                                            if ($privileges[$i]->privilege_name == "validate_this_loan") 
                                                            {                
                                                              echo '<button type="button" id="validatebtn" class="btn green" onclick="validate()">Validate</button>';
                                                            }
                                                          }
                                                        ?>  
                                                       <button type="button" class="btn red" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>   