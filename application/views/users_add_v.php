<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        <div class="theme-panel hidden-xs hidden-sm">
                            <div class="toggler"> </div>
                            <div class="toggler-close"> </div>
                            <div class="theme-options">
                                <div class="theme-option theme-colors clearfix">
                                    <span> THEME COLOR </span>
                                    <ul>
                                        <li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default"> </li>
                                        <li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue"> </li>
                                        <li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue"> </li>
                                        <li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey"> </li>
                                        <li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light"> </li>
                                        <li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2"> </li>
                                    </ul>
                                </div>
                                <div class="theme-option">
                                    <span> Theme Style </span>
                                    <select class="layout-style-option form-control input-sm">
                                        <option value="square" selected="selected">Square corners</option>
                                        <option value="rounded">Rounded corners</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Layout </span>
                                    <select class="layout-option form-control input-sm">
                                        <option value="fluid" selected="selected">Fluid</option>
                                        <option value="boxed">Boxed</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Header </span>
                                    <select class="page-header-option form-control input-sm">
                                        <option value="fixed" selected="selected">Fixed</option>
                                        <option value="default">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Top Menu Dropdown</span>
                                    <select class="page-header-top-dropdown-style-option form-control input-sm">
                                        <option value="light" selected="selected">Light</option>
                                        <option value="dark">Dark</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Mode</span>
                                    <select class="sidebar-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Menu </span>
                                    <select class="sidebar-menu-option form-control input-sm">
                                        <option value="accordion" selected="selected">Accordion</option>
                                        <option value="hover">Hover</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Style </span>
                                    <select class="sidebar-style-option form-control input-sm">
                                        <option value="default" selected="selected">Default</option>
                                        <option value="light">Light</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Position </span>
                                    <select class="sidebar-pos-option form-control input-sm">
                                        <option value="left" selected="selected">Left</option>
                                        <option value="right">Right</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Footer </span>
                                    <select class="page-footer-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>User</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn green btn-sm btn-outline dropdown-toggle" data-toggle="dropdown"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">
                                                <i class="icon-bell"></i> Action</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-shield"></i> Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-user"></i> Something else here</a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon-bag"></i> Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> New User Profile | Account
                            <small>user account page</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN PROFILE SIDEBAR -->
                                <div class="profile-sidebar">
                                    <!-- PORTLET MAIN -->
                                    <div class="portlet light profile-sidebar-portlet ">
                                        <!-- SIDEBAR USERPIC -->
                                        <div class="profile-userpic">
                                            <img src="../assets/pages/media/profile/profile_user.jpg" class="img-responsive" alt=""> </div>
                                        <!-- END SIDEBAR USERPIC -->
                                        <!-- SIDEBAR USER TITLE -->
                                        <div class="profile-usertitle">
                                            <div class="profile-usertitle-name"> Marcus Doe </div>
                                            <div class="profile-usertitle-job"> Developer </div>
                                        </div>
                                        <!-- END SIDEBAR USER TITLE -->
                                        <!-- SIDEBAR BUTTONS -->
                                        <div class="profile-userbuttons">
                                            <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                                            <button type="button" class="btn btn-circle red btn-sm">Message</button>
                                        </div>
                                        <!-- END SIDEBAR BUTTONS -->
                                        <!-- SIDEBAR MENU -->
                                        <div class="profile-usermenu">
                                            <ul class="nav">
                                                <li>
                                                    <a href="page_user_profile_1.html">
                                                        <i class="icon-home"></i> Overview </a>
                                                </li>
                                                <li class="active">
                                                    <a href="page_user_profile_1_account.html">
                                                        <i class="icon-settings"></i> Account Settings </a>
                                                </li>
                                                <li>
                                                    <a href="page_user_profile_1_help.html">
                                                        <i class="icon-info"></i> Help </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- END MENU -->
                                    </div>
                                    <!-- END PORTLET MAIN -->
                                    <!-- PORTLET MAIN -->
                                   
                                    <!-- END PORTLET MAIN -->
                                </div>
                                <!-- END BEGIN PROFILE SIDEBAR -->
                                <!-- BEGIN PROFILE CONTENT -->
                                <div class="profile-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light ">
                                                <div class="portlet-title tabbable-line">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                    </div>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                                        </li>
                                                        <li>
                                                            <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="tab-content">
                                                        <!-- PERSONAL INFO TAB -->
                                                        <div class="tab-pane active" id="tab_1_1">
                                                            <form role="form" action="#">
                                                                <div class="form-group">
                                                                    <label class="control-label">First Name</label>
                                                                    <input type="text" placeholder="John" class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Last Name</label>
                                                                    <input type="text" placeholder="Doe" class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Mobile Number</label>
                                                                    <input type="text" placeholder="+1 646 580 DEMO (6284)" class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Interests</label>
                                                                    <input type="text" placeholder="Design, Web etc." class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Occupation</label>
                                                                    <input type="text" placeholder="Web Developer" class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">About</label>
                                                                    <textarea class="form-control" rows="3" placeholder="We are KeenThemes!!!"></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Website Url</label>
                                                                    <input type="text" placeholder="http://www.mywebsite.com" class="form-control" /> </div>
                                                                <div class="margiv-top-10">
                                                                    <a href="javascript:;" class="btn green"> Save Changes </a>
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END PERSONAL INFO TAB -->
                                                        <!-- CHANGE AVATAR TAB -->
                                                        <div class="tab-pane" id="tab_1_2">
                                                            <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                                                eiusmod. </p>
                                                            <form action="#" role="form">
                                                                <div class="form-group">
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                                <span class="fileinput-new"> Select image </span>
                                                                                <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="..."> </span>
                                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix margin-top-10">
                                                                        <span class="label label-danger">NOTE! </span>
                                                                        <span>Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                                    </div>
                                                                </div>
                                                                <div class="margin-top-10">
                                                                    <a href="javascript:;" class="btn green"> Submit </a>
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END CHANGE AVATAR TAB -->
                                                        <!-- CHANGE PASSWORD TAB -->
                                                        <div class="tab-pane" id="tab_1_3">
                                                            <form action="#">
                                                                <div class="form-group">
                                                                    <label class="control-label">Current Password</label>
                                                                    <input type="password" class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">New Password</label>
                                                                    <input type="password" class="form-control" /> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-type New Password</label>
                                                                    <input type="password" class="form-control" /> </div>
                                                                <div class="margin-top-10">
                                                                    <a href="javascript:;" class="btn green"> Change Password </a>
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END CHANGE PASSWORD TAB -->
                                                        <!-- PRIVACY SETTINGS TAB -->
                                                        <div class="tab-pane" id="tab_1_4">
                                                            <form action="#">
                                                                <table class="table table-light table-hover">
                                                                    <tr>
                                                                        <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                                                                        <td>
                                                                            <div class="mt-radio-inline">
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios1" value="option1" /> Yes
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios1" value="option2" checked/> No
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                        <td>
                                                                            <div class="mt-radio-inline">
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios11" value="option1" /> Yes
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios11" value="option2" checked/> No
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                        <td>
                                                                            <div class="mt-radio-inline">
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios21" value="option1" /> Yes
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios21" value="option2" checked/> No
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                                        <td>
                                                                            <div class="mt-radio-inline">
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios31" value="option1" /> Yes
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="optionsRadios31" value="option2" checked/> No
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <!--end profile-settings-->
                                                                <div class="margin-top-10">
                                                                    <a href="javascript:;" class="btn red"> Save Changes </a>
                                                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END PRIVACY SETTINGS TAB -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PROFILE CONTENT -->
                            </div>
                        </div>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
       $('#alert2').hide();
       list();
       dropdown_list_employees();
       dropdown_list_group();

       /*$('#test').on('switchChange.bootstrapSwitch', function(event, state) {
        
          alert(state); // true | false
          if(state==true)
          {
            $.ajax({
                      url :  "<?php echo site_url('Users/ajax_update_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                            toastr.success('Privilege has been added!','Message:');
                            manage_privilages(userid);

                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
          }else
          {
            $.ajax({
                      url :  "<?php echo site_url('Users/ajax_delete_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                        toastr.error('Privilege has been removed!','Message:');
                        manage_privilages(userid);
                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
          }

        });*/

         /*$('#test').on('switchChange.bootstrapSwitch', function(event, state) {
        
          if(state==false)
          {
             console.log(this.value);
            $.ajax({
                      url :  "<?php echo site_url('Users/ajax_update_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                            toastr.success('Privilege has been added!','Message:');
                            manage_privilages(userid);

                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
          }

        });*/



        $('#privswitch').bootstrapSwitch();
  });

  function list()
  {
     $.ajax({
                  url : "<?php echo site_url('Users/ajax_list')?>/" ,
                  type: "POST",
                  dataType: "JSON",
                  success: function(data)
                  {                
                      var a = new Array();  
                      for (x = 0; x < data.data.length; x++) {
                        a[x] = 
                        {
                            "email": data.data[x][0],
                            "fn": data.data[x][1],
                            "ln": data.data[x][2],
                            "ug": data.data[x][3],
                            "as": data.data[x][5],
                            "action": data.data[x][4],
                        };
                      }
                      $('#tblUsers').bootstrapTable({ data: a });
                      $('#tblUsers').bootstrapTable('load',a);
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error: "ajax_list"');
                  }
                });
  }

  function manage_privilages(id)
  {
      $('#modal_form_privilege').modal('show'); // show bootstrap modal

            $.ajax({
                  url : "<?php echo site_url('users/ajax_list_user/')?>/" + id,
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {   
                   // $('.modal-title-priv').text('Privileges of '+data.username); // Set Title to Bootstrap modal title
                    var a = new Array();
                        for (x = 0; x < data.data.length; x++) {
                          a[x] = 
                          {
                            "priv_name": data.data[x][0],
                            "desc": data.data[x][1],
                            "action":data.data[x][2],
                          };
                        }
                        $('#tblPriv').bootstrapTable({ data: a });
                        $('#tblPriv').bootstrapTable('load',a);
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data!');
                  }
            });
  }

  function add_priv_user(privid,userid)
  {
                if (confirm('Are you sure add this privilege?')) {

                    $.ajax({
                      url :  "<?php echo site_url('Users/ajax_update_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                            toastr.success('Privilege has been added!','Message:');
                            manage_privilages(userid);

                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
                  }

        /*$('#test').on('switchChange.bootstrapSwitch', function(event, state) {
        
          if(state==false)
          {
            $.ajax({
                      url :  "<?php echo site_url('Users/ajax_update_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                            toastr.success('Privilege has been added!','Message:');
                            manage_privilages(userid);

                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
          }

        });*/

  }

  function remove_priv_user(privid,userid)
  {    
                if (confirm('Are you sure remoive this privilege?')) {
                    $.ajax({
                      url :  "<?php echo site_url('Users/ajax_delete_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                        toastr.error('Privilege has been removed!','Message:');
                        manage_privilages(userid);
                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
                  }
/*$('#test').on('switchChange.bootstrapSwitch', function(event, state) {
        
          if(state==true)
          {
            $.ajax({
                      url :  "<?php echo site_url('Users/ajax_delete_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                            toastr.success('Privilege has been added!','Message:');
                            manage_privilages(userid);

                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
          }

        });*/
            
  }

  function edit_user(id)
  {
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('users/ajax_edit/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {           
        $('[name="id"]').val(data.id);
        $('[name="emp_id"]').val(data.emp_id).trigger('chosen:updated');
        $('[name="email"]').val(data.email);
        $('[name="username"]').val(data.username);
        $('[name="password"]').val(data.password);
        $('[name="group_id2"]').val(data.group_id).trigger('chosen:updated');
        $('#user_grp').hide();
        $('#pw').hide();
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }

  function dropdown_list_employees()
  {
      $('[name="emp_id"]').empty();
      var a = "";
      var b = "";
      $.ajax({
          url: "<?php echo site_url('users/ajax_dropdown_list_employees')?>",
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('[name="emp_id"]').append('<option> </option>');
              for (var x = 0; x < data.data.length; x++) {
                  a = data.data[x][0];
                  b = data.data[x][1];
                  if (a != "1") {
                      $('[name="emp_id"]').append('<option value="' + a + '">' + b + '</option>');
                  }
              };
              $('[name="emp_id"]').trigger('chosen:updated');
          },
          error: function(jqXHR, textStatus, errorThrown)
          {
              alert('Error in getting data for dropdown list!');
          }
      });
  }

function save()
{
    var url;
    if (save_method == 'add') {
        url = "<?php echo site_url('users/ajax_add')?>";
        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                //if success close modal and reload ajax table
                if (data.data[0] == 'This user already have an account.') {
                    $('#alert2').fadeIn();
                    setTimeout(function() {
                        $('#alert2').fadeOut();
                    }, 2000);
                } else {
                    console.log(data.data[0]);
                    //save privileges here                    
                    get_group_privileges($('[name="group_id2"]').val(), data.data[0]);
                    $('#modal_form').modal('hide');
                    list();
                };
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert('Error in adding or updating data!');
            }
        });
    } else {
        url = "<?php echo site_url('users/ajax_update')?>";
        // ajax adding data to database
        $.ajax({
            url: url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                //if success close modal and reload ajax table
                $('#modal_form').modal('hide');
                list();
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert('Error in adding or updating data!');
            }
        });
    }
}

function add_user()
{
  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('[name="emp_id"]').trigger('chosen:updated');
  $('[name="group_id2"]').trigger('chosen:updated');
  $('#user_grp').show();
  $('#pw').show();
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add User'); // Set Title to Bootstrap modal title
}

function delete_user(id)
{
    swal({
      title: "Are you sure you want to delete?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "error",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('users/ajax_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table
                        delete_all_privileges(id);                        
                        list();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                });

          swal("Deleted!", "User account has been deleted.", "success");
    });
}

function get_group_privileges_by_id()
{
    for (var i = 0; i < document.getElementsByName("privilege_checkbox[]").length; i++) {
        //console.log("group_privilege[" + data.data[i][0] + "]");
        document.getElementsByName("privilege_checkbox[]")[i].checked = false;
    };
    $.ajax({
        url: "<?php echo site_url('user_groups/ajax_get_privileges_by_id')?>/" + $('[name="group_id"]').val(),
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            for (var i = 0; i < data.data.length; i++) {
                //console.log("group_privilege[" + data.data[i][0] + "]");
                document.getElementById("user_privilege[" + data.data[i][0] + "]").checked = true;
            };
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function dropdown_list_group()
{
    $('[name="group_id"]').empty();
    $('[name="group_id2"]').empty();
    var a = "";
    var b = "";
    $.ajax({
        url: "<?php echo site_url('users/ajax_dropdown_list_group')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="group_id"]').append('<option> </option>');
            $('[name="group_id2"]').append('<option> </option>');
            for (var x = 0; x < data.data.length; x++) {
                a = data.data[x][0];
                b = data.data[x][1];
                if (a != "1") {
                    $('[name="group_id"]').append('<option value="' + a + '">' + b + '</option>');
                    $('[name="group_id2"]').append('<option value="' + a + '">' + b + '</option>');
                }
            };
            $('[name="group_id"]').trigger('chosen:updated');
            $('[name="group_id2"]').trigger('chosen:updated');
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            alert('Error in getting data for dropdown list!');
        }
    });
}

function get_group_privileges(group_id, user_id)
{
    $.ajax({
        url: "<?php echo site_url('user_groups/ajax_get_privileges_by_id')?>/" + group_id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            for (var i = 0; i < data.data.length; i++) 
            {
                $.ajax({
                    url: "<?php echo site_url('users/ajax_update_privileges')?>/"+data.data[i][0]+"/"+user_id,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        user_id: user_id,
                        privilege_id: data.data[i][0]
                    },
                    success: function(data2)
                    {

                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('error');
                    }
                });
            };
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function delete_all_privileges(id)
{
    $.ajax({
        url: "<?php echo site_url('users/ajax_delete_all_privileges')?>/" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            alert('Error in deleting data!');
        }
    });
}

function suspend_user(id)
{  
    swal({
      title: "Are you sure you want to suspend user?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Suspend",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('users/ajax_suspend_user')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        list();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in adding or updating data!');
                    }
                });

          swal("Suspended!", "Account", "warning");
    });
}

function unsuspend_user(id)
{

  swal({
      title: "Are you sure you want to unsuspend user?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Unsuspend",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('users/ajax_unsuspend_user')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        list();
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in adding or updating data!');
                    }
                });

          swal("Unsuspended!", "Account", "success");
    });   
}

</script>

<div class="modal fade" id="modal_form_privilege" role="dialog">
              <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 class="modal-title-priv"> </h3>
                </div>
                <div class="modal-body form">
                      <table id="tblPriv" class="table table-striped table-bordered table-hover" 
                                            data-search="true" 
                                            data-pagination="true"
                                            data-page-size="5"
                                            data-show-export="true"
                                            data-mobile-responsive="true"
                                            data-sort-name = "priv_name"
                                            data-sort-order = "desc"
                                            data-page-list="[5, 10, ALL]">
                                      <thead>
                                        <tr>
                                          <th data-field="priv_name"  data-sortable = "true">Privilege Names</th>
                                          <th data-field="desc"  data-sortable = "true">Description</th>
                                          <th data-field="action" style="width:125px;">Action</th>
                                        </tr>
                                      </thead>
                                </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->   
</div>

<div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">User Account Form</h3>
         </div>
         <div class="modal-body form">
            <form action="#" id="form" class="form-horizontal">
               <input type="hidden" value="" name="id"/> 
               <div class="form-body">
                  <div class="form-group">
                     <label class="control-label col-md-3">Name</label>
                     <div class="col-md-9">
                        <select name="emp_id" data-placeholder="Select..." class="form-control"></select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Email</label>
                     <div class="col-md-9">
                        <input name="email" placeholder="" class="form-control" type="text">
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Username</label>
                     <div class="col-md-9">
                        <input name="username" placeholder="" class="form-control" type="text">
                     </div>
                  </div>
                  <div id="pw" class="form-group">
                     <label class="control-label col-md-3">Password</label>
                     <div class="col-md-9">
                        <input name="password" placeholder="" class="form-control" type="password">
                     </div>
                  </div>
                  <div id="user_grp" class="form-group">
                     <label class="control-label col-md-3">User Group</label>
                     <div class="col-md-9">
                        <select name="group_id2" data-placeholder="Select..." class="form-control"></select>
                     </div>
                  </div>
                  <div id="alert2" class="form-group">
                     <div class="col-md-8 pull-right">
                        <div class="alert alert-danger alert-dismissable">
                           <h4>  <i class="icon fa fa-remove"></i> This user already have an account.</h4>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->