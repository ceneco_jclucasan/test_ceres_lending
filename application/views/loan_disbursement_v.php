<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Loans</span>
                                </li>
                            </ul>
                            <!-- <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div> -->
                        </div>
                       <!--  <h1 class="page-title"> Loan Validated
                        </h1> -->
                        <!--START CONTENT HERE -JC -->
                        <div class="clearfix"> </div>
                        <br>>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Disbursement List</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">TRN</th>
                                                    <th class="all">Creditor</th>
                                                    <th class="all">Company/Branch</th>
                                                    <th class="all">Product</th>
                                                    <th class="all">Amount</th>
                                                    <th class="all">Terms</th>
                                                    <th class="all">Chk Voucher No.</th>
                                                    <th class="all">Date Release</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>  
                          <!-- END -->                    
                    </div>
                </div>

<script type="text/javascript">
var upload_docus = [];
  $(document).ready(function() {
       list();
       uploads();
  });

  function list()
  {
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };
    var url = "";
    if(getUrlParameter('status')=="checking")
    {
      url = "<?php echo site_url('Loans/ajax_list_loan_approved')?>";
    }
    if(getUrlParameter('status')=="approval")
    {
      url = "<?php echo site_url('Loans/ajax_list_loan_checked')?>";
    }   

     table = $('#sample_1').DataTable( {        
        "ajax": "<?php echo site_url('Loans/ajax_list_loan_disbursement')?>", 
        "columns": [
            /*{ "data": "creditor" },
            { "data": "amount" },
            { "data": "terms" },
            { "data": "purpose" },
            { "data": "validated_dt"},
            { "data": "action" },*/
            { "data": "trn" },
            { "data": "creditor" },
            { "data": "company" },
            { "data": "product_name" },
            { "data": "amount" },
            { "data": "terms" },
            { "data": "voucherno" },
            { "data": "release_dt" },
            { "data": "action" }
        ]       
    } );
  }

  function edit_loan(id)
  {
    var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };  
      location =  "<?php echo site_url('Loans/Loan_validation_dt/')?>?id=" + id+"&status=approved";
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function validate()
  {
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('Loans/ajax_validate/')?>/",
      type: "POST",
      data:{
        remarks: $("#remarks").val(),
        docus: upload_docus,
        id: $("#loan_id").val(),
      },
      dataType: "JSON",
      success: function(data)
      {           

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }

  function uploads() {
    var str = "<?php echo base_url();?>";
    var url = str + 'uploads/ ';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function(e, data) {
            $.each(data.result.files, function(key, value) {
                upload_docus.push(value.name);
            });
        },
        progressall: function(e, data) {
        },
        submit: function(e, data) {
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}

</script>
