<style type="text/css">    
   .bars, .chart, .pie {
   height: 100% !important;
   }
</style>
<div class="page-content-wrapper">
   <div class="page-content">
      <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
               <a href="index.html">Home</a>
               <i class="fa fa-circle"></i>
            </li>
            <li>
               <span>Privilege</span>
            </li>
         </ul>
         <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
               <i class="icon-calendar"></i>&nbsp;
               <span class="thin uppercase hidden-xs"></span>&nbsp;
               <i class="fa fa-angle-down"></i>
            </div>
         </div>
      </div>
      <br>

        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Privilege Listing</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="col-md-6">
                                                    <div class="btn-group">
                                                          <button class="btn btn-success" onclick="add_privilege()"><i class="glyphicon glyphicon-plus"></i> Add Privilege</button>
                                                    </div>
                                                </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                   <th class="all">Privilege Name</th>
                                                   <th class="all">Description</th>
                                                   <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

   </div>
</div>

            <script type="text/javascript">
              var save_method; //for save method string
              var table;
              $(document).ready(function() {
                list();
                //dropdown_list();
              });
              function list()
                {
                   table = $('#sample_1').DataTable( {
                      
                      "ajax": "<?php echo site_url('user_privileges/ajax_list')?>/",
                      "columns": [
                          { "data": "priv" },
                          { "data": "desc" },
                          { "data": "action" }
                      ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                  } );
                }

              function add_privilege()
              {
                save_method = 'add';
                $('#form')[0].reset(); // reset form on modals
                $('#modal_form').modal({backdrop:'static',keyboard: true}); // show bootstrap modal
                $('.modal-title').text('Add Privilege'); // Set Title to Bootstrap modal title
              }

              function edit_privilege(id)
              {
                save_method = 'update';
                $('#form')[0].reset(); // reset form on modals
                //Ajax Load data from ajax
                $.ajax({
                  url : "<?php echo site_url('user_privileges/ajax_edit/')?>/" + id,
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {           
                    $('[name="id"]').val(data.id);
                    $('[name="privilege_name"]').val(data.privilege_name);
                    $('[name="description"]').val(data.description);                                                                                         
                    $('#modal_form').modal({backdrop:'static',keyboard: true}); // show bootstrap modal when complete loaded
                    $('.modal-title').text('Edit Privilege'); // Set title to Bootstrap modal title
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data!');
                  }
                });
              }

              function save()
              {
                var url;
                if(save_method == 'add') {
                  url = "<?php echo site_url('user_privileges/ajax_add')?>";
                } else {
                  url = "<?php echo site_url('user_privileges/ajax_update')?>";
                }
                // ajax adding data to database
                $.ajax({
                  url : url,
                  type: "POST",
                  data: $('#form').serialize(),
                  dataType: "JSON",
                  success: function(data)
                  {
                     //if success close modal and reload ajax table
                     $('#modal_form').modal('hide');
                      table.ajax.reload( null, false );
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in adding or updating data!');
                  }
                });
              }

              /*function delete_privilege(id)
              {
                if(confirm('Delete this data?'))
                {
                  // ajax delete data to database
                  $.ajax({
                    url : "<?php echo site_url('user_privileges/ajax_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                       //if success reload ajax table
                       $('#modal_form').modal('hide');
                       list();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                  });
                }
              }*/

              function delete_privilege(id)
              {                
                swal({
                  title: "Are you sure you want to privilege?",
                  //text: "Your will not be able to recover this imaginary file!",
                  type: "error",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Delete",
                  closeOnConfirm: false
                },
                function(){
                 $.ajax({
                        url : "<?php echo site_url('user_privileges/ajax_delete')?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                           table.ajax.reload( null, false );
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error in deleting data!');
                        }
                      }); 

                      swal("Deleted!", "Privilege", "error");
                });
              }

              /*function dropdown_list()
              {
                $('[name="group_id"]').empty();
                var a = "";
                var b = "";
                $.ajax({
                  url : "<?php echo site_url('contacts/ajax_dropdown')?>",
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {           
                      $('[name="group_id"]').append('<option> </option>');
                      for (var x = 0; x < data.data.length; x++) {
                        a = data.data[x][0];
                        b = data.data[x][1];
                        $('[name="group_id"]').append('<option value="' + a + '">' + b + '</option>');
                      };
                      $('[name="group_id"]').trigger('chosen:updated');
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data for dropdown list!');
                  }
                });
              }*/
            </script>

            <!-- MODAL FORM -->
            <div class="modal fade" id="modal_form" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">User Privilege Form</h3>
                  </div>
                  <div class="modal-body form">
                    <form action="#" id="form" class="form-horizontal">
                      <input type="hidden" value="" name="id"/> 
                      <div class="form-body">
                        <div class="form-group">
                          <label class="control-label col-md-3">Privilege Name</label>
                          <div class="col-md-9">
                            <input name="privilege_name" placeholder="" class="form-control" type="text">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Description</label>
                          <div class="col-md-9">
                            <input name="description" placeholder="" class="form-control" type="text">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!--- End of custom details-->
        </section><!-- /.content -->
 