<div class="page-content-wrapper">
   <div class="page-content">
      <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
               <a href="index.html">Home</a>
               <i class="fa fa-circle"></i>
            </li>
            <li>
               <span>Netpays</span>
            </li>
         </ul>
         <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
               <i class="icon-calendar"></i>&nbsp;
               <span class="thin uppercase hidden-xs"></span>&nbsp;
               <i class="fa fa-angle-down"></i>
            </div>
         </div>
      </div>
      <div class="clearfix"> </div>
      <br>
      <div class="row">
  <div class="col-md-12">
            <div class="portlet light bordered">
               <div class="portlet-body">
                                        <ul class="nav nav-tabs">
                                            <li id="tab_net" class="active">
                                                <a href="#tab_1_1" data-toggle="tab"> Netpays & Payments  </a>
                                            </li>
                                            <li id="tab_beg">
                                                <a href="#tab_1_2" data-toggle="tab"> Beginning </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                         <div class="tab-pane fade active in" id="tab_1_1">
                                                              <form method="post" action="<?php echo base_url() ?>Netpays/importnetpays" enctype="multipart/form-data">
                                                          <div class="row">
                                                              <div class="col-md-2">
                                                                  <label class="col-md-1 control-label">ARNO:</label>
                                                                  <input type="text" name="ar_no" class="form-control">
                                                              </div>
                                                              <div class="col-md-3">
                                                                  <label class="col-md-1 control-label">Total:</label>
                                                                  <input type="text" name="total" class="form-control">
                                                              </div>
                                                              <div class="col-md-4">
                                                                  <label class="col-md-1 control-label">Month:</label>
                                                                  <select style="width: 100%" data-placeholder="Choose a month..." name="month" class="form-control" id="month">
                                                                    <option value="1">Jan</option>
                                                                    <option value="2">Feb</option>
                                                                    <option value="3">Mar</option>
                                                                    <option value="4">Apr</option>
                                                                    <option value="5">May</option>
                                                                    <option value="6">Jun</option>
                                                                    <option value="7">Jul</option>
                                                                    <option value="8">Aug</option>
                                                                    <option value="9">Sep</option>
                                                                    <option value="10">Oct</option>
                                                                    <option value="11">Nov</option>
                                                                    <option value="12">Dec</option>
                                                                  </select>
                                                              </div>
                                                              <div class="col-md-3">
                                                                  <label class="col-md-1 control-label">Type:</label>
                                                                <select style="width: 100%" data-placeholder="Choose a type..." name="type" class="form-control" id="type">
                                                                  <option value="1st">First</option>
                                                                  <option value="2nd">Second</option>
                                                                </select>
                                                              </div>
                                                          </div>
                                                      <h4 class="block"> &nbsp;</h4>
                                                          <div class="row">
                                                              <div class="col-md-2">
                                                                <label class="col-md-1 control-label">Particulars:</label>
                                                                  <textarea class="form-control" name="particulars"></textarea>
                                                              </div>
                                                              <!-- <div class="col-md-3">
                                                                <label class="col-md-1 control-label">Classification:</label>
                                                                <select style="width: 100%" data-placeholder="Choose a month..." name="emp_class" class="form-control" id="emp_class">
                                                                  <?php foreach($class as $row) { echo '<option value="'.$row->id.'">'.$row->class_name.'</option>'; } ?>
                                                                </select>
                                                              </div>
                                                              <div class="col-md-4">
                                                                <label class="col-md-1 control-label">Company:</label>
                                                                <select style="width: 100%" data-placeholder="Choose a month..." name="company" class="form-control" id="company">
                                                                  <?php foreach($company as $row) { echo '<option value="'.$row->id.'">'.$row->name.'</option>'; } ?>
                                                                </select>   
                                                              </div> -->
                                                              <div class="col-md-3">
                                                                <label class="col-md-1 control-label">&nbsp;</label>
                                                                  <input type="file" name="userfile" >  
                                                              </div>
                                                          </div>
                                                      <h4 class="block"> &nbsp;</h4>
                                                          <div class="row">
                                                              <div class="col-md-3">
                                                                <label class="col-md-1 control-label">&nbsp;</label>
                                                                <input type="submit" name="submit" value="UPLOAD" class="btn btn-primary">
                                                                <a class="btn dark btn-outline sbold uppercase" id="demo_5"> Confirm </a>
                                                              </div>
                                                          </div>    
                                                      </form>

                                                      <h4 class="block"> &nbsp;</h4>
                                                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                                         <thead id="thead1">
                                                             <tr>
                                                                 <th class="all">Creditor</th>
                                                                 <th class="all">Amount</th>
                                                                 <th class="all">Deduction</th>
                                                             </tr>
                                                         </thead>
                                                         <tbody id="body1">
                                                             
                                                         </tbody>
                                                         </table> 
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_2">
                                                    <form method="post" action="<?php echo base_url() ?>Netpays/importbeg" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label class="col-md-1 control-label">Reference No:</label>
                                                    <input type="text" name="reference_no" class="form-control">
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="col-md-1 control-label">Particulars:</label>
                                                    <textarea name="particulars" class="form-control" row="7">  </textarea>
                                                </div>                                               
                                                <div class="col-md-3">
                                                  <label class="col-md-1 control-label">&nbsp;</label>
                                                    <input type="file" name="userfile" class="form-control">  
                                                </div>
                                            </div>
                                        <h4 class="block"> &nbsp;</h4>
                                            <div class="row">
                                                <div class="col-md-2">
                                                  <label class="col-md-1 control-label">&nbsp;</label>
                                                  <input type="submit" name="submit" value="UPLOAD" class="btn btn-success">
                                                </div
                                            </div>
                                        </form>

                                                                                <h4 class="block"> &nbsp;</h4>
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                     <thead id="thead1">
                         <tr>
                             <th class="all">Creditor</th>
                             <th class="all">Reference</th>
                             <th class="all">Particulars</th>
                             <th class="all">Date</th>
                             <th class="all">Principal</th>
                             <th class="all">Interest</th>
                             <th class="all">Total Principal</th>
                             <th class="all">Total Interest</th>
                             <th class="all">Total</th>
                             <th class="all">Months Remaining</th>
                         </tr>
                     </thead>
                     <tbody id="body1">
                         
                     </tbody>
                     </table>   
                                            </div>
                                        </div>
                                        <div class="clearfix margin-bottom-20"> </div>
                                     
                                    </div>
            </div>
            <div class="portlet-title">
               <div class="tools"> </div>
            </div>
            <div class="portlet-body">
            </div>
         </div>
      </div>
      

   </div>
</div>
<script type="text/javascript">
var table;
  $(document).ready(function() {
       list();
       select2();
       $('.fileinput').fileinput();
       //$("#pp").select2();
       $('#demo_5').click(function(){
                bootbox.dialog({
                    message: "Are you sure you want to Upload this data?",
                    title: "Confirmation",
                    buttons: {
                      success: {
                        label: "Upload",
                        className: "green",
                        callback: function()
                        {
                            $.ajax({
                                url: "<?php echo site_url('Netpays/ajax_delete_uploads')?>",
                                type: "POST",
                                data: $('#form').serialize(),
                                dataType: "JSON",
                                success: function(data) {
                                    alert('Success');
                                    table.ajax.reload( null, false );
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    alert('Error adding / update data');
                                }
                            });
                        }
                      },
                      danger: {
                        label: "Cancel",
                        className: "red",
                        callback: function() {
                          $.ajax({
                                url: "<?php echo site_url('Netpays/ajax_cancel_uploads')?>",
                                type: "POST",
                                data: $('#form').serialize(),
                                dataType: "JSON",
                                success: function(data) {
                                    alert('Cancelled');
                                    table.ajax.reload( null, false );
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    alert('Error adding / update data');
                                }
                            });
                        }
                      }
                    }
                });
       });
  });

  function select2()
  {
    // Return today's date and time
    var currentTime = new Date();
    var years = [];
    // returns the year (four digits)
    var prev_year = currentTime.getFullYear()-1;
    var curr_year = currentTime.getFullYear();
    var future_year = currentTime.getFullYear()+1;
    years.push(prev_year);
    years.push(curr_year);
    years.push(future_year);
    $("#year").select2({
           data: years
    });
  }

  function list()
  {
     table = $('#sample_1').DataTable( {
        
        "ajax": "<?php echo site_url('Netpays/ajax_list_repo')?>", 
        "columns": [
            { "data": "creditor" },
            { "data": "amount" },
            { "data": "deduction" }
        ]
       
    } );

     var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                }

    if(getUrlParameter('ref')!=null)
    {
      $("#tab_net").removeClass('active');
      $("#tab_1_1").removeClass('active');
      $("#tab_beg").addClass('active');
      $("#tab_1_2").addClass('active');
      $("#tab_1_2").addClass('in');
      table = $('#sample_2').DataTable( {          
          "ajax": "<?php echo site_url('Netpays/ajax_list_repo_beg')?>/"+getUrlParameter('ref'), 
        dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
            'csvHtml5',
            'print',
        ],
          "columns": [
              { "data": "creditor" },
              { "data": "ref" },
              { "data": "particulars" },
              { "data": "date" },
              { "data": "principal" },
              { "data": "interest" },
              { "data": "tp" },
              { "data": "ti" },
              { "data": "total" },
              { "data": "months" }
          ]         
      } );
    }


  }

  function edit_class(id)
  {
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('Creditors/ajax_edit_classification/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {           
        $('[name="id"]').val(data.id);
        $('[name="class"]').val(data.class_name);
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Bracket'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }



function save()
{
   /*var url;
    if (save_method == 'add') {
        url = "<?php echo site_url('Creditors/ajax_add_creditors')?>";
    } else {
        url = "<?php echo site_url('Creditors/ajax_update_classification')?>";
    }*/
    $.ajax({
        url: "<?php echo site_url('Creditors/ajax_add_creditors')?>",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data) {
            alert('ok');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
        }
    });
}

function add_classification()
{
  save_method = 'add';
  //$('#form')[0].reset(); // reset form on modals
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add Bracket'); // Set Title to Bootstrap modal title
}

function delete_class(id)
{
    swal({
      title: "Are you sure you want to delete?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "error",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('Creditors/ajax_delete_classification')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table                     
                         table.ajax.reload( null, false );
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                });

          swal("Deleted!", "Bracket has been deleted.", "success");
    });
}

</script>
