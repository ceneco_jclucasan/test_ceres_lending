<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Login_m extends CI_Model
{
  /*function login($username, $password)
  { 
    $this->db->select('tblusers.*,tblemployees.firstname,tblemployees.lastname,tblemployees.code as emp_code');
    $this->db->from('tblusers');
    $this->db->join('tblemployees','tblemployees.id = tblusers.emp_id');
    $this->db->where('username', $username);
    $this->db->where('password', MD5($password));
    $this->db->limit(1);
   
    $query = $this->db->get();
   
    if($query -> num_rows() == 1)
    {
      return $query->result();
    }
    else
    {
      return false;
    }
  }*/

  function login($username, $password)
  { 
    $this->db->select('tblusers.*,creditors.firstname,creditors.lastname,creditors.employee_code as emp_code,creditors.picture');
    $this->db->from('tblusers');
    $this->db->join('creditors','creditors.id = tblusers.emp_id');
    $this->db->where('username', $username);
    $this->db->where('password', MD5($password));
    $this->db->limit(1);
   
    $query = $this->db->get();
   
    if($query -> num_rows() == 1)
    {
      return $query->result();
    }
    else
    {
      return false;
    }
  }

  function update_log($where, $data)
  {
    $this->db->update('tblusers', $data, $where);
    return $this->db->affected_rows();
  }

  function get_privileges_by_id($id)
  {
    $this->db->select('tblprivilegeusers.privilege_id, tblprivileges.privilege_name');
    $this->db->from('tblprivilegeusers');
    $this->db->join('tblprivileges','tblprivileges.id = tblprivilegeusers.privilege_id');
    $this->db->where('tblprivilegeusers.user_id',$id);
    $this->db->order_by('tblprivilegeusers.privilege_id','asc');
    $query = $this->db->get();
    return $query->result();
  }

  public function get_by_uname($uname)
  {
    $this->db->from('tblusers');
    $this->db->where('username',$uname);
    $query = $this->db->get();
    return $query->row();
  }

}
?>