<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_privileges_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables()
	{
		$this->db->from('tblprivileges');
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_id($id)
	{
		$this->db->from('tblprivileges');
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert('tblprivileges', $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update('tblprivileges', $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tblprivileges');
	}


}
