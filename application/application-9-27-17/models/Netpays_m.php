<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Netpays_m extends CI_Model {

	var $table = 'creditor_netpay';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_addressbook() {     
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
 
    function insert_csv($data) {
        $this->db->insert($this->table, $data);
    }

    function insert_csv_repo($data) {
        $this->db->insert('uploads', $data);
		return $this->db->insert_id();
    }

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('creditor_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_empid($id)
	{
		$this->db->from($this->table);
		$this->db->where('creditor_id',$id);
		//$this->db->order_by('year','desc');
		//$this->db->order_by('month','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_payroll_period($id,$creditor)
	{
		$this->db->from($this->table);
		$this->db->where('payroll_period',$id);
		$this->db->where('creditor_id',$creditor);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	function get_datatables()
	{
		$query = $this->db->query('SELECT * FROM creditor_netpay ORDER BY id desc'); 
		return $query->result();
	}

	function get_datatables_repo()
	{
		$query = $this->db->query('SELECT * FROM uploads ORDER BY id desc'); 
		return $query->result();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function delete_by_uplodid($id)
	{
		$this->db->where('uploadid', $id);
		$this->db->delete($this->table);
	}

	public function delete_by_uploads()
	{
		$this->db->empty_table('uploads');
	}

	function get_query_builder_result($qry)
	{
		$this->db->from($this->table);
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}
}