<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trans_loan_m extends CI_Model {

	var $table = 'transloan';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_by_id($id)
	{
		//$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,c.fullname,t.comaker1,t.comaker2,t.validators_remark,t.product_id,t.terms,t.creditor_id,t.created_on');
		$this->db->select('t.id,t.*,c.fullname');
		$this->db->from('transloan t');
		$this->db->join('creditors c','t.creditor_id = c.employee_code');
		$this->db->where('t.id', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_creditor($id)
	{
		$this->db->from('transloan');
		$this->db->where('creditor_id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_docu($id)
	{
		$this->db->from('transloan_docs');
		$this->db->where('transloan_id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	function get_datatables()
	{
		$query = $this->db->query('SELECT * FROM transloan ORDER BY id desc'); 
		return $query->result();
	}

	function get_datatables_payperiod()
	{
		$query = $this->db->query('SELECT * FROM payperiod ORDER BY id desc'); 
		return $query->result();
	}

	public function get_payperiod_id($id)
	{
		$this->db->from('payperiod');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_transloan_detail($id,$payperiod)
	{
		$this->db->from('transloan_details');
		$this->db->where('creditor_id',$id);
		$this->db->where('payperiod',$payperiod);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_tcreditor_loan_period($id)
	{
		$this->db->from('transloan_details');
		$this->db->where('creditor_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_loan_by_head($id)
	{
		$this->db->from('transloan_details');
		$this->db->where('transloan_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_loan_period($id)
	{
		$this->db->from('transloan_details');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	/*public function check_for_payment($id)
	{
		$this->db->from('transloan_details');
		$this->db->where('amount !=',null);
		$this->db->where('amount >=',1);
		$this->db->where('transloan_id',$id);
		$query = $this->db->get();
		return $query->result();
	}*/

	function check_for_payment($id)
	{
		$this->db->select('b.amount,h.or_arno,h.particulars,h.created_dt,t.id');
		$this->db->from('transloan_details t');
		$this->db->join('payment_body b','t.id = b.trans_details_id');
		$this->db->join('payment_head h','b.payment_head_id = h.id');
		$this->db->where('t.transloan_id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	function get_pending_app()
	{
		$this->db->select('t.id,t.created_on,t.applied_amount,t.terms,t.purpose,c.fullname,t.created_on,t.validators_remark,p.product_name,t.comaker1,t.comaker2');
		$this->db->from('transloan t');
		$this->db->join('creditors c','t.creditor_id = c.employee_code');
		$this->db->join('products p','t.product_id = p.id');
		$this->db->where('t.status', 'Pending');
		$this->db->order_by('t.id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_validated_app()
	{
		//$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,c.fullname,t.validate_on');
		$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,t.checker_remark,c.fullname,t.validate_on,p.product_name,t.comaker1,t.comaker2');
		$this->db->from('transloan t');
		$this->db->join('creditors c','t.creditor_id = c.employee_code');
		$this->db->join('products p','t.product_id = p.id');
		$this->db->where('t.status', 'Validated');
		$this->db->order_by('t.id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_checked_app()
	{
		//$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,c.fullname,t.validate_on');
		$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,t.checker_remark,t.approver_remark,c.fullname,t.validate_on,p.product_name,t.comaker1,t.comaker2');
		$this->db->from('transloan t');
		$this->db->join('creditors c','t.creditor_id = c.employee_code');
		$this->db->join('products p','t.product_id = p.id');
		$this->db->where('t.status', 'Checked');
		$this->db->order_by('t.id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_approved_loan()
	{
		//$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,c.fullname,t.validate_on');
		$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,c.fullname,c.emp_company,t.voucherno,t.release_on,t.validate_on,t.approved_on,p.product_name,t.comaker1,t.comaker2');
		$this->db->from('transloan t');
		$this->db->join('creditors c','t.creditor_id = c.employee_code');
		$this->db->join('products p','t.product_id = p.id');
		$this->db->where('t.status', 'Approved');
		$this->db->order_by('t.id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_released_loan()
	{
		//$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,c.fullname,t.validate_on');
		$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,c.fullname,c.emp_company,t.release_on,t.approved_amount,t.approved_term,t.validate_on,t.approved_on,p.product_name,t.comaker1,t.comaker2');
		$this->db->from('transloan t');
		$this->db->join('creditors c','t.creditor_id = c.employee_code');
		$this->db->join('products p','t.product_id = p.id');
		$this->db->where('t.status', 'Released');
		$this->db->order_by('t.id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_monitor_loan()
	{
		//$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,c.fullname,t.validate_on');
		$this->db->select('t.id,t.applied_amount,t.creditor_id,t.terms,t.purpose,c.fullname,c.emp_company,t.release_on,t.validate_on,t.approved_on,p.product_name,t.status,t.validate_on,t.validate_by,t.checked_on,t.checked_by,t.approved_on,t.approved_by,t.cancel_on,t.cancel_by,t.isDisbursed,t.isClosed');
		$this->db->from('transloan t');
		$this->db->join('creditors c','t.creditor_id = c.employee_code');
		$this->db->join('products p','t.product_id = p.id');
		$this->db->order_by('t.id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function save_transloan_details($data)
	{
		$this->db->insert('transloan_details', $data);
		return $this->db->insert_id();
	}

	public function update_transloan_details($where, $data)
	{
		$this->db->update('transloan_details', $data, $where);
		return $this->db->affected_rows();
	}

	public function get_amortization_schedule($id)
	{
		/*$this->db->from('transloan_details');
		$this->db->where('transloan_id', $id);
		$query = $this->db->get();
		return $query->result();*/

		//$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,c.fullname,t.validate_on');
		$this->db->select('b.month,b.year,b.level,a.interest,a.principal,a.balance');
		$this->db->from('payperiod b');
		$this->db->join('transloan_details a','b.id = a.payperiod');		
		$this->db->where('a.transloan_id', $id);
		$this->db->order_by('a.id','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_amortization_schedule_by_id($id)
	{
		/*$this->db->from('transloan_details');
		$this->db->where('transloan_id', $id);
		$query = $this->db->get();
		return $query->result();*/

		//$this->db->select('t.id,t.applied_amount,t.terms,t.purpose,c.fullname,t.validate_on');
		$this->db->from('transloan_details');	
		$this->db->where('transloan_id', $id);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_query_builder_result($qry)
	{
		$this->db->from($this->table);
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function add_docu($data)
	{
		$this->db->insert('transloan_docs', $data);
		return $this->db->insert_id();
	}

	public function save_payperiod($data)
	{
		$this->db->insert('payperiod', $data);
		return $this->db->insert_id();
	}

	function count_pending_loans()
	{
		$this->db->from($this->table);	
		$this->db->where('status','Pending');
		return $this->db->count_all_results();//$query->row();
	}

	function count_validated_loans()
	{
		$this->db->from($this->table);	
		$this->db->where('status','Validated');
		return $this->db->count_all_results();//$query->row();
	}

	function count_recommended_loans()
	{
		$this->db->from($this->table);	
		$this->db->where('status','Checked');
		return $this->db->count_all_results();//$query->row();
	}

	function count_approved_loans()
	{
		$this->db->from($this->table);	
		$this->db->where('status','Approved');
		return $this->db->count_all_results();//$query->row();
	}

}