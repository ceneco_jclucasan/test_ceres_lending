<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Domain_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_by_id($id)
	{
		$this->db->from('domain');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update('domain', $data, $where);
		return $this->db->affected_rows();
	}

	function get_datatables()
	{
		$query = $this->db->query('SELECT * FROM domain ORDER BY id ASC'); 
		return $query->result();
	}

	public function count_all()
	{
		$this->db->from("domain");
		return $this->db->count_all_results();
	}
	public function save($data)
	{
		$this->db->insert("domain", $data);
		return $this->db->insert_id();
	}

	public function delete_by_id_domain($id)
	{
		$this->db->where('id', $id);
		$this->db->delete("domain");
	}

	function get_query_builder_result_domain($qry)
	{
		$this->db->from("domain");
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}
}