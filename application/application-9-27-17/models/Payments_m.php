<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments_m extends CI_Model {

	var $table = 'payment_head';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_addressbook() {     
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
 
    function insert_head($data) {
        $this->db->insert($this->table, $data);
    }

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('creditor_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_trans_id($id)
	{
		$this->db->select('h.or_no,h.particulars,b.amount');
		$this->db->from('payment_body b');
		$this->db->join('payment_head h','b.payment_head_id = h.id');
		$this->db->where('b.trans_details_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function update_body($where, $data)
	{
		$this->db->update('payment_body', $data, $where);
		return $this->db->affected_rows();
	}

	function get_datatables_head()
	{
		$query = $this->db->query('SELECT * FROM payment_head ORDER BY id desc'); 
		return $query->result();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	
	public function save_head($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	
	public function save_body($data)
	{
		$this->db->insert('payment_body', $data);
		return $this->db->insert_id();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_query_builder_result($qry)
	{
		$this->db->from($this->table);
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function delete_by_uplodid($id)
	{
		$this->db->where('uploadid', $id);
		$this->db->delete('payment_body');
	}

	function get_by_payperiod($period)
	{
		$query = $this->db->query('SELECT * FROM payment_head where `payperiod` = "'.$period.'" ORDER BY id desc'); 
		return $query->result();
	}

	public function get_by_body_id($id)
	{
		$this->db->from('payment_body');
		$this->db->where('payment_head_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

}