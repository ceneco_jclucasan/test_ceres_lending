<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Creditors_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	//CREDITORS - CREDITORS
	public function get_by_id_creditors($id)
	{
		$this->db->from('creditors');
		$this->db->where('employee_code',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_creditors_loan_list($id)
	{
		$this->db->from('transloan');
		$this->db->where('creditor_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_comakers($id)
	{
		$this->db->from('creditors');
		$this->db->where('id !=',$id);
		//$this->db->where('lengthofservice >=',5);
		$query = $this->db->get();
		return $query->result();
	}

	public function update_creditors($where, $data)
	{
		$this->db->update('creditors', $data, $where);
		return $this->db->affected_rows();
	}

	/*function get_datatables_creditors()
	{
		$query = $this->db->query('SELECT * FROM creditors where lastname !="BERTUCIO" ORDER BY id ASC'); 
		return $query->result();
	}*/

	function get_datatables_creditors()
	{
		$query = $this->db->query('SELECT * FROM creditors ORDER BY id ASC'); 
		return $query->result();
	}

	function get_datatables_creditors_by_company()
	{
		$query = $this->db->query('SELECT count(`emp_company`) as counter,emp_company FROM `creditors` group by emp_company ORDER BY `id` DESC'); 
		return $query->result();
	}

	public function search_creditors($value)
	{
		$this->db->from('creditors');
		$this->db->like('lastname',$value);
		$this->db->or_like('firstname',$value);
		$this->db->or_like('employee_code',$value);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_all_creditors()
	{
		$this->db->from("creditors");
		return $this->db->count_all_results();
	}
	public function save_creditors($data)
	{
		$this->db->insert("creditors", $data);
		return $this->db->insert_id();
	}

	public function delete_by_id_creditors($id)
	{
		$this->db->where('id', $id);
		$this->db->delete("creditors");
	}

	function get_query_builder_result_creditors($qry)
	{
		$this->db->from("creditors");
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function count_all_qb_creditors($qry)
	{
		$this->db->from("creditors");
		$this->db->where($qry);
		return $this->db->count_all_results();
	}

	//CREDITORS - CLASS
	public function get_by_id_creditors_class($id)
	{
		$this->db->from('creditor_class');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update_creditors_class($where, $data)
	{
		$this->db->update('creditor_class', $data, $where);
		return $this->db->affected_rows();
	}

	function get_datatables_creditors_class()
	{
		$query = $this->db->query('SELECT * FROM creditor_class ORDER BY id ASC'); 
		return $query->result();
	}

	public function count_all_creditors_class()
	{
		$this->db->from("creditor_class");
		return $this->db->count_all_results();
	}
	public function save_creditors_class($data)
	{
		$this->db->insert("creditor_class", $data);
		return $this->db->insert_id();
	}

	public function delete_by_id_creditors_class($id)
	{
		$this->db->where('id', $id);
		$this->db->delete("creditor_class");
	}

	function get_query_builder_result_creditors_class($qry)
	{
		$this->db->from("creditor_class");
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function count_all_qb_creditors_class($qry)
	{
		$this->db->from("creditor_class");
		$this->db->where($qry);
		return $this->db->count_all_results();
	}

	//
	public function get_by_id_creditors_netpay($id)
	{
		$this->db->from('creditor_netpay');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update_creditors_netpay($where, $data)
	{
		$this->db->update('creditor_netpay', $data, $where);
		return $this->db->affected_rows();
	}

	function get_datatables_creditors_netpay()
	{
		$query = $this->db->query('SELECT * FROM creditor_netpay ORDER BY id ASC'); 
		return $query->result();
	}

	public function count_all_creditors_netpay()
	{
		$this->db->from("creditor_netpay");
		return $this->db->count_all_results();
	}
	public function save_creditors_netpay($data)
	{
		$this->db->insert("creditor_netpay", $data);
		return $this->db->insert_id();
	}
	
	public function delete_by_id_creditors_netpay($id)
	{
		$this->db->where('id', $id);
		$this->db->delete("creditor_netpay");
	}

	function get_query_builder_result_creditors_netpay($qry)
	{
		$this->db->from("creditor_netpay");
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function count_all_qb_creditors_netpays($qry)
	{
		$this->db->from("creditor_netpay");
		$this->db->where($qry);
		return $this->db->count_all_results();
	}

	//BRACKET
	public function get_by_id_creditors_bracket($id)
	{
		$this->db->from('class_bracket');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update_creditors_bracket($where, $data)
	{
		$this->db->update('class_bracket', $data, $where);
		return $this->db->affected_rows();
	}

	function get_datatables_creditors_bracket()
	{
		$query = $this->db->query('SELECT * FROM class_bracket ORDER BY id ASC'); 
		return $query->result();
	}

	public function count_all_creditors_bracket()
	{
		$this->db->from("class_bracket");
		return $this->db->count_all_results();
	}

	public function save_creditors_bracket($data)
	{
		$this->db->insert("class_bracket", $data);
		return $this->db->insert_id();
	}
	
	public function delete_by_id_creditors_bracket($id)
	{
		$this->db->where('id', $id);
		$this->db->delete("class_bracket");
	}

	public function get_bracket_creditors_bracket($id)
	{
		$this->db->from('class_bracket');
		$this->db->where('creditor_class_id',$id);
		/*$this->db->where('from_years <=',$year);
		$this->db->where('to_years >=',$year);*/
		$query = $this->db->get();
		return $query->result();
	}

	function get_query_builder_result_creditors_bracket($qry)
	{
		$this->db->from("class_bracket");
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_query_builder_result_creditors_bracket_join_class()
	{
		$this->db->select("b.from_years,b.to_years,b.amount,c.class_name,b.id");
		$this->db->from("class_bracket b");
		$this->db->join("creditor_class c","b.creditor_class_id = c.id");
		$query = $this->db->get();
		return $query->result();
	}

	public function count_all_qb_creditors_bracket($qry)
	{
		$this->db->from("creditor_netpay");
		$this->db->where($qry);
		return $this->db->count_all_results();
	}

}
