<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">

                        </div>
                        <!--START CONTENT HERE -JC -->
                          <div class="clearfix"> </div>
                        <br>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Loan List</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">TRN No</th>
                                                    <th class="all">Company</th>
                                                    <th class="all">Creditor</th>
                                                    <th class="all">Product</th>
                                                    <th class="all">Amount</th>
                                                    <th class="all">Status</th>
                                                    <th class="all">Validated By</th>
                                                    <th class="all">Date</th>
                                                    <th class="all">Recommended By</th>
                                                    <th class="all">Date</th>
                                                    <th class="all">Approved By</th>
                                                    <th class="all">Date</th>
                                                    <th class="all">Cancelled By</th>
                                                    <th class="all">Date</th>
                                                    <th class="all">Disbursed</th>
                                                    <th class="all">Paid</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!--END CONTENT-->
                        
                    </div>
                </div>

<script type="text/javascript">
  $(document).ready(function() {
    list();
  });

  function list()
  {
     table1 = $('#sample_1').DataTable( {        
        "ajax": "<?php echo site_url('Loans/ajax_list_loan_monitor')?>", 
        "order": [[ 0, "desc" ]],
        "columns": [
            { "data": "trn" },
            { "data": "company" },
            { "data": "creditor" },
            { "data": "product" },
            { "data": "amount" },
            { "data": "status" },
            { "data": "val_by" },
            { "data": "val_date" },
            { "data": "recommend_by" },
            { "data": "recommend_date" },
            { "data": "approved_by" },
            { "data": "approved_date" },
            { "data": "cancelled_by" },
            { "data": "cancelled_date" },
            { "data": "disbursed" },
            { "data": "paid" },
            { "data": "action" }
        ]
       
    } );
  }

  function edit_loan(id)
  {
    var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };  
      location =  "<?php echo site_url('Loans/Loan_validation_dt/')?>?id=" + id+"&status=view_detail";
  }

</script>
