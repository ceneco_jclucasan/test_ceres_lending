<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Loan Recommendation & Approval</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!--START CONTENT HERE -JC -->
                            <div class="modal-body"> 
                                                        <div class="portlet box blue">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-gift"></i>Loan Details</div>
                                                                <div class="tools">
                                                                    <!-- <a href="javascript:;" class="collapse"> </a>
                                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                                    <a href="javascript:;" class="reload"> </a>
                                                                    <a href="javascript:;" class="remove"> </a> -->
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <!-- BEGIN FORM-->
                                                                <form class="form-horizontal" role="form">
                                                                    <div class="form-body">
                                                                        <h3 class="form-section">Creditor's Information </h3>
                                                                        <div class="row">
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Transaction ID:</b></label>
                                                                                    <div class="col-md-2">
                                                                                        <p class="form-control-static" id="loan_termid">  </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Product:</b></label>
                                                                                    <div class="col-md-3">
                                                                                        <p class="form-control-static" id="loan_product">  </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Interest:</b></label>
                                                                                    <div class="col-md-3">
                                                                                        <p class="form-control-static" id="loan_interest">  </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div> -->
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Creditor ID:</b></label>
                                                                                    <div class="col-md-3">
                                                                                        <p class="form-control-static" id="loan_creditorid">  </p>
                                                                                        <input type="hidden" id="loan_creditorid2">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Name:</b></label>
                                                                                    <div class="col-md-7">
                                                                                        <p class="form-control-static" id="loan_name">  </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Branch:</b></label>
                                                                                    <div class="col-md-7">
                                                                                        <p class="form-control-static" id="loan_branch">  </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                                            
                                                                        <div class="row">
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Date Applied:</b></label>
                                                                                    <div class="col-md-4">
                                                                                        <p class="form-control-static" id="loan_tdateapplied">  </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Terms:</b></label>
                                                                                    <div class="col-md-3">
                                                                                        <p class="form-control-static" id="loan_terms">  </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                                          
                                                                        <div class="row">
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Comaker 1:</b></label>
                                                                                    <div class="col-md-5">
                                                                                        <p class="form-control-static" id="loan_comaker1">  </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Comaker 2:</b></label>
                                                                                    <div class="col-md-5">
                                                                                        <p class="form-control-static" id="loan_comaker2">  </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-4"><b>Purpose:</b></label>
                                                                                    <div class="col-md-5">
                                                                                        <p class="form-control-static" id="loan_purpose"> </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <!-- <h3 class="form-section is_hide" > Amortization Per Payroll Period</h3>
                                                                        <div class="row is_hide">
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-1">
                                                                            </div>
                                                                            <label class="control-label col-md-1"><b>Applied</b></label>
                                                                            <div class="col-md-1">
                                                                            </div>
                                                                            <label class="control-label col-md-1"><b>Prescribe</b></label>
                                                                            <div class="col-md-1">
                                                                            </div>
                                                                            <label class="control-label col-md-1 forapproved hidden"><b>Approved</b></label>
                                                                            <div class="col-md-1">
                                                                            </div>
                                                                        </div>
                                                                        <label class="control-label col-md-1 is_hide"><b>&nbsp;</b></label>
                                                                        <div class="row is_hide">
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Amount:</label>
                                                                                </div>
                                                                            </div>
                                                                  
                                                                        </div>
                                                                            <label classis_hide="control-label col-md-1 is_hide"><b>&nbsp;</b></label>
                                                                        <div class="row is_hide">
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Terms:</label>
                                                                                </div>
                                                                            </div>
                                                                           
                                                                        </div>        
                                                                            <label class="control-label col-md-1 is_hide"><b>&nbsp;</b></label>
                                                                        <div class="row is_hide">
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Interest:</label>
                                                                                </div>
                                                                            </div>
                                                                           
                                                                        </div>   
                                                                            <label class="control-label col-md-1 is_hide"><b>&nbsp;</b></label>
                                                                        <div class="row is_hide">
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Total Amount:</label>
                                                                                </div>
                                                                            </div>
                                                                           
                                                                        </div>           
                                                                            <label class="control-label col-md-1 is_hide"><b>&nbsp;</b></label>
                                                                        <div class="row is_hide">
                                                                            <div class="col-md-1">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Payable cutoff:</label>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                        <label class="control-label col-md-1 is_hide"><b>&nbsp;</b></label>
                                                                        <div class="row is_hide">                                                                          
                                                                            <div class="col-md-1">
                                                                            </div>
                                                                            <label class="control-label col-md-1">  
                                                                                    <?php
                                                                                      for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                        if ($privileges[$i]->privilege_name == "approved_applied_loan") 
                                                                                        {                
                                                                                          echo '<button type="button" id="btnforapplied" class="btn green btn-green forapproved hidden" data-dismiss="modal" onclick="selectApplied()"><i class="fa fa-check" aria-hidden="true"></i> Applied</button>';
                                                                                        }
                                                                                      }
                                                                                    ?> 
                                                                            </label>
                                                                            <div class="col-md-1">
                                                                            </div>
                                                                            <label class="control-label col-md-1">  
                                                                                    <?php
                                                                                      for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                        if ($privileges[$i]->privilege_name == "approved_prescribe_loan") 
                                                                                        {                
                                                                                          echo '<button type="button" id="btnforprescribe" class="btn blue btn-blue forapproved hidden"  onclick="selectPrescribe()"><i class="fa fa-check" aria-hidden="true"></i> Prescribe</button>';
                                                                                        }
                                                                                      }
                                                                                    ?>
                                                                            </label>
                                                                            <div class="col-md-1">
                                                                            </div>
                                                                            <label class="control-label col-md-1">  
                                                                                    <?php
                                                                                      for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                        if ($privileges[$i]->privilege_name == "approved_final_loan")
                                                                                        {                
                                                                                          echo '<button type="button" id="btnfinalloan" class="btn yellow btn-yellow forapproved hidden"  onclick="calculatefinalloan()"><i class="fa fa-check" aria-hidden="true"></i> Calculate</button>';
                                                                                        }
                                                                                      }
                                                                                    ?>
                                                                            </label>
                                                                        </div> -->
                                                                        <h4>Applied Amount</h4>
                                                                        <hr>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-1">Principal:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="applied_amount" name="applied_amount" class="form-control" readonly>
                                                                            </div>
                                                                            <label class="control-label col-md-2">Terms:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="d_terms"  name="applied_terms" class="form-control" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-1">Interest:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="d_interest"  name="applied_interest" class="form-control" readonly>
                                                                            </div>
                                                                            <label class="control-label col-md-2">Total Amount:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="d_total_amount" name="applied_total_amnt"  class="form-control" readonly>
                                                                            </div>
                                                                            <label class="control-label col-md-2">Payable Per Cut-off:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="d_cutoffpayment" name="applied_cutoff" class="form-control" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-1">&nbsp;</label>
                                                                            <div class="col-md-3">
                                                                                 <?php
                                                                                      for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                        if ($privileges[$i]->privilege_name == "approved_applied_loan") 
                                                                                        {                
                                                                                          echo '<button type="button" id="btnforapplied" class="btn green btn-green forapproved hidden" data-dismiss="modal" onclick="selectApplied()"><i class="fa fa-check" aria-hidden="true"></i> Applied</button>';
                                                                                        }
                                                                                      }
                                                                                    ?> 
                                                                            </div>
                                                                        </div>
                                                                        <h4>Prescribe Amount</h4>
                                                                        <hr>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-1">Principal:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="prescribe_amount" name="prescribe_amount" class="form-control" readonly>
                                                                            </div>
                                                                            <label class="control-label col-md-2">Terms:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="p_terms"  name="prescribe_terms" class="form-control" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-1">Interest:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="p_interest"  name="prescribe_interest" class="form-control" readonly>
                                                                            </div>
                                                                            <label class="control-label col-md-2">Total Amount:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="p_total_amount" name="precribe_total_amnt"  class="form-control" readonly>
                                                                            </div>
                                                                            <label class="control-label col-md-2">Payable Per Cut-off:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="p_cutoffpayment" name="prescribe_cutoff" class="form-control" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-1">&nbsp;</label>
                                                                            <div class="col-md-3">
                                                                                 <?php
                                                                                      for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                        if ($privileges[$i]->privilege_name == "approved_prescribe_loan") 
                                                                                        {                
                                                                                          echo '<button type="button" id="btnforprescribe" class="btn blue btn-blue forapproved hidden"  onclick="selectPrescribe()"><i class="fa fa-check" aria-hidden="true"></i> Prescribe</button>';
                                                                                        }
                                                                                      }
                                                                                    ?> 
                                                                            </div>
                                                                        </div>
                                                                        <h4 class="is_hide_approved">Approved Amount</h4>
                                                                        <hr>
                                                                        <div class="form-group is_hide_approved">
                                                                            <label class="control-label col-md-1">Principal:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="prescribe_amount"  name="approved_amount"  class="form-control" onclick="updatefield()">
                                                                            </div>
                                                                            <label class="control-label col-md-2">Terms:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="p_terms"   name="approved_terms" class="form-control" onclick="updatefield()">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group is_hide_approved">
                                                                            <label class="control-label col-md-1">Interest:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="p_interest"   name="approved_interest" class="form-control">
                                                                            </div>
                                                                            <label class="control-label col-md-2">Total Amount:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="p_total_amount"  name="approved_total_amnt"  class="form-control">
                                                                            </div>
                                                                            <label class="control-label col-md-2">Payable Per Cut-off:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="p_cutoffpayment" name="approved_cutoff"  class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <!-- <div class="form-group is_hide_approved">
                                                                            <label class="control-label col-md-1">Start Date:</label>
                                                                            <div class="input-group date col-md-2 dt_ndd">
                                                                              <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                                              <input type="text" class="form-control" name="loan_date" id="loan_date" />
                                                                            </div>
                                                                        </div> -->
                                                                        <div class="form-group is_hide_approved">
                                                                            <label class="control-label col-md-1">&nbsp;</label>
                                                                            <div class="col-md-3">
                                                                                 <?php
                                                                                      for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                        if ($privileges[$i]->privilege_name == "approved_final_loan")
                                                                                        {                
                                                                                          echo '<button type="button" id="btnfinalloan" class="btn yellow btn-yellow forapproved hidden"  onclick="calculatefinalloan()"><i class="fa fa-check" aria-hidden="true"></i> Calculate</button>';
                                                                                        }
                                                                                      }
                                                                                    ?>
                                                                            </div>
                                                                        </div>
                                                                        <h4 class="is_hide_released">Loan Summary</h4>
                                                                        <div class="form-group is_hide_released">                                                                        
                                                                        <hr>
                                                                            <!-- <label class="control-label col-md-1">First Deduction:</label>
                                                                            <div class="col-md-1">
                                                                                    <select style="width: 100%" data-placeholder="Choose a year..." name="pyear" class="form-control" id="pyear">
                                                                                    <option selected disabled>Choose Year</option>
                                                                                    </select>
                                                                            </div> -->
                                                                            <label class="control-label col-md-1">Month:</label>
                                                                            <div class="col-md-2">
                                                                                 <select style="width: 100%" data-placeholder="Choose a month..." name="pmonth" class="form-control" id="pmonth">
                                                                                    <option selected disabled>Choose Month</option>
                                                                                      <option value="1">Jan</option>
                                                                                      <option value="2">Feb</option>
                                                                                      <option value="3">Mar</option>
                                                                                      <option value="4">Apr</option>
                                                                                      <option value="5">May</option>
                                                                                      <option value="6">Jun</option>
                                                                                      <option value="7">Jul</option>
                                                                                      <option value="8">Aug</option>
                                                                                      <option value="9">Sep</option>
                                                                                      <option value="10">Oct</option>
                                                                                      <option value="11">Nov</option>
                                                                                      <option value="12">Dec</option>
                                                                                    </select>
                                                                            </div>
                                                                            <label class="control-label col-md-2">Type:</label>
                                                                            <div class="col-md-2">
                                                                                <select style="width: 100%" data-placeholder="Choose a type..." name="ptype" class="form-control" id="ptype">
                                                                                    <option selected disabled>Choose Deduction Type</option>
                                                                                      <option value="1">First</option>
                                                                                      <option value="2">Second</option>
                                                                                    </select>
                                                                            </div>
                                                                            <label class="control-label col-md-2">Class Type:</label>
                                                                            <div class="col-md-2">
                                                                                <select class="form-control" style="width:100%" id="class_type" name="class_type">
                                                                                    <option selected disabled>Choose Class</option>
                                                                                    <option vlaue="Class A">Class A</option>
                                                                                    <option vlaue="Class B">Class B</option>
                                                                                </select>
                                                                            </div>
                                                                            <!-- <label class="control-label col-md-2">Start Date:</label>
                                                                            <div class="input-group date col-md-2 dt_ndd">
                                                                              <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                                              <input type="text" class="form-control" name="loan_date" id="loan_date" />
                                                                            </div> -->
                                                                        </div>
                                                                        <div class="form-group is_hide_released">
                                                                            <label class="control-label col-md-1">Voucher No:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="voucher_no"   name="voucher_no" class="form-control">
                                                                            </div>
                                                                            <label class="control-label col-md-2">Check No:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="check_no"  name="check_no"  class="form-control">
                                                                            </div>
                                                                            <label class="control-label col-md-2">Bank Name:</label>
                                                                            <div class="col-md-2">
                                                                                <input type="text" id="bankname" name="bankname"  class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group is_hide_released">
                                                                            <label class="control-label col-md-1">&nbsp;</label>
                                                                            <div class="col-md-3">
                                                                                 <?php
                                                                                      for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                        if ($privileges[$i]->privilege_name == "release_loan")
                                                                                        {                
                                                                                          echo '<button type="button" id="btnfinalloanrelease" class="btn yellow btn-yellow forreleased"  onclick="releaseloan()"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Release</button>';
                                                                                        }
                                                                                      }
                                                                                    ?>
                                                                            </div>
                                                                        </div>
                                                                        <h3 class="form-section">Netpay History & Documents</h3>
                                                                        <div class="row ">
                                                                            <div class="col-md-6">
                                                                              <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="tblnetpay">
                                                                                    <thead id="thead1">
                                                                                        <tr>
                                                                                            <th class="all">Amount</th>
                                                                                            <th class="all">Date</th>
                                                                                            <th class="all">Remarks</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody id="body1">                                                                  
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                              <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="tbldocus">
                                                                                    <thead id="thead1">
                                                                                        <tr>
                                                                                            <th class="all">Filename</th>   
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody id="body1">                                                                  
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>   
                                                                        <br>
                                                                        <div style="display: none!important"></div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                              <table class="table table-striped table-bordered table-hover table-responsive" width="100%" id="tblAmortizationSchedule">
                                                                                    <thead id="thead1">
                                                                                        <tr>
                                                                                            <th class="all">Date</th>
                                                                                            <th class="all">Paid</th> 
                                                                                            <!-- <th class="all">Payment Type</th> --> 
                                                                                            <th class="all">Interest</th>
                                                                                            <th class="all">Prinicpal</th>
                                                                                            <th class="all">Balance</th>
                                                                                            <th class="all">Variance</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody id="body1">                                                                  
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>                                                      
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="row">
                                                                                    <div class="col-md-5">
                                                                                        <label class="control-label col-md-3">Remarks:</label>
                                                                                           <textarea class="form-control" name="checker_remarks" id="checker_remarks"></textarea>
                                                                                           <?php
                                                                                          for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                            if ($privileges[$i]->privilege_name == "checked_this_loan") 
                                                                                            {                
                                                                                              echo '<button type="button" id="btnChecked" onclick="checked_this()" class="btn green hidden">
                                                                                                <i class="fa fa-check"></i> Recommend</button>';
                                                                                            }
                                                                                          }
                                                                                        ?> 
                                                                                        <?php
                                                                                          for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                            if ($privileges[$i]->privilege_name == "approved_this_loan") 
                                                                                            {                
                                                                                              echo '<button type="button" id="btnApproved" onclick="approved_this()" class="btn green hidden">
                                                                                                <i class="fa fa-check"></i> Approved</button>';
                                                                                            }
                                                                                          }
                                                                                        ?>  
                                                                                        <button type="button" class="btn blue hidden" id="btnSavedChecked" onclick="save_checked()"><i class="fa fa-save"></i> Save</button>
                                                                                        <button type="button" class="btn blue hidden" id="btnSavedApproved" onclick="save_approved()"><i class="fa fa-save"></i> Save</button>
                                                                                        <button type="button" class="btn red" onclick="closePage()"><i class="fa fa-close"></i> Close</button>
                                                                                    </div>

                                                                                    <label class="control-label col-md-3">&nbsp;</label>
                                                                                    <div class="col-md-offset-5 col-md-8">
                                                                                        <!-- <?php
                                                                                          for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                            if ($privileges[$i]->privilege_name == "checked_this_loan") 
                                                                                            {                
                                                                                              echo '<button type="button" id="btnChecked" onclick="checked_this()" class="btn green hidden">
                                                                                                <i class="fa fa-check"></i> Recommend</button>';
                                                                                            }
                                                                                          }
                                                                                        ?> 
                                                                                        <?php
                                                                                          for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                            if ($privileges[$i]->privilege_name == "approved_this_loan") 
                                                                                            {                
                                                                                              echo '<button type="button" id="btnApproved" onclick="approved_this()" class="btn green hidden">
                                                                                                <i class="fa fa-check"></i> Approved</button>';
                                                                                            }
                                                                                          }
                                                                                        ?>  
                                                                                        <button type="button" class="btn blue hidden" id="btnSavedChecked" onclick="save_checked()"><i class="fa fa-save"></i> Save</button>
                                                                                        <button type="button" class="btn blue hidden" id="btnSavedApproved" onclick="save_approved()"><i class="fa fa-save"></i> Save</button>
                                                                                        <button type="button" class="btn red" onclick="closePage()"><i class="fa fa-close"></i> Close</button> -->
                                                                                    </div>
                                                                                </div>
                                                                                <!-- <div class="row">
                                                                                    <div class="col-md-offset-3 col-md-9">
                                                                                    <?php
                                                                                      for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                        if ($privileges[$i]->privilege_name == "checked_this_loan") 
                                                                                        {                
                                                                                          echo '<button type="button" id="btnChecked" onclick="checked_this()" class="btn green hidden">
                                                                                            <i class="fa fa-check"></i> Recommend</button>';
                                                                                        }
                                                                                      }
                                                                                    ?> 
                                                                                    <?php
                                                                                      for ($i=0; $i<sizeof($privileges); $i++) { 
                                                                                        if ($privileges[$i]->privilege_name == "approved_this_loan") 
                                                                                        {                
                                                                                          echo '<button type="button" id="btnApproved" onclick="approved_this()" class="btn green hidden">
                                                                                            <i class="fa fa-check"></i> Approved</button>';
                                                                                        }
                                                                                      }
                                                                                    ?>  
                                                                                    <button type="button" class="btn blue hidden" id="btnSavedChecked" onclick="save_checked()"><i class="fa fa-save"></i> Save</button>

                                                                                    <button type="button" class="btn blue hidden" id="btnSavedApproved" onclick="save_approved()"><i class="fa fa-save"></i> Save</button>
                                                                                        <button type="button" class="btn red" onclick="closePage()"><i class="fa fa-close"></i> Close</button>
                                                                                    </div>
                                                                                </div> -->
                                                                            </div>
                                                                            <div class="col-md-6"> </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                                <!-- END FORM-->
                                                            </div>
                                                        </div>
                                                    </div>
                        </div>                        
                    </div>
                </div>

<script type="text/javascript">
var applied_amount = 0;
var applied_terms = 0;
var applied_interest = 0;
var applied_total = 0;
var applied_payable = 0;

var prescribe_amount = 0;
var prescribe_terms = 0;
var prescribe_interest = 0;
var prescribe_total = 0;
var prescribe_payable = 0;

var get_creditors_bracket = 0;

var upload_docus = [];
  $(document).ready(function() {
    loan_detail();
    $(".dt_ndd").datetimepicker({
         format: "MMM DD, YYYY",
         locale: "en",
         allowInputToggle: true
      });
    select2s(); 
  });

  function select2s() {
      $("#first_deduc").select2();
      $("#class_type").select2();

      $("#pyear").select2();
      $("#pmonth").select2();
      $("#ptype").select2();

      // Return today's date and time
        var currentTime = new Date();
        var years = [];
        // returns the year (four digits)
        var prev_year = currentTime.getFullYear()-1;
        var curr_year = currentTime.getFullYear();
        var future_year = currentTime.getFullYear()+1;
        years.push(prev_year);
        years.push(curr_year);
        years.push(future_year);
        $("#pyear").select2({
               data: years
        });
  }
  
  function loan_detail()
  {
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };
                //Ajax Load data from ajax
                $.ajax({
                  url : "<?php echo site_url('Loans/ajax_get_loan_data/')?>/" + getUrlParameter('id'),
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {           
                    $('#loan_termid').text(data.data.id);  
                    $('#loan_product').text(data.product.product_name);
                    $('#loan_interest').text(data.data.interest);
                    $('#loan_creditorid').text(data.data.creditor_id);
                    $("#loan_creditorid2").val(data.data.creditor_id);
                    $('#loan_name').text(data.data.fullname);
                    $('#loan_branch').text(data.data.branch);
                    $('#loan_tdateapplied').text(data.data.created_on);  
                    $('#loan_terms').text(data.data.terms);
                    $('#loan_comaker1').text(data.comaker1.fullname);
                    $('#loan_comaker2').text(data.comaker2.fullname); 
                    $('#loan_purpose').text(data.data.purpose);    

                    var pc = Math.round(data.data.payable_cutoff * 100) / 100 ;
                    $('[name="applied_amount"]').val(numberWithCommas("₱ "+data.data.applied_amount));  
                    $('[name="applied_terms"]').val(data.data.terms+" months");  
                    $('[name="applied_interest"]').val(numberWithCommas("₱ "+data.data.interest_amount));
                    $('[name="applied_total_amnt"]').val(numberWithCommas("₱ "+data.data.total_amount));
                    $('[name="applied_cutoff"]').val(numberWithCommas("₱ "+pc));

                    var ppc = Math.round(data.data.prescribe_cutoff * 100) / 100 ;
                    $('[name="prescribe_amount"]').val(numberWithCommas("₱ "+data.data.suggested_amount));  
                    $('[name="prescribe_terms"]').val(data.data.terms+" months");  
                    $('[name="prescribe_interest"]').val(numberWithCommas("₱ "+data.data.prescribe_interest));
                    $('[name="precribe_total_amnt"]').val(numberWithCommas("₱ "+data.data.prescribe_total));
                    $('[name="prescribe_cutoff"]').val(numberWithCommas("₱ "+ppc));

                    get_creditors_bracket = data.data.creditor_id;

                    applied_amount = data.data.applied_amount;
                    applied_terms = data.data.terms;
                    applied_interest = data.data.interest_amount;
                    applied_total = data.data.total_amount;
                    applied_payable = pc;

                    prescribe_amount = data.data.suggested_amount;
                    prescribe_terms = data.data.terms;
                    prescribe_interest = data.data.prescribe_interest;
                    prescribe_total = data.data.prescribe_total;
                    prescribe_payable = ppc;

                    netpay(data.data.creditor_id);  
                    documents(data.data.id); 
                    if(getUrlParameter('status')=="validated")
                    {
                        $("#btnChecked").removeClass("hidden");
                        $("#btnSavedChecked").removeClass("hidden");
                        $("#checker_remarks").val(data.data.checker_remark);
                        $(".is_hide_approved").addClass("hidden");
                        $(".is_hide_released").addClass("hidden");
                    }

                    if(getUrlParameter('status')=="checked")
                    {
                        $("#btnApproved").removeClass("hidden");
                        $("#btnSavedApproved").removeClass("hidden"); 
                        $(".forapproved").removeClass("hidden"); 
                        $("#checker_remarks").val(data.data.approver_remark);                        
                        $(".is_hide_released").addClass("hidden");
                    }

                    if(getUrlParameter('status')=="approved")
                    {
                        $(".is_hide").addClass("hidden"); 
                        //amortization_schedule(getUrlParameter('id'));
                        $('[name="approved_amount"]').val(data.data.approved_amount);  
                        $('[name="approved_terms"]').val(data.data.approved_term);  
                        $('[name="approved_interest"]').val(data.data.approved_interest);
                        $('[name="approved_total_amnt"]').val(data.data.approved_total);
                        $('[name="approved_cutoff"]').val(data.data.approved_cutoff);  

                        $('[name="approved_amount"]').prop("disabled",true);  
                        $('[name="approved_terms"]').prop("disabled",true);  
                        $('[name="approved_interest"]').prop("disabled",true);
                        $('[name="approved_total_amnt"]').prop("disabled",true);
                        $('[name="approved_cutoff"]').prop("disabled",true);      
                    }

                    if(getUrlParameter('status')=="released"||getUrlParameter('status')=="view_detail")
                    {
                        $(".is_hide").addClass("hidden"); 
                        amortization_schedule(getUrlParameter('id'));
                        $('[name="approved_amount"]').val(data.data.approved_amount);  
                        $('[name="approved_terms"]').val(data.data.approved_term);  
                        $('[name="approved_interest"]').val(data.data.approved_interest);
                        $('[name="approved_total_amnt"]').val(data.data.approved_total);
                        $('[name="approved_cutoff"]').val(data.data.approved_cutoff); 

                       $('[name="approved_amount"]').prop("disabled",true);  
                        $('[name="approved_terms"]').prop("disabled",true);  
                        $('[name="approved_interest"]').prop("disabled",true);
                        $('[name="approved_total_amnt"]').prop("disabled",true);
                        $('[name="approved_cutoff"]').prop("disabled",true);   

                        $('[name="loan_date"]').val(data.data.start_date);   
                        $(".forreleased").addClass("hidden");
                        //$("#loan_date").data("DateTimePicker").date(new Date(data.start_date));    
                        var get_date_first_payment = data.data.first_deduct_sched;
                        var date_of_deduction = get_date_first_payment.split("-");
                        $('[name="pyear"]').select2().val(date_of_deduction[0]).trigger("change");
                        $('[name="pmonth"]').select2().val(date_of_deduction[1]).trigger("change");
                        $('[name="ptype"]').select2().val(date_of_deduction[2]).trigger("change");
                        $('[name="class_type"]').select2().val(data.data.class).trigger("change");
                        $('[name="voucher_no"]').val(data.data.voucherno);
                        $('[name="check_no"]').val(data.data.checked_on);
                        $('[name="bankname"]').val(data.data.bankname);
                    }
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    alert('Error in getting data!');
                  }
                });
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function checked_this()
  {
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                }

                swal({
                  title: "Are you sure you want to Recommend this Loan?",
                  //text: "Your will not be able to recover this imaginary file!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-warning",
                  confirmButtonText: "OK",
                  closeOnConfirm: false
                },
                function(){
                    $.ajax({
                      url : "<?php echo site_url('Loans/ajax_checked/')?>/",
                      type: "POST",
                      data:{
                        id: getUrlParameter('id'),
                      },
                      dataType: "JSON",
                      success: function(data)
                      {           
                        $("#btnChecked").prop("disabled",true);
                        $("#btnSaved").prop("disabled",true);
                        location = "<?php echo site_url('Loans/Loan_validation?status=checking/')?>";
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        alert('Backend Error!');
                      }
                    });
                    //swal("Deleted!", "Privilege", "error");
                });                
  }

  function save_checked()
  {
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                }
                //Ajax Load data from ajax
                $.ajax({
                  url : "<?php echo site_url('Loans/ajax_save_check_details/')?>/",
                  type: "POST",
                  data:{
                    checker_remarks: $("#checker_remarks").val(),
                    id: getUrlParameter('id'),
                  },
                  dataType: "JSON",
                  success: function(data)
                  {           
                    //toastr.success('Remarks Updated!','Message:');                    
                    location = location = "<?php echo site_url('Loans/Loan_validation?status=checking/')?>";
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    alert('Error in getting data!');
                  }
                });
  }

  function approved_this()
  {
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                }

                if(validate_approval_fields()==true)
                {
                    swal({
                      title: "Are you sure you want to Approve this Loan?",
                      //text: "Your will not be able to recover this imaginary file!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonClass: "btn-warning",
                      confirmButtonText: "OK",
                      closeOnConfirm: false
                    },
                    function(){
                        //Ajax Load data from ajax
                        $.ajax({
                          url : "<?php echo site_url('Loans/ajax_approved/')?>",
                          type: "POST",
                          data:{
                            id: getUrlParameter('id'),
                            amount: $('[name="approved_amount"]').val(),
                            interest: $('[name="approved_interest"]').val(),
                            total: $('[name="approved_total_amnt"]').val(),
                            terms: $('[name="approved_terms"]').val(),
                            cutoff: $('[name="approved_cutoff"]').val(),
                            pay_off_date: $('[name="loan_date"]').val(),
                            bank: $('[name="bankname"]').val(),
                            voucherno: $('[name="voucher_no"]').val(),
                            checkno: $('[name="check_no"]').val(),
                            class: $('[name="class_type"]').val(),
                            creditor: get_creditors_bracket,
                          },
                          dataType: "JSON",
                          success: function(data)
                          {           
                            $("#btnApproved").prop("disabled",true);
                            $("#btnSavedApproved").prop("disabled",true);
                            location = location = "<?php echo site_url('Loans/Loan_validation?status=approval/')?>";
                            //amortization_schedule(getUrlParameter('id'));
                          },
                          error: function (jqXHR, textStatus, errorThrown)
                          {
                            alert('Error in getting data!');
                          }
                        });
                        //swal("Deleted!", "Privilege", "error");
                    }); 
                }
                else{
                    toastr.error('Check Fields of Approval!','Message:'); 
                }               
  }

  function validate_approval_fields()
  {
      var principal = $('[name="approved_amount"]').val();
      var interest = $('[name="approved_interest"]').val();
      var total_amt = $('[name="approved_total_amnt"]').val();
      var terms = $('[name="approved_terms"]').val();
      var cutoff = $('[name="approved_cutoff"]').val();

      if(principal!=""&&interest!=""&&total_amt!=""&&terms!=""&&cutoff!="")
      {
        return true;
      }else{
        return false;
      }
  }

  function validate_release_fields()
  {
      var fd = $('[name="pyear"]').val();
      var fm = $('[name="pmonth"]').val();
      var ft = $('[name="ptype"]').val();
      var fc = $('[name="class_type"]').val();
      var vn = $('[name="voucher_no"]').val();
      var cn = $('[name="check_no"]').val();
      var bn = $('[name="bankname"]').val();
      //"" - for text fields
      //null for selects
      //remove fd because its current year
      if(fm!=null&&ft!=null&&fc!=null&&vn!=""&&cn!=""&&bn!="")
      {
        return true;
      }else{
        return false;
      }
  }

  function releaseloan()
  {
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                }
                if(validate_release_fields()==true)
                {
                swal({
                      title: "Are you sure you want to Release this Loan?",
                      //text: "Your will not be able to recover this imaginary file!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonClass: "btn-warning",
                      confirmButtonText: "OK",
                      closeOnConfirm: true
                    },
                    function()
                    {                     
                            //Ajax Load data from ajax
                            $.ajax({
                              url : "<?php echo site_url('Loans/ajax_released/')?>",
                              type: "POST",
                              data:{
                                id: getUrlParameter('id'),
                                creditor_id:$("#loan_creditorid2").val(),
                                amount: $('[name="approved_amount"]').val(),
                                interest: $('[name="approved_interest"]').val(),
                                total: $('[name="approved_total_amnt"]').val(),
                                terms: $('[name="approved_terms"]').val(),
                                cutoff: $('[name="approved_cutoff"]').val(),
                                pay_off_date: $('[name="loan_date"]').val(),
                                creditor: get_creditors_bracket,
                                year: $('[name="pyear"]').val(),
                                month: $('[name="pmonth"]').val(),
                                type: $('[name="ptype"]').val(),                    
                                class_type: $('[name="class_type"]').val(),
                                voucher_no: $('[name="voucher_no"]').val(),
                                bankname: $('[name="bankname"]').val(),
                                check_no: $('[name="check_no"]').val(),
                              },
                              dataType: "JSON",
                              success: function(data)
                              {

                                    $("#btnfinalloanrelease").prop("disabled",true);
                                    $("#btnApproved").prop("disabled",true);
                                    $("#btnSavedApproved").prop("disabled",true);
                                amortization_schedule(getUrlParameter('id'));
                              },
                              error: function (jqXHR, textStatus, errorThrown)
                              {
                                alert('Backend Error!');
                              }
                            });
                            //swal("Deleted!", "Privilege", "error");
                    });                
                }else{                    
                    toastr.error('CHECK EMPTY FIELDS!','Message:'); 
                }
  }

  function save_approved()
  {
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                }
                //Ajax Load data from ajax
                $.ajax({
                  url : "<?php echo site_url('Loans/ajax_save_approve_details/')?>/",
                  type: "POST",
                  data:{
                    checker_remarks: $("#checker_remarks").val(),
                    id: getUrlParameter('id'),
                  },
                  dataType: "JSON",
                  success: function(data)
                  {           
                    //toastr.success('Remarks Updated!','Message:');
                    location = "<?php echo site_url('Loans/Loan_validation?status=approval/')?>";
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    alert('Error in getting data!');
                  }
                });
  }

  function netpay(id)
  {
     table = $('#tblnetpay').DataTable( {        
        "ajax": '<?php echo site_url('Creditors/ajax_get_netpay')?>/'+id, 
        "columns": [
            { "data": "amount" },
            { "data": "date" },
            { "data": "remarks" },
        ]  } );
  }

  function documents(id)
  {
        $('#tbldocus').DataTable( {        
        "ajax": "<?php echo site_url('Loans/ajax_list_loan_app_documents/')?>/"+id, 
        "columns": [
            { "data": "filename" }
        ] } );
  }

  function selectApplied()
  {
        var pc = Math.round(applied_payable * 100) / 100 ;
        $('[name="approved_amount"]').val(applied_amount);  
        $('[name="approved_terms"]').val(applied_terms);  
        $('[name="approved_interest"]').val(applied_interest);
        $('[name="approved_total_amnt"]').val(applied_total);
        $('[name="approved_cutoff"]').val(pc);
  }

  function selectPrescribe()
  {
        var pc = Math.round(prescribe_payable * 100) / 100 ;
        $('[name="approved_amount"]').val(prescribe_amount);  
        $('[name="approved_terms"]').val(prescribe_terms);  
        $('[name="approved_interest"]').val(prescribe_interest);
        $('[name="approved_total_amnt"]').val(prescribe_total);
        $('[name="approved_cutoff"]').val(pc);
  }

  function calculatefinalloan()
  {
        var amount = $('[name="approved_amount"]').val();
        var terms = $('[name="approved_terms"]').val();
        var interest = (parseFloat(amount)* parseInt(terms))* parseFloat(0.01);
        var total = parseFloat(interest)+parseFloat(amount);
        var payable = parseFloat(total)/(parseInt(terms)*2);
        var pc = Math.round(payable * 100) / 100 ;
        $('[name="approved_interest"]').val(interest);
        $('[name="approved_total_amnt"]').val(total);
        $('[name="approved_cutoff"]').val(pc);
  }

  function amortization_schedule(id)
  {
        table = $('#tblAmortizationSchedule').DataTable( {        
                "ajax": "<?php echo site_url('Loans/ajax_get_amortization_schedule/')?>/"+id, 
                 "iDisplayLength": 100,
                 "order": [[ 3, "desc" ]],
                 dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                        'print',
                    ],
                "columns": [
                    { "data": "date" },
                    { "data": "status" },
                    { "data": "interest" },
                    { "data": "principal" },
                    { "data": "balance" },
                    { "data": "variance" },
                ]       
        } );
  }

  function closePage()
  {
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                }

                if(getUrlParameter('status')=="validated")
                {
                    location = "<?php echo site_url('Loans/Loan_validation?status=checking/')?>";
                }

                if(getUrlParameter('status')=="checked")
                {
                    location = "<?php echo site_url('Loans/Loan_validation?status=approval/')?>";
                }

                if(getUrlParameter('status')=="approved")
                {
                    location = "<?php echo site_url('Loans/Loan_disbursement/')?>";
                }

                if(getUrlParameter('status')=="released")
                {
                    location = "<?php echo site_url('Loans/Loan_approved/')?>";
                }

                if(getUrlParameter('status')=="view_detail")
                {
                    location = "<?php echo site_url('Loans/Loan_transaction_monitor/')?>";
                }                
  }

  function updatefield() {
      


$('[name="approved_interest"]').val('');
$('[name="approved_total_amnt"]').val('');
$('[name="approved_cutoff"]').val('');




  }

</script>
