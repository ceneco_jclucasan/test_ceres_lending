
<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">

                        </div>
                        <!--START CONTENT HERE -JC -->
                     <div class="clearfix"> </div>
                        <br>

<div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Period Listing</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="col-md-6">
                                                    <div class="btn-group">
                                                    <?php
                                                        for ($i=0; $i<sizeof($privileges); $i++) { 
                                                          if ($privileges[$i]->privilege_name == "add_payperiod") 
                                                          {                
                                                            echo ' <button class="btn btn-success" onclick="add_period()"><i class="glyphicon glyphicon-plus"></i>  Add Period</button>';
                                                          }
                                                        }
                                                      ?>
                                                        
                                                    </div>
                                                </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">Year</th>
                                                    <th class="all">Month</th>
                                                    <th class="all">Level</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        <!--END CONTENT-->
                        
                    </div>
                </div>

<script type="text/javascript">
  $(document).ready(function() {
       list();
       $("#Level").select2();
       $(".dt_ndd").datetimepicker({
          format: "YYYY-MM",
          locale: "en",
          allowInputToggle: true,
          viewMode: 'years',
        });
  });

  function list()
  {
     table = $('#sample_1').DataTable( {
        
        "ajax": "<?php echo site_url('Payperiod/ajax_list')?>", 
        "columns": [
            { "data": "year" },
            { "data": "month" },
            { "data": "level" },
            { "data": "action" }
        ]       
      });
  }

  function edit_period(id)
  {
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('Payperiod/ajax_edit_bracket/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {           
        $('[name="id"]').val(data.id);
        $('[name="fy"]').val(data.from_years);
        $('[name="ty"]').val(data.to_years);
        $('[name="amount"]').val(data.amount);
        $('[name="class"]').select2().val(data.creditor_class_id).trigger("change");
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Bracket'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }

  function save()
  {
     var url;
      if (save_method == 'add') {
          url = "<?php echo site_url('Payperiod/ajax_add')?>";
      } else {
          url = "<?php echo site_url('Payperiod/ajax_update')?>";
      }
      $.ajax({
          url: url,
          type: "POST",
          data: $('#form').serialize(),
          dataType: "JSON",
          success: function(data) {
              //if success close modal and reload ajax table
              $('#modal_form').modal('hide');
              table.ajax.reload( null, false );
          },
          error: function(jqXHR, textStatus, errorThrown) {
              alert('Error adding / update data');
          }
      });
  }

  function add_period()
  {
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('[name="class"]').select2().val('').trigger("change");
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add Bracket'); // Set Title to Bootstrap modal title
  }

  function delete_period(id)
  {
      swal({
        title: "Are you sure you want to delete?",
        //text: "Your will not be able to recover this imaginary file!",
        type: "error",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Delete",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
                      url: "<?php echo site_url('Payperiod/ajax_delete')?>/" + id,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                          //if success reload ajax table                  
                           table.ajax.reload( null, false );
                      },
                      error: function(jqXHR, textStatus, errorThrown)
                      {
                          alert('Error in deleting data!');
                      }
                  });

            swal("Deleted!", "Bracket has been deleted.", "success");
      });
  }

</script>

<div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Bracket Form</h3>
         </div>
         <div class="modal-body form">
            <form action="#" id="form" class="form-horizontal">
               <input type="hidden" value="" name="id"/> 
               <div class="form-body">
                  <div id="pw" class="form-group">
                     <label class="control-label col-md-3">From Year</label>
                     <div class="col-md-9">
                        <div class="input-group date col-md-12 dt_ndd">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            <input type="text" class="form-control" name="ym" id="ym" />
                          </div>
                     </div>

                  </div>
                  <div class="form-group">
                     <label class="control-label col-md-3">Level</label>
                     <div class="col-md-9">
                       <select class="form-control" name="Level" id="Level" style="width:100%">
                         <option selected disabled>Select Level</option>
                         <option value="1st">First Half</option>
                         <option value="2nd">Second Half</option>
                       </select>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
