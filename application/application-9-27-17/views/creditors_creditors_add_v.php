<div class="page-content-wrapper">
   <div class="page-content">
      <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
               <a href="<?php echo site_url(); ?>Dashboard">Home</a>
               <i class="fa fa-circle"></i>
            </li>
            <li>
               <a href="<?php echo site_url(); ?>Creditors/creditors_creditors"> Creditors</a>
               <i class="fa fa-circle"></i>
            </li>
               <span>Add</span>
            </li>
         </ul>
         <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
               <i class="icon-calendar"></i>&nbsp;
               <span class="thin uppercase hidden-xs"></span>&nbsp;
               <i class="fa fa-angle-down"></i>
            </div>
         </div>
      </div>
      <div class="clearfix"> </div>
      <br>
      <div class="row">
         <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
               <!-- PORTLET MAIN -->
               <div class="portlet light profile-sidebar-portlet ">
                  <!-- SIDEBAR USERPIC -->
                  <div class="profile-userpic">
                     <img id="pp" name="pp" src="../assets/pages/media/profile/default.jpg" class="img-responsive" alt=""> 
                  </div>
                  <!-- END SIDEBAR USERPIC -->
                  <!-- SIDEBAR USER TITLE -->
                  <div class="profile-usertitle">
                     <div class="profile-usertitle-name" id="pp_fullname">  </div>
                     <div class="profile-usertitle-job" id="pp_position"> </div>
                  </div>
                  <!-- END SIDEBAR USER TITLE -->
                  <!-- SIDEBAR BUTTONS -->
                  <div class="profile-userbuttons">
                    <!--  <button type="button" class="btn btn-circle green btn-sm">Update</button> -->
                      <input id="fileupload" type="file" name="files[]"  multiple>
                      
                      <button type="button" class="btn btn-circle green btn-sme" onclick="save()">Save</button>
                  </div>
                  <!-- END SIDEBAR BUTTONS -->
                  <!-- SIDEBAR MENU -->
                  <!-- <div class="profile-usermenu">
                     <ul class="nav">
                        <li>
                            <a href="javascript:;">
                                <i class="icon-home"></i> Overview </a>
                        </li>
                        </ul> 
                  </div> -->
                  <!-- END MENU -->
               </div>
               <!-- END PORTLET MAIN -->
               <!-- PORTLET MAIN -->
               <!-- END PORTLET MAIN -->
            </div>
            <div class="profile-content">
               <div class="row">
                  <div class="col-md-12">
                     <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                           <div class="caption caption-md">
                              <i class="icon-globe theme-font hide"></i>
                              <span class="caption-subject font-blue-madison bold uppercase">Creditor's Profile</span>
                           </div>
                           <ul class="nav nav-tabs">
                           <li class="active">
                              <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                           </li>
                           <li>
                              <a href="#tab_1_2" data-toggle="tab">Other Information</a>
                           </li>
                           <li>
                              <a href="#tab_1_3" data-toggle="tab">Netpay </a>
                           </li>
                           <li>
                              <a href="#tab_1_4" data-toggle="tab">Loans </a>
                           </li>
                           <li>
                              <a href="#tab_1_5" data-toggle="tab">Change Password </a>
                           </li>
                        </div>
                        <div class="portlet-body">
                           <div class="tab-content">
                              <!-- PERSONAL INFO TAB -->
                           <div class="tab-pane active" id="tab_1_1">
                                 <form role="form" action="#" id="form">
                                 <input type="text" id="id" name="id">
                              <input type="hidden" name="imageholder" id="imageholder">                  
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="control-label">First Name</label>
                                             <input type="text" id="fname" name="fname" class="form-control">
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="control-label">Middle Name</label>
                                             <input type="text" id="mname" name="mname" class="form-control">
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="control-label">Last Name</label>
                                             <input type="text" id="lname" name="lname" class="form-control">
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="control-label">Suffix</label>
                                             <input type="text" id="suffix" name="suffix" class="form-control">
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="control-label">Gender</label>
                                             <select class="form-control" id="gender" name="gender">
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                             </select>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="control-label">Date of Birth</label>                                             
                                             <div class="input-group date col-md-12 dt_ndd">
                                              <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                              <input type="text" class="form-control" name="dob" id="dob" />
                                            </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label class="control-label">Civil Status</label>
                                             <select class="form-control" id="civil" name="civil" data-placeholder="Choose a Category" tabindex="1">
                                                <option value="Single">Single</option>
                                                <option value="Married">Married</option>
                                                <option value="Widow">Widow</option>
                                             </select>
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <div class="radio-list">
                                                <label class="control-label">Age</label>
                                                <input type="number" id="age" name="age" class="form-control">
                                             </div>
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <h3 class="form-section">Complete Address</h3>
                                    <div class="row">
                                       <div class="col-md-12 ">
                                          <div class="form-group">
                                             <textarea class="form-control" id="address" name="address"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label>Mobile</label>
                                             <input type="text" id="mobo" name="mobo" class="form-control"> 
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label>Telephone</label>
                                             <input type="text" id="telno" name="telno" class="form-control"> 
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                       <div class="col-md-4">
                                          <div class="form-group">
                                             <label>No. of Dependents</label>
                                             <input type="text" id="no_dep" name="no_dep" class="form-control"> 
                                          </div>
                                       </div>
                                       <!--/span-->
                                       <div class="col-md-4">
                                          <div class="form-group">
                                             <label>No. of Studying Dependents</label>
                                             <input type="text" id="no_stud" name="no_stud" class="form-control"> 
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="form-group">
                                             <label>Home Ownership</label>
                                             <input type="text" class="form-control" id="hometype" name="hometype"> 
                                          </div>
                                       </div>
                                       <!--/span-->
                                    </div>
                              </div>
                              <!-- END PERSONAL INFO TAB -->
                              <!-- CHANGE AVATAR TAB -->
                              <div class="tab-pane" id="tab_1_2">
                              <h3 class="form-section">Employer</h3>
                              <div class="row">
                              <div class="col-md-3">
                              <div class="form-group">
                              <label class="control-label">Employer</label>
                              <input type="text" id="employer" name="employer" class="form-control">
                              </div>
                              </div>
                              <!--/span-->
                              <div class="col-md-3">
                              <div class="form-group">
                              <label class="control-label">Company</label>
                              <select class="form-control" id="emp_companh" name="emp_companh">
                                 <option value="VTI-Bacolod">VTI-Bacolod</option>
                              </select>
                              </div>
                              </div>
                              <!--/span-->
                              <div class="col-md-3">
                              <div class="form-group">
                              <label class="control-label">Position</label>
                              <input type="text" id="emp_position" name="emp_position" class="form-control">
                              </div>
                              </div>
                              <div class="col-md-3">
                              <div class="form-group">
                              <label class="control-label">Contact No</label>
                              <input type="text" id="emp_contactno" name="emp_contactno"  class="form-control">
                              </div>
                              </div>
                              <!--/span-->
                              </div>
                              <div class="row">
                              <div class="col-md-12 ">
                              <div class="form-group">
                              <label>Complete Address</label>
                              <textarea class="form-control" id="emp_address" name="emp_address"></textarea>
                              </div>
                              </div>
                              </div>
                              <h3 class="form-section">Employee</h3>
                              <div class="row">
                              <div class="col-md-6">
                              <div class="form-group">
                              <label class="control-label">Employee Code</label>
                              <input type="text" id="emp_code" name="emp_code" class="form-control">
                              </div>
                              </div>
                              <!--/span-->
                              <div class="col-md-6">
                              <div class="form-group">
                              <label class="control-label">Length of Service</label>
                              <input type="text" id="los" name="los" class="form-control">
                              </div>
                              </div>
                              <!--/span-->
                              </div>
                              <div class="row">
                              <div class="col-md-6">
                              <div class="form-group">
                              <label class="control-label">Classification</label>
                              <select class="form-control" id="class" name="class">
                              <?php foreach($class as $row) { echo '<option value="'.$row->id.'">'.$row->class_name.'</option>'; } ?>
                              </select>
                              </div>
                              </div>
                              <!--/span-->
                              <div class="col-md-6">
                              <div class="form-group">
                              <label class="control-label">Employed Date</label>
                              
                        <div class="input-group date col-md-12 dt_ndd">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            <input type="text" class="form-control" name="employeddate" id="employeddate" />
                          </div>
                              </div>
                              </div>
                              <!--/span-->
                              </div>
                              <div class="row">
                              <div class="col-md-4">
                              <div class="form-group">
                              <label class="control-label">Salary Level</label>
                              <input type="number" id="sal_level" name="sal_level" class="form-control confidential_data" >
                              <span class="help-block"><b><font color="red" class="confidential_msg">Confidential Salary Level</font></b></span>
                              </div>
                              </div>
                              <!--/span-->
                              <div class="col-md-4">
                              <div class="form-group">
                              <div class="radio-list">
                              <label class="control-label">Salary Daily</label>
                              <input type="number" id="sal_daily" name="sal_daily" class="form-control confidential_data">
                              <span class="help-block"><b><font color="red" class="confidential_msg">Confidential Salary Daily</font></b></span>
                              </div>
                              </div>
                              </div>
                              <div class="col-md-4">
                              <div class="form-group">
                              <div class="radio-list">
                              <label class="control-label">Salary Monthly</label>
                              <input type="number" id="sal_monthly" name="sal_monthly" class="form-control">
                              <span class="help-block"><b><font color="red" class="confidential_msg">Confidential Salary Monthly</font></b></span>
                              </div>
                              </div>
                              </div>
                              <!--/span-->
                              </div>
                              <div class="row">
                              <div class="col-md-4">
                              <div class="form-group">
                              <label>Supervisor Name</label>
                              <input type="text" id="super_name" name="super_name" class="form-control"> </div>
                              </div>
                              <!--/span-->
                              <div class="col-md-4">
                              <div class="form-group">
                              <label>Contact No</label>
                              <input type="text" id="super_contactno" name="super_contactno" class="form-control"> </div>
                              </div>
                              <div class="col-md-4">
                              <label>Active</label>
                              <div class="form-group">
                              <input type="checkbox" class="make-switch"> </div>
                              </div>
                              <!--/span-->
                              </div>
                              <!--/row-->
                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group">
                              <label>Remarks</label>
                              <textarea class="form-control" id="remarks" name="remarks"></textarea> </div>
                              </div>
                              </div>
                              <div class="form-actions right">
                              <button type="button" class="btn blue" onclick="save()">
                              <i class="fa fa-check"></i> Save</button>
                              </div>
                              </div>

                              <div class="tab-pane hidden" id="tab_1_3">
                                 <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">Amount</th>
                                                    <th class="all">Date</th>
                                                    <th class="all">Remarks</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                              </div>

                              <div class="tab-pane hidden" id="tab_1_4">
                                 <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">TRN</th>
                                                    <th class="all">Amount</th>
                                                    <th class="all">Status</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                              </div>

   </form>
                              <div class="tab-pane" id="tab_1_5">
                                 <form role="form" action="#" id="updatepw">
                                 <input type="hidden" id="id2" name="id2">
                                                                <div class="form-group">
                                                                    <label class="control-label">Current Password</label>
                                                                    <input type="password" class="form-control" name="curr_pws" id="curr_pws"/> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">New Password</label>
                                                                    <input type="password" class="form-control"  name="new_pw" id="new_pw"/> </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-type New Password</label>
                                                                    <input type="password" class="form-control"  name="re_pw" id="re_pw"/> </div>
                                                                <div class="margin-top-10">
                                                                    <button type="button" class="btn blue" onclick="change_password()" >Change Password </button>
                                                                    <button type="button" class="btn yellow" onclick="reset_password()" >Reset Password </button>
                                                                </div>
                                                            </form>
                              </div>
                           
                              <!-- END PRIVACY SETTINGS TAB -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <!-- END PROFILE CONTENT -->
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
var table;
  $(document).ready(function() {
       list();
       loadinfo();
       uploadImage();
       $('.fileinput').fileinput();
   
      $(".dt_ndd").datetimepicker({
         format: "MMM DD, YYYY",
         locale: "en",
         allowInputToggle: true
      });

      <?php
       if($id == 1)
         {  
      ?>
         $("#tab_1_5").addClass('hidden');
      <?php 
         }
      ?>
  });

  function list()
  {
      var getUrlParameter = function getUrlParameter(sParam) {
                       var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                           sURLVariables = sPageURL.split('&'),
                           sParameterName,
                           i;

                       for (i = 0; i < sURLVariables.length; i++) {
                           sParameterName = sURLVariables[i].split('=');
                           if (sParameterName[0] === sParam) {
                               return sParameterName[1] === undefined ? true : sParameterName[1];
                           }
                       }
                   };
                   if(getUrlParameter('id')!=''){
                        table = $('#sample_1').DataTable( {
           
                                   "ajax": "<?php echo site_url('Creditors/ajax_get_netpay')?>/"+getUrlParameter('id'), 
                                   "columns": [
                                       { "data": "amount" },
                                       { "data": "date" },
                                       { "data": "remarks" },
                                   ]
                                  
                               } );
                   }
        
  }

  function edit_class(id)
  {
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('Creditors/ajax_edit_creditors/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {           
        $('[name="id"]').val(data.id);
        $('[name="class"]').val(data.class_name);
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Bracket'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }



function save()
{
   var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };
   var url;
    if (getUrlParameter('id') == '0') {
        url = "<?php echo site_url('Creditors/ajax_add_creditors')?>";
    } else {
        url = "<?php echo site_url('Creditors/ajax_update_creditors')?>";
    }

    $.ajax({
        url: url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data) {
         if (getUrlParameter('id') == '0') {
              toastr.success('Information has been added!','Message:');
          } else {
             toastr.success('Information has been updated!','Message:');
          }
           
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
        }
    });
}

function add_creditor()
{
  save_method = 'add';
  //$('#form')[0].reset(); // reset form on modals
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add Bracket'); // Set Title to Bootstrap modal title
}

function delete_class(id)
{
    swal({
      title: "Are you sure you want to delete?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "error",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('Creditors/ajax_delete_classification')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table                     
                         table.ajax.reload( null, false );
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                });

          swal("Deleted!", "Bracket has been deleted.", "success");
    });
}

function loadinfo()
{
         var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };

            if(getUrlParameter('id')!=0||getUrlParameter('id')!=''){
               $.ajax({
                    url: "<?php echo site_url('Creditors/ajax_edit_creditors')?>/" + getUrlParameter('id'),
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {                        
                        $("#pp_fullname").append(data.fullname);
                        $("#pp_position").append(data.emp_position);
                        $("#fname").val(data.firstname);
                        $("#id").val(data.id);
                        $("#id2").val(data.id);
                        $("#mname").val(data.middlename);
                        $("#lname").val(data.lastname);
                        $("#suffix").val(data.suffix);
                        $("#gender").val(data.gender);
                        $("#dob").val(moment(data.birthdate).format('MMM DD, YYYY'));
                        $("#civil").val(data.civil_status);
                        $("#age").val(data.age);
                        $("#address").val(data.Address);
                        $("#mobo").val(data.mobileno);
                        $("#telno").val(data.telno);
                        $("#no_dep").val(data.no_dependents);
                        $("#no_stud").val(data.no_studying);
                        $("#hometype").val(data.home_type);

                        $(".confidential_data").prop("readonly",true);
                        //$(".confidential_msg").addClass('hidden');
                        <?php
                           for ($i=0; $i<sizeof($privileges); $i++) {
                                 if ($privileges[$i]->privilege_name == "view_salary_emp") {
                        ?>
                           $("#sal_level").val(data.salary_level);
                           $("#sal_daily").val(data.salary_daily);     
                           $("#sal_monthly").val(data.salary_monthly);                      
                           $(".confidential_data").prop("readonly",false);
                           $(".confidential_msg").addClass('hidden');
                        <?php
                              }
                            }
                        ?>

                        <?php
                           for ($i=0; $i<sizeof($privileges); $i++) {
                                 if ($privileges[$i]->privilege_name == "view_netpays") {
                        ?>
                           $("#tab_1_3").removeClass('hidden');
                        <?php
                              }
                            }
                        ?>

                        <?php
                           for ($i=0; $i<sizeof($privileges); $i++) {
                                 if ($privileges[$i]->privilege_name == "view_loans") {
                        ?>
                           $("#tab_1_4").removeClass('hidden');
                        <?php
                              }
                            }
                        ?>                        
  
                        $("#employeddate").val(moment(data.employed_date).format('MMM DD, YYYY'));
                        $("#employer").val(data.employer);
                        $("#emp_position").val(data.emp_position);
                        $("#emp_contactno").val(data.employer_contactno);
                        $("#emp_address").val(data.employer_address);
                        $("#emp_code").val(data.employee_code);
                        $("#los").val(data.lengthofservice);
                        $("#class").val(data.employee_class_id);
                        
                        
                        $("#super_name").val(data.supervisor_name);
                        $("#super_contactno").val(data.supervisor_contact);
                        $("#remarks").val(data.remarks);

                        $("#imageholder").val(data.picture);

                        if(data.picture!=null)
                        {
                            $("#pp").attr("src", "<?php echo base_url(); ?>uploads/files/"+data.picture);
                        }

                        table = $('#sample_2').DataTable( {        
                          "ajax": "<?php echo site_url('Creditors/ajax_creditors_loan')?>/"+data.employee_code, 
                          "columns": [
                              { "data": "trn" },
                              { "data": "amount" },
                              { "data": "status" },
                              { "data": "action" },
                          ]                         
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                });
            }  
}

function uploadImage() {
    var str = "<?php echo base_url();?>";
    var url = str + 'uploads/';
    $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            done: function(e, data) {
                $("#pp").attr("src", data.result.files[0].url);
                filename = data.result.files[0].name;
                // $("#imageholder").val(filename.replace(/\s/g, ''));
                $("#imageholder").val(filename);
            },
            progressall: function(e, data) {
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                alert(url);
            },
        }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

function numberWithCommas(x)
{
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function change_password()
{
   new_pw = $("#new_pw").val();
   re_pw =   $("#re_pw").val();

   if(new_pw!=re_pw)
   {

      toastr.error('Pasword did not match!','Message:');
   }

   if(new_pw==re_pw)
   {
      swal({
                      title: "Are you sure you want to change password?",
                      //text: "Your will not be able to recover this imaginary file!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonClass: "btn-success",
                      confirmButtonText: "Update",
                      closeOnConfirm: true
                    },
                    function(){
                        $.ajax({
                           url : "<?php echo site_url('Dashboard/cp/')?>",
                           type: "POST",
                           data: $('#updatepw').serialize(),
                           dataType: "JSON",
                           success: function(data)
                           {           
                              alert(data.status);
                           },
                           error: function (jqXHR, textStatus, errorThrown)
                           {
                             alert('Error in getting data!');
                           }
                         });
                        //swal("deleted!", "Employee has been deleted.", "success");
                    });
   }
}

function change_password()
{
   new_pw = $("#new_pw").val();
   re_pw =   $("#re_pw").val();

   if(new_pw!=re_pw)
   {

      toastr.error('Pasword did not match!','Message:');
   }

   if(new_pw==re_pw)
   {
      swal({
                      title: "Are you sure you want to change password?",
                      //text: "Your will not be able to recover this imaginary file!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonClass: "btn-success",
                      confirmButtonText: "Update",
                      closeOnConfirm: true
                    },
                    function(){
                        $.ajax({
                           url : "<?php echo site_url('Dashboard/cp/')?>",
                           type: "POST",
                           data: $('#updatepw').serialize(),
                           dataType: "JSON",
                           success: function(data)
                           {           
                              alert(data.status);
                           },
                           error: function (jqXHR, textStatus, errorThrown)
                           {
                             alert('Error in getting data!');
                           }
                         });
                        //swal("deleted!", "Employee has been deleted.", "success");
                    });
   }
}

function reset_password()
{
      swal({
                      title: "Are you sure you want to reset password?",
                      //text: "Your will not be able to recover this imaginary file!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonClass: "btn-success",
                      confirmButtonText: "Update",
                      closeOnConfirm: true
                    },
                    function(){
                        $.ajax({
                           url : "<?php echo site_url('Dashboard/reset_pass/')?>",
                           type: "POST",
                           data: $('#updatepw').serialize(),
                           dataType: "JSON",
                           success: function(data)
                           {           
                              alert(data.status);
                           },
                           error: function (jqXHR, textStatus, errorThrown)
                           {
                             alert('Error in getting data!');
                           }
                         });
                        //swal("deleted!", "Employee has been deleted.", "success");
                    });
   
}

</script>
