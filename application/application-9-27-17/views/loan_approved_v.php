<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Loans</span>
                                </li>
                            </ul>
                            <!-- <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div> -->
                        </div>
                       <!--  <h1 class="page-title"> Loan Validated
                        </h1> -->
                        <!--START CONTENT HERE -JC -->
                        <div class="clearfix"> </div>
                        <br>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Released Loans</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">TRN</th>
                                                    <th class="all">Creditor</th>
                                                    <th class="all">Product</th>
                                                    <th class="all">Amount</th>
                                                    <th class="all">Terms</th>
                                                    <th class="all">Co-maker</th>
                                                    <th class="all">Co-maker</th>
                                                    <th class="all">Purpose</th>
                                                    <th class="all">Date Approved</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>

<script type="text/javascript">
var upload_docus = [];
  $(document).ready(function() {
       list();
       uploads();
  });

  function list()
  {
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };
    var url = "";
    if(getUrlParameter('status')=="checking")
    {
      url = "<?php echo site_url('Loans/ajax_list_loan_approved')?>";
    }
    if(getUrlParameter('status')=="approval")
    {
      url = "<?php echo site_url('Loans/ajax_list_loan_checked')?>";
    }   

     table = $('#sample_1').DataTable( {        
        "ajax": "<?php echo site_url('Loans/ajax_list_loan_approved')?>", 
        "order": [[ 0, "desc" ]],
        "columns": [
            /*{ "data": "creditor" },
            { "data": "amount" },
            { "data": "terms" },
            { "data": "purpose" },
            { "data": "validated_dt"},
            { "data": "action" },*/
            { "data": "trn" },
            { "data": "creditor" },
            { "data": "product_name" },
            { "data": "amount" },
            { "data": "terms" },
            { "data": "com1" },
            { "data": "com2" },
            { "data": "purpose" },
            { "data": "released_dt" },
            { "data": "action" }
        ]       
    } );
  }

  function edit_loan(id)
  {
    var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };
   

      location =  "<?php echo site_url('Loans/Loan_validation_dt/')?>?id=" + id+"&status=released";

     
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function validate()
  {
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('Loans/ajax_validate/')?>/",
      type: "POST",
      data:{
        remarks: $("#remarks").val(),
        docus: upload_docus,
        id: $("#loan_id").val(),
      },
      dataType: "JSON",
      success: function(data)
      {           

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }

  function uploads() {
    var str = "<?php echo base_url();?>";
    var url = str + 'uploads/ ';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function(e, data) {
            $.each(data.result.files, function(key, value) {
                upload_docus.push(value.name);
            });
        },
        progressall: function(e, data) {
        },
        submit: function(e, data) {
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}

</script>

<div class="modal fade" id="modal_form_privilege" role="dialog">
              <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 class="modal-title-priv"> </h3>
                </div>
                <div class="modal-body form">
                      <table id="tblPriv" class="table table-striped table-bordered table-hover" 
                                            data-search="true" 
                                            data-pagination="true"
                                            data-page-size="5"
                                            data-show-export="true"
                                            data-mobile-responsive="true"
                                            data-sort-name = "priv_name"
                                            data-sort-order = "desc"
                                             data-url="<?php echo site_url('Privillege')?>"
                                            data-page-list="[5, 10, ALL]">
                                      <thead>
                                        <tr>
                                          <th data-field="priv_name"  data-sortable = "true">Privilege Names</th>
                                          <th data-field="desc"  data-sortable = "true">Description</th>
                                          <th data-field="action" style="width:125px;">Action</th>
                                        </tr>
                                      </thead>
                                </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->   
</div>


<!-- <div class="modal fade draggable-modal" id="draggable" tabindex="-1" role="basic" aria-hidden="true"> -->

 <div class="modal fade bs-modal-lg" id="draggable" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">&nbsp;</h4>
                                                    </div>
                                                    <div class="modal-body"> 
                                                        <div class="portlet box blue">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-gift"></i>Loan Checked</div>
                                                                <div class="tools">
                                                                    <!-- <a href="javascript:;" class="collapse"> </a>
                                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                                    <a href="javascript:;" class="reload"> </a>
                                                                    <a href="javascript:;" class="remove"> </a> -->
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <!-- BEGIN FORM-->
                                                                <form class="form-horizontal" role="form">
                                                                    <div class="form-body">
                                                                        <!-- <h2 class="margin-bottom-20"> Personal Information </h2> -->
                                                                        <h3 class="form-section">Loan Information </h3>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><b>Term ID:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 213 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><b>Product:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 12 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><b>Interest:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 12 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><b>Creditor ID:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 1234 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><b>Name:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 20.01.1984 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><b>Branch:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 20.01.1984 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                                            
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><b>Date Applied:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 2133 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><b>Terms:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 2133 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                                          
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-5"><b>Comaker 1:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 2133 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-5"><b>Comaker 2:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 2133 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><b>Purpose:</b></label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 2133 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <h3 class="form-section">Applied </h3>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Amount:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 213 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Terms:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 12 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Interest:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 1234 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Total:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 20.01.1984 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                                            
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Payable per cutoff:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 2133 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <!--/row-->
                                                                        <h3 class="form-section">Prescribe</h3>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Amount:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 12321 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Terms:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 12 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            
                                                                            <!--/span-->
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Interest:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 4435 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Total:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 457890 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--/span-->
                                                                        </div>
                                                                        <!--/row-->
                                                                        <div class="row">
                                                                            
                                                                            <!--/span-->
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Payable per cutoff:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 21321 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!--/span-->
                                                                        </div>
                                                                        <h3 class="form-section">Netpay History & Documents</h3>
                                                                        <div class="row">
                                                                        <div class="col-md-6">
                                                                          <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                                                                                <thead id="thead1">
                                                                                    <tr>
                                                                                        <th class="all">TRN</th>
                                                                                        <th class="all">Creditor</th>
                                                                                        <th class="all">Product</th>
                                                                                        <th class="all">Amount</th>
                                                                                        <th class="all">Terms</th>
                                                                                        <th class="all">Co-maker</th>
                                                                                        <th class="all">Co-maker</th>
                                                                                        <th class="all">Purpose</th>
                                                                                        <th class="all">Date Applied</th>
                                                                                        <th class="all">Action</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="body1">                                                                  
                                                                                </tbody>
                                                                            </table>
                                                                          </div>
                                                                        <div class="col-md-6">
                                                                          <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3">
                                                                                <thead id="thead1">
                                                                                    <tr>
                                                                                        <th class="all">TRN</th>
                                                                                        <th class="all">Creditor</th>
                                                                                        <th class="all">Product</th>
                                                                                        <th class="all">Amount</th>
                                                                                        <th class="all">Terms</th>
                                                                                        <th class="all">Co-maker</th>
                                                                                        <th class="all">Co-maker</th>
                                                                                        <th class="all">Purpose</th>
                                                                                        <th class="all">Date Applied</th>
                                                                                        <th class="all">Action</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="body1">                                                                  
                                                                                </tbody>
                                                                            </table>
                                                                          </div>
                                                                        </div>   
                                                                        <h3 class="form-section">Final Evauluation</h3>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Approved Amount:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 12321 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">First Evaluation Date:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 12 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3">Terms:</label>
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-control-static"> 12 </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                                       
                                                                    </div>
                                                                    <!--<div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="row">
                                                                                    <div class="col-md-offset-3 col-md-9">
                                                                                        <button type="submit" class="btn green">
                                                                                            <i class="fa fa-pencil"></i> Edit</button>
                                                                                        <button type="button" class="btn default">Cancel</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6"> </div>
                                                                        </div>
                                                                    </div>-->
                                                                </form>
                                                                <!-- END FORM-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-close" aria-hidden="true"></i> Close</button>
                                                        <button type="button" class="btn yellow"><i class="fa fa-check" aria-hidden="true"></i> Checked</button>
                                                        <button type="button" class="btn green"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Approved</button>
                                                        <button type="button" class="btn red"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i> Disapproved</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>