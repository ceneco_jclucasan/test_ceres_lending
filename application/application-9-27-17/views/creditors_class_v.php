<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Creditors</span>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Creditor's Classification</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                        <br>

                         <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Creditor's Classification List</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="col-md-6">
                                                    <div class="btn-group">
                                                         <button class="btn btn-success" onclick="add_classification()"><i class="glyphicon glyphicon-plus"></i>  Add Classification</button>
                                                    </div>
                                                </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">Classification</th>
                                                    <!-- <th class="all">Length of Service</th>
                                                    <th class="all">Maximum Loan Amount</th> -->
                                                    <th class="all">Payroll Type</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        
                    </div>
                </div>

<script type="text/javascript">
var table;
  $(document).ready(function() {
       list();
  });

  function list()
  {
     table = $('#sample_1').DataTable( {
        
        "ajax": "<?php echo site_url('Creditors/ajax_list_classification')?>", 
        "columns": [
            { "data": "class" },
            //{ "data": "years" },
            //{ "data": "amount" },
            { "data": "type" },
            { "data": "action" }
        ]
       
    } );
  }

  function edit_class(id)
  {
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
    $.ajax({
      url : "<?php echo site_url('Creditors/ajax_edit_classification/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {           
        $('[name="id"]').val(data.id);
        $('[name="class"]').val(data.class_name);
        $('[name="amount"]').val(data.maximum_amount);
        $('[name="los"]').val(data.length_of_service);
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Bracket'); // Set title to Bootstrap modal title
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });
  }



function save()
{
   var url;
    if (save_method == 'add') {
        url = "<?php echo site_url('Creditors/ajax_add_classification')?>";
    } else {
        url = "<?php echo site_url('Creditors/ajax_update_classification')?>";
    }
    $.ajax({
        url: url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data) {
            //if success close modal and reload ajax table
            $('#modal_form').modal('hide');
            table.ajax.reload( null, false );
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
        }
    });
}

function add_classification()
{
  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add Bracket'); // Set Title to Bootstrap modal title
}

function delete_class(id)
{
    swal({
      title: "Are you sure you want to delete?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "error",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('Creditors/ajax_delete_classification')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table                     
                         table.ajax.reload( null, false );
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                });

          swal("Deleted!", "Bracket has been deleted.", "success");
    });
}

</script>

<div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Classification Form</h3>
         </div>
         <div class="modal-body form">
             <form action="#" id="form" class="form-horizontal">
               <input type="hidden" value="" name="id"/> 
               <div class="form-body">                 
                  <div class="form-group">
                     <label class="control-label col-md-3">Classification</label>
                     <div class="col-md-9">
                        <input name="class" name="class" placeholder="" class="form-control" type="text">
                     </div>
                  </div> 
                  <!-- <div class="form-group">                    
                     <label class="control-label col-md-3">From</label>
                     <div class="col-md-9">
                        <input name="from" name="from" placeholder="" class="form-control" type="text">
                     </div>                     
                  </div>
                  <div class="form-group">                    
                     <label class="control-label col-md-3">To</label>
                     <div class="col-md-9">
                        <input name="to" name="to" placeholder="" class="form-control" type="text">
                     </div>                     
                  </div> 
                  <div class="form-group">
                     <label class="control-label col-md-3">Amount</label>
                     <div class="col-md-9">
                        <input name="amount" name="amount" placeholder="" class="form-control" type="text">
                     </div>
                  </div>  -->               
                  <div class="form-group">
                     <label class="control-label col-md-3">Classification</label>
                     <div class="col-md-9">
                        <select class="form-control" name="type" id="type">
                          <option value="Semi-Monthly">Semi-Monthly</option>
                          <option value="Monthly">Monthly</option>
                        </select>
                     </div>
                  </div>
               </div>
            </form> 
            <!-- <form method="post" action="<?php echo base_url() ?>Creditors/importcsv" enctype="multipart/form-data">
                    <input type="file" name="userfile" ><br><br>
                    <input type="submit" name="submit" value="UPLOAD" class="btn btn-primary">
                </form> -->
         </div>
         <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->