<div class="page-content-wrapper">
   <div class="page-content">
      <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
               <a href="<?php echo site_url(); ?>Dashboard">Home</a>
               <i class="fa fa-circle"></i>
            </li>
            <li>
               <span>Creditors</span>
            </li>
         </ul>
         <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
               <i class="icon-calendar"></i>&nbsp;
               <span class="thin uppercase hidden-xs"></span>&nbsp;
               <i class="fa fa-angle-down"></i>
            </div>
         </div>
      </div>
      <div class="clearfix"> </div>
      <br>
      <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Creditors Listing</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="col-md-6">
                                                    <div class="btn-group">                                                      
                                                      <a href="<?php echo site_url(); ?>Creditors/creditors_creditors_add?id=0" class="btn btn-success" ></i>  Add Creditor</a>
                                                    </div>
                                                </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">Company</th>
                                                    <th class="all">Name</th>
                                                    <th class="all">Classification</th>
                                                    <th class="all">Code</th>
                                                    <th class="all">Position</th>
                                                    <th class="all">Gender</th>
                                                    <th class="all">Civil Status</th>
                                                    <th class="all">Age</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
      
   </div>
</div>
<script type="text/javascript">
var table;
  $(document).ready(function() {
       list();
       $('.fileinput').fileinput();
  });

  function list()
  {
     table = $('#sample_1').DataTable( {
        
        "ajax": "<?php echo site_url('Creditors/ajax_list_creditors')?>", 
        "columns": [
            { "data": "company" },
            { "data": "fn" },
            { "data": "classfication" },
            { "data": "code" },
            { "data": "position" },
            { "data": "sex" },
            { "data": "cs" },
            { "data": "age" },
            { "data": "action" }
        ]
       
    } );
  }


function save()
{
   /*var url;
    if (save_method == 'add') {
        url = "<?php echo site_url('Creditors/ajax_add_creditors')?>";
    } else {
        url = "<?php echo site_url('Creditors/ajax_update_classification')?>";
    }*/
    $.ajax({
        url: "<?php echo site_url('Creditors/ajax_add_creditors')?>",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data) {
            alert('ok');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Error adding / update data');
        }
    });
}

function add_creditor()
{
  save_method = 'add';
  //$('#form')[0].reset(); // reset form on modals
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add Bracket'); // Set Title to Bootstrap modal title
}

function delete_creditor(id)
{
    swal({
      title: "Are you sure you want to delete?",
      //text: "Your will not be able to recover this imaginary file!",
      type: "error",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Delete",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
                    url: "<?php echo site_url('Creditors/ajax_delete_creditors')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                        //if success reload ajax table                     
                         table.ajax.reload( null, false );
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                });

          swal("Deleted!", "Bracket has been deleted.", "success");
    });
}

</script>
