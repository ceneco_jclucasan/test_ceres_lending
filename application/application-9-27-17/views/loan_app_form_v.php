
<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <!-- <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div> -->
                        </div>
                        <br>
                        <!--START CONTENT HERE -JC -->
                        <div class="tab-pane" id="tab_2">
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>New Loan Application </div>
                                                    <div class="tools">
                                                        <a href="javascript:;" class="collapse"> </a>
                                                        <!-- <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                        <a href="javascript:;" class="reload"> </a>
                                                        <a href="javascript:;" class="remove"> </a> -->
                                                    </div>
                                                </div>

                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                <br>
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                         <button class="btn btn-primary" onclick="search_creditor()"><i class="fa fa-search" aria-hidden="true"></i>  Search Creditor</button>
                                                    </div>
                                                </div>
                                                    <form action="#" class="form-horizontal">
                                                        <div class="form-body">                                                      
                                                            <h4 class="form-section">Creditor's Details</h4>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Name</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="fname" class="form-control" placeholder="">
                                                                            <span class="help-block hidden"> This is inline help </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Position</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="emp_position" class="form-control"> </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Employer</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="emp_employer" class="form-control"> </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Date of Employment</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="emp_doe" class="form-control"> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Length of Service</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="emp_length" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Employee Code</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="emp_idno" class="form-control"> </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row hidden">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Salary Level</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="emp_sal_level" class="form-control"> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Daily Rate</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="emp_daily_rate" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Monthly Rate</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="emp_mon_rate" class="form-control"> </div>
                                                                    </div>
                                                                </div>
                                                            </div>     

                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Name of Supervisor</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="emp_sup" class="form-control"> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" id="creditor_type">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Contact No.</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="emp_sup_con" name="emp_sup_con" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Purpose</label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" id="purpose" name="purpose" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <h4 class="form-section">Enter Desired Loan Amount</h4>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Amount</label>
                                                                        <div class="col-md-5">
                                                                            <input type="number" id="amount" class="form-control"> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Terms(month)</label>
                                                                        <div class="col-md-4">
                                                                            <div class="btn-group">
                                                                            <div class="input-group">
                                                                              <!--  <input type="text" class="form-control" width="10%" id="terms"> -->
                                                                               <select class="form-control" style="width:100%" id="terms" name="terms">
                                                                                <option value="12">12 months</option>
                                                                                <!-- <option value="18">18 months</option>
             -->                                                                    <option value="24">24 months</option>
                                                                              <!--   <option value="36">36 months</option> -->
                                                                                </select>
                                                                               <span class="input-group-btn">
                                                                               <button class="btn btn-primary" id="btnCalculate" disabled type="button" onclick="calculate2()"> <i class="fa fa-calculator" aria-hidden="true"></i> Calculate</button>
                                                                               </span>
                                                                            </div>
                                                                         </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> 

                                                            <div class="row hidden">
                                                                <div class="col-md-12">
                                                                    <table class="table table-striped table-bordered table-hover" id="tblamortization">
                                                                    <thead>
                                                                       <tr>
                                                                          <th> Date </th>
                                                                          <th> Interest </th>
                                                                          <th> Principal </th>
                                                                          <th> Balance </th>
                                                                       </tr>
                                                                    </thead>
                                                                    <tbody id="tbody1"></tbody>
                                                                 </table>
                                                                </div>
                                                            </div>
                                                            <h4 class="form-section">Loan Summary</h4>
                                                            <div class="row">         
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-2">Principal:</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="d_principal" class="form-control" readonly>
                                                                    </div>
                                                                    <label class="control-label col-md-2">Terms:</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="d_terms" class="form-control" readonly>
                                                                    </div>
                                                                </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-2">Interest:</label>
                                                                        <div class="col-md-2">
                                                                            <input type="text" id="d_interest" class="form-control" readonly>
                                                                        </div>
                                                                        <label class="control-label col-md-2">Total Amount:</label>
                                                                        <div class="col-md-2">
                                                                            <input type="text" id="d_total_amount" class="form-control" readonly>
                                                                        </div>
                                                                        <label class="control-label col-md-2">Payable Per Cut-off:</label>
                                                                        <div class="col-md-2">
                                                                            <input type="text" id="d_cutoffpayment" class="form-control" readonly>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                            <h4 class="form-section">Prescribe Loanable Amount</h4>
                                                            <div class="row">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-2">Principal:</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="p_principal" class="form-control" readonly>
                                                                    </div>
                                                                    <label class="control-label col-md-2">Terms:</label>
                                                                    <div class="col-md-2">
                                                                        <input type="text" id="p_terms" class="form-control" readonly>
                                                                    </div>
                                                                </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Interest:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="p_interest" class="form-control" readonly>
                                                                </div>
                                                                <label class="control-label col-md-2">Total Amount:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="p_total_amount" class="form-control" readonly>
                                                                </div>
                                                                <label class="control-label col-md-2">Payable Per Cut-off:</label>
                                                                <div class="col-md-2">
                                                                    <input type="text" id="p_cutoffpayment" class="form-control" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label col-md-2">Co-Maker1:</label>
                                                                <div class="col-md-3">
                                                                    <select class="form-control" style="width:100%" id="comaker1" name="comaker1">
                                                                    </select>
                                                                </div>
                                                                <label class="control-label col-md-2">Co-Maker2:</label>
                                                                <div class="col-md-3">
                                                                    <select class="form-control" style="width:100%" id="comaker2" name="comaker2">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            </div>
                                                        </div>                                                     
                                                            
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-3 col-md-9">
                                                                            <button type="button" onclick="save()" id="btnSaveLoan" disabled class="btn green">Save</button>
                                                                            <a href="<?php echo site_url(); ?>Dashboard" type="button" class="btn btn-danger">Close</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6"> </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>  
                                        </div>
                        <!--END CONTENT-->
                        
                    </div>
                </div>

<script type="text/javascript">
var table;
var year_of_service = 0;
 var emp_class = 0;
var get_creditor_id = 0;
var raw_suggested_amt = 0;
var p_interest = 0;
 var p_total = 0;
 var p_cutoff = 0;
  var a_interest = 0;
 var a_total = 0;
 var a_cutoff = 0;
 var emp_company;
  $(document).ready(function() {

  });

 function search_creditor()
 {
  $('#modal_form').modal({backdrop: 'static',keyboard: false});
  table = $('#tblcreditors').DataTable();
 }

function select()
{
    if($("#search_creditor").val()!="")
    {
       $('#tblcreditors').dataTable().fnDestroy();
        $('#tblcreditors').DataTable( {
            //"processing": true,
            //"retrieve": true,
            //"paging": true,
            //"serverSide": true,
            "ajax": "<?php echo site_url('Creditors/search_creditors')?>/"+$("#search_creditor").val(),
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "company" },
                { "data": "action" },
            ] 
        } ); 
    }else{
        toastr.error('Search Field is Empty!','Message:');
    }    
 }

function select_creditor(id)
{
    $.ajax({
          url : "<?php echo site_url('Creditors/ajax_edit_creditors')?>/" +id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {           
            $('#modal_form').modal('hide');
            /*$("#fname").val(data.fullname);
            $("#bday").val(data.firstname);
            $("#age").val(data.firstname);
            $("#sex").val(data.firstname);
            $("#spo_name").val(data.firstname);
            $("#no_of_dep").val(data.firstname);
            $("#no_stud").val(data.firstname);
            $("#home_add").val(data.firstname);*/
            //$("#home_type").val(data.firstname);
            //$("#contact").val(data.firstname);
            $("#fname").val(data.fullname);
            $("#emp_employer").val(data.employer);
            $("#emp_position").val(data.emp_position);
            $("#emp_address").val(data.employer_address);
            $("#emp_contact").val(data.employer_contactno);
            $("#emp_doe").val(data.employed_date);
            $("#emp_length").val(data.lengthofservice);
            $("#emp_idno").val(data.id);
            $("#emp_sal_level").val(data.salary_level);
            $("#emp_daily_rate").val(data.salary_daily);
            $("#emp_mon_rate").val(data.salary_monthly);
            $("#emp_sup").val(data.supervisor_name);
            $("#emp_sup_con").val(data.supervisor_contact);

            get_creditor_type(data.employee_class_id);
            get_creditor_id = data.employee_code;
            var year = data.employed_date;
            year_of_service = data.lengthofservice;// year.split("-");
            emp_class = data.employee_class_id;
            comakers(data.id);
            emp_company = data.emp_company;
            //Enable Calculate btn
            $("#btnCalculate").prop("disabled",false);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error in getting data!');
          }
    });
}

 function get_creditor_type(id)
 {
     $.ajax({
          url : "<?php echo site_url('Creditors/ajax_edit_classification')?>/" +id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {           
            $("#creditor_type").val(data.type);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error in getting data!');
          }
        });
 }

 function calculate()
 {
        $('#tbody1').empty();
        var amount = $('#amount').val();
        var terms = $('#terms').val();
        var interest = (parseFloat(amount)*parseInt(terms))*parseFloat(0.01);
        var f_interest = 0;
        var f_principal = 0;
        var f_payable = 0;
        var total = parseFloat(interest)+parseFloat(amount);

        var cd = moment().add(1, 'months');//moment();
        
        var currentDate =  moment(cd).format('MMMM, YYYY');

        if($("#creditor_type").val()=="Monthly")
        {
            f_interest = parseFloat(interest)/parseInt(terms);
            f_principal = parseFloat(amount)/parseInt(terms);        
            f_payable = parseFloat(f_principal)+parseFloat(f_interest);
            for(i=1;i<=terms;i++)
            {
                f_balance = parseFloat(total)-parseFloat(f_payable); 
                $('#tbody1').append('<tr><td>' + currentDate+ '</td><td>' + numberWithCommas(roundoff(f_interest)) + '</td><td>' + numberWithCommas(roundoff(f_principal)) + '</td><td>'+ numberWithCommas(roundoff(f_balance)) + '</td></tr>');
                total = f_balance;
                currentDate = cd.add(1, 'months').format('MMMM, YYYY');
            }
        }

        if($("#creditor_type").val()=="Semi-Monthly")
        {
            new_terms = terms*2;

            f_interest = parseFloat(interest)/parseInt(new_terms);
            f_principal = parseFloat(amount)/parseInt(new_terms);        
            f_payable = parseFloat(f_principal)+parseFloat(f_interest);
            for(i=1;i<=new_terms;i++)
            {
                f_balance = parseFloat(total)-parseFloat(f_payable); 
                $('#tbody1').append('<tr><td>' + currentDate+ '</td><td>' + numberWithCommas(roundoff(f_interest)) + '</td><td>' + numberWithCommas(roundoff(f_principal)) + '</td><td>'+ numberWithCommas(roundoff(f_balance)) + '</td></tr>');
                total = f_balance;
                currentDate = cd.add(1, 'months').format('MMMM, YYYY');
            }
        }     
        calculate2();            
 }

 function roundoff(num) {
    return Math.round(num * 100) / 100;
 }

 function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
 }

function calculate2()
{
      var amount = $("#amount").val();
      var terms = $("#terms").val();
      var interest = 0;

      interest = parseFloat(amount) * parseFloat(terms/100);
      total = parseFloat(interest) + parseFloat(amount);
      payable_cutoff = parseFloat(total) / (parseFloat(terms*2));

      a_cutoff = payable_cutoff;
      a_total = total;
      a_interest = interest;

        if($("#amount").val()!=""&&$("#terms").val()!="")
        {
          $("#d_interest").val(numberWithCommas(interest.toFixed(2)));
          $("#d_principal").val(numberWithCommas(parseFloat(amount)));
          $("#d_terms").val(terms);
          $("#d_total_amount").val(numberWithCommas(total.toFixed(2)));
          $("#d_cutoffpayment").val(numberWithCommas(payable_cutoff.toFixed(2))); 
        
            $.ajax({
               url: "<?php echo site_url('Creditors/ajax_get_bracket')?>/"+emp_class,//year_of_service[0],
               type: "POST",
               dataType: "JSON",
               success: function(data)
               {  
                      /*$("#prescribe_amt").val(numberWithCommas(data.amount));

                      p_interest = parseFloat(data.amount) * parseFloat(terms/100);
                      p_total = parseFloat(p_interest) + parseFloat(data.amount);
                      p_payable_cutoff = parseFloat(p_total) / (parseFloat(terms*2));
                      $("#p_principal").val(numberWithCommas(data.amount));
                      $("#p_terms").val(terms);
                      $("#p_interest").val(numberWithCommas(p_interest.toFixed(2)));
                      $("#p_total_amount").val(numberWithCommas(p_total.toFixed(2)));
                      $("#p_cutoffpayment").val(numberWithCommas(p_payable_cutoff.toFixed(2)));
                      raw_suggested_amt = data.amount;
                      p_cutoff = p_payable_cutoff;*/
                      //Enable Save btn

                      if(getAge(year_of_service)>=data[0].from_years&&getAge(year_of_service)<=data[0].to_years)
                            {
                                $("#prescribe_amt").val(numberWithCommas(data[0].amount));
                                p_interest = parseFloat(data[0].amount) * parseFloat(terms/100);
                                p_total = parseFloat(p_interest) + parseFloat(data[0].amount);
                                p_payable_cutoff = parseFloat(p_total) / (parseFloat(terms*2));
                                $("#p_principal").val(numberWithCommas(data[0].amount));
                                $("#p_terms").val(terms);
                                $("#p_interest").val(numberWithCommas(p_interest.toFixed(2)));
                                $("#p_total_amount").val(numberWithCommas(p_total.toFixed(2)));
                                $("#p_cutoffpayment").val(numberWithCommas(p_payable_cutoff.toFixed(2)));
                                raw_suggested_amt = data[0].amount;
                                p_cutoff = p_payable_cutoff;
                            }
                            else  if(getAge(year_of_service)>=data[1].from_years&&getAge(year_of_service)<=data[1].to_years)
                            {
                                $("#prescribe_amt").val(numberWithCommas(data[1].amount));
                                p_interest = parseFloat(data[1].amount) * parseFloat(terms/100);
                                p_total = parseFloat(p_interest) + parseFloat(data[1].amount);
                                p_payable_cutoff = parseFloat(p_total) / (parseFloat(terms*2));
                                $("#p_principal").val(numberWithCommas(data[1].amount));
                                $("#p_terms").val(terms);
                                $("#p_interest").val(numberWithCommas(p_interest.toFixed(2)));
                                $("#p_total_amount").val(numberWithCommas(p_total.toFixed(2)));
                                $("#p_cutoffpayment").val(numberWithCommas(p_payable_cutoff.toFixed(2)));
                                raw_suggested_amt = data[1].amount;
                                p_cutoff = p_payable_cutoff;
                            }
                            else  if(getAge(year_of_service)>=data[2].to_years)
                            {
                                $("#prescribe_amt").val(numberWithCommas(data[2].amount));
                                p_interest = parseFloat(data[2].amount) * parseFloat(terms/100);
                                p_total = parseFloat(p_interest) + parseFloat(data[2].amount);
                                p_payable_cutoff = parseFloat(p_total) / (parseFloat(terms*2));
                                $("#p_principal").val(numberWithCommas(data[2].amount));
                                $("#p_terms").val(terms);
                                $("#p_interest").val(numberWithCommas(p_interest.toFixed(2)));
                                $("#p_total_amount").val(numberWithCommas(p_total.toFixed(2)));
                                $("#p_cutoffpayment").val(numberWithCommas(p_payable_cutoff.toFixed(2)));
                                raw_suggested_amt = data[2].amount;
                                p_cutoff = p_payable_cutoff;
                            }

                      $("#btnSaveLoan").prop("disabled",false);
               },
               error: function(jqXHR, textStatus, errorThrown) {

                   console.log('Server Side Error!');
               }
           });  
        }else{
            toastr.error('Check Amount & Terms!','Message:');
        }
               
}

  function comakers(id)
  {
      var creditor = [];
      $.ajax({
               url: "<?php echo site_url('Creditors/get_comakers')?>/"+id,
               type: "POST",
               dataType: "JSON",
               success: function(data)
               {  
                     $("#comaker1,#comaker2").empty();
                     $('#comaker2').append($("<option selected disabled>Select Comaker 2</option>"));
                     $('#comaker1').append($("<option selected disabled>Select Comaker 1</option>"));
                     $.each(data, function(key, value) {
                        if(value.emp_company==emp_company){                          
                            if(getAge(value.employed_date)>=5)
                            {
                                creditor.push({text:value.fullname,id:value.employee_code});
                            }     
                        }                        
                     });
                     $("#comaker1,#comaker2").select2({
                             data: creditor
                      });
               },
               error: function(jqXHR, textStatus, errorThrown) {
                   console.log('Server Side Error!');
               }
           });       
  }

  function save()
  {
        var fieldStat1 = 0;
        var fieldStat2 = 0;
        var fieldStat3 = 0;
        if($("#comaker1").val()==$("#comaker2").val())
        {
           toastr.error('COMAKERS SHOULD NOT BE THE SAME!','Message:'); 
           fieldStat1 = 1;
        }else{
            fieldStat1 = 0;
        }

        if($("#comaker1").val()==null)
        {
           toastr.error('COMAKER 1 SHOULD NOT BE THE EMPTY!','Message:'); 
           fieldStat2 = 1;
        }else{
            fieldStat2 = 0;
        }

        if($("#comaker2").val()==null)
        {
           toastr.error('COMAKER 2 SHOULD NOT BE THE EMPTY!','Message:'); 
           fieldStat3 = 1;
        }else{
            fieldStat3 = 0;
        }

        if(fieldStat1==0&&fieldStat2==0&&fieldStat3==0)
        {
                $.ajax({
                   url: "<?php echo site_url('ElectronicForm/ajax_saved_loan_ef')?>/",
                   type: "POST",
                   data:{
                        product_id: 1,
                        creditor_id: get_creditor_id,
                        applied_amt: $("#amount").val(),
                        suggested_amt: raw_suggested_amt,
                        pres_interest: p_interest ,
                        pres_total: p_total ,
                        pres_cutoff: p_cutoff ,
                        app_interest: a_interest,
                        app_total: a_total,
                        app_cutoff: a_cutoff,
                        terms: $("#terms").val(),
                        purpose: $("#purpose").val(),
                        //payment_opt: $('input[name="payment_opt"]:checked').val(),
                        comaker1: $("#comaker1").val(),
                        comaker2: $("#comaker2").val(),
                        //remarks: $("#remarks").val()
                   },
                   dataType: "JSON",
                   success: function(data)
                   {  
                        toastr.info('Loan details Saved!','Message:');
                        location = "<?php echo site_url('ElectronicForm/LoanForm')?>/";
                   },
                   error: function(jqXHR, textStatus, errorThrown) {

                       console.log('Server Side Error!');
                   }
               });
        }
  }

  function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
  }
</script>

<div class="modal fade" id="modal_form" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title-priv"> </h3>
         </div>
         <div class="modal-body form">
            <div class="portlet light portlet-fit portlet-datatable bordered">
               <div class="portlet-title">
                  <div class="col-md-8">
                     <div class="btn-group">
                        <div class="input-group">
                           <span class="input-group-btn">
                           <button class="btn btn-primary" type="button" onclick="select()"><i class="fa fa-search-plus" aria-hidden="true"></i></button>
                           </span>
                           <input type="text" class="form-control" placeholder="Firstname, Lastname or Employee Code" id="search_creditor">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="portlet-body">
                  <div class="table-container">
                     <table class="table table-striped table-bordered table-hover" id="tblcreditors">
                        <thead>
                           <tr>
                              <th> ID </th>
                              <th> Name </th>
                              <th> Company </th>
                              <th> Action </th>
                           </tr>
                        </thead>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div> 
</div>