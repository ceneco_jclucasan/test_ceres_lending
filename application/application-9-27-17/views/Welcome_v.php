<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            BIMS
            <small>(Barangay Information Management System)</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        </br>
        <div class="row">
          <a href="<?php echo base_url(); ?>NewBimsea"><div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3>BEA</h3>
                <p>BIMS Electronic Assistant</p>
              </div>
              <div class="icon">
                <i class="fa fa-lightbulb-o"></i>
              </div>
            </div></a>
          </div><!-- ./col -->
          <a href="<?php echo base_url(); ?>Registration">
          <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
              <div class="inner">
               <h3>Brgy. Clearance</h3>
                <p>New Resident Registration</p>
              </div>
              <div class="icon">
                <i class="fa fa-pencil-square-o"></i>
              </div>
            </div></a>
          </div><!-- ./col -->                  
          <a href="<?php echo base_url(); ?>Residents">
            <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
              <div class="inner">
                <h3>Vehicle Clearance</h3>
                <p>Registered Residents</p>
              </div>
              <div class="icon">
                <i class="fa fa-users"></i>
              </div>
            </div></a>
          </div><!-- ./col -->

           <a href="<?php echo base_url(); ?>Bimsea"><div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3>COI</h3>
                <p>Certificate of Indigency</p>
              </div>
              <div class="icon">
                <i class="fa fa-lightbulb-o"></i>
              </div>
            </div></a>
          </div><!-- ./col -->
          <a href="<?php echo base_url(); ?>Registration">
          <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>COR</h3>
                <p>Certificate of Resedency</p>
              </div>
              <div class="icon">
                <i class="fa fa-pencil-square-o"></i>
              </div>
            </div></a>
          </div><!-- ./col -->                  
          <a href="<?php echo base_url(); ?>Residents">
            <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
              <div class="inner">
                <h3>Others</h3>
                <p>Other Certificate</p>
              </div>
              <div class="icon">
                <i class="fa fa-users"></i>
              </div>
            </div></a>
          </div><!-- ./col -->

           <a href="<?php echo base_url(); ?>Bimsea"><div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-maroon">
              <div class="inner">
                <h3>Appointment</h3>
                <p>BIMS Electronic Assistant</p>
              </div>
              <div class="icon">
                <i class="fa fa-lightbulb-o"></i>
              </div>
            </div></a>
          </div><!-- ./col -->
          <a href="<?php echo base_url(); ?>Registration">
          <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-teal">
              <div class="inner">
                <h3>Registration</h3>
                <p>New Resident Registration</p>
              </div>
              <div class="icon">
                <i class="fa fa-pencil-square-o"></i>
              </div>
            </div></a>
          </div><!-- ./col -->                  
          <a href="<?php echo base_url(); ?>Residents">
            <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>NEXT <i class="fa fa-arrow-right fa-3" aria-hidden="true"></i></h3>
                <p>&nbsp;</p>
              </div>
              <div class="icon">
                <i class="fa fa-users"></i>
              </div>
            </div></a>
          </div><!-- ./col -->
          


       
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-7 connectedSortable"></section>
        </div><!-- /.row -->         
      
       </section>            
        <!-- endMain content -->
         </div>
      </div>
          <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/select2/select2.min.css">
          <script src="<?php echo base_url();?>assets/plugins/select2/select2.min.js"></script>
          <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/switch/css/bootstrap-switch.min.css">
          <script src="<?php echo base_url();?>assets/plugins/switch/js/bootstrap-switch.min.js"></script>
          <script src="<?php echo base_url();?>js/vendor/jquery.ui.widget.js"></script> 
          <script src="<?php echo base_url();?>js/jquery.iframe-transport.js"></script>
          <script src="<?php echo base_url();?>js/jquery.fileupload.js"></script>
          <script type="text/javascript">

            var clrnc_id=null;
            $(document).ready(function() {
                $('#active').click(function() {
                    var myVal = ($('#active').is(':checked')) ? 1 : 0;
                    $('#is_active').val(myVal);
                    //alert('myVal is: ' + myVal);
                });
                $("#issmoker,#isAlcoholic,#allergy,#asthma,#polio,#diabetic,#hypertension,#heartdisease,#kidneydisease,#stroke").bootstrapSwitch({
                    onText: "Yes",
                    offText: "No"
                });
                //$("#image_holder").hide();
                //Datemask yyyy-mm-dd
                $("#datemask").inputmask("yyyy-mm-dd", {
                    "placeholder": "yyyy-mm-dd"
                });
               //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {
                    "placeholder": "mm/dd/yyyy"
                });

                //Money Euro

                $("[data-mask]").inputmask();
                $('#purpose').select2();
                $('#precinct').select2();
                $('#services').select2();
                $('#s2').select2();
                $('#civil_stat').select2();
                $('#work_status').select2();;
                //$('#cs').select2();
               // $('#resident_id').select2();
                $("#imageholder").val("");

                
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };

              clrnc_id = getUrlParameter('id');
           if(clrnc_id!=null){
               LoadInfo(clrnc_id);
               $("#btnCancel").removeClass("hidden");
               $("#btnRelease").removeClass("hidden");
           }

           

        });
  
  function modalClose () {
    location = location;
  }
            function Save() { 
                  
                  if(validation()!="empty"){
                    $.ajax({
                          url: url = "<?php echo site_url('Bimsea/ajax_add')?>",
                          type: "POST",
                          data: $('#form').serialize(),
                          dataType: "JSON",
                          success: function(data) {
                            $("#trans_id").val(data.id);
                            $("#counter").val(data.counter);
                            $('#messageModal').modal({
                                  backdrop: 'static',
                                  keyboard: false  // to prevent closing with Esc button (if you want this too)
                              });

                            $("#messageModal").modal("show");     
                             $('[name="services"]').select2().val(data.resident_id).trigger("change");
                              $('[name="purpose"]').select2().val(data.resident_id).trigger("change");
                             $('#form')[0].reset(); // reset form on modals                    
                          },
                          error: function(jqXHR, textStatus, errorThrown) {
                              //alert('Error adding / update data');
                              console.log('Upload Failed!');
                          }
                      });
                  }else{
                       var notify = $.notify('<strong>Erorr:</strong> Please fill up required fields!', {

                    type: 'danger',

                    allow_dismiss: true

                  });

                  }
                                            
            }


          function cancel() {  
                     $.ajax({
                          url: "<?php echo site_url('Brgy_Clearance_Entry/cancel_clearance')?>/"+clrnc_id,
                          type: "POST",
                          data: $('#form').serialize(),
                          dataType: "JSON",
                          success: function(data) {
                              alert("Record Successfully Cancelled!");
                          },
                          error: function(jqXHR, textStatus, errorThrown) {
                              //alert('Error adding / update data');
                             console.log('Upload Failed!');
                          }
                      });           

                

            }

           function release() {  
                   $.ajax({
                          url: "<?php echo site_url('Brgy_Clearance_Entry/release_clearance')?>/"+clrnc_id,
                          type: "POST",
                          data: $('#form').serialize(),
                          dataType: "JSON",
                          success: function(data) {
                                 location = "<?php echo base_url().'uploads/"+data.refno+".docx';?>";
                                  setTimeout(function(){ 
                                   if(location){
                                    location = "<?php echo site_url('Brgy_Clearance_List')?>";
                                    }
                                  }, 1000);
                          },
                           eror: function(jqXHR, textStatus, errorThrown) {
                              //alert('Error adding / update data');
                              console.log('Upload Failed!');
                          }
                      });     
                 //document.location.href = "<?php echo site_url('Brgy_Clearance_Entry/release_clearance')?>/"+clrnc_id;
            }


             function counter() {  
                $.ajax({
                    url: "<?php echo site_url('Services/ajax_edit/')?>/" + $('#services').val(),
                    type: "GET",
                    dataType: "JSON",
                    success: function(data) {
                        $('#counter_no').val(data.counter_number);
                        $('#cid').val(data.counter_number);
                        $('#services_id').val(data.id);

                        

                        if($('#services').val()==2||$('#services').val()==3||$('#services').val()==7){
                          $("#purpose").attr("disabled", "disabled"); 
                        }else{
                          $("#purpose").removeAttr("disabled"); 
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                       alert('Error get data from ajax');
                    }
                });               
            }

   
            function LoadInfo(id) {
                $.ajax({
                    url: "<?php echo site_url('Brgy_Clearance_Entry/ajax_edit/')?>/" + id,
                    type: "GET",
                    dataType: "JSON",

                    success: function(data) {
                        $("#imageholder").val("");
                        if (data != null) {
                            $('[name="id"]').val(data.id);
                            $('[name="resident_id"]').select2().val(data.resident_id).trigger("change");
                            $('[name="s1"]').select2().val(data.signatory1).trigger("change");
                            $('[name="s2"]').select2().val(data.signatory2).trigger("change");
                            $('[name="release_date"]').val(data.release_date);
                            $('[name="purpose"]').val(data.purpose);
                            if(data.status=='Cancelled'){
                                $("#btnRelease").addClass("hidden");
                                $("#btnSave").addClass("hidden");
                                $("#btnCancel").addClass("hidden");
                            }
                               $('[name="reerence_no"]').val(data.reference_no);

                        }

                    },

                    error: function(jqXHR, textStatus, errorThrown) {
                       alert('Error get data from ajax');
                    }
                });
            }       
        

            function validation() {
              var services = $("#services").val();
              var fname = $("#fullname").val();
              var purpose = $("#purpose").val();

              if(services=="" || fname=="" || purpose=="" ){
                  $(".req").addClass("has-error");
                  $($('#services').data('select2').$container).addClass('error');
                  $($('#purpose').data('select2').$container).addClass('error');
                  return "empty";
              }
          }

          function searchById () {           

               if($('#resident_id').val()!=""){
                $.ajax({
                    url: "<?php echo site_url('Residents_Entry/ajax_edit/')?>/" + $('#resident_id').val(),
                    type: "GET",
                    dataType: "JSON",
                    success: function(data) {
                        $('#fullname').val(data.data.fullname);
                        $("#picture").attr("src", "<?php echo base_url();?>uploads/files/" + data.data.picture);       
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                       alert('Error get data from ajax');
                    }
                }); 
               }                  

          }

          function searchByName() {
            $.ajax({
                url: "<?php echo site_url('Residents_Entry/ajax_get_data/')?>/",
                type: "POST",
                data: {
                    term: $("#fullname").val()
                },
                dataType: "JSON",
                success: function(data) {
                   console.log(data);
                    $('#resident_id').val(data.data.id);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error get data from ajax');
                }
            });
        }

    </script>