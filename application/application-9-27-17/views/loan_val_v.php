<style type="text/css">    
.bars, .chart, .pie {
    height: 100% !important;
}
</style>

<div class="page-content-wrapper">
                    <div class="page-content">
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Loan Recommendation & Approval</span>
                                </li>
                            </ul>
                            <!-- <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div> -->
                        </div>
                        <!--START CONTENT HERE -JC -->
                        <div class="clearfix"> </div>
                        <br>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Loan Recommendation & Approval</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                    <th class="all">TRN</th>
                                                    <th class="all">Creditor</th>
                                                    <th class="all">Product</th>
                                                    <th class="all">Amount</th>
                                                    <th class="all">Terms</th>
                                                    <th class="all">Co-maker</th>
                                                    <th class="all">Co-maker</th>
                                                    <th class="all">Purpose</th>
                                                    <th class="all">Remarks</th>
                                                    <th class="all">Date Applied</th>
                                                    <th class="all">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="body1">                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>

<script type="text/javascript">
var upload_docus = [];
  $(document).ready(function() {
       list();
       uploads();
  });

  function list()
  {
                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };
    var url = "";
    if(getUrlParameter('status')=="checking")
    {
      url = "<?php echo site_url('Loans/ajax_list_loan_val')?>";
    }
    if(getUrlParameter('status')=="approval")
    {
      url = "<?php echo site_url('Loans/ajax_list_loan_checked')?>";
    }   

     table = $('#sample_1,#sample_2,#sample_3').DataTable( {        
        "ajax": url, 
        "columns": [
            /*{ "data": "creditor" },
            { "data": "amount" },
            { "data": "terms" },
            { "data": "purpose" },
            { "data": "validated_dt"},
            { "data": "action" },*/
            { "data": "trn" },
            { "data": "creditor" },
            { "data": "product_name" },
            { "data": "amount" },
            { "data": "terms" },
            { "data": "com1" },
            { "data": "com2" },
            { "data": "purpose" },
            { "data": "checker_remark" },
            { "data": "validated_dt" },
            { "data": "action" }
        ]       
    } );
  }

  function edit_loan(id)
  {
    var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');
                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };
    //Ajax Load data from ajax
    /*$.ajax({
      url : "<?php echo site_url('Loans/ajax_get_loan_data/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {           
        $('#loan_id').val(data.data.id);    
        $('.form').find('input:text').val('');
        $('#creditor').val(data.data.fullname);
        $('#amount').val(numberWithCommas(data.data.applied_amount));
        $('#terms').val(data.data.terms);
        $('#purpose').val(data.data.purpose);
        $('#c1').val(data.comaker1.fullname);
        $('#c2').val(data.comaker2.fullname);         
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error in getting data!');
      }
    });*/
    if(getUrlParameter('status')=="checking")
    {
      location =  "<?php echo site_url('Loans/Loan_validation_dt/')?>?id=" + id+"&status=validated";

    }
    if(getUrlParameter('status')=="approval")
    {
      location =  "<?php echo site_url('Loans/Loan_validation_dt/')?>?id=" + id+"&status=checked";
    } 
    
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function validate()
  {
              swal({
                  title: "Are you sure you want to Validate this Loan?",
                  //text: "Your will not be able to recover this imaginary file!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-warning",
                  confirmButtonText: "OK",
                  closeOnConfirm: false
                },
                function(){
                    //Ajax Load data from ajax
                    $.ajax({
                      url : "<?php echo site_url('Loans/ajax_validate/')?>/",
                      type: "POST",
                      data:{
                        remarks: $("#remarks").val(),
                        docus: upload_docus,
                        id: $("#loan_id").val(),
                      },
                      dataType: "JSON",
                      success: function(data)
                      {           

                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        alert('Error in getting data!');
                      }
                    });
                    //swal("Deleted!", "Privilege", "error");
                }); 

  }

  function uploads() {
    var str = "<?php echo base_url();?>";
    var url = str + 'uploads/ ';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function(e, data) {
            $.each(data.result.files, function(key, value) {
                upload_docus.push(value.name);
            });
        },
        progressall: function(e, data) {
        },
        submit: function(e, data) {
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}

</script>

<div class="modal fade" id="modal_form_privilege" role="dialog">
              <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 class="modal-title-priv"> </h3>
                </div>
                <div class="modal-body form">
                      <table id="tblPriv" class="table table-striped table-bordered table-hover" 
                                            data-search="true" 
                                            data-pagination="true"
                                            data-page-size="5"
                                            data-show-export="true"
                                            data-mobile-responsive="true"
                                            data-sort-name = "priv_name"
                                            data-sort-order = "desc"
                                             data-url="<?php echo site_url('Privillege')?>"
                                            data-page-list="[5, 10, ALL]">
                                      <thead>
                                        <tr>
                                          <th data-field="priv_name"  data-sortable = "true">Privilege Names</th>
                                          <th data-field="desc"  data-sortable = "true">Description</th>
                                          <th data-field="action" style="width:125px;">Action</th>
                                        </tr>
                                      </thead>
                                </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->   
</div>

