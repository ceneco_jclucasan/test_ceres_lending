<style type="text/css">    
   .bars, .chart, .pie {
   height: 100% !important;
   }
</style>
<div class="page-content-wrapper">
   <div class="page-content">
      <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
               <a href="index.html">Home</a>
               <i class="fa fa-circle"></i>
            </li>
            <li>
               <span>Group Accounts</span>
            </li>
         </ul>
         <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
               <i class="icon-calendar"></i>&nbsp;
               <span class="thin uppercase hidden-xs"></span>&nbsp;
               <i class="fa fa-angle-down"></i>
            </div>
         </div>
      </div>
      <br>
       <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Group Listing</span>
                                        </div>
                                        <div class="tools"> </div>
                                    </div>
                                    <div class="col-md-6">
                                                    <div class="btn-group">
                                                         <button class="btn btn-success" onclick="add_group()"><i class="glyphicon glyphicon-plus"></i> Add Group</button>
                                                    </div>
                                                </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                            <thead id="thead1">
                                                <tr>
                                                   <th class="all">Group name</th>
                                                   <th class="all">Description</th>
                                                   <th class="all">Action</th
                                                </tr>
                                            </thead>
                                            <tbody id="body1">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

   </div>
</div>
              
            <script type="text/javascript">
              var save_method; //for save method string
                var table;
              $(document).ready(function() {
                list();
                list_privileges();   
                $('#alert').hide();    
                //dropdown_list();
              });              


               function list()
                {
                   table = $('#sample_1').DataTable( {
                      
                      "ajax": "<?php echo site_url('user_groups/ajax_list')?>/",
                      "columns": [
                          { "data": "grp" },
                          { "data": "desc" },
                          { "data": "action" }
                      ],
        "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
                  } );
                }

              function add_group()
              {
                save_method = 'add';
                $('#form')[0].reset(); // reset form on modals
                $('#modal_form').modal({backdrop:'static',keyboard: true}); // show bootstrap modal
                $('.modal-title').text('Add Group'); // Set Title to Bootstrap modal title
              }

              function edit_group(id)
              {
                save_method = 'update';
                $('#form')[0].reset(); // reset form on modals
                //Ajax Load data from ajax
                $.ajax({
                  url : "<?php echo site_url('user_groups/ajax_edit/')?>/" + id,
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {           
                    $('[name="id"]').val(data.id);
                    $('[name="group_name"]').val(data.group_name);
                    $('[name="description"]').val(data.description);                                                                                         
                    $('#modal_form').modal({backdrop:'static',keyboard: true}); // show bootstrap modal when complete loaded
                    $('.modal-title').text('Edit Group'); // Set title to Bootstrap modal title
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data!');
                  }
                });
              }

              function save()
              {
                var url;
                if(save_method == 'add') {
                  url = "<?php echo site_url('user_groups/ajax_add')?>";
                } else {
                  url = "<?php echo site_url('user_groups/ajax_update')?>";
                }
                // ajax adding data to database
                $.ajax({
                  url : url,
                  type: "POST",
                  data: $('#form').serialize(),
                  dataType: "JSON",
                  success: function(data)
                  {
                     //if success close modal and reload ajax table
                     $('#modal_form').modal('hide');
                    table.ajax.reload( null, false );
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in adding or updating data!');
                  }
                });
              }

              /*function delete_Group(id)
              {
                if(confirm('Delete this data?'))
                {
                  // ajax delete data to database
                  $.ajax({
                    url : "<?php echo site_url('user_groups/ajax_delete')?>/" + id,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data)
                    {
                       //if success reload ajax table
                       $('#modal_form').modal('hide');
                       list();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error in deleting data!');
                    }
                  });
                }
              }*/

              function delete_group(id)
              {
                

                swal({
                      title: "Are you sure you want to delete?",
                      //text: "Your will not be able to recover this imaginary file!",
                      type: "error",
                      showCancelButton: true,
                      confirmButtonClass: "btn-danger",
                      confirmButtonText: "Delete",
                      closeOnConfirm: false
                    },
                    function(){
                      $.ajax({
                                    url: "<?php echo site_url('user_groups/ajax_delete')?>/" + id,
                                    type: "POST",
                                    dataType: "JSON",
                                    success: function(data)
                                    {
                                        //if success reload ajax table                    
                                       table.ajax.reload( null, false );
                                    },
                                    error: function(jqXHR, textStatus, errorThrown)
                                    {
                                        alert('Error in deleting data!');
                                    }
                                });

                          swal("Deleted!", "Employee has been deleted.", "success");
                    });

              }

              function delete_all_privileges(id)
              {
                $.ajax({
                  url : "<?php echo site_url('user_groups/ajax_delete_all_privileges')?>/" + id,
                  type: "POST",
                  dataType: "JSON",
                  success: function(data)
                  {

                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in deleting data!');
                  }
                });
              }

              /*function manage_privilages(id)
              {
                save_method = 'add';
                $('#form_privileges')[0].reset(); // reset form on modals
                $.ajax({
                  url : "<?php echo site_url('user_groups/ajax_edit/')?>/" + id,
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {           
                    $('[name="id2"]').val(data.id);
                    $('[name="group_name2"]').val(data.group_name);  
                    get_privileges_by_id();                                                                             
                    $('#modal_form_privileges').modal('show'); // show bootstrap modal when complete loaded
                    $('.modal-title').text('Manage Privileges'); // Set title to Bootstrap modal title
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data!');
                  }
                });
              }*/

              function manage_privilages(id)
              {
                  $('#modal_form_privilege').modal('show'); // show bootstrap modal
                        $.ajax({
                              url : "<?php echo site_url('user_groups/ajax_list_group/')?>/" + id,
                              type: "GET",
                              dataType: "JSON",
                              success: function(data)
                              {   
                               // $('.modal-title-priv').text('Privileges of '+data.username); // Set Title to Bootstrap modal title
                                var a = new Array();
                                    for (x = 0; x < data.data.length; x++) {
                                      a[x] = 
                                      {
                                        "priv_name": data.data[x][0],
                                        "desc": data.data[x][1],
                                        "action": data.data[x][2],
                                      };
                                    }

                                    $('#tblPriv').on('post-body.bs.table', function() {
                                        $('.make-switch').bootstrapSwitch({
                                          onText: "YES",
                                          offText: "NO",
                                          size: "mini",
                                          onColor: "success",
                                          offColor: "danger"
                                          });
                                    });
                                    
                                    $('#tblPriv').bootstrapTable({ data: a });
                                    $('#tblPriv').bootstrapTable('load',a);
                              },
                              error: function (jqXHR, textStatus, errorThrown)
                              {
                                  alert('Error in getting data!');
                              }
                        });
              }

              function add_priv_grp(privid,userid)
  {
                if (confirm('Are you sure add this privilege?')) {

                    $.ajax({
                      url :  "<?php echo site_url('user_groups/ajax_update_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                            toastr.success('Privilege has been added!','Message:');
                            manage_privilages(userid);

                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
                  }
  }

  function remove_priv_grp(privid,userid)
  {    
                if (confirm('Are you sure remoive this privilege?')) {
                    $.ajax({
                      url :  "<?php echo site_url('user_groups/ajax_delete_privileges')?>/"+privid+"/"+userid,
                      type: "POST",
                      dataType: "JSON",
                      success: function(data)
                      {
                        toastr.error('Privilege has been removed!','Message:');
                        manage_privilages(userid);
                      },

                      error: function (jqXHR, textStatus, errorThrown)
                      {
                          alert('Error adding / update data');
                      }
                  });
                  }
  }

              /*function dropdown_list()
              {
                $('[name="group_id"]').empty();
                var a = "";
                var b = "";
                $.ajax({
                  url : "<?php echo site_url('contacts/ajax_dropdown')?>",
                  type: "GET",
                  dataType: "JSON",
                  success: function(data)
                  {           
                      $('[name="group_id"]').append('<option> </option>');
                      for (var x = 0; x < data.data.length; x++) {
                        a = data.data[x][0];
                        b = data.data[x][1];
                        $('[name="group_id"]').append('<option value="' + a + '">' + b + '</option>');
                      };
                      $('[name="group_id"]').trigger('chosen:updated');
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                      alert('Error in getting data for dropdown list!');
                  }
                });
              }*/

              function list_privileges()
              {
                $('#list').empty();
                var a = "";
                var b = "";
                var c = "";
                $.ajax({
                    url : "<?php echo site_url('user_groups/ajax_list_privilege/')?>",
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    { 
                      for (var i = 0; i < data.data.length; i++) {
                        //console.log(data.data[i]);
                        for (var n = 0; n < data.data[i].length; n++) {
                          if (n == 0) {
                            a = '<td><input type="checkbox" id="group_privilege[' + data.data[i][n] + ']" name="privilege_checkbox[]" value="1"><input type="hidden" id="privilege_hidden[' + i + ']" value="' + data.data[i][n] + '"></td>';
                          } else if (n == 1) {
                            b = '<td>' + data.data[i][n] + '</td>';
                          } else if (n == 2) {
                            c = '<td>' + data.data[i][n] + '</td>';
                          }
                        };
                        $('#list').append('<tr>' + c + a + '</tr>');
                      };
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });
              }

              function get_privileges_by_id()
              {
                $.ajax({
                    url : "<?php echo site_url('user_groups/ajax_get_privileges_by_id')?>/" + $('[name="id2"]').val(),
                    type: "GET",
                    dataType: "JSON",
                    success: function(data)
                    { 
                      for (var i = 0; i < data.data.length; i++) {
                        //console.log("group_privilege[" + data.data[i][0] + "]");
                        document.getElementById("group_privilege[" + data.data[i][0] + "]").checked = true;
                      };
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      alert('Error get data from ajax');
                    }
                });
              }

              function update_privileges()
              {
                var group_id = $('#id2').val();
                var privilege_id = "";
                for (var i = 0; i < $('#table_privileges tr').length-1; i++) {
                  privilege_id = document.getElementById("privilege_hidden[" + i + "]").value;
                  if (document.getElementsByName("privilege_checkbox[]")[i].checked) {
                    $.ajax({
                      url : "<?php echo site_url('user_groups/ajax_update_privileges')?>",
                      type: "POST",
                      dataType: "JSON",
                      data: { group_id : group_id, privilege_id : privilege_id },
                      success: function(data)
                      { 
                        $('#alert').fadeIn();
                        setTimeout(function(){ $('#alert').fadeOut(); }, 2000);
                        //console.log(data.data[0]);
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        alert('error');
                      }
                    });
                    //console.log(i + ": Checked!");
                    //console.log(privilege_id + ": Checked!");
                  } else {
                    $.ajax({
                      url : "<?php echo site_url('user_groups/ajax_delete_privileges')?>",
                      type: "POST",
                      dataType: "JSON",
                      data: { group_id : group_id, privilege_id : privilege_id },
                      success: function(data)
                      { 
                        $('#alert').fadeIn();
                        setTimeout(function(){ $('#alert').fadeOut(); }, 2000);
                        //console.log(data.data[0]);
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        alert('error');
                      }
                    });
                    //console.log(i + ": Unchecked!");
                    //console.log(privilege_id + ": Unchecked!");
                  };
                };
                //alert($('#table_privileges tr').length-1);
                update_group_member_privilege(group_id);
              }

              function update_group_member_privilege(group_id)
              {
                $.ajax({
                  url : "<?php echo site_url('user_groups/ajax_update_group_member_privilege')?>/" + group_id,
                  type: "POST",
                  dataType: "JSON",
                  success: function(data)
                  { 
                    for (var i = 0; i < data.data.length; i++) {
                      save_group_member_privilege(data.data[i][0]);
                    };
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    alert('error');
                  }
                });
              }

              function save_group_member_privilege(user_id)
              {
                //console.log("start: " + user_id);
                var privilege_id = "";
                for (var i = 0; i < $('#table_privileges tr').length-1; i++) {
                  var num = i;
                  privilege_id = document.getElementById("privilege_hidden[" + i + "]").value;
                  if (document.getElementsByName("privilege_checkbox[]")[i].checked) {
                    $.ajax({
                      url : "<?php echo site_url('users/ajax_update_privileges')?>",
                      type: "POST",
                      dataType: "JSON",
                      data: { user_id : user_id, privilege_id : privilege_id },
                      success: function(data)
                      { 
                        $('#alert').fadeIn();
                        setTimeout(function(){ $('#alert').fadeOut(); }, 2000);
                        //console.log(data.data[0]);
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        alert('error');
                      }
                    });
                    //console.log(i + ": Checked!");
                    //console.log(privilege_id + ": Checked!");
                  } else {
                    $.ajax({
                      url : "<?php echo site_url('users/ajax_delete_privileges')?>",
                      type: "POST",
                      dataType: "JSON",
                      data: { user_id : user_id, privilege_id : privilege_id },
                      success: function(data)
                      { 
                        $('#alert').fadeIn();
                        setTimeout(function(){ $('#alert').fadeOut(); }, 2000);
                        //console.log(data.data[0]);
                      },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        alert('error');
                      }
                    });
                    //console.log(i + ": Unchecked!");
                    //console.log(privilege_id + ": Unchecked!");
                  };
                };
                //alert($('#table_privileges tr').length-1);
                //console.log("end: " + user_id);
              }

            </script>

            <!-- MODAL FORM -->
            <div class="modal fade" id="modal_form" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">User Group Form</h3>
                  </div>
                  <div class="modal-body form">
                    <form action="#" id="form" class="form-horizontal">
                      <input type="hidden" value="" name="id"/> 
                      <div class="form-body">
                        <div class="form-group">
                          <label class="control-label col-md-3">Group Name</label>
                          <div class="col-md-9">
                            <input name="group_name" placeholder="" class="form-control" type="text">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3">Description</label>
                          <div class="col-md-9">
                            <input name="description" placeholder="" class="form-control" type="text">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- PRIVILAGES MODAL FORM -->
            <div class="modal fade" id="modal_form_privileges" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Manage Privileges Form</h3>
                  </div>
                  <div class="modal-body form">
                    <form action="#" id="form_privileges" class="form-horizontal">
                      <input type="hidden" value="" name="id2" id="id2"/> 
                      <div class="form-body">
                        <div class="form-group">
                          <label class="control-label col-md-2">Group Name</label>
                          <div class="col-md-10">
                            <input name="group_name2" placeholder="" class="form-control" type="text" disabled>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 table-responsive">
                            <h5><b>Privileges</b></h5>
                            <table id="table_privileges" class="table table-striped table-bordered table-hover">
                              <thead>
                                <tr>
                                  <!-- <th style="width:30%">Group Name</th> -->
                                  <th style="width:60%">Description</th>
                                  <th style="width:10%">Action</th>
                                </tr>
                              </thead>
                              <tbody id="list">
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div id="alert" class="form-group">
                          <div class="col-md-4 pull-right">
                            <div class="alert alert-success alert-dismissable">
                              <h4>  <i class="icon fa fa-check"></i> Privileges Updated!</h4>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>

                    
                  <div class="modal-footer">
                    <button type="button" id="btnUpdatePrivileges" onclick="update_privileges()" class="btn btn-primary">Update</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
       

       <div class="modal fade" id="modal_form_privilege" role="dialog">
              <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 class="modal-title-priv"> </h3>
                </div>
                <div class="modal-body form">
                      <table id="tblPriv" class="table table-striped table-bordered table-hover" 
                                            data-search="true" 
                                            data-pagination="true"
                                            data-page-size="5"
                                            data-show-export="true"
                                            data-mobile-responsive="true"
                                            data-sort-name = "priv_name"
                                            data-sort-order = "desc"
                                             data-url="<?php echo site_url('Privillege')?>"
                                            data-page-list="[5, 10, ALL]">
                                      <thead>
                                        <tr>
                                          <th data-field="priv_name"  data-sortable = "true">Privilege Names</th>
                                          <th data-field="desc"  data-sortable = "true">Description</th>
                                          <th data-field="action" style="width:125px;">Action</th>
                                        </tr>
                                      </thead>
                                </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->   
</div>