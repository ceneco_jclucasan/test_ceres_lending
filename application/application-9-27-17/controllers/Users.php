<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Users_m');
		$this->load->model('User_groups_m');
		$this->load->model('User_privileges_m','tblprivilge');
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	      	$data['id'] = $session_data['id'];
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
 	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'no_code';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "View Users") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('users_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function view_add_page()
	{
		
	      	$session_data = $this->session->userdata('logged_in');
	      	$data['id'] = $session_data['id'];
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
 	      	$privileges = $session_data['privileges'];
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "View Users") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('users_add_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login', 'refresh');
	}
	
	public function ajax_list() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->Users_m->get_datatables();	
		$data = array();
		
		foreach($list as $arr) {
			$row = array();
			$row[] = $arr->email;
			$row[] = $arr->firstname;
			$row[] = $arr->lastname;
			$row[] = $arr->description;
			$privileges = $session_data['privileges'];
			$button = '';
            if ($arr->id == "1") {
				for ($i=0; $i<sizeof($privileges); $i++) { 
	                if ($privileges[$i]->privilege_name == "Manage User Privileges") {
	                	if ($session_data['group_id'] == "1") {
	                		$button = $button.'<a class="btn btn-sm btn-success" title="Manage Privilages" onclick="manage_privilages('."'".$arr->id."'".')"><i class="glyphicon glyphicon-cog"></i></a>';
	                	} else {
	                		$button = $button.'<i class="glyphicon glyphicon-cog btn btn-sm btn-default"></i>';
	                	}
	                } else if ($privileges[$i]->privilege_name == "Update Users") {
	                	$button = $button.'<i class="glyphicon glyphicon-pencil btn btn-sm btn-default"></i>';
	                } else if ($privileges[$i]->privilege_name == "Suspend Users") {
	                	$button = $button.'<i class="glyphicon glyphicon-warning-sign btn btn-sm btn-default"></i>';
	                } else if ($privileges[$i]->privilege_name == "Delete Users") {
	                	$button = $button.'<i class="glyphicon glyphicon-trash btn btn-sm btn-default"></i>';
	                }
	            }
			} else {
				for ($i=0; $i<sizeof($privileges); $i++) { 
	                if ($privileges[$i]->privilege_name == "Manage User Privileges") {
	                	$button = $button.'<a class="btn btn-sm btn-success" title="Manage Privilages" onclick="manage_privilages('."'".$arr->id."'".')"><i class="glyphicon glyphicon-cog"></i></a>';
	                } else if ($privileges[$i]->privilege_name == "Update Users") {
	                	$button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_user('."'".$arr->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
	                } else if ($privileges[$i]->privilege_name == "Suspend Users") {
	                	if($arr->is_suspended!=0){
	                		$button = $button.'<a class="btn btn-sm btn-warning" title="Unsuspend" onclick="unsuspend_user('."'".$arr->id."'".')"><i class="fa fa-lock"></i></a>';
		                }else{
		                	$button = $button.'<a class="btn btn-sm btn-info" title="Suspend" onclick="suspend_user('."'".$arr->id."'".')"><i class="fa fa-unlock"></i></a>';
		                }
	                } else if ($privileges[$i]->privilege_name == "Delete Users") {
	                	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_user('."'".$arr->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
	                }
	            }
			}
			$row[] = $button;
			$row[] = ($arr->is_suspended==0?'Active':'Suspended');
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_suspended() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->Users_m->get_datatables_suspended();	
		$data = array();
		
		foreach($list as $arr) {
			$row = array();
			$row[] = $arr->email;
			$row[] = $arr->firstname;
			$row[] = $arr->lastname;
			$row[] = $arr->description;
			$privileges = $session_data['privileges'];
			$button = '';
			for ($i=0; $i<sizeof($privileges); $i++) { 
				if ($privileges[$i]->privilege_name == "Unsuspend Users") {
					$button = $button.'<a class="btn btn-sm btn-warning" title="Unsuspend" onclick="unsuspend_user('."'".$arr->id."'".')"> Remove Suspension</a>';
				}
			}  
			$row[] = $button;
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function ajax_edit($id)
	{
		$data = $this->Users_m->get_by_id($id);
		echo json_encode($data);
	}
	
	public function ajax_add()
	{	
		$emp_id = $this->input->post('emp_id');
		$session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');
	    $date = date('Y-m-d H:i:s');
	    $list = $this->Users_m->check_user($emp_id);	
		$data = array();

		if ($list != null) {
			$data[] = 'This user already have an account.';
		} else {
			$data2 = array(
					'emp_id' => $this->input->post('emp_id'),
					'group_id' => $this->input->post('group_id2'),
					'email' => $this->input->post('email'),
					'username' => $this->input->post('username'),
					'password' => MD5($this->input->post('password')),
					'is_suspended' => 0,
					'added_by' => $session_data['username'],
					'added_date' => $date,
				);
			$insert = $this->Users_m->save($data2);
			$data[] = $this->db->insert_id();
			$data[] = 'Saved!';
		}
		$output = array("data" => $data,);
		echo json_encode($output);
	}
	
	public function ajax_update()
	{
		$session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');
	    $date = date('Y-m-d H:i:s');

		$data = array(
				'emp_id' => $this->input->post('emp_id'),
				//'group_id' => $this->input->post('group_id2'),
				'email' => $this->input->post('email'),
				'username' => $this->input->post('username'),
				'password' => MD5($this->input->post('password')),
				'is_suspended' => 0,
				//'added_by' => $session_data['username'],
				//'added_date' => $date,
			);
		$this->Users_m->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update_user_group()
	{
		$session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');
	    $date = date('Y-m-d H:i:s');

		$data = array(
				'group_id' => $this->input->post('group_id'),
				//'added_by' => $session_data['username'],
				//'added_date' => $date,
			);
		$this->Users_m->update(array('id' => $this->input->post('id2')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_suspend_user($id)
	{
		$data = array(
				'is_suspended' => 1,
				//'added_by' => $session_data['username'],
				//'added_date' => $date,
			);
		$this->Users_m->update(array('id' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}	

	public function ajax_unsuspend_user($id)
	{
		$data = array(
				'is_suspended' => 0,
				//'added_by' => $session_data['username'],
				//'added_date' => $date,
			);
		$this->Users_m->update(array('id' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_delete($id)
	{
		$this->Users_m->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete_all_privileges($id)
	{
		$this->Users_m->delete_all_privileges_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_list_privilege() 
	{
		$list = $this->tblprivilge->get_datatables();	
		$data = array();
		
		foreach($list as $arr) {
			$row = array();
			$row[] = $arr->id;
			$row[] = $arr->privilege_name;
			$row[] = $arr->description;
				  
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_get_privileges_by_id($id) 
	{
		$list = $this->Users_m->get_privileges_by_id($id);	
		$data = array();
		
		foreach($list as $arr) {
			$row = array();
			$row[] = $arr->privilege_id;
				  
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_update_privileges($privid,$userid) 
	{
		$data = array(
			'user_id' => $userid,
			'privilege_id' => $privid,
		);
		$insert = $this->Users_m->save_privileges($data);				
		$output = array(
			"status" => true,
			"id"=>$insert
		);
		echo json_encode($output);
	}

	/*public function ajax_delete_privileges() 
	{
		$user_id = $this->input->post('user_id');
		$privilege_id = $this->input->post('privilege_id');
		$list = $this->Users_m->update_privileges($user_id, $privilege_id);	
		$data = array();
		
		if ($list != null) {
			foreach($list as $arr) {
				$this->Users_m->delete_privileges($arr->id);
				$data[] = $privilege_id.' deleted';
			}
		} else {
			$data[] = $privilege_id.' did not delete';
		}
			
		$output = array(
						"data" => $data,
				);
		echo json_encode($output);
	}*/

	public function ajax_delete_privileges($priv,$userid)
	{
			$data = $this->Users_m->delete_privileges($priv,$userid);
			echo json_encode(array("status" => TRUE,"id"=>$priv));
	}

	public function ajax_dropdown_list_group() 
	{
		$list = $this->Users_m->dropdown_list_group();	
		$data = array();
		foreach($list as $arr) {
			$row = array();
			$row[] = $arr->id;
			$row[] = $arr->group_name;
			$data[] = $row;
		}
		$output = array("data" => $data);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_dropdown_list_employees() 
	{
		$list = $this->Users_m->dropdown_list_employees();	
		$data = array();
		foreach($list as $arr) {
			$row = array();
			$row[] = $arr->id;
			$row[] = $arr->firstname.' '.$arr->lastname;
			$data[] = $row;
		}
		$output = array("data" => $data);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_user($id) 
	{
		$list = $this->tblprivilge->get_datatables();
		$data = array();

		foreach ($list as $myList) {
			$row = array();

			$get_data = $this->Users_m->get_privileges_by_id2($id,$myList->id);
			$row[] = $myList->privilege_name;
			$row[] = $myList->description;
				/*if($get_data!=null)
				{
						$row[] = '<a class="btn btn-sm btn-danger" title="Edit" onclick="remove_priv_user('."'".$myList->id."',"."'".$id."'".')"><i class="fa fa-times	"></i></a>';				
				}else{
						$row[] = '<a class="btn btn-sm btn-primary" title="Edit" onclick="add_priv_user('."'".$myList->id."',"."'".$id."'".')"><i class="fa fa-plus"></i></a> ';
				}*/

				if($get_data!=null)
				{
$row[] = '<input type="checkbox" value="'.$myList->id.'" id="a'.$myList->id."_".$id.'" onchange="remove_priv_user('."'".$myList->id."',"."'".$id."','".$myList->id."_".$id."'".')" checked class="make-switch" data-size="small">';				
				}else{
						$row[] = '<input type="checkbox" value="'.$myList->id.'"  id="a'.$myList->id."_".$id.'" onchange="add_priv_user('."'".$myList->id."',"."'".$id."','".$myList->id."_".$id."'".')"  class="make-switch" data-size="small">';
				}

				

			$data[] = $row;
		}

		$output = array("data"=>$data);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list1() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->Users_m->get_datatables();	
		$data = array();
		
		foreach($list as $arr) {
			$row = array();
			$row['email'] = $arr->email;
			$row['fname'] = $arr->firstname;
			$row['lname'] = $arr->lastname;
			$row['desc'] = $arr->description;
			$privileges = $session_data['privileges'];
			$button = '';
            if ($arr->id == "1") {
				for ($i=0; $i<sizeof($privileges); $i++) { 
	                if ($privileges[$i]->privilege_name == "Manage User Privileges") {
	                	if ($session_data['group_id'] == "1") {
	                		$button = $button.'<a class="btn btn-sm btn-success" title="Manage Privilages" onclick="manage_privilages('."'".$arr->id."'".')"><i class="glyphicon glyphicon-cog"></i></a>';
	                	} else {
	                		$button = $button.'<i class="glyphicon glyphicon-cog btn btn-sm btn-default"></i>';
	                	}
	                } else if ($privileges[$i]->privilege_name == "Update Users") {
	                	$button = $button.'<i class="glyphicon glyphicon-pencil btn btn-sm btn-default"></i>';
	                } else if ($privileges[$i]->privilege_name == "Suspend Users") {
	                	$button = $button.'<i class="glyphicon glyphicon-warning-sign btn btn-sm btn-default"></i>';
	                } else if ($privileges[$i]->privilege_name == "Delete Users") {
	                	$button = $button.'<i class="glyphicon glyphicon-trash btn btn-sm btn-default"></i>';
	                }
	            }
			} else {
				for ($i=0; $i<sizeof($privileges); $i++) { 
	                if ($privileges[$i]->privilege_name == "Manage User Privileges") {
	                	$button = $button.'<a class="btn btn-sm btn-success" title="Manage Privilages" onclick="manage_privilages('."'".$arr->id."'".')"><i class="glyphicon glyphicon-cog"></i></a>';
	                } else if ($privileges[$i]->privilege_name == "Update Users") {
	                	$button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_user('."'".$arr->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
	                } else if ($privileges[$i]->privilege_name == "Suspend Users") {
	                	if($arr->is_suspended!=0){
	                		$button = $button.'<a class="btn btn-sm btn-warning" title="Unsuspend" onclick="unsuspend_user('."'".$arr->id."'".')"><i class="fa fa-lock"></i></a>';
		                }else{
		                	$button = $button.'<a class="btn btn-sm btn-info" title="Suspend" onclick="suspend_user('."'".$arr->id."'".')"><i class="fa fa-unlock"></i></a>';
		                }
	                } else if ($privileges[$i]->privilege_name == "Delete Users") {
	                	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_user('."'".$arr->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
	                }
	            }
			}
			$row['btn'] = $button;
			$row['stat'] = ($arr->is_suspended==0?'Active':'Suspended');
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

}