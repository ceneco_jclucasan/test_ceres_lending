<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_groups extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_groups_m');
		$this->load->model('User_privileges_m');
		$this->load->model('Users_m');
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      $session_data = $this->session->userdata('logged_in');
	      $data['id'] = $session_data['id'];
	      $data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
		  	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
		  	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'no_code';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "View Groups") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
				$this->load->view('templates/nav1', $data);	
				$this->load->view('user_groups_v');
				$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login', 'refresh');
	}
	
	public function ajax_list() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->User_groups_m->get_datatables();	
		$data = array();
		
		foreach($list as $arr) {
			$row = array();
			$row['grp'] = $arr->group_name;
			$row['desc'] = $arr->description;
			$privileges = $session_data['privileges'];
			$button = '';
			if ($arr->id == "1") {
				for ($i=0; $i<sizeof($privileges); $i++) { 
					if ($privileges[$i]->privilege_name == "Manage Group Privileges") {
	                	if ($session_data['group_id'] == "1") {
	                		$button = $button.'<a class="btn btn-sm btn-success" title="Manage Privilages" onclick="manage_privilages('."'".$arr->id."'".')"><i class="glyphicon glyphicon-cog"></i></a>';
	                	} else {
	                		$button = $button.'<i class="glyphicon glyphicon-cog btn btn-sm btn-default"></i>';
	                	}
	                } else if ($privileges[$i]->privilege_name == "Update Groups") {
	                	$button = $button.'<i class="glyphicon glyphicon-pencil btn btn-sm btn-default"></i>';
	                } else if ($privileges[$i]->privilege_name == "Delete Groups") {
	                	$button = $button.'<i class="glyphicon glyphicon-trash btn btn-sm btn-default"></i>';
	                }
				}
				/*$row[] = '<a class="btn btn-sm btn-success" title="Manage Privilages" onclick="manage_privilages('."'".$arr->id."'".')"><i class="glyphicon glyphicon-cog"></i></a>
					<i class="glyphicon glyphicon-pencil btn btn-sm btn-default"></i>
				  	<i class="glyphicon glyphicon-trash btn btn-sm btn-default"></i>';*/
			} else if ($arr->id == "2") {
				for ($i=0; $i<sizeof($privileges); $i++) { 
					if ($privileges[$i]->privilege_name == "Manage Group Privileges") {
	                	if ($session_data['group_id'] == "1" || $session_data['group_id'] == "2") {
	                		$button = $button.'<a class="btn btn-sm btn-success" title="Manage Privilages" onclick="manage_privilages('."'".$arr->id."'".')"><i class="glyphicon glyphicon-cog"></i></a>';
	                	} else {
	                		$button = $button.'<i class="glyphicon glyphicon-cog btn btn-sm btn-default"></i>';
	                	}
	                } else if ($privileges[$i]->privilege_name == "Update Groups") {
	                	$button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_group('."'".$arr->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
	                } else if ($privileges[$i]->privilege_name == "Delete Groups") {
	                	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_group('."'".$arr->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
	                }
				}
				/*$row[] = '<a class="btn btn-sm btn-success" title="Manage Privilages" onclick="manage_privilages('."'".$arr->id."'".')"><i class="glyphicon glyphicon-cog"></i></a>
					<i class="glyphicon glyphicon-pencil btn btn-sm btn-default"></i>
				  	<i class="glyphicon glyphicon-trash btn btn-sm btn-default"></i>';*/
			} else {
				for ($i=0; $i<sizeof($privileges); $i++) { 
					if ($privileges[$i]->privilege_name == "Manage Group Privileges") {
						$button = $button.'<a class="btn btn-sm btn-success" title="Manage Privilages" onclick="manage_privilages('."'".$arr->id."'".')"><i class="glyphicon glyphicon-cog"></i></a>';
	                } else if ($privileges[$i]->privilege_name == "Update Groups") {
	                	$button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_group('."'".$arr->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
	                } else if ($privileges[$i]->privilege_name == "Delete Groups") {
	                	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_group('."'".$arr->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
	                }
				}
				/*$row[] = '<a class="btn btn-sm btn-success" title="Manage Privilages" onclick="manage_privilages('."'".$arr->id."'".')"><i class="glyphicon glyphicon-cog"></i></a>
					<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_group('."'".$arr->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  	<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_group('."'".$arr->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';*/
			}  
			$row['action'] = $button;
			$data[] = $row;
		}
		$row[] = $button;
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function ajax_edit($id)
	{
		$data = $this->User_groups_m->get_by_id($id);
		echo json_encode($data);
	}
	
	public function ajax_add()
	{	
		$session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');
	    $date = date('Y-m-d H:i:s');

		$data = array(
				'group_name' => $this->input->post('group_name'),
				'description' => $this->input->post('description'),
				//'add_date' => $date,
				//'add_by' => $session_data['firstname'].' '.$session_data['lastname'],
			);
		$insert = $this->User_groups_m->save($data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_update()
	{
		$session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');
	    $date = date('Y-m-d H:i:s');

		$data = array(
				'group_name' => $this->input->post('group_name'),
				'description' => $this->input->post('description'),
				//'update_date' => $date,
				//'update_by' => $session_data['firstname'].' '.$session_data['lastname'],
			);
		$this->User_groups_m->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_delete($id)
	{
		$this->User_groups_m->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete_all_privileges($id)
	{
		$this->User_groups_m->delete_all_privileges_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_list_privilege() 
	{
		$list = $this->User_privileges_m->get_datatables();	
		$data = array();
		
		foreach($list as $arr) {
			$row = array();
			$row[] = $arr->id;
			$row[] = $arr->privilege_name;
			$row[] = $arr->description;
				  
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_get_privileges_by_id($id) 
	{
		$list = $this->User_groups_m->get_privileges_by_id($id);	
		$data = array();
		
		foreach($list as $arr) {
			$row = array();
			$row[] = $arr->privilege_id;
				  
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	/*public function ajax_update_privileges() 
	{
		$group_id = $this->input->post('group_id');
		$privilege_id = $this->input->post('privilege_id');
		$list = $this->User_groups_m->update_privileges($group_id, $privilege_id);	
		$data = array();
		
		if ($list != null) {
			$data[] = $privilege_id.' did not save';
		} else {
			$data2 = array(
				'group_id' => $this->input->post('group_id'),
				'privilege_id' => $this->input->post('privilege_id'),
				//'add_date' => $date,
				//'add_by' => $session_data['firstname'].' '.$session_data['lastname'],
			);
			$insert = $this->User_groups_m->save_privileges($data2);
			$data[] = $privilege_id.' saved';
		}
			
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}*/

	public function ajax_update_privileges($privid,$group) 
	{
		$data = array(
			'group_id' => $group,
			'privilege_id' => $privid,
		);
		$this->User_groups_m->save_privileges($data);				
		$output = array(
			"status" => true,
		);
		echo json_encode($output);
	}

	public function ajax_delete_privileges($priv,$group_id)
	{
			$data = $this->User_groups_m->delete_privileges($priv,$group_id);
			echo json_encode(array("status" => TRUE,"id"=>$priv));
	}

	/*public function ajax_delete_privileges() 
	{
		$group_id = $this->input->post('group_id');
		$privilege_id = $this->input->post('privilege_id');
		$list = $this->User_groups_m->update_privileges($group_id, $privilege_id);	
		$data = array();
		
		if ($list != null) {
			foreach($list as $arr) {
				$this->User_groups_m->delete_privileges($arr->id);
				$data[] = $privilege_id.' deleted';
			}
		} else {
			$data[] = $privilege_id.' did not delete';
		}
			
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}*/

	function ajax_update_group_member_privilege($group_id)
	{
		$list = $this->User_groups_m->get_group_members($group_id);	
		$data = array();
		
		foreach($list as $arr) {
			$row = array();
			$row[] = $arr->id;
			$row[] = $arr->username;
			$data[] = $row;
		}
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_group($id) 
	{
		$list = $this->User_privileges_m->get_datatables();
		$data = array();
		foreach ($list as $myList) {
			$row = array();

			$get_data = $this->User_groups_m->get_privileges_by_id2($id,$myList->id);
			$row[] = $myList->privilege_name;
			$row[] = $myList->description;
				/*if($get_data!=null)
				{
					$row[] = '<a class="btn btn-sm btn-danger" title="Edit" onclick="remove_priv_grp('."'".$myList->id."',"."'".$id."'".')"><i class="fa fa-times"></i></a>';	
				}else{
					$row[] = '<a class="btn btn-sm btn-primary" title="Edit" onclick="add_priv_grp('."'".$myList->id."',"."'".$id."'".')"><i class="fa fa-plus"></i></a>';
				}*/

				if($get_data!=null)
				{
					$row[] = '<input type="checkbox" value="'.$myList->id.'" id="a'.$myList->id."_".$id.'" onchange="remove_priv_grp('."'".$myList->id."',"."'".$id."','".$myList->id."_".$id."'".')" checked class="make-switch" data-size="small">';				
				}else{
					$row[] = '<input type="checkbox" value="'.$myList->id.'"  id="a'.$myList->id."_".$id.'" onchange="add_priv_grp('."'".$myList->id."',"."'".$id."','".$myList->id."_".$id."'".')"  class="make-switch" data-size="small">';
				}
			$data[] = $row;
		}
		$output = array("data"=>$data);
		//output to json format
		echo json_encode($output);
	}
}
