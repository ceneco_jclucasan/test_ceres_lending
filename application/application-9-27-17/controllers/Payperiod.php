<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Payperiod extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Payperiod_m','tblPayperiod');
		$this->load->helper('url');
		$this->load->database();
	}

	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "view_payperiod") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('payperiod_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}
	
	public function ajax_list() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->tblPayperiod->get_datatables();
		$data = array();

		foreach ($list as $myList) {
			$row = array();
			$row['year'] = $myList->year;
			$row['month'] = $myList->month;
			$row['level'] = $myList->level;
			$privileges = $session_data['privileges'];
			$button = '';

		    for ($i=0; $i<sizeof($privileges); $i++) { 
               if ($privileges[$i]->privilege_name == "edit_payperiod") 
               {
					 //$button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_period('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';	
			   } else if ($privileges[$i]->privilege_name == "delete_payperiod"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_period('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }

			}
			
			$row['action'] = $button;
			$data[] = $row;
		}

		$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->tblPayperiod->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{	

			$ym = explode("-", $this->input->post('ym'));

			$data = array(
			'year' => $ym[0],
			'month' => $ym[1],
			'level' => $this->input->post('Level'),);
			$insert = $this->tblPayperiod->save($data);
			echo json_encode(array("status" => TRUE));
	}
	public function ajax_update()
	{
			$data = array(
			'year' => $this->input->post('lastname'),
			'month' => $this->input->post('firstname'),
			'level' => $this->input->post('middlename'),);
			$this->tblPayperiod->update(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
	}
	public function ajax_delete($id)
	{
			$this->tblPayperiod->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
	}

}
