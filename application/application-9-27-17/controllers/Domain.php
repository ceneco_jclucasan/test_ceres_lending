<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Domain extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Domain_m','TblDomain');
		$this->load->helper('url');
		$this->load->database();
	}

	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	      	$data['id'] = $session_data['id'];
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
 	      	
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "domain_view") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('Domain_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login', 'refresh');
	}

	
	public function ajax_list() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->TblDomain->get_datatables();
		$data = array();
		//$no = "";
		
		foreach ($list as $myList) {
			//$no++;
			$row = array();
			$row['name'] = $myList->name;
			$row['id'] = $myList->id;
			$privileges = $session_data['privileges'];
			$button = '';

		    for ($i=0; $i<sizeof($privileges); $i++) { 
               if ($privileges[$i]->privilege_name == "domain_edit") 
               {
					 $button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_domain('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';	
			   } else if ($privileges[$i]->privilege_name == "domain_delete"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_domain('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }
			}
			
			$row['action'] = $button;
			$data[] = $row;
		}

			$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->TblDomain->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{	
			$data = array(
			'name' => $this->input->post('name'),);
			$insert = $this->TblDomain->save($data);
			echo json_encode(array("status" => TRUE));
	}
	public function ajax_update()
	{			
			$data = array(
			'name' => $this->input->post('name'),);
			$this->TblDomain->update(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
	}
	public function ajax_delete($id)
	{
			$this->TblDomain->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
	}

}
