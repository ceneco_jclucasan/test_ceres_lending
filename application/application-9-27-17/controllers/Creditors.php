<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Creditors extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Creditors_m','tblCreditors');
		$this->load->model('Netpays_m','tblNetpay');
		$this->load->model('Trans_loan_m','tblTransloan');
		$this->load->helper('url');
		$this->load->library('csvimport');	
		$this->load->database();
	}

	public function creditors_bracket()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
	      	$data['picture'] = $session_data['picture'];
	      	$datas['class'] = $this->tblCreditors->get_datatables_creditors_class();
 	      	$privileges = $session_data['privileges'];

	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "creditors_view_bracket") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('creditors_bracket_v',$datas);
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function creditors_class()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];

	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "creditors_view_class") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('creditors_class_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function creditors_creditors()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
 	      	
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "creditors_view_creditors") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('creditors_creditors_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}

	public function creditors_creditors_add()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
	      	$data['picture'] = $session_data['picture'];
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
 	      	$privileges = $session_data['privileges'];
	      	$datas['class'] = $this->tblCreditors->get_datatables_creditors_class();
	      	$datas['id'] = $session_data['id'];
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "creditors_creditors_add") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('creditors_creditors_add_v',$datas);
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}
	
	public function ajax_list_querybuilder() 
	{
		$query = $this->input->post('query');
		$list = $this->tblCreditors->get_query_builder_result($query);	
		$data = array();
		$no = "";
		
		foreach($list as $myList) {
			$no++;	  
			$row[] = $myList->lastname;
			$row[] = $myList->firstname;
			$row[] = $myList->middlename;
			$row[] = $myList->suffix;
			$row[] = $myList->email;
			$row[] = $myList->position;
			$row[] = $myList->is_active;		
			$row[] = '<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_Tblemployees('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
			<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_Tblemployees('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';		  
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function get_comakers($id)
	{
		$data = $this->tblCreditors->get_comakers($id);
        echo json_encode($data);
	}
	//BRAKET - SECTION
	public function ajax_list_bracket() 
	{
	    $session_data = $this->session->userdata('logged_in');
		$list = $this->tblCreditors->get_query_builder_result_creditors_bracket_join_class();
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['fy'] = $myList->from_years;
			$row['ty'] = $myList->to_years;
			$row['amt'] = $myList->amount;
			$row['class'] = $myList->class_name;
			$row['los'] = ($myList->from_years!=0?$myList->from_years.'-':'').$myList->to_years.' years';
			$button = '';
			$privileges = $session_data['privileges'];


		    for ($i=0; $i<sizeof($privileges); $i++) { 
               if ($privileges[$i]->privilege_name == "creditors_bracket_edit") 
               {
					 $button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_bracket('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';	
			   } else if ($privileges[$i]->privilege_name == "creditors_bracket_delete"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_bracket('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }

			}
			
			$row['action'] = $button;
			$data[] = $row;
		}

			$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit_bracket($id)
	{
		$data = $this->tblCreditors->get_by_id_creditors_bracket($id);
		echo json_encode($data);
	}

	public function ajax_add_bracket()
	{	
			$data = array(
			'from_years' => $this->input->post('fy'),
			'to_years' => $this->input->post('ty'),
			'amount' => $this->input->post('amount'),
			'creditor_class_id' => $this->input->post('class'),);
			$insert = $this->tblCreditors->save_creditors_bracket($data);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_update_bracket()
	{
			$data = array(
			'from_years' => $this->input->post('fy'),
			'to_years' => $this->input->post('ty'),
			'amount' => $this->input->post('amount'),
			'creditor_class_id' => $this->input->post('class'),);
			$this->tblCreditors->update_creditors_bracket(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete_bracket($id)
	{
			$this->tblCreditors->delete_by_id_creditors_bracket($id);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_get_bracket($id)
	{
		$data = $this->tblCreditors->get_bracket_creditors_bracket($id);
		echo json_encode($data);
	}

	//CLASSIFICATION - SECTION	
	public function ajax_list_classification() 
	{
	    $session_data = $this->session->userdata('logged_in');
		$list = $this->tblCreditors->get_datatables_creditors_class();
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['class'] = $myList->class_name;
			$row['type'] = $myList->type;
			$row['years'] = ($myList->yfrom!=0?$myList->yfrom.'-':'').$myList->yto.' years';
			$row['amount'] = $myList->maximum_amount;
			$button = '';
			$privileges = $session_data['privileges'];
		    for ($i=0; $i<sizeof($privileges); $i++) { 
               if ($privileges[$i]->privilege_name == "creditors_class_edit") 
               {
					 $button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_class('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';	
			   } else if ($privileges[$i]->privilege_name == "creditors_class_delete"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_class('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }
			}			
			$row['action'] = $button;
			$data[] = $row;
		}
		$output = array("data" => $data,"month"=>$this->input->post('month'));
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit_classification($id)
	{
		$data = $this->tblCreditors->get_by_id_creditors_class($id);
		echo json_encode($data);
	}

	public function search_creditors($id)
	{
		$list = $this->tblCreditors->search_creditors($id);
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['id'] = $myList->id;
			$row['name'] = $myList->fullname;
			$row['company'] = $myList->emp_company;
		    $button =' <a href="javascript:;" class="btn blue" onclick="select_creditor('."'".$myList->employee_code."'".')"><i class="fa fa-user" aria-hidden="true"></i> Select Creditor </a>';		
			$row['action'] = $button;
			$data[] = $row;
		}
			$output = array("data" => $data,"val"=>$this->input->post('val'));
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add_classification()
	{	
			$data = array(
			'class_name' => $this->input->post('class'),
			'type' => $this->input->post('type'),
			/*'yfrom' => $this->input->post('from'),
			'yto' => $this->input->post('to'),
			'maximum_amount' => $this->input->post('amount')*/);
			$insert = $this->tblCreditors->save_creditors_class($data);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_update_classification()
	{
			$data = array(
			'class_name' => $this->input->post('class'),
			'type' => $this->input->post('type'),
			/*'yfrom' => $this->input->post('from'),
			'yto' => $this->input->post('to'),
			'maximum_amount' => $this->input->post('amount')*/);
			$this->tblCreditors->update_creditors_class(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete_classification($id)
	{
			$this->tblCreditors->delete_by_id_creditors_class($id);
			echo json_encode(array("status" => TRUE));
	}

	//CREDITORS - SECTION
	public function ajax_add_creditors()
	{	
		  	$date=date_create($this->input->post('dob'));
            $dob = date_format($date,"Y-m-d");

            $date2=date_create($this->input->post('employeddate'));
            $empdate = date_format($date2,"Y-m-d");

            $from = new DateTime($this->input->post('dobs'));
			$to   = new DateTime('today');
			$age =  $from->diff($to)->y;

			$los =  $date2->diff($to)->y;

			$data = array(
			'lastname' => $this->input->post('lname'),
			'firstname' => $this->input->post('fname'),
			'middlename' => $this->input->post('mname'),
			'suffix' => $this->input->post('suffix'),
			'fullname' => $this->input->post('fname').' '.substr($this->input->post('mame'),0,1).' '.$this->input->post('lname'),
			'picture' => $this->input->post('imageholder'),
			'gender' => $this->input->post('gender'),
			'civil_status' => $this->input->post('civil'),
			'age' => $age,
			'Address' => $this->input->post('address'),
			'mobileno' => $this->input->post('mobo'),
			'telno' => $this->input->post('telno'),
			'emailadd' => $this->input->post('emailadd'),
			'birthdate' =>  $dob,
			'no_dependents' => $this->input->post('no_dep'),
			'no_studying' => $this->input->post('no_stud'),
			'home_type' => $this->input->post('hometype'),
			'employer' => $this->input->post('employer'),
			'employer_address' => $this->input->post('emp_address'),
			'emp_position' => $this->input->post('emp_position'),
			'employer_contactno' => $this->input->post('emp_contactno'),
			'employed_date' => $empdate,
			'lengthofservice' => $los,//$this->input->post('los'),
			'employee_code' => $this->input->post('emp_code'),
			'salary_level' => $this->input->post('sal_level'),
			'employee_class_id' => $this->input->post('class'),
			'salary_daily' => $this->input->post('sal_daily'),
			'salary_monthly' => $this->input->post('sal_monthly'),
			'supervisor_name' => $this->input->post('super_name'),
			'supervisor_contact' => $this->input->post('super_contactno'),
			'isnotactive' => '',
			'remarks' => $this->input->post('remarks'),
			);
			$insert = $this->tblCreditors->save_creditors($data);
			echo json_encode(array("status" => TRUE,"dob"=>$dob));
	}


	public function ajax_update_creditors()
	{	
		 	$date=date_create($this->input->post('dob'));
            $dob = date_format($date,"Y-m-d");

            $date2=date_create($this->input->post('employeddate'));
            $empdate = date_format($date2,"Y-m-d");

            $from = new DateTime($this->input->post('dob'));
			$to   = new DateTime('today');
			$age =  $from->diff($to)->y;

			$los =  $date2->diff($to)->y;
            
			$data = array(
			'lastname' => $this->input->post('lname'),
			'firstname' => $this->input->post('fname'),
			'middlename' => $this->input->post('mname'),
			'suffix' => $this->input->post('suffix'),
			'fullname' => $this->input->post('fname').' '.substr($this->input->post('mame'),0,1).' '.$this->input->post('lname'),
			'picture' => $this->input->post('imageholder'),
			'gender' => $this->input->post('gender'),
			'civil_status' => $this->input->post('civil'),
			'age' => $age,
			'Address' => $this->input->post('address'),
			'mobileno' => $this->input->post('mobo'),
			'telno' => $this->input->post('telno'),
			'emailadd' => $this->input->post('emailadd'),
			'birthdate' =>  $dob,
			'no_dependents' => $this->input->post('no_dep'),
			'no_studying' => $this->input->post('no_stud'),
			'home_type' => $this->input->post('hometype'),
			'employer' => $this->input->post('employer'),
			'employer_address' => $this->input->post('emp_address'),
			'emp_position' => $this->input->post('emp_position'),
			'employer_contactno' => $this->input->post('emp_contactno'),
			'employed_date' => $empdate,
			'lengthofservice' =>$los,
			'employee_code' => $this->input->post('emp_code'),
			'salary_level' => $this->input->post('sal_level'),
			'employee_class_id' => $this->input->post('class'),
			'salary_daily' => $this->input->post('sal_daily'),
			'salary_monthly' => $this->input->post('sal_monthly'),
			'supervisor_name' => $this->input->post('super_name'),
			'supervisor_contact' => $this->input->post('super_contactno'),
			'isnotactive' => '',
			'remarks' => $this->input->post('remarks'),
			);
			$this->tblCreditors->update_creditors(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE,"dob"=>$dob));
	}

	public function ajax_list_creditors() 
	{
	    $session_data = $this->session->userdata('logged_in');
		$list = $this->tblCreditors->get_datatables_creditors();
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['pic'] = $myList->picture;
			$row['fn'] = $myList->fullname;
			$row['sex'] = $myList->gender;
			$row['cs'] = $myList->civil_status;
			$row['age'] = $myList->age;
			$row['code'] = $myList->employee_code;
			$row['position'] = $myList->emp_position;
			$row['company'] = $myList->emp_company;
			$row['classfication'] = $myList->emp_classification;
			$button = '';
			$privileges = $session_data['privileges'];

		    for ($i=0; $i<sizeof($privileges); $i++) { 
               if ($privileges[$i]->privilege_name == "creditors_creditors_edit") 
               {
					 $button = $button.'<a href="'.site_url().'Creditors/creditors_creditors_add?id='.$myList->employee_code.'" class="btn btn-xs btn-primary" title="Edit">View</a>';	
			   } else if ($privileges[$i]->privilege_name == "creditors_creditors_delet"){
				 	$button = $button.'<a class="btn btn-xs btn-danger" title="Delete" onclick="delete_creditor('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }

			}
			
			$row['action'] = $button;
			$data[] = $row;
		}

			$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_creditors_company() 
	{
	    $session_data = $this->session->userdata('logged_in');
		$list = $this->tblCreditors->get_datatables_creditors_by_company();
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
		
			$row['company'] = $myList->emp_company;
			$row['counter'] = $myList->counter;
			$data[] = $row;
		}

			$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_creditors_loan($id) 
	{
	    $session_data = $this->session->userdata('logged_in');
		$list = $this->tblCreditors->get_creditors_loan_list($id);
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['trn'] = $myList->id;
			$row['amount'] = $myList->approved_amount;
			$row['status'] = $myList->status;
			$row['action'] = '<a href="'.site_url().'Creditors/creditors_creditors_add?id='.$myList->creditor_id.'" class="btn btn-xs btn-primary" title="Edit">View</a>';
			$data[] = $row;
		}

			$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit_creditors($id)
	{
		$data = $this->tblCreditors->get_by_id_creditors($id);
		echo json_encode($data);
	}

	public function ajax_delete_creditors($id)
	{
			$this->tblCreditors->delete_by_id_creditors($id);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_get_netpay($id)
	{
		$list = $this->tblNetpay->get_by_empid($id);
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$get_period = $this->tblTransloan->get_payperiod_id($myList->payroll_period);
			$row['amount'] = number_format(($myList->amount), 2);
			$row['remarks'] = $myList->remarks;
			$row['date'] = ($get_period!=null?$get_period->month.'-'.$get_period->year.'-'.$get_period->level:'');
			$data[] = $row;
		}
		$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

}
