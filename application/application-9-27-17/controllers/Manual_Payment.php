<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Manual_Payment extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Manual_Payment_m','TblManualPayment');
		$this->load->model('Trans_loan_m','tblTransloan');
		$this->load->model('Ledger_m','tblLedger');
		$this->load->helper('url');
		$this->load->database();
	}

	public function index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	      	$data['id'] = $session_data['id'];
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
 	      	
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "view_manualpayment") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('Manual_payment_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}
	
	public function ajax_list() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->TblManualPayment->get_payment();
		$data = array();
		
		foreach ($list as $myList)
		{
			$row = array();
			$row['creditor'] = $myList->fullname;
			$row['period'] = $myList->payperiod;
			$row['amount'] = $myList->amount;
			$row['orno'] = $myList->orno;
			$row['ordate'] = $myList->ordate;
			$privileges = $session_data['privileges'];
			$button = '';

		    for ($i=0; $i<sizeof($privileges); $i++) { 
               if ($privileges[$i]->privilege_name == "edit_payment") 
               {
					 $button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_payment('."'".$myList->id."','".$myList->hid."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';	
			   } /*else if ($privileges[$i]->privilege_name == "domain_delete"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_domain('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }*/
			}
			
			$row['action'] = $button;
			$data[] = $row;
		}

			$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{	
			date_default_timezone_set('Asia/Singapore');	
			$session_data = $this->session->userdata('logged_in');  
			$currDate  = date("Y-m-d H:i:s");

			$data = array(
				'orno' => $this->input->post('orno'),
				'creditor' => $this->input->post('search_creditor'),
				'ordate' => $this->input->post('ordate'),
				'partticulars' => $this->input->post('particulars'),
				'created_by' =>  $session_data['id'],
				'created_dt' =>  $currDate, 
			);
			$insert_h = $this->TblManualPayment->save_h($data);

			//if purpose not equal to Others proceed
			//Others is only for Ledger with only beginning balance
			if($this->input->post('period_id')!="others")
			{
				$get_loan_detail = $this->tblTransloan->get_loan_period($this->input->post('period_id'));
				$data2 = array(
					'head_id' => $insert_h,
					'trans_loan_detail_id' => $this->input->post('period_id'),
					'payperiod' => $get_loan_detail->payperiod,
					'amount' => $this->input->post('amount'),
				);
				$insert_b = $this->TblManualPayment->save_b($data2);
				//total_payment = interest + principal
				$deduction = floatval($get_loan_detail->interest) + floatval($get_loan_detail->principal);
				$balance = ($get_loan_detail->variance!=null&&$get_loan_detail->variance>=1?$get_loan_detail->variance:$deduction);
				$variance = floatval($balance) - floatval($this->input->post('amount'));
				$data3 = array(
					'variance' => $variance,
					"manual_payment_id" => $insert_b
				);
				$update_trans_loan_details = $this->tblTransloan->update_transloan_details(array('id' => $this->input->post('period_id')), $data3);

				//insert into ledger
				$get_ledger = $this->tblLedger->get_by_creditor($this->input->post('search_creditor'));
		     	$balance = floatval($get_ledger->balance) - floatval($this->input->post('amount'));
		     	$ledger_data = array(
			        'reference_no'=> $this->input->post('orno'),
			        'creditor_code'=> $this->input->post('search_creditor'),
			        'dt'=> $this->input->post('ordate'),
		        	'particulars' => $this->input->post('particulars'),
		        	'amount'=> $this->input->post('amount'),
		        	'balance'=> $balance,
		        	'type'=> 'Cr',
		        	'is_final' => 1,
			    );
			    $this->tblLedger->save($ledger_data);
			    //update the final balance so that we can get easily which balance    
			    $ledger_data2 = array(
		        	'is_final' => 0,
		        );
			    $this->tblLedger->update(array('id' => $get_ledger->id), $ledger_data2);

				echo json_encode(array("status" => TRUE,"with_balance"=>FALSE,"deduction"=>$deduction,"variance"=>$variance,"loan"=>$update_trans_loan_details));
			}else{
				//save to ledger only for Beginning balance
				//insert it to ledger
		                    	//this will only reflect to ledger
		                    	$get_ledger = $this->tblLedger->get_by_creditor($this->input->post('search_creditor'));
		                    	$balance = floatval($get_ledger->balance) - floatval($this->input->post('amount'));
		                    	$ledger_data = array(
			                        'reference_no'=> $this->input->post('orno'),
			                        'creditor_code'=> $this->input->post('search_creditor'),
			                        'dt'=> $this->input->post('ordate'),
		                        	'particulars' => 'Payment for '.$this->input->post('orno'),//$this->input->post('particulars'),
		                        	'amount'=> $this->input->post('amount'),
		                        	'balance'=> $balance,
		                        	'type'=> 'Cr',
		                        	'is_final' => 1,
			                    );
			                    $this->tblLedger->save($ledger_data);
			                    //update the final balance so that we can get easily which balance    
			                    $ledger_data2 = array(
		                        	'is_final' => 0,
			                    );
								$this->tblLedger->update(array('id' => $get_ledger->id), $ledger_data2);
								echo json_encode(array("status" => TRUE,"with_balance"=>TRUE));

			}
			
	}

	public function ajax_update()
	{			
			date_default_timezone_set('Asia/Singapore');	
			$session_data = $this->session->userdata('logged_in');  
			$currDate  = date("Y-m-d H:i:s");

			$data = array(
				'orno' => $this->input->post('orno'),
				'creditor' => $this->input->post('search_creditor'),
				'ordate' => $this->input->post('ordate'),
				'partticulars' => $this->input->post('particulars'),
				'created_by' =>  $session_data['id'],
				'created_dt' =>  $currDate, 
			);
			$insert_h = $this->TblManualPayment->update_h($data);

			$get_loan_detail = $this->tblTransloan->get_loan_period($this->input->post('period_id'));
			$data2 = array(
				'head_id' => $insert_h,
				'trans_loan_detail_id' => $this->input->post('period_id'),
				'payperiod' => $get_loan_detail->payperiod,
				'amount' => $this->input->post('amount'),
			);
			$insert_b = $this->TblManualPayment->update_b($data2);

			//total_payment = interest + principal
			$deduction = floatval($get_loan_detail->interest) + floatval($get_loan_detail->principal);
			$balance = ($get_loan_detail->variance!=null&&$get_loan_detail->variance>=1?$get_loan_detail->variance:$deduction);
			$variance = floatval($balance) - floatval($this->input->post('amount'));
			$data3 = array(
				'variance' => $variance,
				"manual_payment_id" => $insert_b
			);
			$update_trans_loan_details = $this->tblTransloan->update_transloan_details(array('id' => $this->input->post('period_id')), $data3);

			echo json_encode(array("status" => TRUE,"deduction"=>$deduction,"variance"=>$variance,"loan"=>$update_trans_loan_details));
	}

	public function ajax_delete($id)
	{
			$this->TblManualPayment->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit_b($id)
	{
		$data = $this->TblManualPayment->get_by_id_b($id);
		echo json_encode($data);
	}

	public function ajax_edit_h($id)
	{
		$data = $this->TblManualPayment->get_by_id_h($id);
		echo json_encode($data);
	}
}
