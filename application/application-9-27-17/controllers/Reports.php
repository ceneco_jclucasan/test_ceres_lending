<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Reports extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Payments_m','tblPayments');
		$this->load->model('Netpays_m','tblNetpays');
		$this->load->model('Products_m','tblProducts');
		$this->load->model('Trans_loan_m','tblTransloans');
		$this->load->model('Creditors_m','tblCreditors');
		$this->load->helper('url');
		$this->load->database();
	}

	public function deduction_upload()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "view_reports") {
			    	$viewpage = TRUE;
				}
		    }

		    if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}

		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('reports_deduction_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}
	
	public function reports_deduction_upload() 
	{
		$session_data = $this->session->userdata('logged_in');
		$period = date("Y").'-'.$this->input->post('month').'-'.$this->input->post('type');
		$list = $this->tblPayments->get_by_payperiod($period);
		$data = array();
		$data2 = array();

		$row2[] = "Name of Employee";
		$row2[] = "Amount";
		$data2[] = $row2;
		foreach ($list as $myList)
		{
			$row = array();		
			$row2 = array();			
		   	$get_body_payment = $this->tblPayments->get_by_body_id($myList->id);			
			foreach ($get_body_payment as $key)
			{
				$get_employee = $this->tblCreditors->get_by_id_creditors($key->employee_id);
				$row['employee'] = $get_employee->fullname;
				$row['amount'] = $key->amount;
				$data[] = $row;

				$row2[] = $get_employee->fullname;
				$row2[] = $key->amount;
				$data2[] = $row2;
			}		
		}
		$output = array("data" => $data,"export" => $data2,);
		echo json_encode($output);
	}

}
