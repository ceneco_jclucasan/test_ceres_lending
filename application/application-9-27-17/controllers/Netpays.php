<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Netpays extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Netpays_m','tblNetpays');
		$this->load->model('Payments_m','tblPayments');
		$this->load->model('Trans_loan_m','tblTransloan');
		$this->load->model('Creditors_m','tblCreditors');
		$this->load->model('Domain_m','tblDomains');
		$this->load->model('Ledger_m','tblLedger');
		$this->load->helper('url');
		$this->load->library('csvimport');	
		$this->load->database();
	}

	public function Index()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['picture'] = $session_data['picture'];
	      	
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$data['id'] = $session_data['id'];
 	      	$privileges = $session_data['privileges'];
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "netpays_view") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$dropdown['payperiod'] = $this->tblTransloan->get_datatables_payperiod();
		    	$dropdown['class'] = $this->tblCreditors->get_datatables_creditors_class();
		    	$dropdown['company']  = $this->tblDomains->get_datatables();
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('netpay_v',$dropdown);
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      //If no session, redirect to login page
	      redirect('login', 'refresh');
	    }
	}
	
	public function ajax_list_querybuilder() 
	{
		$query = $this->input->post('query');
		$list = $this->tblNetpays->get_query_builder_result($query);	
		$data = array();
		$no = "";
		
		foreach($list as $myList) {
			$no++;	  
			$row[] = $myList->lastname;
			$row[] = $myList->firstname;
			$row[] = $myList->middlename;
			$row[] = $myList->suffix;
			$row[] = $myList->email;
			$row[] = $myList->position;
			$row[] = $myList->is_active;		
			$row[] = '<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_Tblemployees('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
			<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_Tblemployees('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';		  
			$data[] = $row;
		}
		
		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list() 
	{
	    $session_data = $this->session->userdata('logged_in');
		$list = $this->tblNetpays->get_datatables();
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['creditor'] = $myList->class_name;
			$row['amount'] = $myList->class_name;
			$row['remarks'] = $myList->class_name;
			$button = '';
			$privileges = $session_data['privileges'];

		    for ($i=0; $i<sizeof($privileges); $i++) { 
               if ($privileges[$i]->privilege_name == "creditors_class_edit") 
               {
					 $button = $button.'<a class="btn btn-sm btn-primary" title="Edit" onclick="edit_class('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';	
			   } else if ($privileges[$i]->privilege_name == "creditors_class_delete"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_class('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }
			}
			
			$row['action'] = $button;
			$data[] = $row;
		}

			$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_repo() 
	{
	    $session_data = $this->session->userdata('logged_in');
		$list = $this->tblNetpays->get_datatables_repo();
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['creditor'] = $myList->creditor;
			$row['amount'] = $myList->amount;
			$row['deduction'] = $myList->deduction;
			$data[] = $row;
		}
		$output = array("data" => $data,);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->tblNetpays->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{	
			$data = array(
			'class_name' => $this->input->post('class'));
			$insert = $this->tblNetpays->save($data);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
			$data = array(
			'class_name' => $this->input->post('class'));
			$this->tblNetpays->update(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
			$this->tblNetpays->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete_uploads()
	{
		$list = $this->tblNetpays->get_datatables_repo();
			foreach ($list as $key) {
				$data = array(
				'status' => 'Confirmed');
				$this->tblNetpays->update(array('uploadid' => $key->id), $data);
				$this->tblPayments->update_body(array('uploadid' => $key->id), $data);
			}
			$this->tblNetpays->delete_by_uploads();
			echo json_encode(array("status" => TRUE));
	}

	public function ajax_cancel_uploads()
	{		
			$list = $this->tblNetpays->get_datatables_repo();
			foreach ($list as $key) {
				$this->tblNetpays->delete_by_uplodid($key->id);
				$this->tblPayments->delete_by_uplodid($key->id);
			}
			$this->tblNetpays->delete_by_uploads();
			echo json_encode(array("status" => TRUE));
	}
	
	public function importnetpays()
	{
		$currDate  = date("Y-m-d H:i:s");
		$session_data = $this->session->userdata('logged_in');
        $data['addressbook'] = $this->tblNetpays->get_addressbook();
        $data['error'] = '';    //initialize image upload error array to empty
 
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';
 
        $this->load->library('upload', $config); 
 
        // If upload failed, display error
        if (!$this->upload->do_upload()) {
            $data['error'] = $this->upload->display_errors();
 
            $this->load->view('csvindex', $data);
        }
        else{

            $file_data = $this->upload->data();
            $file_path =  './uploads/'.$file_data['file_name'];
 
            if ($this->csvimport->get_array($file_path))
            {
                $csv_array = $this->csvimport->get_array($file_path);
                $payment_counter = false;

                		if($this->input->post('ar_no')!=null&&$this->input->post('total')!=null)
	                    {
		                    $ph = array(
		                        'or_arno'=>$this->input->post('ar_no'),
		                        'amount'=>$this->input->post('total'),
		                        //'payperiod_id'=> $this->input->post('pp'),
		                        'payperiod'=> date("Y").'-'.$this->input->post('month').'-'.$this->input->post('type'),
		                        'created_by'=>$session_data['id'],
		                        'created_dt'=>$currDate,
		                        'particulars'=> $this->input->post('particulars'),
		                        'company'=> $this->input->post('company'),
		                        'emp_class'=> $this->input->post('emp_class'),
		                    );
		                    $payments = $this->tblPayments->save_head($ph);   
		                    $payment_counter = true;                 	
	                    }
                 $pay_period = date("Y").'-'.$this->input->post('month').'-'.($this->input->post('type')=="1st" ? '1':'2');
                foreach ($csv_array as $row)
                {
              		$insert_data1 = array(
		                'creditor'=>$row['creditor_id'],
		                'amount'=> ($row['amount']!=null?$row['amount']:'0'),
		                'deduction'=>$row['deduction'],
		            );                    
		            //insert to repository temporary
		            $uploadid = $this->tblNetpays->insert_csv_repo($insert_data1);	

                	//check if theres is column amount in the file being uploaded
                	if($row['amount']!=null)
                	{             
                		$pp_payroll_period = date("Y").'-'.$this->input->post('month').'-'.$this->input->post('type');
	                	$check_newtpay = $this->tblNetpays->get_by_payroll_period($pp_payroll_period ,$row['creditor_id']);
	                	//if no netpay for the year and creditor insert,else just do nothing
	                	if($check_newtpay == null)
	                	{
	                		
		                    $insert_data = array(
		                        'creditor_id'=>$row['creditor_id'],
		                        'payroll_period'=> $pp_payroll_period,//$this->input->post('year').'-'.$this->input->post('month').'-'.$this->input->post('type'),
		                        'amount'=>$row['amount'],
		                        'createdby'=>$session_data['id'],
		                        'createdt'=>$currDate,
		                        'uploadid' => $uploadid 
		                    );                    
		                    $this->tblNetpays->insert_csv($insert_data);
	                	} 
                	}                	              

                	//below code is for payment upload, if ARNO & TOTAL IS NOT NULL, means that they want to upload payment  
                	if($payment_counter == true)
                	{
                		if($row['creditor_id']!=null&&$row['deduction']!=null)
	                    {
		                    $check_transloan_detail = $this->tblTransloan->get_transloan_detail($row['creditor_id'],$pay_period);       
		                    if($check_transloan_detail!=null)
		                    {
		                    	//insert it to payment body
		                    	$pb = array(
			                        'payment_head_id'=>$payments,
			                        'employee_id'=>$row['creditor_id'],
			                        'amount'=>$row['deduction'],
		                        	'uploadid' => $uploadid,
		                        	'trans_details_id'=> $check_transloan_detail->id
			                    );
			                    $insert_payment_body = $this->tblPayments->save_body($pb);    

			                    $payment = floatval($check_transloan_detail->interest)+floatval($check_transloan_detail->principal);
			                    //insert to transloan to update schedule
		                    	$data = array(
									'or_arno' => $this->input->post('ar_no'),
									'amount' => $row['deduction'],									
		                        	'variance'=>floatval($payment)-floatval($row['deduction']),
									'payment_body_id' => $insert_payment_body,
								);
								$this->tblTransloan->update_transloan_details(array('id' => $check_transloan_detail->id), $data);

		                    } 

		                    	//insert it to ledger
		                    	//this will only reflect to ledger
		                    	$get_ledger = $this->tblLedger->get_by_creditor($row['creditor_id']);
		                    	$balance = floatval($get_ledger->balance) - floatval($row['deduction']);
		                    	$ledger_data = array(
			                        'reference_no'=>$this->input->post('ar_no'),
			                        'creditor_code'=>$row['creditor_id'],
			                        'dt'=>$currDate,
		                        	'particulars' => $this->input->post('particulars'),
		                        	'amount'=> $row['deduction'],
		                        	'balance'=> $balance,
		                        	'type'=> 'Cr',
		                        	'is_final' => 1,
			                    );
			                    $this->tblLedger->save($ledger_data);
			                    //update the final balance so that we can get easily which balance    
			                    $ledger_data2 = array(
		                        	'is_final' => 0,
			                    );
								$this->tblLedger->update(array('id' => $get_ledger->id), $ledger_data2);       	
	                    }
                	}
                }

                $this->session->set_flashdata('success', 'Csv Data Imported Succesfully');
                redirect(base_url().'Netpays?id=ok');
            } else 
                $data['error'] = "Error occured";
                $this->load->view('csvindex', $data);
         } 
    }//end 
}
