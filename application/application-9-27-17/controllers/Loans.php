<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Loans extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Netpays_m','tblNetpays');
		$this->load->helper('url');
		$this->load->library('csvimport');	
		$this->load->model('Products_m','tblProducts');
		$this->load->model('Trans_loan_m','tblTransloan');
		$this->load->model('Creditors_m','tblCreditors');
		$this->load->model('Ledger_m','tblLedger');
		$this->load->database();
	}

	public function Loan_app()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];

	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "loanapp_view") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('loan_app_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      redirect('login', 'refresh');
	    }
	}

	public function Loan_validation()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];

	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "loanvalidation_view") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('loan_val_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      redirect('login', 'refresh');
	    }
	}

	public function Loan_transaction_monitor()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
	      	$data['picture'] = $session_data['picture'];

	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
 	      	$privileges = $session_data['privileges'];
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "view_trans_monitor") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('transaction_monitor_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      redirect('login', 'refresh');
	    }
	}

	public function Loan_approved()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
 	      	$privileges = $session_data['privileges'];
	      	$data['picture'] = $session_data['picture'];

	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "loanapproved_view") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('loan_approved_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      redirect('login', 'refresh');
	    }
	}

	public function Loan_disbursement()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['id'] = $session_data['id'];
	      	$data['picture'] = $session_data['picture'];

	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
 	      	$privileges = $session_data['privileges'];
	      	$viewpage = "";
	      	for ($i=0; $i<sizeof($privileges); $i++) {
	      		if ($privileges[$i]->privilege_name == "loandisbursement_view") {
			    	$viewpage = TRUE;
				}
		    }
		    if ($viewpage) {
		    	$this->load->view('templates/header');
			  	$this->load->view('templates/nav1', $data);	
			  	$this->load->view('loan_disbursement_v');
			  	$this->load->view('templates/footer');
		    } else if ($viewpage == "") {
		    	redirect('dashboard', 'refresh');	
		    }
	    }
	    else
	    {
	      redirect('login', 'refresh');
	    }
	}

	public function Loan_validation_dt()
	{
		if($this->session->userdata('logged_in'))
	    {
	      	$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['firstname'].' '.$session_data['lastname'];
	      	$data['privileges'] = $session_data['privileges'];
	      	$data['group_id'] = $session_data['group_id'];
	      	$data['picture'] = $session_data['picture'];
	      	
	      	if($session_data['emp_code']!=null)
	      	{
	      		$data['emp_code'] = $session_data['emp_code'];
	      	}else{
	      		$data['emp_code'] = 'test';
	      	}
	      	$data['id'] = $session_data['id'];
		    $this->load->view('templates/header');
			$this->load->view('templates/nav1', $data);	
			$this->load->view('loan_val_dt_v');
			$this->load->view('templates/footer');		    
	    }
	    else
	    {
	      redirect('login', 'refresh');
	    }
	}

	public function ajax_list_loan_app() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->tblTransloan->get_pending_app();
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['trn'] = $myList->id;
			$com1 = $this->tblCreditors->get_by_id_creditors($myList->comaker1);
			$com2 = $this->tblCreditors->get_by_id_creditors($myList->comaker2);
			$row['com1'] = $com1->fullname;
			$row['com2'] = $com2->fullname;
			$row['product_name'] = $myList->product_name;
			$row['creditor'] = $myList->fullname;
			$row['amount'] = number_format($myList->applied_amount, 2, '.', ',');
			$row['terms'] = $myList->terms;
			$row['purpose'] = $myList->purpose;
			$row['validators_remark'] = $myList->validators_remark;
			$button = '';
			$privileges = $session_data['privileges'];
		    for ($i=0; $i<sizeof($privileges); $i++)
		    { 
               if ($privileges[$i]->privilege_name == "loanapp_edit") 
               {
					$button = $button.'<a class="btn green btn-outline sbold" data-toggle="modal" href="#full" title="Edit" onclick="edit_loan('."'".$myList->id."'".')">View</a>';	
			   } else if ($privileges[$i]->privilege_name == "loanapp_delete"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_loan('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }
			}			
			$row['action'] = $button;
			$row['created_dt'] = date("M d, Y", strtotime($myList->created_on));
			$data[] = $row;
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}

	public function ajax_list_loan_app_documents($id) 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->tblTransloan->get_docu($id);
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			if($myList!=null)
			{
				$row['filename'] = $myList->filename;

				/*$row['filename'] = '<div id="js-grid-juicy-projects" class="cbp">'.
                                '<div class="cbp-item graphic">'.
                                    '<div class="cbp-caption">'.
                                        '<div class="cbp-caption-defaultWrap">'.
                                            '<img src="'.base_url()."uploads/files/Form4.png".'" alt=""> </div>'.
                                        '<div class="cbp-caption-activeWrap">'.
                                            '<div class="cbp-l-caption-alignCenter">'.
                                               ' <div class="cbp-l-caption-body">'.
                                                    '<a href="'.base_url()."uploads/files/Form4.png".'" class="cbp-lightbox cbp-l-caption-buttonRight btn red uppercase btn red uppercase" data-title="Dashboard<br>by Paul Flavius Nechita">view larger</a>'.
                                                '</div> </div> </div> </div>'.
                                    '<div class="cbp-l-grid-projects-title uppercase text-center uppercase text-center">Dashboard</div>'.
                                    '<div class="cbp-l-grid-projects-desc uppercase text-center uppercase text-center">Web Design / Graphic</div>'.
                                '</div>';*/

                                // /$myList->filename;
				$button = '';
				$privileges = $session_data['privileges'];
			    for ($i=0; $i<sizeof($privileges); $i++)
			    { 
	               if ($privileges[$i]->privilege_name == "docu_delete") 
	               {
						 $button = $button.'<a class="btn red btn-outline sbold"  title="Edit" onclick="delete_docu('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	
				   } 
				}			
				$row['action'] = $button;
				$data[] = $row;	
			}
			
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}

	public function ajax_get_loan_data($id)
	{
		$data = $this->tblTransloan->get_by_id($id);
		$c1 = $this->tblCreditors->get_by_id_creditors($data->comaker1);
		$c2 = $this->tblCreditors->get_by_id_creditors($data->comaker2);
		$product = $this->tblProducts->get_by_id_products($data->product_id);
		$data2 = array("data"=>$data,"comaker1"=>$c1,"comaker2"=>$c2,"product"=>$product);
		echo json_encode($data2);
	}

	public function ajax_validate()
	{	
	    $session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');	  
		$currDate  = date("Y-m-d H:i:s");
		$data = array(
			//'validators_remark' => $this->input->post('remarks'),
			'status' => 'Validated',
			'validate_by' => $session_data['firstname'].' '.$session_data['lastname'],
			'validate_on' => $currDate,
		);
		$insert = $this->tblTransloan->update(array('id' => $this->input->post('id')), $data);		
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_checked()
	{	
	    $session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');	  
		$currDate  = date("Y-m-d H:i:s");
		$data = array(
			'status' => 'Checked',
			'checked_by' => $session_data['firstname'].' '.$session_data['lastname'],
			'checked_on' => $currDate,
		);
		$insert = $this->tblTransloan->update(array('id' => $this->input->post('id')), $data);		
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_approved()
	{	
	    $session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');	  
		$currDate  = date("Y-m-d H:i:s");
		$data = array(
			'status' => 'Approved',
			'approved_by' => $session_data['firstname'].' '.$session_data['lastname'],
			'approved_on' => $currDate,
			'approved_amount' => $this->input->post('amount'),
			'approved_interest' => $this->input->post('interest'),
			'approved_term' => $this->input->post('terms'),
			'approved_total' => $this->input->post('total'),
			'approved_cutoff' => $this->input->post('cutoff'),
		);
		$insert = $this->tblTransloan->update(array('id' => $this->input->post('id')), $data);	
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_released()
	{	
	    $session_data = $this->session->userdata('logged_in');
		date_default_timezone_set('Asia/Singapore');	  
		//date_default_timezone_set("Asia/Kuala_Lumpur");
		$currDate  = date("Y-m-d H:i:s");
		$get_creditors_data = $this->tblCreditors->get_by_id_creditors($this->input->post('creditor'));
		$get_type = $this->tblCreditors->get_by_id_creditors_class($get_creditors_data->employee_class_id);
		
		$date2 = date_create($this->input->post('pay_off_date'));
        $load_date = date_format($date2,"Y-m-d");
		$data = array(
			'status' => 'Released',
			'release_by' => $session_data['firstname'].' '.$session_data['lastname'],
			'release_on' => $currDate,
			//'first_deduct_sched' => $this->input->post('year').'-'.$this->input->post('month').'-'.$this->input->post('type'),
			'first_deduct_sched' => date('Y').'-'.$this->input->post('month').'-'.$this->input->post('type'),
			'class' => $this->input->post('class_type'),
			'voucherno' => $this->input->post('voucher_no'),
			'bankname' => $this->input->post('bankname'),
			'checkno' =>$this->input->post('check_no'),
			'isDisbursed' => 1,
			/*'approved_amount' => $this->input->post('amount'),
			'approved_interest' => $this->input->post('interest'),
			'approved_term' => $this->input->post('terms'),
			'approved_total' => $this->input->post('total'),
			'approved_cutoff' => $this->input->post('cutoff'),
			'start_date' => $this->input->post('pay_off_date'),*/
		);
		$insert = $this->tblTransloan->update(array('id' => $this->input->post('id')), $data);	
		$id = $this->input->post('id');
		$amount = $this->input->post('amount');
		$interest = $this->input->post('interest');
		$total = $this->input->post('total');
		$terms = $this->input->post('terms');
		$pay_off_date = $this->input->post('pay_off_date');

		$test_date_only = $load_date;// date('Y-m-d', strtotime('+1 month'));
		date("Y-m-d");

			//$new_terms = $terms*2;
			$new_terms = $terms;
			$new_terms2 = $terms*2;
			$f_interest = floatval($interest)/intval($new_terms2);
			$f_principal = floatval($amount)/intval($new_terms2);		
			$f_payable = floatval($f_principal)+floatval($f_interest);
			$a=0;
			$b='';
			$year = $this->input->post('year');
			$month = $this->input->post('month');
			$type = $this->input->post('type');
			$year_counter = intval(date("Y"));//intval($this->input->post('year')); 

			if($type==2)
			{
				$a=2;
			}
			if($type==1)
			{
				$a=1;
			}
			for($i=1;$i<=$new_terms;$i++)
			{
				/*$f_balance = floatval($total)-floatval($f_payable);
				$transaction_detail = array(
					'transloan_id' => $id,
					'payment_schedule' => $test_date_only,
					'interest' => $f_interest,
					'principal' => $f_principal,
					'balance' => $f_balance,
				);
				$insert = $this->tblTransloan->save_transloan_details($transaction_detail);*/
				if($type==1)
				{
					if($month>12)
					{
						$month = 1;
						$year_counter++;
					}

					if($a==1)
					{
						/*$payperiod_detail = array(
							'year' => date("Y", strtotime($test_date_only)),
							'month' => date("m", strtotime($test_date_only)),
							'level' => '1st',
						);*/

						//$ppd = $this->tblTransloan->save_payperiod($payperiod_detail); 
						$type1=1;
						$f_balance = floatval($total)-floatval($f_payable);
						$transaction_detail = array(
							'transloan_id' => $id,
							'payperiod' => $year_counter.'-'.$month.'-'.$type1,
							'interest' => $f_interest,
							'principal' => $f_principal,
							'balance' => $f_balance,
							'creditor_id' => $this->input->post('creditor_id'),
						);
						$insert = $this->tblTransloan->save_transloan_details($transaction_detail);
						$a=2;
						$total = $f_balance;
					}

					if($a==2)
					{
						$type2=2;

						$f_balance = floatval($total)-floatval($f_payable);
						$transaction_detail = array(
							'transloan_id' => $id,
							'payperiod' => $year_counter.'-'.$month.'-'.$type2,
							'interest' => $f_interest,
							'principal' => $f_principal,
							'balance' => $f_balance,
							'creditor_id' => $this->input->post('creditor_id'),
						);
						$insert = $this->tblTransloan->save_transloan_details($transaction_detail);
						$a=1;
						$total = $f_balance;
					}
					$month++;
				}

				if($type==2)
				{
					if($month>12)
					{
						$month = 1;
						$year_counter++;
					}

					if($a==2)
					{
						$type3=2;
						$f_balance = floatval($total)-floatval($f_payable);
						$transaction_detail = array(
							'transloan_id' => $id,
							'payperiod' => $year_counter.'-'.$month.'-2',
							'interest' => $f_interest,
							'principal' => $f_principal,
							'balance' => $f_balance,
							'creditor_id' => $this->input->post('creditor_id'),
						);
						$insert = $this->tblTransloan->save_transloan_details($transaction_detail);
						$a=1;
						$total = $f_balance;
					}

					
					$month++;
					if($month>12)
					{
						$month = 1;
						$year_counter++;
					}
					if($a==1)
					{
						$type4=1;
						$f_balance = floatval($total)-floatval($f_payable);
						$transaction_detail = array(
							'transloan_id' => $id,
							'payperiod' => $year_counter.'-'.$month.'-1',
							'interest' => $f_interest,
							'principal' => $f_principal,
							'balance' => $f_balance,
							'creditor_id' => $this->input->post('creditor_id'),
						);
						$insert = $this->tblTransloan->save_transloan_details($transaction_detail);
						$a=2;
						$total = $f_balance;
					}
				}					

				if($type == 1)
				{
					$type = 1;
				}
				if($type == 2)
				{
					$type = 2;
				}
				$year=intval($year)+intval(1);				
			}
			//add approved amount in ledger balance
			//amount without interest
			$get_ledger = $this->tblLedger->get_by_creditor($this->input->post('creditor_id'));
		                    	$balance = floatval($get_ledger->balance) + floatval($this->input->post('amount'));
		                    	$ledger_data = array(
			                        'reference_no'=> 'TRN-'.$this->input->post('id'),
			                        'creditor_code'=> $this->input->post('creditor_id'),
			                        'dt'=> $currDate,
		                        	'particulars' => 'Salary Loan',
		                        	'amount'=> $this->input->post('amount'),
		                        	'balance'=> $balance,
		                        	'type'=> 'Dr',
		                        	'is_final' => 0,
			                    );
			                    $this->tblLedger->save($ledger_data);

			                    //amount with interest
		                    	$balance2 = floatval($balance) + floatval($this->input->post('interest'));
		                    	$ledger_data = array(
			                        'reference_no'=> 'TRN-'.$this->input->post('id'),
			                        'creditor_code'=> $this->input->post('creditor_id'),
			                        'dt'=> $currDate,
		                        	'particulars' => 'Loan Interest',
		                        	'amount'=> $this->input->post('interest'),
		                        	'balance'=> $balance2,
		                        	'type'=> 'Dr',
		                        	'is_final' => 1,
			                    );
			                    $this->tblLedger->save($ledger_data);
			                    //update the final balance so that we can get easily which balance    
			                    $ledger_data2 = array(
		                        	'is_final' => 0,
			                    );
								$this->tblLedger->update(array('id' => $get_ledger->id), $ledger_data2);

		echo json_encode(array("status" => TRUE,"typex"=>$this->input->post('type'),"loan_date"=>$this->input->post('pay_off_date'),"type"=>$get_type->type,"terms"=>$terms,"amount"=>$total,"total"=>$f_payable,"interest"=>$f_interest,"principal"=>$f_principal));
	}

	public function ajax_list_loan_val() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->tblTransloan->get_validated_app();
		$data = array();
		foreach ($list as $myList)
		{
			/*$row = array();
			$row['creditor'] = $myList->fullname;
			$row['amount'] = number_format($myList->applied_amount, 2, '.', ',');
			$row['terms'] = $myList->terms;
			$row['purpose'] = $myList->purpose;*/
			$row = array();
			$row['trn'] = $myList->id;
			$com1 = $this->tblCreditors->get_by_id_creditors($myList->comaker1);
			$com2 = $this->tblCreditors->get_by_id_creditors($myList->comaker2);
			$row['com1'] = $com1->fullname;
			$row['com2'] = $com2->fullname;
			$row['product_name'] = $myList->product_name;
			$row['creditor'] = $myList->fullname;
			$row['amount'] = number_format($myList->applied_amount, 2, '.', ',');
			$row['terms'] = $myList->terms;
			$row['purpose'] = $myList->purpose;
			$row['checker_remark'] = $myList->checker_remark;
			$button = '';
			$privileges = $session_data['privileges'];
		    for ($i=0; $i<sizeof($privileges); $i++)
		    { 
               if ($privileges[$i]->privilege_name == "loanapp_edit") 
               {
					 $button = $button.'<a class="btn green btn-outline sbold" data-toggle="modal" href="#draggable" title="Edit" onclick="edit_loan('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';	
			   } else if ($privileges[$i]->privilege_name == "loanapp_delete"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_loan('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }
			}			
			$row['action'] = $button;
			$row['validated_dt'] = date("M d, Y", strtotime($myList->validate_on));
			$data[] = $row;
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}

	public function ajax_list_loan_checked() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->tblTransloan->get_checked_app();
		$data = array();
		foreach ($list as $myList)
		{
			/*$row = array();
			$row['creditor'] = $myList->fullname;
			$row['amount'] = number_format($myList->applied_amount, 2, '.', ',');
			$row['terms'] = $myList->terms;
			$row['purpose'] = $myList->purpose;*/
			$row = array();
			$row['trn'] = $myList->id;
			$com1 = $this->tblCreditors->get_by_id_creditors($myList->comaker1);
			$com2 = $this->tblCreditors->get_by_id_creditors($myList->comaker2);
			$row['com1'] = $com1->fullname;
			$row['com2'] = $com2->fullname;
			$row['product_name'] = $myList->product_name;
			$row['creditor'] = $myList->fullname;
			$row['amount'] = number_format($myList->applied_amount, 2, '.', ',');
			$row['terms'] = $myList->terms;
			$row['purpose'] = $myList->purpose;
			$row['checker_remark'] = $myList->approver_remark;
			$button = '';
			$privileges = $session_data['privileges'];
		    for ($i=0; $i<sizeof($privileges); $i++)
		    { 
               if ($privileges[$i]->privilege_name == "loanapp_edit") 
               {
					 $button = $button.'<a class="btn green btn-outline sbold" data-toggle="modal" href="#draggable" title="Edit" onclick="edit_loan('."'".$myList->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';	
			   } else if ($privileges[$i]->privilege_name == "loanapp_delete"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_loan('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }
			}			
			$row['action'] = $button;
			$row['validated_dt'] = date("M d, Y", strtotime($myList->validate_on));
			$data[] = $row;
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}

	public function ajax_list_loan_approved() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->tblTransloan->get_released_loan();
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['trn'] = $myList->id;
			$com1 = $this->tblCreditors->get_by_id_creditors($myList->comaker1);
			$com2 = $this->tblCreditors->get_by_id_creditors($myList->comaker2);
			$row['com1'] = $com1->fullname;
			$row['com2'] = $com2->fullname;
			$row['product_name'] = $myList->product_name;
			$row['company'] = $myList->emp_company;
			$row['creditor'] = '<b>'.$myList->fullname.'</b>';
			$row['amount'] = number_format($myList->applied_amount, 2, '.', ',');
			$row['terms'] = $myList->terms;
			$row['purpose'] = $myList->purpose;
			$button = '';
			$privileges = $session_data['privileges'];
		    for ($i=0; $i<sizeof($privileges); $i++)
		    { 
               if ($privileges[$i]->privilege_name == "loanapp_edit") 
               {
					 $button = $button.'<a class="btn green btn-outline sbold" data-toggle="modal" href="#draggable" title="Edit" onclick="edit_loan('."'".$myList->id."'".')"><i class="fa fa-street-view" aria-hidden="true"></i> View</a>';	
			   }
			}			
			$row['action'] = $button;
			$row['released_dt'] = date("M d, Y", strtotime($myList->release_on));
			$data[] = $row;
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}

	public function ajax_list_loan_disbursement() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->tblTransloan->get_approved_loan();
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['trn'] = $myList->id;
			$row['product_name'] = $myList->product_name;
			$row['company'] = $myList->emp_company;
			$row['creditor'] = '<b>'.$myList->fullname.'</b>';
			$row['amount'] = number_format($myList->applied_amount, 2, '.', ',');
			$row['terms'] = $myList->terms;
			$row['purpose'] = $myList->purpose;
			$button = '';
			$privileges = $session_data['privileges'];
		    for ($i=0; $i<sizeof($privileges); $i++)
		    { 
               if ($privileges[$i]->privilege_name == "loanapp_edit") 
               {
					 $button = $button.'<a class="btn green btn-outline sbold" data-toggle="modal" href="#draggable" title="Edit" onclick="edit_loan('."'".$myList->id."'".')"><i class="fa fa-street-view" aria-hidden="true"></i> View</a>';	
			   }
			}			
			$row['action'] = $button;
			$row['voucherno'] = $myList->voucherno;
			$row['release_dt'] = ($myList->release_on!=null?date("M d, Y", strtotime($myList->release_on)):'');
			$data[] = $row;
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}

	public function ajax_save_validation_details()
	{
	    $session_data = $this->session->userdata('logged_in');
		$currDate  = date("Y-m-d H:i:s");
		$data = array(
		 'validators_remark' => $this->input->post('remarks')
		);
		$this->tblTransloan->update(array('id' => $this->input->post('id')), $data);
		$uploads = $this->input->post('docus');
		if($uploads!=null)
		{
			foreach ($uploads as $key) {
			//print_r($key);
				$extension = explode(".", $key);
					$ss = end($extension);
				$docu = array(
					'transloan_id' => $this->input->post('id'),
					'filename' => $key,
					'filetype' => $ss ? $ss : false,//$this->get_extension($get_file),
					'created_by' => $session_data['firstname'].' '.$session_data['lastname'],
					'created_on' => $currDate,
				);
				$this->tblTransloan->add_docu($docu);
			}				
		}
		echo json_encode(array("status" => TRUE,/*"uploads"=>$this->get_extension($uploads)*/));
	}

	public function get_file_extension($file)
	{
	 return substr(strrchr($file, '.'),1);
	}

	public function get_extension($file)
	{
		$extension = end(explode(".", $file));
		return $extension ? $extension : false;
	}

	public function ajax_save_check_details()
	{
	    $session_data = $this->session->userdata('logged_in');
		$currDate  = date("Y-m-d H:i:s");
		$data = array(
		 'checker_remark' => $this->input->post('checker_remarks')
		);
		$this->tblTransloan->update(array('id' => $this->input->post('id')), $data);		
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_save_approve_details()
	{
	    $session_data = $this->session->userdata('logged_in');
		$currDate  = date("Y-m-d H:i:s");
		$data = array(
		 'approver_remark' => $this->input->post('checker_remarks')
		);
		$this->tblTransloan->update(array('id' => $this->input->post('id')), $data);		
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_get_amortization_schedule($id) 
	{
		$list = $this->tblTransloan->get_amortization_schedule_by_id($id);
		$data = array();			
		$half = 1;
		//$counter = '';
		foreach ($list as $myList)
		{
			
			$row = array();
			$row['date'] = $myList->payperiod;//$myList->month.'-'.$myList->year.'-'.$myList->level;//date_format(date_create($myList->payment_schedule),"F ,Y");
			//$row['status'] = ($myList->amount!=null?'<div class="fa-item col-md-3 col-sm-4"><i class="fa fa-check" style="color:green;"></i></div>':'<div class="fa-item col-md-3 col-sm-4"><i class="fa fa-close" style="color:red";></i></div>');
			$row['status'] = ($myList->amount!=null||$myList->manual_payment_id!=null?'<font color="green">Paid</font>':'<font color="red">Pending</font>');
			$row['interest'] = number_format(($myList->interest), 2); 
			$row['principal'] = number_format(($myList->principal), 2);  
			$row['balance'] = number_format(($myList->balance), 2);
			$row['variance'] = number_format(($myList->variance), 2); 
			$data[] = $row;			
			//	$counter = "First Half";
		}
		$output = array("data" => $data,'half'=>$half);
		echo json_encode($output);
	}

	public function ajax_list_loan_monitor() 
	{
		$session_data = $this->session->userdata('logged_in');
		$list = $this->tblTransloan->get_monitor_loan();
		$data = array();
		foreach ($list as $myList)
		{
			$row = array();
			$row['trn'] = $myList->id;
			$creditor_info = $this->tblCreditors->get_by_id_creditors($myList->creditor_id);
			$row['company'] = $creditor_info->emp_company;
			$row['creditor'] = '<b>'.$creditor_info->fullname.'</b>';
			$row['product'] = $myList->product_name;
			$row['amount'] = $myList->applied_amount;
			$row['status'] = $myList->status;
			$row['val_by'] = $myList->validate_by;
			$get_date1 = date_create($myList->validate_on);
			$row['val_date'] = date_format($get_date1,"F d,Y");
			$row['recommend_by'] = $myList->checked_by;
			$get_date3 = date_create($myList->approved_on);
			$row['recommend_date'] = date_format($get_date3,"F d,Y");
			$row['approved_by'] = $myList->approved_by;
			$get_date2 = date_create($myList->approved_on);
			$row['approved_date'] = date_format($get_date2,"F d,Y");
			$row['cancelled_by'] = $myList->cancel_by;
			$row['cancelled_date'] = $myList->cancel_on;
			$row['disbursed'] = ($myList->isDisbursed!=0?'Disbursed':'Not Disbursed');
			$row['paid'] = ($myList->isClosed!=0?'Closed':'Not Closed');
			$button = '';
			$privileges = $session_data['privileges'];
		    for ($i=0; $i<sizeof($privileges); $i++)
		    { 
               if ($privileges[$i]->privilege_name == "loanapp_edit") 
               {
					$button = $button.'<a class="btn green btn-outline sbold" data-toggle="modal" href="#draggable" title="Edit" onclick="edit_loan('."'".$myList->id."'".')">View</a>';	
			   } else if ($privileges[$i]->privilege_name == "loanapp_delete"){
				 	$button = $button.'<a class="btn btn-sm btn-danger" title="Delete" onclick="delete_loan('."'".$myList->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';	  
			   }
			}			
			$row['action'] = $button;
			$data[] = $row;
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}

	public function get_loan_period()
	{
		$list = $this->tblTransloan->get_tcreditor_loan_period($this->input->post('id'));
		$data = array();	
		foreach ($list as $myList)
		{		
			if($myList->variance==null||$myList->variance>0)
			{
				$deduction = floatval($myList->interest) + floatval($myList->principal);
				$row = array();
				$row['id'] = $myList->id;
				$row['trans_loan_id'] = $myList->transloan_id;
				$row['payperiod'] = $myList->payperiod; 
				$row['deduction'] = ($myList->variance!=null&&$myList->variance>1?number_format(($myList->variance), 2):number_format(($deduction), 2));
				$row['balance'] = number_format(($myList->balance), 2);
				$data[] = $row;					
			}		
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}
}
