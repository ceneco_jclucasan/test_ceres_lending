<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_groups_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables()
	{
		$this->db->from('tblgroups');
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_id($id)
	{
		$this->db->from('tblgroups');
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert('tblgroups', $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update('tblgroups', $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tblgroups');
	}

	public function delete_all_privileges_by_id($id)
	{
		$this->db->where('group_id', $id);
		$this->db->delete('tblprivilegegroups');
	}

	function get_privileges_by_id($id)
	{
		$this->db->from('tblprivilegegroups');
		$this->db->where('group_id',$id);
		$this->db->order_by('privilege_id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_privileges_by_id2($id,$priv)
	{
		$this->db->from('tblprivilegegroups');
		$this->db->where('group_id',$id);
		$this->db->where('privilege_id',$priv);
		$query = $this->db->get();
		return $query->row();
	}

	function update_privileges($group_id, $privilege_id)
	{
		$this->db->from('tblprivilegegroups');
		$this->db->where('group_id',$group_id);
		$this->db->where('privilege_id',$privilege_id);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function save_privileges($data)
	{
		$this->db->insert('tblprivilegegroups', $data);
		return $this->db->insert_id();
	}

	public function delete_privileges($priv,$userid)
	{
		$this->db->where('group_id', $userid);
		$this->db->where('privilege_id', $priv);
		$this->db->delete('tblprivilegegroups');
	}

	function get_group_members($group_id)
	{
		$this->db->from('tblusers');
		$this->db->where('group_id',$group_id);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

}
