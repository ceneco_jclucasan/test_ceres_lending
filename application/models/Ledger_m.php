<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ledger_m extends CI_Model {

	var $table = 'ledger';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_addressbook() {     
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
 
    function insert_csv($data) {
        $this->db->insert($this->table, $data);
    }

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	function get_datatables()
	{
		$query = $this->db->query('SELECT * FROM ledger ORDER BY id desc'); 
		return $query->result();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_query_builder_result($qry)
	{
		$this->db->from($this->table);
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_creditor($id)
	{
		$this->db->from('ledger');
		$this->db->where('creditor_code',$id);
		$this->db->where('is_final',1);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_creditors_ledger($id)
	{
		$this->db->from('ledger');
		$this->db->where('creditor_code',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_datatables_repo_beg($id)
	{
		$this->db->from('ledger');
		$this->db->where('reference_no',$id);
		$query = $this->db->get();
		return $query->result();
	}
}