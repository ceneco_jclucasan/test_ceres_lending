<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_by_id_products($id)
	{
		$this->db->from('products');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update_products($where, $data)
	{
		$this->db->update('products', $data, $where);
		return $this->db->affected_rows();
	}

	function get_datatables_products()
	{
		$query = $this->db->query('SELECT * FROM products ORDER BY id ASC'); 
		return $query->result();
	}

	public function count_all_products()
	{
		$this->db->from("products");
		return $this->db->count_all_results();
	}
	public function save_products($data)
	{
		$this->db->insert("products", $data);
		return $this->db->insert_id();
	}

	public function delete_by_id_products($id)
	{
		$this->db->where('id', $id);
		$this->db->delete("products");
	}

	function get_query_builder_result_products($qry)
	{
		$this->db->from("products");
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	////

	public function get_by_id_products_othercharges($id)
	{
		$this->db->from('product_othercharges');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update_products_othercharges($where, $data)
	{
		$this->db->update('product_othercharges', $data, $where);
		return $this->db->affected_rows();
	}

	function get_datatables_products_othercharges()
	{
		$query = $this->db->query('SELECT * FROM product_othercharges ORDER BY id ASC'); 
		return $query->result();
	}

	public function count_all_products_othercharges()
	{
		$this->db->from("product_othercharges");
		return $this->db->count_all_results();
	}
	public function save_products_othercharges($data)
	{
		$this->db->insert("product_othercharges", $data);
		return $this->db->insert_id();
	}

	public function delete_by_id_products_othercharges($id)
	{
		$this->db->where('id', $id);
		$this->db->delete("product_othercharges");
	}

	function get_query_builder_result_products_othercharges($qry)
	{
		$this->db->from("product_othercharges");
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}
}