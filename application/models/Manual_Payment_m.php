<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manual_Payment_m extends CI_Model {

	var $table = 'manual_payment_h';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_by_id_h($id)
	{
		$this->db->from('manual_payment_h');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_id_b($id)
	{
		$this->db->from('manual_payment_body');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	function get_datatables_h()
	{
		$query = $this->db->query('SELECT * FROM manual_payment_h ORDER BY id ASC'); 
		return $query->result();
	}

	public function update_h($where, $data)
	{
		$this->db->update('manual_payment_h', $data, $where);
		return $this->db->affected_rows();
	}

	public function save_h($data)
	{
		$this->db->insert("manual_payment_h", $data);
		return $this->db->insert_id();
	}

	public function delete_h($id)
	{
		$this->db->where('id', $id);
		$this->db->delete("manual_payment_h");
	}

	public function update_b($where, $data)
	{
		$this->db->update('manual_payment_body', $data, $where);
		return $this->db->affected_rows();
	}

	public function save_b($data)
	{
		$this->db->insert("manual_payment_body", $data);
		return $this->db->insert_id();
	}

	public function delete_b($id)
	{
		$this->db->where('id', $id);
		$this->db->delete("manual_payment_body");
	}

	function get_payment()
	{
		$this->db->select('b.payperiod,b.amount,h.orno,h.ordate,c.fullname,b.id,h.id as hid');
		$this->db->from('manual_payment_body b');
		$this->db->join('manual_payment_h h','b.head_id = h.id');
		$this->db->join('creditors c','c.employee_code = h.creditor');
		$this->db->order_by('h.ordate','desc');
		$query = $this->db->get();
		return $query->result();
	}	

	function get_payment_creditor($id)
	{
		$this->db->select('b.payperiod,b.amount,h.orno,h.ordate,b.id');
		$this->db->from('manual_payment_body b');
		$this->db->join('manual_payment_h h','b.head_id = h.id');
		$this->db->where('h.creditor', $id);
		$query = $this->db->get();
		return $query->result();
	}

	function get_payment_trans_detail($id)
	{
		$this->db->select('b.payperiod,b.amount,h.orno,h.ordate,b.id,h.partticulars');
		$this->db->from('manual_payment_body b');
		$this->db->join('manual_payment_h h','b.head_id = h.id');
		$this->db->where('b.trans_loan_detail_id', $id);
		$query = $this->db->get();
		return $query->result();
	}
}