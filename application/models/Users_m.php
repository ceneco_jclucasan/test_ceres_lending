<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_datatables()
	{
		$this->db->select('tblusers.*,creditors.firstname,creditors.lastname,tblgroups.description');
		$this->db->from('tblusers');
		$this->db->join('creditors','creditors.id = tblusers.emp_id');
		$this->db->join('tblgroups','tblgroups.id = tblusers.group_id');
		//$this->db->where('is_suspended',0);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	/*function get_datatables()
	{
		$this->db->select('tblusers.*,tblemployees.firstname,tblemployees.lastname,tblgroups.description');
		$this->db->from('tblusers');
		$this->db->join('tblemployees','tblemployees.id = tblusers.emp_id');
		$this->db->join('tblgroups','tblgroups.id = tblusers.group_id');
		//$this->db->where('is_suspended',0);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}
*/
	function get_datatables_suspended()
	{
		$this->db->select('tblusers.*,creditors.firstname,creditors.lastname,tblgroups.description');
		$this->db->from('tblusers');
		$this->db->join('creditors','creditors.id = tblusers.emp_id');
		$this->db->join('tblgroups','tblgroups.id = tblusers.group_id');
		$this->db->where('is_suspended',1);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	/*function get_datatables_suspended()
	{
		$this->db->select('tblusers.*,tblemployees.firstname,tblemployees.lastname,tblgroups.description');
		$this->db->from('tblusers');
		$this->db->join('tblemployees','tblemployees.id = tblusers.emp_id');
		$this->db->join('tblgroups','tblgroups.id = tblusers.group_id');
		$this->db->where('is_suspended',1);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}*/

	public function get_by_id($id)
	{
		$this->db->select('tblusers.*,creditors.firstname,creditors.lastname,tblgroups.description');
		$this->db->from('tblusers');
		$this->db->join('creditors','creditors.id = tblusers.emp_id');
		$this->db->join('tblgroups','tblgroups.id = tblusers.group_id');
		$this->db->where('tblusers.id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	/*public function get_by_id($id)
	{
		$this->db->select('tblusers.*,tblemployees.firstname,tblemployees.lastname,tblgroups.description');
		$this->db->from('tblusers');
		$this->db->join('tblemployees','tblemployees.id = tblusers.emp_id');
		$this->db->join('tblgroups','tblgroups.id = tblusers.group_id');
		$this->db->where('tblusers.id',$id);
		$query = $this->db->get();

		return $query->row();
	}*/

	public function check_user($emp_id)
	{
		$this->db->from('tblusers');
		$this->db->where('emp_id',$emp_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function save($data)
	{
		$this->db->insert('tblusers', $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update('tblusers', $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tblusers');
	}

	public function delete_all_privileges_by_id($id)
	{
		$this->db->where('user_id', $id);
		$this->db->delete('tblprivilegeusers');
	}

	function get_privileges_by_id($id)
	{
		$this->db->from('tblprivilegeusers');
		$this->db->where('user_id',$id);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	function get_privileges_by_id2($id,$priv)
	{
		$this->db->from('tblprivilegeusers');
		$this->db->where('user_id',$id);
		$this->db->where('privilege_id',$priv);
		$query = $this->db->get();
		return $query->row();
	}

	function update_privileges($user_id, $privilege_id)
	{
		$this->db->from('tblprivilegeusers');
		$this->db->where('user_id',$user_id);
		$this->db->where('privilege_id',$privilege_id);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function save_privileges($data)
	{
		$this->db->insert('tblprivilegeusers', $data);
		return $this->db->insert_id();
	}

	public function delete_privileges($priv,$userid)
	{
		$this->db->where('user_id', $userid);
		$this->db->where('privilege_id', $priv);
		$this->db->delete('tblprivilegeusers');
	}

	public function dropdown_list_group()
	{
		$this->db->from('tblgroups');
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function dropdown_list_employees()
	{
		//$this->db->from('tblemployees');
		$this->db->from('creditors');
		$this->db->order_by('firstname','asc');
		$this->db->order_by('middlename','asc');
		$this->db->order_by('lastname','asc');
		$query = $this->db->get();
		return $query->result();
	}

}
