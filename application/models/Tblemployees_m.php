<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tblemployees_m extends CI_Model {

	var $table = 'tblemployees';
	var $column = array('id','lastname','firstname','middlename','suffix','email','position','is_active');
	var $order = array('id' => 'lastname');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}



	private function _get_datatables_query()
	{
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$query = $this->db->query('SELECT * FROM tblemployees ORDER BY id ASC'); //$this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function get_query_builder_result($qry)
	{
		$this->db->from($this->table);
		$this->db->where($qry);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered_qb($qry)
	{
		$this->_get_datatables_query_qb();
		$this->db->where($qry);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all_qb($qry)
	{
		$this->db->from($this->table);
		$this->db->where($qry);
		return $this->db->count_all_results();
	}
}